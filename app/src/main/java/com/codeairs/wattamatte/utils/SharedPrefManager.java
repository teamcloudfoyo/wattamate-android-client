package com.codeairs.wattamatte.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.gson.Gson;
import com.codeairs.wattamatte.models.entry.FBGraphRequest;

import static com.codeairs.wattamatte.constants.Constants.LOGIN_TYPE_NORMAL;
import static com.codeairs.wattamatte.constants.Constants.USER_STATUS_NEW;

public class SharedPrefManager {

    public static final String PREFERENCES = "app_preferences";

    public static SharedPreferences getPreferences(Context context) {
        return context.getSharedPreferences(PREFERENCES, Context.MODE_PRIVATE);
    }

    public static SharedPreferences.Editor getEditor(Context context) {
        return getPreferences(context).edit();
    }

    public static void removePreference(Context context, String preferenceName) {
        getEditor(context).remove(preferenceName);
    }

    public static <T> T loadObject(Context context, String preferenceName, Class<T> type, T defValue) {
        T object = defValue;
        Gson gson = new Gson();
        String json = loadString(context, preferenceName, null);
        if (json != null) {
            object = gson.fromJson(json, type);
        }
        return object;
    }

    public static void saveObject(Context context, String preferenceName, Object object) {
        Gson gson = new Gson();
        String json = gson.toJson(object);
        saveString(context, preferenceName, json);
    }

    public static String loadString(Context context, String preferenceName, String defValue) {
        SharedPreferences sharedPreferences = getPreferences(context);
        return sharedPreferences.getString(preferenceName, defValue);
    }

    public static void saveString(Context context, String preferenceName, String value) {
        getEditor(context).putString(preferenceName, value).apply();
    }

    public static int loadInt(Context context, String preferenceName, int defValue) {
        SharedPreferences sharedPreferences = getPreferences(context);
        return sharedPreferences.getInt(preferenceName, defValue);
    }

    public static void saveInt(Context context, String preferenceName, int value) {
        getEditor(context).putInt(preferenceName, value).apply();
    }

    public static long loadLong(Context context, String preferenceName, long defValue) {
        SharedPreferences sharedPreferences = getPreferences(context);
        return sharedPreferences.getLong(preferenceName, defValue);
    }

    public static void saveLong(Context context, String preferenceName, long value) {
        getEditor(context).putLong(preferenceName, value).apply();
    }

    public static boolean loadBoolean(Context context, String preferenceName, boolean defValue) {
        SharedPreferences sharedPreferences = getPreferences(context);
        return sharedPreferences.getBoolean(preferenceName, defValue);
    }

    public static void saveBoolean(Context context, String preferenceName, boolean value) {
        getEditor(context).putBoolean(preferenceName, value).apply();
    }

    public static String getAccessToken(Context context) {
        SharedPreferences sharedPreferences = getPreferences(context);
        return sharedPreferences.getString("User-Access-Token", null);
    }

    public static String getRefreshToken(Context context) {
        SharedPreferences sharedPreferences = getPreferences(context);
        return sharedPreferences.getString("User-Refresh-Token", null);
    }

    public static void saveUserId(Context context, String userId) {
        saveString(context, "userId", userId);
    }

    public static String getUserId(Context context) {
        SharedPreferences sharedPreferences = getPreferences(context);
        return sharedPreferences.getString("userId", null);
    }

    public static void saveSafeUserType(Context context, String userId) {
        saveString(context, "safeUserType", userId);
    }

    public static String getSafeUserType(Context context) {
        SharedPreferences sharedPreferences = getPreferences(context);
        return sharedPreferences.getString("safeUserType", null);
    }

    public static void saveProfilePic(Context context, String userId) {
        saveString(context, "profilePic", userId);
    }

    public static String getProfilePic(Context context) {
        SharedPreferences sharedPreferences = getPreferences(context);
        return sharedPreferences.getString("profilePic", null);
    }

    public static void saveProfilePicThumb(Context context, String userId) {
        saveString(context, "profilePicThumb", userId);
    }

    public static String getProfilePicThumb(Context context) {
        SharedPreferences sharedPreferences = getPreferences(context);
        return sharedPreferences.getString("profilePicThumb", null);
    }

    public static void saveUserStatus(Context context, int status) {
        saveInt(context, "userStatus", status);
    }

    public static int getUserStatus(Context context) {
        SharedPreferences sharedPreferences = getPreferences(context);
        return sharedPreferences.getInt("userStatus", USER_STATUS_NEW);
    }

    public static long getAccessTokenExpiry(Context context) {
        SharedPreferences sharedPreferences = getPreferences(context);
        return sharedPreferences.getLong("Token-Expires_In", -1);
    }

    public static String getTokenType(Context context) {
        SharedPreferences sharedPreferences = getPreferences(context);
        return sharedPreferences.getString("Token_Type", null);
    }

    public static String getPhoneNo(Context context) {
        SharedPreferences sharedPreferences = getPreferences(context);
        return sharedPreferences.getString("Phone_Number", null);
    }

    public static void savePhoneNo(Context context, String phoneNo) {
        Log.d("SharedPreference","Phone Number : "+phoneNo);
        saveString(context, "Phone_Number", phoneNo);
    }

    public static int getLoginType(Context context) {
        SharedPreferences sharedPreferences = getPreferences(context);
        return sharedPreferences.getInt("login_type", LOGIN_TYPE_NORMAL);
    }

    public static void saveLoginType(Context context, int phoneNo) {
        saveInt(context, "login_type", phoneNo);
    }


    public static void saveFBGraph(Context context, FBGraphRequest request) {
        String json = new Gson().toJson(request);
        saveString(context, "fbGraphRequest", json);
    }

    public static FBGraphRequest getFBGraph(Context context) {
        SharedPreferences sharedPreferences = getPreferences(context);
        String json = sharedPreferences.getString("fbGraphRequest", null);
        if (null != json) {
            return new Gson().fromJson(json, FBGraphRequest.class);
        }
        return null;
    }

    public static String getFirebaseToken(Context context) {
        SharedPreferences sharedPreferences = getPreferences(context);
        return sharedPreferences.getString("firebase_token","");
    }

    public static void saveFirebaseToken(Context context, String firebaseToken) {
        saveString(context, "firebase_token", firebaseToken);
    }

}
