package com.codeairs.wattamatte.utils.internet;

import android.os.AsyncTask;
import android.util.Log;

import com.codeairs.wattamatte.constants.ApiPaths;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class ImagePostUtility extends AsyncTask<String, Object, String> {
    private static final MediaType MEDIA_TYPE_JPEG = MediaType.parse("image/jpeg");
    private final String TAG = getClass().getSimpleName();
    private int responseCode = 0;
    private String tag;
    private OkHttpClient client = new OkHttpClient();
    private OnImagePostedCallback onImagePosted;
    private Response response;
    private RequestBody requestBody;
    private Request request;
    private File imageFile;
    private String usedId;

    public ImagePostUtility(OnImagePostedCallback onImagePosted, String usedId, File imageFile) {
        this.onImagePosted = onImagePosted;
        this.imageFile = imageFile;
        this.usedId = usedId;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        client = new OkHttpClient.Builder()
                .connectTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(15, TimeUnit.SECONDS)
                .readTimeout(20, TimeUnit.SECONDS)
                .build();
    }

    @Override
    protected String doInBackground(String... strings) {
        MultipartBody.Builder multipartBuilder = new MultipartBody.Builder();
        multipartBuilder.addFormDataPart("userId", usedId);
        multipartBuilder.addFormDataPart("file", imageFile.getName(), RequestBody.create(MEDIA_TYPE_JPEG, imageFile));

        requestBody = multipartBuilder
                .setType(MultipartBody.FORM)
                .build();

        request = new Request.Builder()
                .url(strings[0])
                .addHeader("Authorization", ApiPaths.TOKEN)
                .addHeader("userId", usedId)
                .post(requestBody)
                .build();

        try {
            response = client.newCall(request).execute();
            return response.body().string();
        } catch (IOException e) {
            Log.e(TAG, "Exception: " + e.getLocalizedMessage());
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(String response) {
        super.onPostExecute(response);
        if (response != null) {
            responseCode = this.response.code();
            onImagePosted.onImageUploaded(responseCode, response);
        } else {
            onImagePosted.onImageUploadError(responseCode, "Something Error Occured Please try again");
        }
    }

    public interface OnImagePostedCallback {
        void onImageUploaded(int statusCode, String respone);

        void onImageUploadError(int statusCode, String errorResponse);
    }
}