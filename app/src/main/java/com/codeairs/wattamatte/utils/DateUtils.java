package com.codeairs.wattamatte.utils;

import android.content.Context;
import com.codeairs.wattamatte.R;

import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DateUtils {

    public static final String PRIMARY_DATE_FORMAT = "dd/MM/yyyy";
    public static final String SERVER_FORMAT = "EEE MMM dd yyyy HH:mm:ss 'GMT'Z '(UTC)'";
    public static final String SERVER_DATE_FORMAT = "MM/dd/yyyy";


    public static String toFormat(Date date, String format) {
        try {
            DateFormat df = new SimpleDateFormat(format, Locale.US);
            return df.format(date);
        } catch (NullPointerException e) {
            return null;
        }
    }

    public static String convert(String date, String from, String to) {
        DateFormat fromFormat = new SimpleDateFormat(from, Locale.US);
        DateFormat toFormat = new SimpleDateFormat(to, Locale.US);

        try {
            Date dt = fromFormat.parse(date);
            return toFormat.format(dt);
        } catch (ParseException e) {
            return null;
        }
    }

    public static int getAge(Date dateOfBirth) {

        Calendar today = Calendar.getInstance();
        Calendar birthDate = Calendar.getInstance();

        int age;

        birthDate.setTime(dateOfBirth);
        if (birthDate.after(today)) {
            throw new IllegalArgumentException("Can't be born in the future");
        }

        age = today.get(Calendar.YEAR) - birthDate.get(Calendar.YEAR);

        // If birth date is greater than todays date (after 2 days adjustment of leap year) then decrement age one year
        if ((birthDate.get(Calendar.DAY_OF_YEAR) - today.get(Calendar.DAY_OF_YEAR) > 3) ||
                (birthDate.get(Calendar.MONTH) > today.get(Calendar.MONTH))) {
            age--;

            // If birth date and todays date are of same month and birth day of month is greater than todays day of month then decrement age
        } else if ((birthDate.get(Calendar.MONTH) == today.get(Calendar.MONTH)) &&
                (birthDate.get(Calendar.DAY_OF_MONTH) > today.get(Calendar.DAY_OF_MONTH))) {
            age--;
        }

        return age;
    }

    public static String getTimeDifference(Date date, Context context) {
        return new TimeDifference(new Date(), date, context).differenceString;
    }

    public static String formatDateTimeForChatMsg(Date date) {
        if (date != null) {
            String format = android.text.format.DateUtils.isToday(date.getTime()) ? "h:mm aa" : "d/M h:mm aa";
            DateFormat sdf = new SimpleDateFormat(format, Locale.US);
            return sdf.format(date);
        }
        return "";
    }

    public static Date subtractDays(Date date, int days) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DAY_OF_YEAR, days);
        return cal.getTime();
    }

    public static class TimeDifference {

        private Context mContext;

        int years;
        int months;
        int days;
        int hours;
        int minutes;
        int seconds;
        String differenceString;

        public TimeDifference(Context context) {
            mContext = context;
        }

        public TimeDifference(Date curDate, Date oldDate, Context context) {
            mContext = context;

            float diff = curDate.getTime() - oldDate.getTime();
            if (diff >= 0) {
                int yearDiff = Math.round((diff / (365l * 2592000000f)) >= 1 ? (diff / (365l * 2592000000f)) : 0);
                if (yearDiff > 0) {
                    years = yearDiff;
                    setDifferenceString(years == 1 ? mContext.getString(R.string.a_year_ago)
                            : MessageFormat.format(mContext.getString(R.string.years_ago), years));
                } else {
                    int monthDiff = Math.round((diff / 2592000000f) >= 1 ? (diff / 2592000000f) : 0);
                    if (monthDiff > 0) {
                        if (monthDiff > 11)
                            monthDiff = 11;

                        months = monthDiff;
                        setDifferenceString(months == 1 ? mContext.getString(R.string.a_month_ago)
                                : MessageFormat.format(mContext.getString(R.string.months_ago), months));
                    } else {
                        int dayDiff = Math.round((diff / (86400000f)) >= 1 ? (diff / (86400000f)) : 0);
                        if (dayDiff > 0) {
                            days = dayDiff;
                            if (days == 30)
                                days = 29;
                            setDifferenceString(days == 1 ? mContext.getString(R.string.a_day_ago)
                                    : MessageFormat.format(mContext.getString(R.string.days_ago), days));
                        } else {
                            int hourDiff = Math.round((diff / (3600000f)) >= 1 ? (diff / (3600000f)) : 0);
                            if (hourDiff > 0) {
                                hours = hourDiff;
                                setDifferenceString(hours == 1 ? mContext.getString(R.string.a_hour_ago)
                                        : MessageFormat.format(mContext.getString(R.string.hours_ago), hours));
                            } else {
                                int minuteDiff = Math.round((diff / (60000f)) >= 1 ? (diff / (60000f)) : 0);
                                if (minuteDiff > 0) {
                                    minutes = minuteDiff;
                                    setDifferenceString(minutes == 1 ? mContext.getString(R.string.a_minute_ago)
                                            : MessageFormat.format(mContext.getString(R.string.minutes_ago), minutes));
                                } else {
                                    int secondDiff = Math.round((diff / (1000f)) >= 1 ? (diff / (1000f)) : 0);
                                    if (secondDiff > 0)
                                        seconds = secondDiff;
                                    else
                                        seconds = 1;
                                    setDifferenceString(seconds == 1 ? mContext.getString(R.string.a_second_ago)
                                            : MessageFormat.format(mContext.getString(R.string.seconds_ago), seconds));
                                }
                            }
                        }

                    }
                }

            }

        }

        public String getDifferenceString() {
            return differenceString;
        }

        public void setDifferenceString(String differenceString) {
            this.differenceString = differenceString;
        }

        public int getYears() {
            return years;
        }

        public void setYears(int years) {
            this.years = years;
        }

        public int getMonths() {
            return months;
        }

        public void setMonths(int months) {
            this.months = months;
        }

        public int getDays() {
            return days;
        }

        public void setDays(int days) {
            this.days = days;
        }

        public int getHours() {
            return hours;
        }

        public void setHours(int hours) {
            this.hours = hours;
        }

        public int getMinutes() {
            return minutes;
        }

        public void setMinutes(int minutes) {
            this.minutes = minutes;
        }

        public int getSeconds() {
            return seconds;
        }

        public void setSeconds(int seconds) {
            this.seconds = seconds;
        }

    }

}
