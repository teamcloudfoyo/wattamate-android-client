package com.codeairs.wattamatte.utils.lazylist;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;
import android.util.Log;
import android.widget.ImageView;

import com.codeairs.wattamatte.AppController;
import com.codeairs.wattamatte.constants.ApiPaths;
import com.codeairs.wattamatte.constants.Constants;

import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Collections;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import id.zelory.compressor.Compressor;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


public class ImageLoader {

    // Initialize MemoryCache
    MemoryCache memoryCache = new MemoryCache();
    MediaType MEDIA_TYPE_JSON = MediaType.parse("application/json");
    FileCache fileCache;

    //Create Map (collection) to store image and image url in key value pair
    private Map<ImageView, String> imageViews = Collections.synchronizedMap(
            new WeakHashMap<ImageView, String>());
    ExecutorService executorService;

    //handler to display images in UI thread
    Handler handler = new Handler();

    public ImageLoader(Context context) {

        fileCache = new FileCache(context);

        // Creates a thread pool that reuses a fixed number of
        // threads operating off a shared unbounded queue.
        executorService = Executors.newFixedThreadPool(5);

    }

    // default image show in list (Before online image download)
    int defaultImageID;

    public void DisplayImage(String url, ImageView imageView, int drawableID) {
        defaultImageID = drawableID;

        //Store image and url in Map
        imageViews.put(imageView, url);

        //Check image is stored in MemoryCache Map or not (see MemoryCache.java)
        Bitmap bitmap = memoryCache.get(url + Constants.IMAGE_LARGE);

        if (bitmap != null) {
            // if image is stored in MemoryCache Map then
            // Show image in listview row
            imageView.setImageBitmap(bitmap);
        } else {
            //queue Photo to download from url
            queuePhoto(url, Constants.IMAGE_LARGE, imageView);

            //Before downloading image show default image
            imageView.setImageResource(drawableID);
        }
    }

    public void DisplayThumbImage(String url, ImageView imageView, int drawableID) {
        defaultImageID = drawableID;
        imageViews.put(imageView, url);

        Bitmap bitmap = memoryCache.get(url + Constants.IMAGE_SMALL);

        if (bitmap != null) {
            // if image is stored in MemoryCache Map then
            // Show image in listview row
            imageView.setImageBitmap(bitmap);
        } else {
            //queue Photo to download from url
            queuePhoto(url, Constants.IMAGE_SMALL, imageView);

            //Before downloading image show default image
            imageView.setImageResource(drawableID);
        }
    }

    private void queuePhoto(String url, String imageType, ImageView imageView) {
        // Store image and url in PhotoToLoad object
        PhotoToLoad p = new PhotoToLoad(url, imageType, imageView);

        // pass PhotoToLoad object to PhotosLoader runnable class
        // and submit PhotosLoader runnable to executers to run runnable
        // Submits a PhotosLoader runnable task for execution

        executorService.submit(new PhotosLoader(p));
    }

    //Task for the queue
    private class PhotoToLoad {
        public String url, imageType;
        public ImageView imageView;

        public PhotoToLoad(String u, String imageType, ImageView i) {
            url = u;
            imageView = i;
            this.imageType = imageType;
        }
    }

    class PhotosLoader implements Runnable {
        PhotoToLoad photoToLoad;

        PhotosLoader(PhotoToLoad photoToLoad) {
            this.photoToLoad = photoToLoad;
        }

        @Override
        public void run() {
            try {
                //Check if image already downloaded
                if (imageViewReused(photoToLoad))
                    return;
                // download image from web url
                Bitmap bmp = getBitmap(photoToLoad.url, photoToLoad.imageType);

                // set image data in Memory Cache
                memoryCache.put(photoToLoad.url + photoToLoad.imageType, bmp);

                if (imageViewReused(photoToLoad))
                    return;

                // Get bitmap to display
                BitmapDisplayer bd = new BitmapDisplayer(bmp, photoToLoad);

                // Causes the Runnable bd (BitmapDisplayer) to be added to the message queue.
                // The runnable will be run on the thread to which this handler is attached.
                // BitmapDisplayer run method will call
                handler.post(bd);

            } catch (Throwable th) {
                th.printStackTrace();
            }
        }
    }

    private Bitmap getBitmap(String userId, String imageType) {
        File f = fileCache.getFile(userId);

        //from SD cache
        Bitmap b = decodeFile(f);
        if (b != null)
            return b;
        //from web

        try {
            Bitmap bitmap = null;
            JSONObject postData = new JSONObject();
            postData.put("id", userId);
            if (imageType.equalsIgnoreCase(Constants.IMAGE_SMALL))
                postData.put("type", "small");
            else
                postData.put("type", "default");
            Log.d("ImageLoader","Json : "+postData);
            OkHttpClient client = new OkHttpClient();
            RequestBody body = RequestBody.create(MEDIA_TYPE_JSON, postData.toString());
            Request request = new Request.Builder()
                    .url(ApiPaths.PATH_USER_AVATAR)
                    .post(body)
                    .addHeader("content-type", "application/json; charset=utf-8")
                    .addHeader("Authorization", ApiPaths.TOKEN)
                    .build();
            Response response = client.newCall(request).execute();
            if (response.isSuccessful()) {
                InputStream is = response.body().byteStream();
                OutputStream os = new FileOutputStream(f);
                CopyStream(is, os);
                os.close();
                bitmap = decodeFile(f);
                return bitmap;
            }
        } catch (Throwable ex) {
            ex.printStackTrace();
            if (ex instanceof OutOfMemoryError)
                memoryCache.clear();
            return null;
        }
        return null;
    }

    private void CopyStream(InputStream is, OutputStream os) {
        final int buffer_size = 1024;
        try {
            byte[] bytes = new byte[buffer_size];
            for (; ; ) {
                //Read byte from input stream
                int count = is.read(bytes, 0, buffer_size);
                if (count == -1)
                    break;
                //Write byte from output stream
                os.write(bytes, 0, count);
            }
        } catch (Exception ex) {
        }
    }

    //Decodes image and scales it to reduce memory consumption
    private Bitmap decodeFile(File f) {
        try {
            Bitmap compressedImage = new Compressor(AppController.getInstance())
                    .setQuality(100)
                    .setCompressFormat(Bitmap.CompressFormat.WEBP)
                    .compressToBitmap(f);
            return compressedImage;
        } catch (FileNotFoundException e) {
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    boolean imageViewReused(PhotoToLoad photoToLoad) {

        String tag = imageViews.get(photoToLoad.imageView);
        //Check url is already exist in imageViews MAP
        if (tag == null || !tag.equals(photoToLoad.url))
            return true;
        return false;
    }

    //Used to display bitmap in the UI thread
    class BitmapDisplayer implements Runnable {
        Bitmap bitmap;
        PhotoToLoad photoToLoad;

        public BitmapDisplayer(Bitmap b, PhotoToLoad p) {
            bitmap = b;
            photoToLoad = p;
        }

        public void run() {
            if (imageViewReused(photoToLoad))
                return;

            // Show bitmap on UI
            if (bitmap != null)
                photoToLoad.imageView.setImageBitmap(bitmap);
            else
                photoToLoad.imageView.setImageResource(defaultImageID);
        }
    }

    public void clearCache() {
        //Clear cache directory downloaded images and stored data in maps
        memoryCache.clear();
        fileCache.clear();
    }
}