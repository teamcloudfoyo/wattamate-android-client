package com.codeairs.wattamatte.utils.internet;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Looper;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.codeairs.wattamatte.AppController;
import com.codeairs.wattamatte.constants.ApiPaths;
import com.codeairs.wattamatte.utils.SharedPrefManager;
import com.google.common.collect.ImmutableMap;
import com.squareup.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Map;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

public class NetworkUtils {

    private static ConnectivityManager manager;

    public static boolean isOnline(Context context) {
        return isConnectedMobile(context) || isConnectedWifi(context);
    }

    public static boolean isConnectedWifi(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.getType() == ConnectivityManager.TYPE_WIFI
                && networkInfo.isAvailable() && networkInfo.isConnected();
    }

    public static boolean isConnectedMobile(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.getType() == ConnectivityManager.TYPE_MOBILE
                && networkInfo.isAvailable() && networkInfo.isConnected();
    }

    public static void handleVolleyError(VolleyError error) {
        Context context = AppController.getInstance().getApplicationContext();
        if (error instanceof TimeoutError || error instanceof NoConnectionError) {
            Toast.makeText(context,
                    "NoConnectionError",
                    Toast.LENGTH_LONG).show();
        } else if (error instanceof AuthFailureError) {
            //TODO
        } else if (error instanceof ServerError) {
            Toast.makeText(context, "Server Error",
                    Toast.LENGTH_LONG).show();
        } else if (error instanceof NetworkError) {
            //TODO
        } else if (error instanceof ParseError) {
            //TODO
        }
    }

    public static class VolleyErrorHandler implements Response.ErrorListener {
        @Override
        public void onErrorResponse(VolleyError error) {
            handleVolleyError(error);
        }
    }

    public static Map<String, String> authenticatedHeader(Context context) {
        return ImmutableMap.<String, String>builder()
                .put("userId", SharedPrefManager.getUserId(context))
                .put("Authorization", ApiPaths.TOKEN)
                .build();
    }

    public static Map<String, String> authenticatedHeader(Context context, String token) {
        return ImmutableMap.<String, String>builder()
                .put("Authorization", token)
                .build();
    }

    public static byte[] getJsonBody(String json) {
        try {
            return json == null ? null : json.getBytes("utf-8");
        } catch (UnsupportedEncodingException uee) {
            VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", json, "utf-8");
            return null;
        }
    }

    public static byte[] getJsonBody(JSONObject jsonObject) {
        String json = jsonObject.toString();
        try {
            return json == null ? null : json.getBytes("utf-8");
        } catch (UnsupportedEncodingException uee) {
            VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", json, "utf-8");
            return null;
        }
    }

    public static Picasso getAuthenticatedPicasso(Context context) {
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(new Interceptor() {
                    @Override
                    public okhttp3.Response intercept(Interceptor.Chain chain) throws IOException {
                        okhttp3.Request newRequest = chain.request().newBuilder()
                                .addHeader("userId", SharedPrefManager.getUserId(context))
                                .addHeader("Authorization", ApiPaths.TOKEN)
                                .build();
                        return chain.proceed(newRequest);
                    }
                })
                .build();

        return new Picasso.Builder(context)
                .downloader(new OkHttp3Downloader(client))
                .build();
    }

    public static JSONObject commonPostBody(Context context) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("userId", SharedPrefManager.getUserId(context));
        } catch (JSONException e) {
            Log.e("SignUp", e.toString());
        }
        return jsonObject;
    }

    public static JsonObjectRequest authenticatedPostRequest(Context context, String url, JSONObject jsonObject,
                                                             Response.Listener<JSONObject> responseHandler,
                                                             Response.ErrorListener errorHandler) {

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST,
                url, jsonObject,
                responseHandler, errorHandler) {

            @Override
            public Map<String, String> getHeaders() {
                return authenticatedHeader(context);
            }

        };
        request.setShouldCache(true);
        return request;
    }

    public static JsonObjectRequest authenticatedBasicPostRequest(Context context, String url, JSONObject jsonObject,
                                                                  Response.Listener<JSONObject> responseHandler,
                                                                  Response.ErrorListener errorHandler) {

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST,
                url, jsonObject,
                responseHandler, errorHandler) {

            @Override
            public Map<String, String> getHeaders() {
                return authenticatedHeader(context, ApiPaths.TOKEN);
            }

        };
        request.setShouldCache(true);
        return request;
    }

    public static JsonObjectRequest authenticatedPutRequest(Context context, String url, JSONObject jsonObject,
                                                            Response.Listener<JSONObject> responseHandler,
                                                            Response.ErrorListener errorHandler) {

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.PUT,
                url, jsonObject,
                responseHandler, errorHandler) {

            @Override
            public Map<String, String> getHeaders() {
                return authenticatedHeader(context);
            }

        };
        request.setShouldCache(true);
        return request;
    }

    public static StringRequest authenticatedGetRequest(Context context, String url,
                                                        Response.Listener<String> responseHandler,
                                                        Response.ErrorListener errorHandler) {
        StringRequest request = new StringRequest(Request.Method.GET, url,
                responseHandler, errorHandler) {

            @Override
            public Map<String, String> getHeaders() {
                return NetworkUtils.authenticatedHeader(context);
            }

        };

        request.setShouldCache(true);
        return request;
    }

    public static JsonObjectRequest toggleTenantFavoritesRequest(Context context, String userId, boolean fav,
                                                                 Response.Listener<JSONObject> responseHandler,
                                                                 Response.ErrorListener errorHandler) {
        JSONObject jsonObject = new JSONObject((ImmutableMap.<String, String>builder()
                .put("userId", SharedPrefManager.getUserId(context))
                .put("userIdFav", userId)
                .build()));
        JsonObjectRequest request = new JsonObjectRequest(fav ? Request.Method.POST : Request.Method.DELETE,
                ApiPaths.USER_FAVORITE_UPDATE, jsonObject, responseHandler, errorHandler) {

            @Override
            public Map<String, String> getHeaders() {
                return authenticatedHeader(context);
            }

        };
        request.setShouldCache(false);
        return request;
    }

    public static JsonObjectRequest toggleRoomFavoritesRequest(Context context, String propertyId, boolean fav,
                                                               Response.Listener<JSONObject> responseHandler,
                                                               Response.ErrorListener errorHandler) {
        JSONObject jsonObject = new JSONObject((ImmutableMap.<String, String>builder()
                .put("userId", SharedPrefManager.getUserId(context))
                .put("propertyId", propertyId)
                .build()));
        JsonObjectRequest request = new JsonObjectRequest(fav ? Request.Method.POST : Request.Method.DELETE,
                ApiPaths.PROPERTY_FAVORITE_UPDATE, jsonObject,
                responseHandler, errorHandler) {

            @Override
            public Map<String, String> getHeaders() {
                return NetworkUtils.authenticatedHeader(context);
            }

        };
        request.setShouldCache(false);
        return request;
    }

    public static JsonObjectRequest sendContactRequest(Context context, String userId,
                                                       Response.Listener<JSONObject> responseHandler,
                                                       Response.ErrorListener errorHandler) {
        JSONObject post = NetworkUtils.commonPostBody(context);
        try {
            post.put("userIdSend", userId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST,
                ApiPaths.SEND_CONTACT_REQUEST, post, responseHandler, errorHandler) {
            @Override
            public Map<String, String> getHeaders() {
                return NetworkUtils.authenticatedHeader(context);
            }
        };
        request.setShouldCache(false);
        return request;
    }

    public static JsonObjectRequest getContactsRequest(Context context,
                                                       Response.Listener<JSONObject> responseHandler,
                                                       Response.ErrorListener errorHandler) {
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST,
                ApiPaths.CHATTED_USERS, NetworkUtils.commonPostBody(context),
                responseHandler, errorHandler) {

            @Override
            public Map<String, String> getHeaders() {
                return NetworkUtils.authenticatedHeader(context);
            }

        };
        request.setShouldCache(true);
        return request;
    }

    public static void loadBitmap(String id, ImageView imageView) throws JSONException {
        MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        JSONObject imageJson = new JSONObject();
        imageJson.put("id", id);
        imageJson.put("type", "default");
        Log.d("BitmapLoader", "Request : " + imageJson);
        String imageJsonString = imageJson.toString();
        OkHttpClient client = new OkHttpClient();
        RequestBody body = RequestBody.create(JSON, imageJsonString);
        okhttp3.Request request = new okhttp3.Request.Builder()
                .url(ApiPaths.PATH_USER_AVATAR)
                .post(body)
                .addHeader("content-type", "application/json; charset=utf-8")
                .addHeader("Authorization", ApiPaths.TOKEN)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.e("response", "Error : " + call.request().body().toString());
            }

            @Override
            public void onResponse(Call call, okhttp3.Response response) throws IOException {
                byte[] bytes = response.body().bytes();
                Bitmap decodedByte = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                new android.os.Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        imageView.setImageBitmap(decodedByte);
                    }
                });
            }
        });
    }
}
