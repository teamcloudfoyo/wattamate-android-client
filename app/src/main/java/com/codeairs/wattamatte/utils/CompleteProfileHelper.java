package com.codeairs.wattamatte.utils;

import android.content.Context;
import android.widget.Button;
import com.codeairs.wattamatte.constants.Identifiers;
import com.codeairs.wattamatte.models.completeprofiledetails.*;
import com.google.common.collect.ImmutableMap;

import java.util.ArrayList;
import java.util.List;

public class CompleteProfileHelper {

    private static final int MIN_COUNT = 3;

    public static <T extends ProfileItem> void handleClipArtStateChange(List<T> items, Button nextButton) {
        int count = 0;
        for (ProfileItem item : items) {
            if (item.isChecked)
                count++;
            if (count >= MIN_COUNT) {
                AndroidUtils.enableView(nextButton);
                return;
            }
        }
        AndroidUtils.disableView(nextButton);
    }

    public static CompleteProfileDetails convert(
            ProfileDetails details, List<Hobby> hobbies, List<Feature> features,
            List<Feature> mateFeatures, Context context) {
        CompleteProfileDetails cpd = new CompleteProfileDetails();
        cpd.userId = SharedPrefManager.getUserId(context);
        cpd.registrationToken = SharedPrefManager.getFirebaseToken(context);
        cpd.dateOfBirth = details.dob;
        cpd.firstName = details.name;
        cpd.lastName = details.name;
        cpd.civility = "M";
        if (details.gender == 1) {
            cpd.sex = "M";
        } else {
            cpd.sex = "F";
        }
        if (details.professionId == 0) {
            cpd.post = "Étudiant";
            cpd.university = details.profession;
        } else if (details.professionId == 1) {
            cpd.post = "salarié";
            cpd.job = details.profession;
        } else {
            cpd.post = "En recherche";
            cpd.job = details.profession;
        }

        cpd.phone = SharedPrefManager.getPhoneNo(context);
        cpd.address = details.location.address;

        List<HobbyRequest> hobbyRequests = new ArrayList<>();
        for (Hobby hobby : hobbies) {
            if (hobby.isChecked) {
                hobbyRequests.add(new HobbyRequest(hobby.id, hobby.name));
            }
        }
        cpd.hobbies = hobbyRequests;

        List<KeyFeatureMine> mine = new ArrayList<>();
        for (Feature feature : features) {
            if (feature.isChecked) {
                mine.add(new KeyFeatureMine(feature.id, feature.name));
            }
        }

        List<KeyFeatureMate> mate = new ArrayList<>();
        for (Feature feature : mateFeatures) {
            if (feature.isChecked) {
                mate.add(new KeyFeatureMate(feature.id, feature.name));
            }
        }

        cpd.keyFeatures = ImmutableMap.<String, Object>builder()
                .put("mine", mine)
                .put("mate", mate)
                .build();

        List<AdditionInfo> aiRequests = new ArrayList<>();
        aiRequests.add(new AdditionInfo(Identifiers.SMOKER, "Est-ce que tu fumes ?",
                String.valueOf(details.isSmoker == 1)));
        aiRequests.add(new AdditionInfo(Identifiers.PETS, "Tu as un animal ? ",
                String.valueOf(details.havePets == 1)));
        aiRequests.add(new AdditionInfo(Identifiers.CHILDREN, "Tu as des enfants ? ",
                String.valueOf(details.haveChildren == 1)));
        aiRequests.add(new AdditionInfo(Identifiers.FURNITURE, "Tu cherches un meublé ?",
                getHaveFurniture(details.haveFurniture)));
        aiRequests.add(new AdditionInfo(Identifiers.NO_OF_MATES, "Tu cherches à vivre avec ?",
                details.matesCount));
        aiRequests.add(new AdditionInfo(Identifiers.LOCATION, "Tu recherche à ? ",
                details.location.location));
        aiRequests.add(new AdditionInfo(Identifiers.BUDGET_MIN, "Budget min",
                String.valueOf(details.minBudget)));
        aiRequests.add(new AdditionInfo(Identifiers.BUDGET_MAX, "Budget max",
                String.valueOf(details.maxBudget)));
        aiRequests.add(new AdditionInfo(Identifiers.ENTRY_DATE, "Date d'entrée",
                DateUtils.toFormat(details.entryDate, DateUtils.PRIMARY_DATE_FORMAT)));
        cpd.additionalInfo = aiRequests;

        return cpd;
    }

    private static String getHaveFurniture(int ans) {
        switch (ans) {
            case 0:
                return "false";
            case 1:
                return "true";
            default:
                return "A determiner";
        }
    }

    public static void main(String args[]) {

    }
}
