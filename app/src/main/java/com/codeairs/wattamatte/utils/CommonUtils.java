package com.codeairs.wattamatte.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.codeairs.wattamatte.AppController;
import com.codeairs.wattamatte.R;
import com.codeairs.wattamatte.constants.ApiPaths;
import com.codeairs.wattamatte.constants.Constants;
import com.codeairs.wattamatte.constants.Identifiers;
import com.codeairs.wattamatte.models.Item;
import com.codeairs.wattamatte.models.NotificationData;
import com.codeairs.wattamatte.models.SafeVault.Proof;
import com.codeairs.wattamatte.models.Tenant.Tenant;
import com.codeairs.wattamatte.models.Tenant.TenantHobby;
import com.codeairs.wattamatte.models.Tenant.TenantKeyFeatureMate;
import com.codeairs.wattamatte.models.Tenant.TenantKeyFeatureMine;
import com.codeairs.wattamatte.models.Tenant.TenantKeyFeatures;
import com.codeairs.wattamatte.models.completeprofiledetails.AdditionInfo;
import com.codeairs.wattamatte.models.completeprofiledetails.Feature;
import com.codeairs.wattamatte.models.completeprofiledetails.Hobby;
import com.codeairs.wattamatte.models.profile.MyProfile;
import com.codeairs.wattamatte.models.rooms.Property;
import com.codeairs.wattamatte.models.rooms.Room;
import com.google.common.collect.Lists;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;

import static com.codeairs.wattamatte.constants.ApiPaths.IMG_PATH_PREFIX;
import static com.codeairs.wattamatte.constants.ApiPaths.IMG_PATH_TO_REPLACE;

public class CommonUtils {

    private static final String TAG = CommonUtils.class.getSimpleName();

    public static void startImagePicker(Activity activity) {
        CropImage.activity()
                .setGuidelines(CropImageView.Guidelines.ON)
                .setCropShape(CropImageView.CropShape.OVAL)
                .setAspectRatio(1, 1)
                .start(activity);
    }

    public static void startCardImagePicker(Activity activity) {
        CropImage.activity()
                .setGuidelines(CropImageView.Guidelines.ON)
                .setCropShape(CropImageView.CropShape.RECTANGLE)
                .setAspectRatio(3370, 2125)
                .start(activity);
    }

    public static String getLocaleStringResource(Locale requestedLocale, int resourceId, Context context) {
        String result;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) { // use latest api
            Configuration config = new Configuration(context.getResources().getConfiguration());
            config.setLocale(requestedLocale);
            result = context.createConfigurationContext(config).getText(resourceId).toString();
        } else { // support older android versions
            Resources resources = context.getResources();
            Configuration conf = resources.getConfiguration();
            Locale savedLocale = conf.locale;
            conf.locale = requestedLocale;
            resources.updateConfiguration(conf, null);

            // retrieve resources from desired locale
            result = resources.getString(resourceId);

            // restore original locale
            conf.locale = savedLocale;
            resources.updateConfiguration(conf, null);
        }

        return result;
    }

    public static String getValueFromAdditionalInfos(List<AdditionInfo> infos, String id) {
        if (infos == null) {
            return null;
        }
        for (AdditionInfo info : infos) {
            if (id.equals(info.id))
                return info.response.toString();
        }
        return null;
    }

    public static String getNameWithAge(Tenant tenant) {
        return tenant.firstName + ", " + DateUtils.getAge(tenant.dob);
    }

    public static String getBudget(Tenant tenant) {
        return CommonUtils.getValueFromAdditionalInfos(tenant.additionInfos, Identifiers.BUDGET_MIN) + " € - "
                + CommonUtils.getValueFromAdditionalInfos(tenant.additionInfos, Identifiers.BUDGET_MAX) + " €";
    }

    public static List<Item> getHobbyItems(List<TenantHobby> hobbies) {
        List<Item> items = Lists.newArrayList();
        if (hobbies != null)
            for (TenantHobby request : hobbies) {
                Item item = new Item(request.id, request.name, null);
                if (request.hobby != null && request.hobby.photo != null && request.hobby.photo.size() != 0) {
                    item.imageUrl = request.hobby.photo.get(0);
                }
                items.add(item);
            }
        return items;
    }


    public static List<Item> getMineFeatureItems(TenantKeyFeatures features) {
        List<Item> items = new ArrayList<>();
        if (features != null) {
            for (TenantKeyFeatureMine request : features.features.mine) {
                Item item = new Item(null, request.name, null);
                if (request.id != null)
                    item.idString = request.id.id;

                if (request.id != null && request.id.photo != null && request.id.photo.size() != 0)
                    item.imageUrl = request.id.photo.get(0);
                items.add(item);
            }
        }
        return items;
    }

    public static List<Item> getMateFeatureItems(TenantKeyFeatures features) {
        List<Item> items = new ArrayList<>();
        if (features != null) {
            for (TenantKeyFeatureMate request : features.features.mate) {
                Item item = new Item(null, request.name, null);
                if (request.id != null)
                    item.idString = request.id.id;
                if (request.id != null && request.id.photo != null && request.id.photo.size() != 0)
                    item.imageUrl = request.id.photo.get(0);
                items.add(item);
            }
        }
        return items;
    }

    public static int getProfessionImgResId(String job) {
        switch (job) {
            case "salarié":
                return R.drawable.badge_employee;
            case "Étudiant":
                return R.drawable.bad_employee_no;
        }
        return R.drawable.bad_employee_no;
    }

    public static int getChildrenImgResId(List<AdditionInfo> infos) {
        if (Boolean.valueOf(CommonUtils.getValueFromAdditionalInfos(
                infos, Identifiers.CHILDREN))) {
            return R.drawable.badge_children_yes;
        }
        return R.drawable.badge_children_no;
    }

    public static int getSmokerImgRedId(List<AdditionInfo> infos) {
        if (Boolean.valueOf(CommonUtils.getValueFromAdditionalInfos(
                infos, Identifiers.SMOKER))) {
            return R.drawable.badge_smoker_yes;
        }
        return R.drawable.badge_smoker_no;
    }

    public static int getAnimalsImgResId(List<AdditionInfo> infos) {
        if (Boolean.valueOf(CommonUtils.getValueFromAdditionalInfos(
                infos, Identifiers.PETS))) {
            return R.drawable.badge_animal_yes;
        }
        return R.drawable.badge_animal_no;
    }

    /**
     * Because of shit quality response, we need to replace part of the path coming in the json with another
     */
    public static void loadPicture(String picUrl, ImageView imageView) {
        if (picUrl != null) {
            picUrl = picUrl.replace(IMG_PATH_TO_REPLACE, IMG_PATH_PREFIX);
            AppController.getPicasso().load(picUrl).placeholder(R.drawable.splash_logo).into(imageView);
        } else
            AppController.getPicasso().load(R.drawable.splash_logo).into(imageView);
    }

    public static void loadPicture(String picUrl, ImageView imageView, int placeholder) {
        if (!TextUtils.isEmpty(picUrl)) {
            picUrl = picUrl.replace(IMG_PATH_TO_REPLACE, ApiPaths.PATH_PREFIX + "/");
            AppController.getPicasso().load(picUrl).placeholder(placeholder).into(imageView);
        }
    }

    public static SweetAlertDialog getDialog(Context context, String msg) {
        SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(ContextCompat.getColor(context, R.color.darkSlateBlue));
        pDialog.setTitleText(msg);
        pDialog.setCancelable(false);
        return pDialog;
    }

    public static SweetAlertDialog getRetryDialog(Context context, String conetnt, String confirmMessage, String cancelMessage) {
        SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                .setTitleText("Retry ?")
                .setContentText(conetnt)
                .setConfirmText(confirmMessage)
                .setCancelText(cancelMessage);
        return sweetAlertDialog;
    }

    public static void addOrRemoveFavorite(List<String> favList, String id, boolean status) {
        if (status) {
            if (!favList.contains(id))
                favList.add(id);
        } else {
            if (favList.contains(id))
                favList.remove(id);
        }
    }

    public static List<Property> getProperties(List<Room> rooms) {
        List<Property> properties = new ArrayList<>();
        for (Room room : rooms)
            properties.add(room.property);
        return properties;
    }

    public static String getEmail(MyProfile profile) {
        if (profile.fbDetail != null) {
            return profile.fbDetail.email;
        }
        return profile.accountDetail.email;
    }

    public static Tenant getPagenationTenant() {
        Tenant tenant = new Tenant();
        tenant.isPagenationView = true;
        return tenant;
    }

    public static String getValueFromKey(Map<String, String> map, String searchKey) {
        Iterator myVeryOwnIterator = map.keySet().iterator();
        while (myVeryOwnIterator.hasNext()) {
            String key = (String) myVeryOwnIterator.next();
            String value = (String) map.get(key);
            Log.d("getValueFromKey", "Checking our Key : " + searchKey + " against : " + key);
            if (key.equalsIgnoreCase(searchKey)) {
                return value;
            }
        }
        return "";
    }

    public static Intent copyValuesToIntent(Map<String, String> map, Intent intent) {
        Iterator myVeryOwnIterator = map.keySet().iterator();
        while (myVeryOwnIterator.hasNext()) {
            String key = (String) myVeryOwnIterator.next();
            String value = (String) map.get(key);
            intent.putExtra(key, value);
        }
        return intent;
    }

    public static void addTenantsWithoutDuplication(List<Tenant> currentTenants, List<Tenant> fetchedTenants) {
        if (currentTenants.size() == 0) {
            currentTenants.addAll(fetchedTenants);
            return;
        }
        List<Tenant> distinctTenants = new ArrayList<>();
        for (int i = 0; i < fetchedTenants.size(); i++) {
            if (!checkTenantIdDuplication(currentTenants, fetchedTenants.get(i).userId))
                distinctTenants.add(fetchedTenants.get(i));
        }
        currentTenants.addAll(distinctTenants);
    }

    public static boolean checkTenantIdDuplication(List<Tenant> currentTenants, String id) {
        for (Tenant tenant : currentTenants) {
            if (tenant.userId != null && tenant.userId.equals(id))
                return true;
        }
        return false;
    }

    public static JSONObject getUserIdJson(Context context) {
        JSONObject requestJson = new JSONObject();
        try {
            requestJson.put("userId", SharedPrefManager.getUserId(context));
            return requestJson;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return requestJson;
    }

    public static NotificationData getModelFromNotificationIntent(Intent notificationIntent) {
        if (notificationIntent == null)
            return null;
        Bundle bundle = notificationIntent.getExtras();
        NotificationData notificationData = new NotificationData();
        notificationData.setNotificationType(bundle.getString("title"));
        notificationData.setNotificationDescription(bundle.getString("body"));
        if (notificationData.isFavouriteNotification()) {
            notificationData.setNotifiedUserId(bundle.getString("userId"));
        } else if (notificationData.isChatNotification()) {
            notificationData.setNotifiedUserId(bundle.getString("userOneId"));
        } else if (notificationData.isContactRequestNotification()) {
            notificationData.setNotifiedUserId(bundle.getString("userId"));
        }
        return notificationData;
    }

    public static void dumpIntent(Intent data) {
        Bundle bundle = data.getExtras();
        if (bundle != null) {
            for (String key : bundle.keySet()) {
                Object value = bundle.get(key);
                Log.d(Constants.NOTIFICATION_TAG, String.format("%s %s (%s)", key, value.toString(), value.getClass().getName()));
            }
        }
    }

    public static List<Proof> getIdentityPieceList(Context context) {
        List<Proof> proof = new ArrayList<>();
        proof.add(new Proof("fr_id", context.getResources().getString(R.string.passport)));
        proof.add(new Proof("fr_id", context.getResources().getString(R.string.id_card)));
        proof.add(new Proof("fr_sejour", context.getResources().getString(R.string.residence_permit)));
        return proof;
    }

    public static List<Proof> getAddressProofList(Context context) {
        List<Proof> proof = new ArrayList<>();
        proof.add(new Proof("fr_loyer", context.getResources().getString(R.string.rent_receipt)));
        proof.add(new Proof("fr_foncier", context.getResources().getString(R.string.property_tax)));
        proof.add(new Proof("fr_facture3m", context.getResources().getString(R.string.invoice)));
        return proof;
    }

    public static List<Proof> getGuarantorIdentityProofList(Context context) {
        List<Proof> proof = new ArrayList<>();
        proof.add(new Proof("fr_id", context.getResources().getString(R.string.passport)));
        proof.add(new Proof("fr_id", context.getResources().getString(R.string.id_card)));
        proof.add(new Proof("fr_sejour", context.getResources().getString(R.string.residence_permit)));
        return proof;
    }

    public static void getActiveHobbies(List<Hobby> hobbies) {
        Log.d("Filter", "Hobbies Count : " + hobbies.size());
        List<Hobby> activeHobbies = new ArrayList<>();
        for (Hobby hobby : hobbies) {
            Log.d("Filter", "Iteration Hobby : " + hobby.name + " is Active : " + hobby.gethDisplayed());
            if (hobby.gethDisplayed())
                activeHobbies.add(hobby);
        }
        hobbies.clear();
        hobbies.addAll(activeHobbies);
        Log.d("Filter", "Filtered Hobbies Count : " + hobbies.size());
    }

    public static void getActiveFeatures(List<Feature> features) {
        Log.d("Filter", "Features Count : " + features.size());
        List<Feature> activeFeatures = new ArrayList<>();
        for (Feature feature : features) {
            Log.d("Filter", "Iteration Feature : " + feature.name + " is Active : " + feature.getfDisplayed());
            if (feature.getfDisplayed())
                activeFeatures.add(feature);
        }
        features.clear();
        features.addAll(activeFeatures);
        Log.d("Filter", "Filtered Features Count : " + features.size());
    }

    public static void addDatePickerFeature(EditText editText, View.OnClickListener onClickListener) {
        editText.setKeyListener(null);
        editText.setCursorVisible(false);
        editText.setPressed(false);
        editText.setFocusable(false);
        editText.setOnClickListener(onClickListener);
    }
}
