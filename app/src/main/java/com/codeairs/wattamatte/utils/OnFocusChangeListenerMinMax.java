package com.codeairs.wattamatte.utils;

import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;

public class OnFocusChangeListenerMinMax implements View.OnFocusChangeListener {

    private int offset;
    private EditText otherTextField;
    private boolean isMin;

    public OnFocusChangeListenerMinMax(int value, boolean isMin, EditText otherTextField) {
        this.offset = value;
        this.isMin = isMin;
        this.otherTextField = otherTextField;
    }

    public OnFocusChangeListenerMinMax(String min) {
        this.offset = Integer.parseInt(min);
    }


    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if (!hasFocus) {
            EditText editText = (EditText) v;
            String val = editText.getText().toString();
            if (!TextUtils.isEmpty(val)) {
                int intValue = Integer.valueOf(val);
                int otherFieldValue = Integer.valueOf(otherTextField.getText().toString());
                if (isMin) {
                    if (intValue < offset || intValue > otherFieldValue) {
                        editText.setText(String.valueOf(offset));
                    }
                } else {
                    if (intValue > offset || intValue < otherFieldValue) {
                        editText.setText(String.valueOf(offset));
                    }
                }
            } else {
                editText.setText(String.valueOf(offset));
            }
        }
    }
}
