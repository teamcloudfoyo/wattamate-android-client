package com.codeairs.wattamatte.utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.*;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.AttrRes;
import android.support.annotation.ColorInt;
import android.support.annotation.ColorRes;
import android.support.annotation.DrawableRes;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.ColorUtils;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.view.MarginLayoutParamsCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.*;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.codeairs.wattamatte.R;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;

/**
 * Created at Magora Systems (http://magora-systems.com) on 14.07.16
 *
 * @author Stanislav S. Borzenko
 */
public class AndroidUtils {
    @ColorInt
    public static int getColor(Context context, @ColorRes int colorResId) {
        int color;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            color = context.getColor(colorResId);
        } else {
            //noinspection deprecation
            color = context.getResources().getColor(colorResId);
        }

        return color;
    }

    @ColorInt
    public static int getThemeColor(Context context, @AttrRes int resourceId) {
        TypedValue typedValue = new TypedValue();
        TypedArray typedArray = context.obtainStyledAttributes(typedValue.data,
                new int[]{resourceId});
        try {
            return typedArray.getColor(0, 0);
        } finally {
            typedArray.recycle();
        }
    }

    public static Drawable getDrawable(Context context, int resourceId) {
        Drawable iconDrawable;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            iconDrawable = context.getResources()
                    .getDrawable(resourceId, context.getTheme());
        } else {
            //noinspection deprecation
            iconDrawable = context.getResources().getDrawable(resourceId);
        }

        return iconDrawable;
    }

    public static Drawable getTintDrawableByColor(
            Context context,
            @DrawableRes int drawableResId,
            @ColorInt int color) {
        Drawable drawable = AndroidUtils.getDrawable(context, drawableResId);
//        if (DrawableUtils.canSafelyMutateDrawable(drawable)) {
//            drawable = drawable.mutate();
//        }

        Drawable drawableCompat = DrawableCompat.wrap(drawable);
        DrawableCompat.setTint(drawableCompat, color);

        return drawableCompat;
    }

    public static Drawable getTintDrawableByColor2(
            Context context,
            @DrawableRes int drawableResId,
            @ColorInt int color) {
        Drawable drawable = AndroidUtils.getDrawable(context, drawableResId);

//        if (DrawableUtils.canSafelyMutateDrawable(drawable)) {
//            drawable = drawable.mutate();
//        }

        PorterDuffColorFilter filter
                = new PorterDuffColorFilter(color, PorterDuff.Mode.SRC_IN);
        drawable.setColorFilter(filter);

        return drawable;
    }

    public static Drawable getTintDrawableByColorRes(
            Context context,
            @DrawableRes int drawableResId,
            @ColorRes int colorResId) {
        int color = AndroidUtils.getColor(context, colorResId);
        return getTintDrawableByColor(context, drawableResId, color);
    }

    public static Drawable getTintDrawableByThemeAttr(
            Context context,
            @DrawableRes int drawableResId,
            @AttrRes int attrResId) {
        int color = AndroidUtils.getThemeColor(context, attrResId);
        return getTintDrawableByColor(context, drawableResId, color);
    }

    public static Drawable getTintDrawableByThemeAttr2(
            Context context,
            @DrawableRes int drawableResId,
            @AttrRes int attrResId) {
        int color = AndroidUtils.getThemeColor(context, attrResId);
        return getTintDrawableByColor2(context, drawableResId, color);
    }

    @DrawableRes
    public static int getSelectableItemBackground(Context context) {
        int[] attrs = new int[]{R.attr.selectableItemBackground};
        TypedArray typedArray = context.obtainStyledAttributes(attrs);
        int backgroundResource = typedArray.getResourceId(0, 0);
        typedArray.recycle();

        return backgroundResource;
    }

    public static int getStatusBarHeight(Context context) {
        int resource = context.getResources()
                .getIdentifier("status_bar_height", "dimen", "android");
        if (resource > 0) {
            return context.getResources().getDimensionPixelSize(resource);
        }

        return -1;
    }

    public static int getScreenHeight(Activity activity) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.heightPixels;
    }

    public static float pxToDp(final Context context, final float px) {
        return px / context.getResources().getDisplayMetrics().density;
    }

    public static int dpToPx(final Context context, final float dp) {
        return (int) (dp * context.getResources().getDisplayMetrics().density);
    }

    public static int getScreenWidth(Activity activity) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.widthPixels;
    }

    public static void setMarginEnd(View view, int marginEnd) {
        ViewGroup.MarginLayoutParams layoutParams =
                (ViewGroup.MarginLayoutParams) view.getLayoutParams();
        MarginLayoutParamsCompat.setMarginEnd(layoutParams, marginEnd);
        view.setLayoutParams(layoutParams);
    }

    public static void setMarginStart(View view, int marginStart) {
        ViewGroup.MarginLayoutParams layoutParams =
                (ViewGroup.MarginLayoutParams) view.getLayoutParams();
        MarginLayoutParamsCompat.setMarginStart(layoutParams, marginStart);
        view.setLayoutParams(layoutParams);
    }

    public static void setMargins(View v, int l, int t, int r, int b) {
        if (v.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
            ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) v.getLayoutParams();
            if (p.topMargin == t)
                return;
            p.setMargins(l, t, r, b);
            v.requestLayout();
        }
    }

    public static void setViewWidth(View view, int width) {
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        layoutParams.width = width;
        view.setLayoutParams(layoutParams);
    }

    public static void setViewHeight(View view, int height) {
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        layoutParams.height = height;
        view.setLayoutParams(layoutParams);
    }

    public static void setOverflowButtonColor(final Activity activity, final int color) {
        final String overflowDescription = activity.getString(R.string.abc_action_menu_overflow_description);
        final ViewGroup decorView = (ViewGroup) activity.getWindow().getDecorView();
        final ViewTreeObserver viewTreeObserver = decorView.getViewTreeObserver();
        viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                final ArrayList<View> outViews = new ArrayList<View>();
                decorView.findViewsWithText(outViews, overflowDescription, View.FIND_VIEWS_WITH_CONTENT_DESCRIPTION);
                if (outViews.isEmpty()) {
                    return;
                }
                AppCompatImageView overflow = (AppCompatImageView) outViews.get(0);
                overflow.setColorFilter(color);
//                removeOnGlobalLayoutListener(decorView, this);
            }
        });
    }

//    public static void setTransParentToolbar(ActionBar actionBar, Activity activity) {
//        actionBar.setBackgroundDrawable(new ColorDrawable(ContextCompat.getColor(
//                activity, R.color.yourTranslucentColor)));
//    }

    public static boolean setActionBarBack(MenuItem item, Activity activity) {
        switch (item.getItemId()) {
            case android.R.id.home:
                activity.onBackPressed();
                return true;
            default:
                return activity.onOptionsItemSelected(item);
        }
    }

//    public static void setActionBarTitle(int stringId, AppCompatActivity activity, String font) {
//        setActionBarTitle(activity.getString(stringId), activity, font);
//    }

//    public static void setActionBarTitle(String text, AppCompatActivity activity, String font) {
//        SpannableString s = new SpannableString(text);
//        s.setSpan(new TypefaceSpan(activity, font), 0, s.length(),
//                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//        activity.getSupportActionBar().setTitle(s);
//    }

    public static void setDialogDimension(Activity activity, Dialog dialog) {
        Context context = activity.getApplicationContext();
        DisplayMetrics displayMetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int screenHeight = displayMetrics.heightPixels;
        int screenWidth = displayMetrics.widthPixels;
        int dialogWidth = screenWidth - AndroidUtils.dpToPx(context, 32); // dialog width - padding 16dp
        int dialogHeight = (int) (.74 * screenHeight); // dialog height 74% of screen height
        dialog.getWindow().setLayout(dialogWidth, dialogHeight);
    }

    public static void showNoInternetSnackBar(final Activity activity) {
        Snackbar.make(activity.findViewById(android.R.id.content), "No internet connection.", Snackbar.LENGTH_LONG)
                .setAction("Settings", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        activity.startActivityForResult(
                                new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                    }
                })
                .show();
    }

    public static void setTransparentStatusBar(Window window) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setNavigationBarColor(Color.BLACK);
            window.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
            window.getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            window.setStatusBarColor(Color.TRANSPARENT);
        } else {
            window.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
            window.getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            window.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION, WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
            window.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
    }


    public static void applyFontForToolbarTitle(Context context, Toolbar toolbar) {
        for (int i = 0; i < toolbar.getChildCount(); i++) {
            View view = toolbar.getChildAt(i);
            if (view instanceof TextView) {
                TextView tv = (TextView) view;
                Typeface titleFont = Typeface.
                        createFromAsset(context.getAssets(), "fonts/CircularStd-Bold.otf");
                if (tv.getText().equals(toolbar.getTitle())) {
                    tv.setTypeface(titleFont);
                    break;
                }
            }
        }
    }

    public static void hideSoftKeyboard(Activity activity) {
        View view = activity.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public static Point getNavigationBarSize(Context context) {
        Point appUsableSize = getAppUsableScreenSize(context);
        Point realScreenSize = getRealScreenSize(context);

        // navigation bar on the right
        if (appUsableSize.x < realScreenSize.x) {
            return new Point(realScreenSize.x - appUsableSize.x, appUsableSize.y);
        }

        // navigation bar at the bottom
        if (appUsableSize.y < realScreenSize.y) {
            return new Point(appUsableSize.x, realScreenSize.y - appUsableSize.y);
        }

        // navigation bar is not present
        return new Point();
    }

    public static Point getAppUsableScreenSize(Context context) {
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = windowManager.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return size;
    }

    public static Point getRealScreenSize(Context context) {
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = windowManager.getDefaultDisplay();
        Point size = new Point();

        if (Build.VERSION.SDK_INT >= 17) {
            display.getRealSize(size);
        } else if (Build.VERSION.SDK_INT >= 15) {
            try {
                size.x = (Integer) Display.class.getMethod("getRawWidth").invoke(display);
                size.y = (Integer) Display.class.getMethod("getRawHeight").invoke(display);
            } catch (IllegalAccessException e) {
            } catch (InvocationTargetException e) {
            } catch (NoSuchMethodException e) {
            }
        }

        return size;
    }

    public static void setText(View view, int tvId, int string) {
        TextView title = view.findViewById(tvId);
        title.setText(string);
    }

    public static void setText(View view, int resId, String text) {
        TextView textview = view.findViewById(resId);
        textview.setText(text);
    }

    public static void setImage(View view, int imageResourceId){
        ((ImageView)view).setImageResource(imageResourceId);
    }

    public static void enableViews(View... views) {
        for (View view : views) {
            enableView(view);
        }
    }

    public static void enableView(View view) {
        view.getBackground().setAlpha(255);
        view.setEnabled(true);
        if (view instanceof Button) {
            Button btn = (Button) view;
            int color = btn.getCurrentTextColor();
            int alpha = ColorUtils.setAlphaComponent(color, 255);
            btn.setTextColor(alpha);
        }

    }

    public static void disableViews(View... views) {
        for (View view : views) {
            disableView(view);
        }
    }

    public static void disableView(View view) {
        view.getBackground().setAlpha(80);
        view.setEnabled(false);
        if (view instanceof Button) {
            Button btn = (Button) view;
            int color = btn.getCurrentTextColor();
            int alpha = ColorUtils.setAlphaComponent(color, 80);
            btn.setTextColor(alpha);
        }
    }

    public static void changeStatusBarIconColor(Window window, boolean shouldChangeStatusBarTintToDark) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            View decor = window.getDecorView();
            if (shouldChangeStatusBarTintToDark) {
                decor.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR |
                        View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            } else {
                // We want to change tint color to white again.
                // You can also record the flags in advance so that you can turn UI back completely if
                // you have set other flags before, such as translucent or full screen.
                decor.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE |
                        View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            }
        }
    }

    public static int getToolBarHeight(Context context) {
        int[] attrs = new int[]{R.attr.actionBarSize};
        TypedArray ta = context.obtainStyledAttributes(attrs);
        int toolBarHeight = ta.getDimensionPixelSize(0, -1);
        ta.recycle();
        return toolBarHeight;
    }

    public static void changeStatusBarToWhite(Window window, Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            View decor = window.getDecorView();
            decor.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(ContextCompat.getColor(context, R.color.white));
        }
    }

    public static void changeStatusBarColor(Window window, Context context, int color) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            View decor = window.getDecorView();
            decor.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(ContextCompat.getColor(context, color));
        }
    }

    public static void setStatusBarAppearance(Window window, boolean shouldChangeStatusBarTintToDark, int color) {
        AndroidUtils.changeStatusBarIconColor(window, shouldChangeStatusBarTintToDark);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(color);
        }
    }

    public static Toolbar addToolbarWithBack(AppCompatActivity activity) {
        Toolbar toolbar = activity.findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_back_chevron_24dp);
        activity.setSupportActionBar(toolbar);
        activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        activity.getSupportActionBar().setDisplayShowHomeEnabled(true);
        activity.getSupportActionBar().setDisplayShowTitleEnabled(false);
        return toolbar;
    }

    public static Toolbar addToolbarWithBack(AppCompatActivity activity, View view) {
        Toolbar toolbar = view.findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_back_chevron_24dp);
        activity.setSupportActionBar(toolbar);
        activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        activity.getSupportActionBar().setDisplayShowHomeEnabled(true);
        activity.getSupportActionBar().setDisplayShowTitleEnabled(false);
        return toolbar;
    }

    public static Bitmap createDrawableFromView(Context context, View view) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        view.setLayoutParams(new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.buildDrawingCache();
        Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);

        return bitmap;
    }
}
