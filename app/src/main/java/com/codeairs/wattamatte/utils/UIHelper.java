package com.codeairs.wattamatte.utils;

import android.content.Context;
import android.os.Build;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import cn.pedant.SweetAlert.SweetAlertDialog;
import com.codeairs.wattamatte.R;
import com.codeairs.wattamatte.customcontrols.togglebutton.InterestToggleButton;

public class UIHelper {

    public static InterestToggleButton getInterestsToggleButton(
            Context context, int id, int bgColor, int selectedBgColor, int textResId, int imgResId) {
        InterestToggleButton button = new InterestToggleButton(context);
        button.setBgColor(ContextCompat.getColor(context, bgColor));
        button.setSelectedBgColor(ContextCompat.getColor(context, selectedBgColor));
        button.setImage(imgResId);
        button.setId(id);
        button.setText(textResId);
        return button;
    }


    /**
     * used for EditTexts in entry screens
     */
    public static void setError(TextInputLayout inputLayout, EditText editText, String errorMsg) {
        inputLayout.setErrorEnabled(true);
        inputLayout.setError(errorMsg);
        editText.setBackgroundResource(R.drawable.bg_entry_edittext_error);
    }

    /**
     * used for EditTexts in entry screens
     */
    public static void removeError(TextInputLayout inputLayout, EditText editText) {
        inputLayout.setErrorEnabled(false);
        editText.setBackgroundResource(R.drawable.bg_entry_edittext);
    }

    public static boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

    public static void enableViews(View... views) {
        for (View view : views) {
            AndroidUtils.enableView(view);
        }
    }

    public static void disableViews(View... views) {
        for (View view : views) {
            AndroidUtils.disableView(view);
        }
    }

    public static void showErrorAlert(Context context, String msg) {
        final TextView editText = new TextView(context);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            editText.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        }
        editText.setText(msg);
        new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE)
                .setTitleText("Oops...")
                .setCustomView(editText)
                .show();
    }

}
