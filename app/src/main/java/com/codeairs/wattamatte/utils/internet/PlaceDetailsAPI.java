package com.codeairs.wattamatte.utils.internet;

import android.util.Log;
import com.codeairs.wattamatte.models.completeprofiledetails.Address;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class PlaceDetailsAPI {

    private static final String TAG = PlaceDetailsAPI.class.getSimpleName();

    private static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place/details/json";

    private static final String API_KEY = "AIzaSyBoHumggVBWy1-iywwW8LyrlaKInP8t4dk";

    public Address getDetails(String input) {
        Address address = new Address("aaa", 11111, "", "", "");

        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();

        try {
            StringBuilder sb = new StringBuilder(PLACES_API_BASE);
            sb.append("?key=" + API_KEY);
            sb.append("&placeid=" + input);

            URL url = new URL(sb.toString());
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());

            // Load the results into a StringBuilder
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }
        } catch (MalformedURLException e) {
            Log.e(TAG, "Error processing Places API URL", e);
            return null;
        } catch (IOException e) {
            Log.e(TAG, "Error connecting to Places API", e);
            return null;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }

        try {
            JSONObject jsonObj = new JSONObject(jsonResults.toString());
            Log.d("JSON...", jsonObj.toString());
            JSONArray addressComponents = jsonObj.getJSONObject("result").getJSONArray("address_components");
            for (int i = 0; i < addressComponents.length(); i++) {
                JSONArray typesArray = addressComponents.getJSONObject(i).getJSONArray("types");
                for (int j = 0; j < typesArray.length(); j++) {
                    if (typesArray.get(j).toString().equalsIgnoreCase("postal_code")) {
                        String postalCode = addressComponents.getJSONObject(i).getString("long_name");
                        try {
                            address.zip = Integer.valueOf(postalCode);
                        } catch (Exception e) {
                            Log.e("Error", e.getMessage());
                        }
                    } else if (typesArray.get(j).toString().equalsIgnoreCase("locality")) {
                        address.city = addressComponents.getJSONObject(i).getString("long_name");
                        address.address = addressComponents.getJSONObject(i).getString("long_name");
                    } else if (typesArray.get(j).toString().equalsIgnoreCase("country")) {
                        address.country = addressComponents.getJSONObject(i).getString("long_name");
                    } else if (typesArray.get(j).toString().equalsIgnoreCase("administrative_area_level_1")) {
                        address.state = addressComponents.getJSONObject(i).getString("long_name");
                    }
                }
            }
        } catch (JSONException e) {
            Log.e(TAG, "Cannot process JSON results", e);
        }

        return address;
    }

}