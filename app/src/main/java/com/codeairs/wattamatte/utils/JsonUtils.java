package com.codeairs.wattamatte.utils;

import com.codeairs.wattamatte.models.ApiResponse;
import com.google.common.collect.Lists;
import com.google.common.reflect.TypeParameter;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class JsonUtils {

    private static final Gson GSON = new Gson();

    public static <T> ApiResponse<T> fromJson(String json, Class<T> outputClass) {
        return GSON.fromJson(json, com.google.gson.reflect.TypeToken
                .getParameterized(ApiResponse.class, outputClass).getType());
    }

    public static <T> ApiResponse<List<T>> fromJsonList(String json, Class<T> outputClass) {
        Type type = new TypeToken<ApiResponse<ArrayList<T>>>() {
        }
                .where(new TypeParameter<T>() {
                }, outputClass)
                .getType();
        return GSON.fromJson(json, type);
    }

    public static List<String> getFavoriteRoomIds(String response) throws JSONException {
        List<String> favRoomsIds = Lists.newArrayList();
        JSONObject root = new JSONObject(response);
        JSONArray array = root.getJSONArray("data");
        for (int i = 0; i < array.length(); i++) {
            JSONObject property = array.getJSONObject(i);
            favRoomsIds.add(property.getString("_id"));
        }
        return favRoomsIds;
    }
}
