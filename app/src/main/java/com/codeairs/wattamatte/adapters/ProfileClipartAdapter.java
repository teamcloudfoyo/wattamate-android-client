package com.codeairs.wattamatte.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.codeairs.wattamatte.AppController;
import com.codeairs.wattamatte.R;
import com.codeairs.wattamatte.constants.ApiPaths;
import com.codeairs.wattamatte.models.Item;
import com.squareup.picasso.Picasso;

import java.util.List;

import static com.codeairs.wattamatte.adapters.StyleRecyclerViewAdapter.BGColors;

public class ProfileClipartAdapter extends RecyclerView.Adapter<ProfileClipartAdapter.ViewHolder> {

    private final List<Item> mValues;
    private final Context mContext;

    public ProfileClipartAdapter(List<Item> items, Context context) {
        mValues = items;
        mContext = context;
    }

    @NonNull
    @Override
    public ProfileClipartAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_profile_clipart, parent, false);
        return new ProfileClipartAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ProfileClipartAdapter.ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.text.setText(holder.mItem.name);
        holder.layout.setBackgroundColor(ContextCompat.getColor(mContext,
                BGColors[position % BGColors.length]));
        AppController.getPicasso().load(ApiPaths.IMG_PATH_PREFIX + holder.mItem.imageUrl).into(holder.img);
        holder.mView.setOnClickListener(v -> {
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        final View mView;
        final ImageView img;
        final TextView text;
        final CardView card;
        final LinearLayout layout;
        Item mItem;

        ViewHolder(View view) {
            super(view);
            mView = view;
            card = view.findViewById(R.id.card);
            img = view.findViewById(R.id.img);
            text = view.findViewById(R.id.text);
            layout = view.findViewById(R.id.layout);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + text.getText() + "'";
        }
    }
}
