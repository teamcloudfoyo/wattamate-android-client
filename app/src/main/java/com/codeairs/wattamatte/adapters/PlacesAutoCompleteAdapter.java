package com.codeairs.wattamatte.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;
import com.codeairs.wattamatte.models.Place;
import com.codeairs.wattamatte.utils.internet.PlaceAPI;

import java.util.ArrayList;

public class PlacesAutoCompleteAdapter extends ArrayAdapter<Place> implements Filterable {

    private ArrayList<Place> resultList;

    Context mContext;
    private int mResource;

    private PlaceAPI mPlaceAPI = new PlaceAPI();

    private EmptyResultListener emptyResultListener;

    public PlacesAutoCompleteAdapter(Context context, int resource) {
        super(context, resource);

        mContext = context;
        mResource = resource;
    }

    public PlacesAutoCompleteAdapter(Context context, int resource, EmptyResultListener listener) {
        super(context, resource);

        mContext = context;
        mResource = resource;
        emptyResultListener = listener;
    }

    @Override
    public int getCount() {
        // Last item will be the footer
        return resultList.size();
    }

    @Override
    public Place getItem(int position) {
        return resultList.get(position);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(mResource, parent, false);
        if (rowView instanceof TextView) {
            ((TextView) rowView).setText(getItem(position).description);
        }
        return rowView;
    }

    @NonNull
    @Override
    public Filter getFilter() {

        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();
                if (constraint != null) {
                    resultList = mPlaceAPI.autocomplete(constraint.toString());

                    filterResults.values = resultList;
                    filterResults.count = resultList.size();
                } else {
                    if (emptyResultListener != null)
                        emptyResultListener.onEmpty();
                }

                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if (results != null && results.count > 0) {
                    notifyDataSetChanged();
                } else {
                    notifyDataSetInvalidated();
                }
            }
        };
    }

    public interface EmptyResultListener {
        void onEmpty();
    }

}
