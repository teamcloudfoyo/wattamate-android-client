package com.codeairs.wattamatte.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.codeairs.wattamatte.R;
import com.codeairs.wattamatte.models.SafeVault.InitializeSafe.DocumentType;

import java.util.List;

public class SafeDocumentTypeAdapter extends RecyclerView.Adapter<SafeDocumentTypeAdapter.ViewHolder> {

    private final List<DocumentType> mValues;
    private SafeDocumentTypeSelectedCallback callback;

    public SafeDocumentTypeAdapter(SafeDocumentTypeSelectedCallback callback, List<DocumentType> items) {
        mValues = items;
        this.callback = callback;
    }

    @NonNull
    @Override
    public SafeDocumentTypeAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_proof_item, parent, false);
        return new SafeDocumentTypeAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final SafeDocumentTypeAdapter.ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.proofTitle.setText(holder.mItem.getName());
        holder.mView.setOnClickListener(v -> {
            callback.onSafeDocumentTypeSelected(holder.mItem);
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        final View mView;
        final TextView proofTitle;
        DocumentType mItem;

        ViewHolder(View view) {
            super(view);
            mView = view;
            proofTitle = view.findViewById(R.id.proofTitle);
        }
    }

    public interface SafeDocumentTypeSelectedCallback {
        void onSafeDocumentTypeSelected(DocumentType document);
    }
}




