package com.codeairs.wattamatte.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.codeairs.wattamatte.R;
import com.codeairs.wattamatte.models.SafeVault.SafeUser;

import java.util.List;

public class SafeUserTypeAdapter extends RecyclerView.Adapter<SafeUserTypeAdapter.ViewHolder> {

    private final List<SafeUser> mValues;
    private StudentSubTypeSelectedCallback callback;

    public SafeUserTypeAdapter(StudentSubTypeSelectedCallback callback, List<SafeUser> items) {
        mValues = items;
        this.callback = callback;
    }

    @NonNull
    @Override
    public SafeUserTypeAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_proof_item, parent, false);
        return new SafeUserTypeAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final SafeUserTypeAdapter.ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.proofTitle.setText(holder.mItem.getCdName());
        holder.mView.setOnClickListener(v -> {
            callback.onStudentSubTypeSelected(holder.mItem);
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        final View mView;
        final TextView proofTitle;
        SafeUser mItem;

        ViewHolder(View view) {
            super(view);
            mView = view;
            proofTitle = view.findViewById(R.id.proofTitle);
        }
    }

    public interface StudentSubTypeSelectedCallback {
        void onStudentSubTypeSelected(SafeUser subType);
    }
}



