package com.codeairs.wattamatte.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.codeairs.wattamatte.AppController;
import com.codeairs.wattamatte.R;
import com.codeairs.wattamatte.constants.ApiPaths;
import com.codeairs.wattamatte.constants.Constants;
import com.codeairs.wattamatte.constants.Identifiers;
import com.codeairs.wattamatte.listeners.TenantInteractionListener;
import com.codeairs.wattamatte.models.ApiResponse;
import com.codeairs.wattamatte.models.SuccessMsg;
import com.codeairs.wattamatte.models.Tenant.Tenant;
import com.codeairs.wattamatte.utils.CommonUtils;
import com.codeairs.wattamatte.utils.JsonUtils;
import com.codeairs.wattamatte.utils.SharedPrefManager;
import com.codeairs.wattamatte.utils.internet.NetworkUtils;
import com.codeairs.wattamatte.utils.internet.VolleySingleton;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.Map;

import okhttp3.MediaType;

public class RoomMatesRecyclerViewAdapter extends RecyclerView.Adapter {

    private List<Tenant> mValues;
    private List<String> favUserIds;
    private List<String> contacts;
    private final TenantInteractionListener mListener;
    private final RoomMatesInteractionListener matesListener;
    private final Context mContext;
    private Gson gson;
    private MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    private List<String> requestBeingSentToUsers = Lists.newArrayList();

    public RoomMatesRecyclerViewAdapter(List<Tenant> items, TenantInteractionListener listener,
                                        RoomMatesInteractionListener matesListener,
                                        List<String> favUserIds, List<String> contacts, Context mContext) {
        mValues = items;
        mListener = listener;
        this.matesListener = matesListener;
        this.mContext = mContext;
        this.favUserIds = favUserIds;
        this.contacts = contacts;
        gson = new Gson();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        switch (viewType) {
            case Constants.TENANT_PAGENATION:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_users_pagenation, parent, false);
                return new PagenationViewHolder(view);
            default:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_roommates, parent, false);
                return new TenantViewHolder(view);
        }
    }


    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder viewHolder, int position) {
        Tenant tenant = mValues.get(position);
        switch ((tenant.isPagenationView) ? Constants.TENANT_PAGENATION : Constants.TENANT) {
            case Constants.TENANT_PAGENATION:
                StaggeredGridLayoutManager.LayoutParams layoutParams = (StaggeredGridLayoutManager.LayoutParams) viewHolder.itemView.getLayoutParams();
                layoutParams.setFullSpan(true);
                break;
            default:
                TenantViewHolder holder = (TenantViewHolder) viewHolder;
                holder.mItem = mValues.get(position);
                setImgHeight(holder, position);
                holder.name.setText(CommonUtils.getNameWithAge(holder.mItem));
                holder.job.setText(holder.mItem.job);
                holder.location.setText(CommonUtils.getValueFromAdditionalInfos(holder.mItem.additionInfos, Identifiers.LOCATION));

                holder.favToggle.setChecked(favUserIds.contains(holder.mItem.userId));
                String pic = null;
                //todo please add the loader image for image
                if (mValues.get(position).picture != null && mValues.get(position).picture.getId() != null)
                    AppController.getImageLoader().DisplayImage(mValues.get(position).picture.getId(), holder.img,R.drawable.splash_logo);
                else
                    AppController.getPicasso().load(R.drawable.splash_logo).resize(320, 320).into(holder.img);
                holder.img.setVisibility(View.VISIBLE);

                holder.mView.setOnClickListener(v -> {
                    if (null != mListener) {
                        mListener.goToTenantDetails(holder.mItem, holder.favToggle.isChecked());
                    }
                });
                holder.favToggle.setOnCheckedChangeListener((buttonView, isChecked) -> {
                    if (buttonView.isPressed())
                        toggleFavorite(holder.mItem.userId, isChecked);
                });

                if (contacts.contains(holder.mItem.userId)) {
                    holder.send.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_mail_24dp));
                } else {
                    holder.send.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_send_24dp));
                }

                if (requestBeingSentToUsers.contains(holder.mItem.userId)) {
                    holder.send.setVisibility(View.GONE);
                    holder.progressBar.setVisibility(View.VISIBLE);
                } else {
                    holder.send.setVisibility(View.VISIBLE);
                    holder.progressBar.setVisibility(View.GONE);
                }
                holder.send.setOnClickListener(v -> {
                    if (contacts.contains(holder.mItem.userId)) {
                        mListener.openChat(holder.mItem);
                    } else {
                        sendContactRequest(holder);
                    }
                });

                holder.close.setOnClickListener(v -> blockUser(holder.mItem.userId, position));

                break;
        }
    }

    private void sendContactRequest(TenantViewHolder holder) {
        holder.send.setVisibility(View.GONE);
        holder.progressBar.setVisibility(View.VISIBLE);
        requestBeingSentToUsers.add(holder.mItem.userId);

        JSONObject post = NetworkUtils.commonPostBody(mContext);
        try {
            post.put("userIdSend", holder.mItem.userId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST,
                ApiPaths.SEND_CONTACT_REQUEST, post,
                response -> {
                    Log.d("sendContactReq.Response", response.toString());
                    holder.send.setVisibility(View.VISIBLE);
                    holder.progressBar.setVisibility(View.GONE);
                    requestBeingSentToUsers.remove(holder.mItem.userId);

                    ApiResponse<SuccessMsg> res = JsonUtils.fromJson(response.toString(), SuccessMsg.class);
                    if (!res.success && !res.succes) {
                        mListener.showSnackBar(res.error.message);
                    } else {
                        mListener.showSnackBar(res.data.msg);
                    }
                },
                error -> {

                }
        ) {
            @Override
            public Map<String, String> getHeaders() {
                return NetworkUtils.authenticatedHeader(mContext);
            }
        };
        request.setShouldCache(false);
        VolleySingleton.getInstance(mContext).addToRequestQueue(request);
    }

    @Override
    public int getItemViewType(int position) {
        if (mValues.get(position).isPagenationView)
            return Constants.TENANT_PAGENATION;
        else
            return Constants.TENANT;
    }

    @Override
    public int getItemCount() {
        int size = mValues.size();
        if (size == 0)
            matesListener.showEmptyView();
        else
            matesListener.hideEmptyView();
        return mValues.size();
    }

    public static class TenantViewHolder extends RecyclerView.ViewHolder {
        final View mView;
        final ConstraintLayout layout;
        final ImageView img;
        final View separator;
        final TextView name;
        final TextView job;
        final TextView location;
        final ImageButton send;
        final ToggleButton favToggle;
        final ProgressBar progressBar;
        final ImageButton close;

        Tenant mItem;

        TenantViewHolder(View view) {
            super(view);
            mView = view;
            layout = view.findViewById(R.id.layout);
            img = view.findViewById(R.id.img);
            separator = view.findViewById(R.id.vrSep);
            name = view.findViewById(R.id.name);
            job = view.findViewById(R.id.job);
            location = view.findViewById(R.id.locationTV);
            send = view.findViewById(R.id.send);
            favToggle = view.findViewById(R.id.favoritesToggleButton);
            progressBar = view.findViewById(R.id.progressBar);
            close = view.findViewById(R.id.close);
        }

        @Override
        public String toString() {
            return super.toString();
        }
    }

    public static class PagenationViewHolder extends RecyclerView.ViewHolder {

        public PagenationViewHolder(View itemView) {
            super(itemView);

        }
    }

    private void setImgHeight(final TenantViewHolder holder, int position) {
        ConstraintLayout.LayoutParams params = new ConstraintLayout.LayoutParams(
                0, 0
        );
        int boo = position % 4;
        params.dimensionRatio = (boo == 2 || boo == 0) ? "H,4.5:6" : "H,1:1";

        holder.img.setLayoutParams(params);

        ConstraintSet set = new ConstraintSet();
        set.clone(holder.layout);

        set.connect(holder.img.getId(), ConstraintSet.TOP, ConstraintSet.PARENT_ID, ConstraintSet.TOP, 0);
        set.connect(holder.img.getId(), ConstraintSet.START, ConstraintSet.PARENT_ID, ConstraintSet.START, 0);
        set.connect(holder.img.getId(), ConstraintSet.END, ConstraintSet.PARENT_ID, ConstraintSet.END, 0);

        if (boo == 2 || boo == 0) {
            set.connect(holder.separator.getId(), ConstraintSet.TOP, holder.img.getId(), ConstraintSet.BOTTOM, 16);
            set.connect(holder.separator.getId(), ConstraintSet.BOTTOM, ConstraintSet.PARENT_ID, ConstraintSet.BOTTOM, 16);
        } else {
            set.connect(holder.separator.getId(), ConstraintSet.TOP, holder.img.getId(), ConstraintSet.BOTTOM, 8);
            set.connect(holder.separator.getId(), ConstraintSet.BOTTOM, ConstraintSet.PARENT_ID, ConstraintSet.BOTTOM, 8);
        }

        set.applyTo(holder.layout);
    }

    private void toggleFavorite(String userId, boolean fav) {
        if (fav)
            favUserIds.add(userId);
        else
            favUserIds.remove(userId);

        JSONObject jsonObject = new JSONObject((ImmutableMap.<String, String>builder()
                .put("userId", SharedPrefManager.getUserId(mContext))
                .put("userIdFav", userId)
                .build()));
        JsonObjectRequest request = new JsonObjectRequest(fav ? Request.Method.POST : Request.Method.DELETE,
                ApiPaths.USER_FAVORITE_UPDATE, jsonObject,
                this::handleResponse, this::handleError) {

            @Override
            public Map<String, String> getHeaders() {
                return NetworkUtils.authenticatedHeader(mContext);
            }

        };
        request.setShouldCache(false);
        VolleySingleton.getInstance(mContext).addToRequestQueue(request);
    }

    private void handleResponse(JSONObject response) {
        Log.d("ToggleFav.Response", response.toString());
    }

    private void handleError(VolleyError error) {

    }

    private void blockUser(String userId, int position) {
        JSONObject jsonObject = new JSONObject((ImmutableMap.<String, String>builder()
                .put("userId", SharedPrefManager.getUserId(mContext))
                .put("userIdBlock", userId)
                .build()));
        JsonObjectRequest request = new JsonObjectRequest(
                Request.Method.POST, ApiPaths.BLOCK_USER, jsonObject,
                this::handleResponse, this::handleError) {

            @Override
            public Map<String, String> getHeaders() {
                return NetworkUtils.authenticatedHeader(mContext);
            }

        };
        request.setShouldCache(false);
        VolleySingleton.getInstance(mContext).addToRequestQueue(request);

        mValues.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, mValues.size());
    }

    public void showPagenationIndicator() {
        Tenant pagenationItem;
        pagenationItem = new Tenant();
        pagenationItem.isPagenationView = true;
        mValues.add(pagenationItem);
        notifyDataSetChanged();
    }

    public void removePagenationIndicator(boolean notifyDataSetChanged) {
        for (int i = 0; i < mValues.size(); i++) {
            if (mValues.get(i).isPagenationView) {
                mValues.remove(i);
                if (notifyDataSetChanged) notifyDataSetChanged();
            }
        }
    }

    public interface RoomMatesInteractionListener {

        void hideEmptyView();

        void showEmptyView();

    }


}
