package com.codeairs.wattamatte.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.codeairs.wattamatte.AppController;
import com.codeairs.wattamatte.R;
import com.codeairs.wattamatte.models.Tenant.Tenant;
import com.codeairs.wattamatte.models.contacts.Message;
import com.codeairs.wattamatte.utils.DateUtils;
import com.codeairs.wattamatte.utils.SharedPrefManager;

import java.util.List;

public class MessageListAdapter extends RecyclerView.Adapter {

    private static final int VIEW_TYPE_MESSAGE_SENT = 1;
    private static final int VIEW_TYPE_MESSAGE_RECEIVED = 2;

    private Context mContext;
    private List<Message> mMessageList;

    private final String currentUserId;
    private final Tenant tenant;
    private String pic;

    public MessageListAdapter(Context context, List<Message> messageList, Tenant tenant) {
        mContext = context;
        mMessageList = messageList;
        currentUserId = SharedPrefManager.getUserId(context);
        this.tenant = tenant;
        pic = SharedPrefManager.getProfilePicThumb(mContext);
    }

    @Override
    public int getItemCount() {
        return mMessageList.size();
    }

    // Determines the appropriate ViewType according to the sender of the message.
    @Override
    public int getItemViewType(int position) {
        Message message = mMessageList.get(position);

        if (message.fromUserId.equals(currentUserId)) {
            // If the current user is the sender of the message
            return VIEW_TYPE_MESSAGE_SENT;
        } else {
            // If some other user sent the message
            return VIEW_TYPE_MESSAGE_RECEIVED;
        }
    }

    // Inflates the appropriate layout according to the ViewType.
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;

        if (viewType == VIEW_TYPE_MESSAGE_SENT) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_message_sent, parent, false);
            return new SentMessageHolder(view);
        } else if (viewType == VIEW_TYPE_MESSAGE_RECEIVED) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_message_received, parent, false);
            return new ReceivedMessageHolder(view);
        }

        return null;
    }

    // Passes the message object to a ViewHolder so that the contents can be bound to UI.
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        Message message = mMessageList.get(position);

        switch (holder.getItemViewType()) {
            case VIEW_TYPE_MESSAGE_SENT:
                ((SentMessageHolder) holder).bind(message);
                break;
            case VIEW_TYPE_MESSAGE_RECEIVED:
                ((ReceivedMessageHolder) holder).bind(message);
        }
    }

    private class SentMessageHolder extends RecyclerView.ViewHolder {

        TextView messageText, timeText;
        ImageView profileImage;

        SentMessageHolder(View itemView) {
            super(itemView);

            messageText = itemView.findViewById(R.id.text_message_body);
            timeText = itemView.findViewById(R.id.text_message_time);
            profileImage = itemView.findViewById(R.id.image_message_profile);
        }

        void bind(Message message) {
            messageText.setText(message.msg);

            // Format the stored timestamp into a readable String using method.
            timeText.setText(DateUtils.formatDateTimeForChatMsg(message.date));

            AppController.getPicasso().load(pic).placeholder(R.drawable.holder).into(profileImage);
            Log.d("ImageLoader","User Sent image "+pic);
            AppController.getImageLoader().DisplayThumbImage(pic, profileImage, R.drawable.holder);
        }
    }

    private class ReceivedMessageHolder extends RecyclerView.ViewHolder {

        TextView messageText, timeText, nameText;
        ImageView profileImage;

        ReceivedMessageHolder(View itemView) {
            super(itemView);

            messageText = itemView.findViewById(R.id.text_message_body);
            timeText = itemView.findViewById(R.id.text_message_time);
            nameText = itemView.findViewById(R.id.text_message_name);
            profileImage = itemView.findViewById(R.id.image_message_profile);
        }

        void bind(Message message) {
            messageText.setText(message.msg);

            // Format the stored timestamp into a readable String using method.
            timeText.setText(DateUtils.formatDateTimeForChatMsg(message.date));

            nameText.setText(tenant.firstName);

            // Insert the profile image from the URL into the ImageView.
            //todo Added Image Here, Please replace it
            //CommonUtils.loadPicture(tenant.pictureThumb.getName(), profileImage, R.drawable.holder);
            if (tenant.pictureThumb != null && tenant.pictureThumb.getId() != null)
            {
                Log.d("ImageLoader","Tenant Image loading");
                AppController.getImageLoader().DisplayThumbImage(tenant.pictureThumb.getId(), profileImage, R.drawable.holder);
            }
            else
            {
                AppController.getPicasso().load(R.drawable.holder).into(profileImage);
                Log.d("ImageLoader","Tenant Image Cant be loaded");
            }
        }
    }
}