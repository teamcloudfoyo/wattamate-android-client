package com.codeairs.wattamatte.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.codeairs.wattamatte.R;
import com.codeairs.wattamatte.models.SafeVault.Proof;

import java.util.List;

public class ProofSelectionAdapter extends RecyclerView.Adapter<ProofSelectionAdapter.ViewHolder> {

    private final List<Proof> mValues;
    private ProofSelectionCallback callback;

    public ProofSelectionAdapter(ProofSelectionCallback callback, List<Proof> items) {
        mValues = items;
        this.callback = callback;
    }

    @NonNull
    @Override
    public ProofSelectionAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_proof_item, parent, false);
        return new ProofSelectionAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ProofSelectionAdapter.ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.proofTitle.setText(holder.mItem.getProofName());
        holder.mView.setOnClickListener(v -> {
            callback.onProofSelected(holder.mItem.getProofId());
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        final View mView;
        final TextView proofTitle;
        Proof mItem;

        ViewHolder(View view) {
            super(view);
            mView = view;
            proofTitle = view.findViewById(R.id.proofTitle);
        }
    }

    public interface ProofSelectionCallback{
        void onProofSelected(String proofName);
    }
}


