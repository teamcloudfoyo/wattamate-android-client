package com.codeairs.wattamatte.adapters;

import android.content.Context;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.codeairs.wattamatte.AppController;
import com.codeairs.wattamatte.R;
import com.codeairs.wattamatte.listeners.TenantInteractionListener;
import com.codeairs.wattamatte.models.contacts.Contact;
import com.codeairs.wattamatte.utils.DateUtils;
import com.codeairs.wattamatte.utils.internet.NetworkUtils;

import java.util.List;

public class ConversationRecyclerViewAdapter extends RecyclerView.Adapter<ConversationRecyclerViewAdapter.ViewHolder> {

    private final List<Contact> mValues;
    private final TenantInteractionListener mListener;
    private final Context mContext;

    public ConversationRecyclerViewAdapter(List<Contact> items, TenantInteractionListener listener, Context context) {
        mValues = items;
        mListener = listener;
        mContext = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_conversation, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);

        if(holder.mItem.user!=null && holder.mItem.user.picture!=null&& holder.mItem.user.picture.getId()!=null)
            AppController.getImageLoader().DisplayImage(holder.mItem.user.picture.getId(), holder.img, R.drawable.splash_logo);
        else
            AppController.getPicasso().load(R.drawable.splash_logo).into(holder.img);

        holder.name.setText(holder.mItem.user.firstName);
        if (TextUtils.isEmpty(holder.mItem.msg))
            holder.msg.setText(holder.mItem.note);
        else
            holder.msg.setText(holder.mItem.msg);

        holder.time.setText(DateUtils.getTimeDifference(holder.mItem.dateChat, mContext));

        holder.mView.setOnClickListener(v -> {
            if (null != mListener) {
                mListener.openChat(holder.mItem.user);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        final View mView;
        final ImageView img;
        final TextView name;
        final TextView msg;
        final TextView time;
        Contact mItem;

        ViewHolder(View view) {
            super(view);
            mView = view;
            img = view.findViewById(R.id.img);
            name = view.findViewById(R.id.name);
            msg = view.findViewById(R.id.content);
            time = view.findViewById(R.id.time);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + name.getText() + "'";
        }
    }

    public void selectUser(String userId){
        for (int i = 0; i < mValues.size(); i++) {
            int currentPosition = i;
            if(mValues.get(currentPosition).user.userId.equalsIgnoreCase(userId)){
                if (null != mListener) {
                    mListener.openChat(mValues.get(currentPosition).user);
                }
                break;
            }
        }
    }
}
