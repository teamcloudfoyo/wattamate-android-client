package com.codeairs.wattamatte.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.JsonObjectRequest;
import com.codeairs.wattamatte.AppController;
import com.codeairs.wattamatte.R;
import com.codeairs.wattamatte.constants.ApiPaths;
import com.codeairs.wattamatte.listeners.TenantInteractionListener;
import com.codeairs.wattamatte.models.Tenant.Tenant;
import com.codeairs.wattamatte.utils.SharedPrefManager;
import com.codeairs.wattamatte.utils.internet.NetworkUtils;
import com.codeairs.wattamatte.utils.internet.VolleySingleton;
import com.google.common.collect.ImmutableMap;

import org.json.JSONObject;

import java.util.List;

import mehdi.sakout.fancybuttons.FancyButton;

public class ContactRequestRecyclerViewAdapter extends RecyclerView.Adapter<ContactRequestRecyclerViewAdapter.ViewHolder> {

    private final List<Tenant> mValues;
    private final TenantInteractionListener mListener;
    private final Context mContext;

    public ContactRequestRecyclerViewAdapter(List<Tenant> items, TenantInteractionListener listener, Context mContext) {
        mValues = items;
        mListener = listener;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_contactrequest, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);

        //todo Handle This exception from API Side.
        if (holder.mItem.picture != null && holder.mItem.picture.getId() != null)
            AppController.getImageLoader().DisplayImage(holder.mItem.picture.getId(), holder.img, R.drawable.holder);
        else
            AppController.getPicasso().load(R.drawable.holder).into(holder.img);
        holder.name.setText(holder.mItem.firstName);
        holder.profession.setText(holder.mItem.job);

        holder.profile.setOnClickListener(v -> mListener.goToTenantDetails(holder.mItem, null));
        holder.accept.setOnClickListener(v -> acceptRequest(holder.mItem));
        holder.rejectLayout.setVisibility(View.GONE);

        holder.reject.setOnClickListener(v -> holder.rejectLayout.setVisibility(View.VISIBLE));
        holder.rejectNo.setOnClickListener(v -> holder.rejectLayout.setVisibility(View.GONE));
        holder.rejectYes.setOnClickListener(v -> rejectRequest(holder.mItem));
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        final View mView;
        Tenant mItem;
        final ImageView img;
        final TextView name;
        final TextView profession;
        final FancyButton accept;
        final FancyButton reject;
        final FancyButton profile;
        final FancyButton rejectYes;
        final FancyButton rejectNo;
        final View rejectLayout;

        ViewHolder(View view) {
            super(view);
            mView = view;
            img = view.findViewById(R.id.img);
            name = view.findViewById(R.id.name);
            profession = view.findViewById(R.id.profession);
            accept = view.findViewById(R.id.acceptButton);
            reject = view.findViewById(R.id.rejectButton);
            profile = view.findViewById(R.id.profileButton);
            rejectLayout = view.findViewById(R.id.rejectLayout);
            rejectYes = view.findViewById(R.id.rejectYes);
            rejectNo = view.findViewById(R.id.rejectNo);
        }

        @Override
        public String toString() {
            return super.toString();
        }
    }

    private void acceptRequest(Tenant tenant) {
        JsonObjectRequest request = NetworkUtils.authenticatedPostRequest(
                mContext, ApiPaths.CONTACT_REQUEST_ACCEPT, getRequestBody(tenant.userId),
                response -> {
                    Log.d("ContactAccept.Response", response.toString());
                    removeItem(tenant);
                }, error -> {

                });
        request.setShouldCache(false);
        VolleySingleton.getInstance(mContext).addToRequestQueue(request);
    }

    private void rejectRequest(Tenant tenant) {
        JsonObjectRequest request = NetworkUtils.authenticatedPostRequest(
                mContext, ApiPaths.CONTACT_REQUEST_REJECT, getRequestBody(tenant.userId),
                response -> {
                    Log.d("ContactReject.Response", response.toString());
                    removeItem(tenant);
                }, error -> {

                });
        request.setShouldCache(false);
        VolleySingleton.getInstance(mContext).addToRequestQueue(request);
    }

    private JSONObject getRequestBody(String userId) {
        return new JSONObject(ImmutableMap.<String, String>builder()
                .put("userOneId", SharedPrefManager.getUserId(mContext))
                .put("userTwoId", userId)
                .build());
    }

    /**
     * removes the row after request is invited/accepted
     **/
    private void removeItem(Tenant tenant) {
        int index = mValues.indexOf(tenant);
        mValues.remove(index);
        notifyDataSetChanged();
    }
}
