package com.codeairs.wattamatte.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.codeairs.wattamatte.R;

import java.util.List;

public class RoomDetailAdapter extends RecyclerView.Adapter<RoomDetailAdapter.ViewHolder> {

    private final List<String> mValues;

    public RoomDetailAdapter(List<String> items) {
        mValues = items;
    }

    @NonNull
    @Override
    public RoomDetailAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_room_detail_list_item, parent, false);
        return new RoomDetailAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final RoomDetailAdapter.ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.text.setText(holder.mItem);
        holder.mView.setOnClickListener(v -> {
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        final View mView;
        final TextView text;
        String mItem;

        ViewHolder(View view) {
            super(view);
            mView = view;
            text = view.findViewById(R.id.tv);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + text.getText() + "'";
        }
    }
}

