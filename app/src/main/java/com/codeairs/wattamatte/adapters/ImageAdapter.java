package com.codeairs.wattamatte.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.codeairs.wattamatte.AppController;
import com.codeairs.wattamatte.R;
import com.codeairs.wattamatte.constants.ApiPaths;

import java.util.List;

public class ImageAdapter extends PagerAdapter {

    private static final String TAG = ImageAdapter.class.getSimpleName();
    private Context mContext;
    private List<String> mItems;
    private ImageAdapterItemClickListener mListener;

    public ImageAdapter(Context context, List<String> items) {
        mContext = context;
        mItems = items;
    }

    public ImageAdapter(Context context, List<String> items, ImageAdapterItemClickListener listener) {
        mContext = context;
        mItems = items;
        mListener = listener;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup collection, int position) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        ImageView img = (ImageView) inflater.inflate(R.layout.layout_image, collection, false);

        String url = mItems.get(position);
        if (!TextUtils.isEmpty(url)) {
            Log.d(TAG, "Property Images : " + ApiPaths.IMG_PATH_PREFIX + url);
            AppController.getPicasso().load(ApiPaths.IMG_PATH_PREFIX + url).resize(480, 240).into(img);
        }

        if (mListener != null)
            img.setOnClickListener(v -> mListener.onItemClick(position));

        collection.addView(img);
        return img;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup collection, int position, @NonNull Object view) {
        collection.removeView((View) view);
    }

    @Override
    public int getCount() {
        return mItems.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    public interface ImageAdapterItemClickListener {

        void onItemClick(int pos);

    }

}