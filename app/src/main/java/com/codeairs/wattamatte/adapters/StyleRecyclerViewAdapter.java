package com.codeairs.wattamatte.adapters;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.codeairs.wattamatte.AppController;
import com.codeairs.wattamatte.R;
import com.codeairs.wattamatte.constants.ApiPaths;
import com.codeairs.wattamatte.listeners.ClipArtStateChangeListener;
import com.codeairs.wattamatte.models.completeprofiledetails.ProfileItem;
import com.codeairs.wattamatte.utils.AndroidUtils;
import com.squareup.picasso.Picasso;

import java.util.List;

public class StyleRecyclerViewAdapter<T extends ProfileItem> extends RecyclerView.Adapter<StyleRecyclerViewAdapter<T>.ViewHolder> {

    public static final int[] BGColors = {
            R.color.clipartBg1,
            R.color.clipartBg2,
            R.color.clipartBg3,
            R.color.clipartBg4,
            R.color.clipartBg5,
            R.color.clipartBg6,
    };

    private final Context mContext;
    private ClipArtStateChangeListener mListener;
    private int mItemHeight;

    private List<T> mValues;

    public StyleRecyclerViewAdapter(List<T> items, Context context, ClipArtStateChangeListener listener,
                                    int itemHeight) {
        mValues = items;
        mContext = context;
        mListener = listener;
        mItemHeight = itemHeight;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_complete_profile_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);

        Log.d("Hey", "hello");
        Log.d("id", holder.mItem.getId());
        Log.d("name", holder.mItem.getName());

        AndroidUtils.setViewHeight(holder.layout, mItemHeight - 60);

        AppController.getPicasso().load(ApiPaths.IMG_PATH_PREFIX + holder.mItem.getImage().get(0)).into(holder.img);
        holder.text.setText(mValues.get(position).getName());
        changeSelection(holder);

        holder.mView.setOnClickListener(v -> {
            holder.mItem.isChecked = !holder.mItem.isChecked;
            changeSelection(holder);
            mListener.handleStateChange();
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    private void changeSelection(final ViewHolder holder) {
        if (holder.mItem.isChecked) {
            holder.img.setAlpha(255);
            holder.layout.setBackgroundColor(ContextCompat.getColor(mContext, R.color.mediumTurquoise));
            holder.text.setTextColor(ContextCompat.getColor(mContext, R.color.white));
            holder.text.setAlpha(1);
        } else {
            holder.img.setAlpha(80);
            holder.layout.setBackgroundColor(ContextCompat.getColor(mContext, R.color.white));
            holder.text.setTextColor(ContextCompat.getColor(mContext, android.R.color.black));
            holder.text.setAlpha(0.3f);
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        final View mView;
        final ImageView img;
        final TextView text;
        final ConstraintLayout layout;
        final CardView card;
        T mItem;

        ViewHolder(View view) {
            super(view);
            mView = view;
            img = view.findViewById(R.id.img);
            text = view.findViewById(R.id.text);
            layout = view.findViewById(R.id.layout);
            card = view.findViewById(R.id.card);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + text.getText() + "'";
        }
    }
}
