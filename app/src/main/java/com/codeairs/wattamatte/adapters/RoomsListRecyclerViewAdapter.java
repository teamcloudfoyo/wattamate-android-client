package com.codeairs.wattamatte.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.TextView;
import android.widget.ToggleButton;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.codeairs.wattamatte.R;
import com.codeairs.wattamatte.fragments.RoomsListFragment.OnListFragmentInteractionListener;
import com.codeairs.wattamatte.models.rooms.Property;
import com.codeairs.wattamatte.utils.internet.NetworkUtils;
import com.codeairs.wattamatte.utils.internet.VolleySingleton;
import com.rd.PageIndicatorView;
import org.json.JSONObject;

import java.text.MessageFormat;
import java.util.List;

public class RoomsListRecyclerViewAdapter extends RecyclerView.Adapter<RoomsListRecyclerViewAdapter.ViewHolder> {

    private List<Property> mValues;
    private List<String> favRoomsIds;
    private final OnListFragmentInteractionListener mListener;
    private final RoomListInteractionListener roomListListener;
    private final Context mContext;

    public RoomsListRecyclerViewAdapter(List<Property> items, List<String> favRoomsIds,
                                        OnListFragmentInteractionListener listener,
                                        RoomListInteractionListener roomListListener,
                                        Context mContext) {
        mValues = items;
        this.favRoomsIds = favRoomsIds;
        mListener = listener;
        this.roomListListener = roomListListener;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_rooms_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);

        holder.imgPager.setAdapter(new ImageAdapter(mContext, holder.mItem.pictures,
                pos -> mListener.onListFragmentInteraction(holder.mItem, favRoomsIds.contains(holder.mItem.id))));
        holder.pageIndicatorView.setCount(holder.mItem.pictures.size());
        holder.pageIndicatorView.setSelection(0);
        holder.imgPager.addOnPageChangeListener(getPageChangeListener(holder));

        holder.name.setText(holder.mItem.name);
        holder.location.setText(holder.mItem.address.city);
        holder.price.setText(MessageFormat.format("{0}€ C.C", holder.mItem.price));

        holder.tv1.setText(MessageFormat.format("{0}p", holder.mItem.pieces));
        holder.tv2.setText(MessageFormat.format("{0}ch", holder.mItem.roomCount));
        holder.tv3.setText(MessageFormat.format("{0}m²", holder.mItem.surface));

        holder.favToggle.setChecked(favRoomsIds.contains(holder.mItem.id));
        holder.favToggle.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (buttonView.isPressed())
                toggleFavorite(holder.mItem.id, isChecked);
        });

        holder.mView.setOnClickListener(v -> {
            if (null != mListener) {
                mListener.onListFragmentInteraction(holder.mItem, favRoomsIds.contains(holder.mItem.id));
            }
        });
    }

    @Override
    public int getItemCount() {
        int size = (mValues == null) ? 0 : mValues.size();
        if (size == 0)
            roomListListener.showEmptyView();
        else
            roomListListener.hideEmptyView();
        return size;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        final View mView;
        Property mItem;
        ViewPager imgPager;
        TextView name, location, price, tv1, tv2, tv3;
        ToggleButton favToggle;
        PageIndicatorView pageIndicatorView;

        ViewHolder(View view) {
            super(view);
            mView = view;
            imgPager = view.findViewById(R.id.viewpager);
            name = view.findViewById(R.id.name);
            location = view.findViewById(R.id.location);
            price = view.findViewById(R.id.price);
            tv1 = view.findViewById(R.id.tv1);
            tv2 = view.findViewById(R.id.tv2);
            tv3 = view.findViewById(R.id.tv3);
            favToggle = view.findViewById(R.id.favoritesToggleButton);
            pageIndicatorView = view.findViewById(R.id.pageIndicatorView);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + "'";
        }
    }

    private void toggleFavorite(String propertyId, boolean fav) {
        if (fav)
            favRoomsIds.add(propertyId);
        else
            favRoomsIds.remove(propertyId);

        JsonObjectRequest request = NetworkUtils.toggleRoomFavoritesRequest(
                mContext, propertyId, fav, this::handleResponse, this::handleError);

        VolleySingleton.getInstance(mContext).addToRequestQueue(request);
    }

    private void handleResponse(JSONObject response) {
        Log.d("RoomFav.Response", response.toString());
    }

    private void handleError(VolleyError error) {

    }

    private ViewPager.OnPageChangeListener getPageChangeListener(ViewHolder holder) {
        return new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                holder.pageIndicatorView.setSelected(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        };
    }

    public interface RoomListInteractionListener {

        void hideEmptyView();

        void showEmptyView();

    }

}
