package com.codeairs.wattamatte.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.codeairs.wattamatte.R;
import com.codeairs.wattamatte.models.Item;

import java.util.List;

public class BadgesRecyclerViewAdapter extends RecyclerView.Adapter<BadgesRecyclerViewAdapter.ViewHolder> {

    private final List<Item> mValues;
    private final Context mContext;

    public BadgesRecyclerViewAdapter(List<Item> items, Context context) {
        mValues = items;
        mContext = context;
    }

    @NonNull
    @Override
    public BadgesRecyclerViewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_badges, parent, false);
        return new BadgesRecyclerViewAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final BadgesRecyclerViewAdapter.ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.text.setText(holder.mItem.name);
        holder.img.setImageResource(holder.mItem.imgRes);
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        final View mView;
        final ImageView img;
        final TextView text;
        Item mItem;

        ViewHolder(View view) {
            super(view);
            mView = view;
            img = view.findViewById(R.id.img);
            text = view.findViewById(R.id.text);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + text.getText() + "'";
        }
    }
}
