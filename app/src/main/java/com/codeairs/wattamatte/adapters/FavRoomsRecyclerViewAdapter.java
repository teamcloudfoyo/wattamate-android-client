package com.codeairs.wattamatte.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.codeairs.wattamatte.AppController;
import com.codeairs.wattamatte.R;
import com.codeairs.wattamatte.activity.RoomDetailsActivity;
import com.codeairs.wattamatte.constants.ApiPaths;
import com.codeairs.wattamatte.models.rooms.Property;
import com.squareup.picasso.Picasso;

import java.util.List;

import static com.codeairs.wattamatte.activity.MainActivity.REQUEST_CODE_ROOM_FAV_CHANGE_MY_PROFILE;
import static com.codeairs.wattamatte.activity.RoomDetailsActivity.PARAM_FAV;
import static com.codeairs.wattamatte.activity.RoomDetailsActivity.PARAM_ID;

public class FavRoomsRecyclerViewAdapter extends RecyclerView.Adapter<FavRoomsRecyclerViewAdapter.ViewHolder> {

    private List<Property> mValues;
    private final Context mContext;

    public FavRoomsRecyclerViewAdapter(List<Property> items, Context context) {
        mValues = items;
        mContext = context;
    }

    @NonNull
    @Override
    public FavRoomsRecyclerViewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_profile_fav_room, parent, false);
        return new FavRoomsRecyclerViewAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final FavRoomsRecyclerViewAdapter.ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.name.setText(holder.mItem.name);

        String pic = null;
        if (holder.mItem.pictures != null && holder.mItem.pictures.size() != 0)
            pic = ApiPaths.IMG_PATH_PREFIX + holder.mItem.pictures.get(0);
        AppController.getPicasso().load(pic).placeholder(R.drawable.splash_logo).into(holder.img);

        holder.rate.setText(holder.mItem.price + "€ C.C");
        holder.favToggle.setEnabled(false);
        holder.mView.setOnClickListener(v -> {
            Intent intent = new Intent(mContext, RoomDetailsActivity.class);
            intent.putExtra(PARAM_ID, holder.mItem.id);
            intent.putExtra(PARAM_FAV, true);
            if (mContext instanceof Activity) {
                ((Activity) mContext).startActivityForResult(intent, REQUEST_CODE_ROOM_FAV_CHANGE_MY_PROFILE);
            }
        });
    }

    @Override
    public int getItemCount() {
        if (mValues == null)
            return 0;
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        final View mView;
        final ImageView img;
        final TextView name;
        final TextView rate;
        final ToggleButton favToggle;
        Property mItem;

        ViewHolder(View view) {
            super(view);
            mView = view;
            img = view.findViewById(R.id.img);
            name = view.findViewById(R.id.name);
            rate = view.findViewById(R.id.rate);
            favToggle = view.findViewById(R.id.favoritesToggleButton);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + name.getText() + "'";
        }
    }

}
