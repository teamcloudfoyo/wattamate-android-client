package com.codeairs.wattamatte.constants;

public enum RequestCode {

    PersonalInfoProfilePic(9001),
    PersonalInfoCoverPic(9002);

    private final int id;

    RequestCode(int id) {
        this.id = id;
    }

    public int id() {
        return id;
    }

}
