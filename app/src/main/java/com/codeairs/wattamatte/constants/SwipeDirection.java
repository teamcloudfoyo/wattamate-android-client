package com.codeairs.wattamatte.constants;

public enum SwipeDirection {
    All, Left, Right, None
}
