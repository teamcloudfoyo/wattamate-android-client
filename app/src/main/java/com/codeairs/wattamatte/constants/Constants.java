package com.codeairs.wattamatte.constants;

public class Constants {

    public static final int USER_STATUS_NEW = 0;
    public static final int USER_STATUS_NEEDS_VERIFICATION = 1;
    public static final int USER_STATUS_NEEDS_COMPLETE_PROFILE_DETAILS = 2;
    public static final int USER_STATUS_REGISTER_COMPLETE = 3;

    public static final int LOGIN_TYPE_NORMAL = 0;
    public static final int LOGIN_TYPE_FACEBOOK = 1;
    public static final int LOGIN_TYPE_GOOGLE = 2;

    public static final int BUDGET_MIN = 50;
    public static final int BUDGET_MAX = 2000;

    public static final int TENANT_PAGENATION = 0;
    public static final int TENANT = 1;

    public static final String NOTIFICATION_BUNDLE = "NotificationBundle";
    public static final String BUNDLE_NOTIFICATION = "NotificationType";
    public static final String BUNDLE_NOTIFICATION_USERID = "NotificationUserId";

    public static final String NOTIFICATION_FAVOURITE = "Add to favourite";
    public static final String NOTIFICATION_FAVOURITE_FRENCH = "Nouveau favori";

    public static final String NOTIFICATION_CONTACT_REQUEST = "New request";
    public static final String NOTIFICATION_CONTACT_REQUEST_FRENCH = "Nouvelle demande";

    public static final String NOTIFICATION_MESSAGING = "New message";
    public static final String NOTIFICATION_MESSAGING_FRENCH = "Nouveau message";

    public static final String BUNDLE_TYPE = "Type";
    public static final String BUNDLE_TYPE_CHAT = "TypeChat";
    public static final String BUNDLE_TYPE_CONTACT_REQUEST = "TypeContact";
    public static final String BUNDLE_USER_ID = "UserId";

    public static final String NOTIFICATION_TAG = "RULFOX";
    public static final String SAFE_LOG_TAG = "SecureVault";

    public static final String IMAGE_LARGE = "-L";
    public static final String IMAGE_SMALL = "-S";
}
