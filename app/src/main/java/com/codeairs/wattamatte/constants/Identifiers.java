package com.codeairs.wattamatte.constants;

public class Identifiers {

    public static final String SMOKER = "5addf8ff5570954211c84048";
    public static final String PETS = "5addf8ff5570954211c84049";
    public static final String CHILDREN = "5addf8ff5570954211c8404a";
    public static final String FURNITURE = "5addf8ff5570954211c8404b";
    public static final String NO_OF_MATES = "5addf8ff5570954211c8404c";
    public static final String LOCATION = "5adf5bb8c7091c39300223cc";
    public static final String BUDGET_MIN = "5adf5bb8c7091c39300223cd";
    public static final String BUDGET_MAX = "5adf5bb8c7091c39300223ce";
    public static final String ENTRY_DATE = "5adf5bb8c7091c39300223cf";

}
