package com.codeairs.wattamatte.constants;

public class ApiPaths {

    //public static final String PATH_PREFIX = "https://watta-api.sandbox.wattamate.com";
    public static final String PATH_PREFIX = "https://api.wattamate.com";
    public static final String PATH_IMAGE_PREFIX = PATH_PREFIX;
    public static final String PATH_USER_AVATAR = PATH_PREFIX+"/user/avatar";

    public static final String IMG_PATH_PREFIX = ApiPaths.PATH_IMAGE_PREFIX + "/assets/";
    public static final String IMG_PATH_TO_REPLACE = "/home/api/data/uploads/";

    public static final String SIGN_UP = PATH_PREFIX + "/user/signup";
    public static final String VERIFY_PHONE = PATH_PREFIX + "/user/signup/sms_code_generate";
    public static final String VERIFY_OTP = PATH_PREFIX + "/user/signup/sms_code_confirm";
    public static final String COMPLETE_PROFILE_DETAILS = PATH_PREFIX + "/user/signup/profile";

    public static final String HOBBY = PATH_PREFIX + "/hobbies";
    public static final String FEATURES = PATH_PREFIX + "/features";
    public static final String USER_PROFILE = PATH_PREFIX + "/users";
    public static final String UPDATE_HOBBY_FEATURE = PATH_PREFIX + "/users/hobbies/features";

    public static final String LOGIN = PATH_PREFIX + "/user/signin";
    public static final String LOGOUT = PATH_PREFIX + "/user/signout";

    public static final String PROPERTIES = PATH_PREFIX + "/users/match/properties";
    public static final String PROPERTY_BY_ID = PATH_PREFIX + "/properties/";
    public static final String SEARCH_PROPERTY = PATH_PREFIX + "/property/search";
    public static final String MAIL_CONTACT_WATTA = PATH_PREFIX + "/contact";
    public static final String MAIL_POSTULER = PATH_PREFIX + "/postuler";
    public static final String MAIL_REQUEST_APPOINTMENT = PATH_PREFIX + "/rendezVous";

    public static final String PEOPLES = PATH_PREFIX + "/users/match";
    public static final String SEARCH_PEOPLES = PATH_PREFIX + "/users/search";
    public static final String BLOCK_USER = PATH_PREFIX + "/users/user/block";
    public static final String FILTER_USERS = PATH_PREFIX + "/users/match/filter";

    //public static final String USER_GET_ALL_FAVORITES = PATH_PREFIX + "/users/favourites/received";
    public static final String USER_GET_ALL_FAVORITES = PATH_PREFIX + "/users/favourites/";
    public static final String USER_FAVORITE_UPDATE = PATH_PREFIX + "/users/favourites";

    public static final String PROPERTY_GET_ALL_FAVORITES = PATH_PREFIX + "/users/favourites/property/";
    public static final String PROPERTY_FAVORITE_UPDATE = PATH_PREFIX + "/users/favourites/property";

    public static final String SEND_CONTACT_REQUEST = PATH_PREFIX + "/users/contact/send";
    public static final String CONTACT_REQUESTS = PATH_PREFIX + "/users/contact/received";
    public static final String CONTACT_REQUEST_ACCEPT = PATH_PREFIX + "/users/contact/accept";
    public static final String CONTACT_REQUEST_REJECT = PATH_PREFIX + "/users/contact/reject";

    public static final String CHATTED_USERS = PATH_PREFIX + "/users/chat/already";
    public static final String SEND_CHAT_MSG = PATH_PREFIX + "/users/chat/send";
    public static final String CHAT_GET_MSGS = PATH_PREFIX + "/users/chat";

    public static final String ROOM_USERS_MATCHED = PATH_PREFIX + "/users/avecQui";
    public static final String PROPERTY_APPLY = PATH_PREFIX + "/property/applyFor";
    public static final String POSTULER_APPLY = PATH_PREFIX + "/postuler/final_step";

    public static final String AMENITIES = PATH_PREFIX + "/amenities";
    public static final String HEATINGS = PATH_PREFIX + "/heatingCategories";
    public static final String CATEGORY = PATH_PREFIX + "/categories";
    public static final String FILTER_PROPERTY = PATH_PREFIX + "/users/match/properties/filter";

    public static final String CREATE_SAFE_PASS = PATH_PREFIX + "/user/coffreFort/create";
    public static final String CHECK_SAFE_PASS = PATH_PREFIX + "/user/coffreFort/isSet/";
    public static final String SAFE_PROFESSION_TYPE = PATH_PREFIX + "/user/coffreFort/type";
    public static final String SAFE_CONNECT_WITH_PASS = PATH_PREFIX + "/user/coffreFort/connect";
    public static final String SAFE_USERS_TYPES = PATH_PREFIX + "/user/coffreFort/types/users";
    public static final String SAFE_PROFESSION_GARANT = PATH_PREFIX + "/user/coffreFort/type/garant";
    public static final String SAFE_GARANT_AS_OWNER = PATH_PREFIX + "/user/coffreFort/garant/proprietaire";
    public static final String SAFE_INITIALIZER = PATH_PREFIX + "/user/coffreFort/files/";
    public static final String SAFE_UPLOAD_DOCUMENT = PATH_PREFIX + "/user/coffreFort/file/create";

    public static final String PROFILE_UPLOAD_PIC = PATH_IMAGE_PREFIX + "/user/avatar/add";

    public static final String FACEBOOK_AUTH = PATH_PREFIX + "/wtfbconnect";
    public static final String FACEBOOK_SUBSCRIBE = PATH_PREFIX + "/wtfbsubscribe";

    public static final String SAFE_IDENTITY_PIECE = PATH_PREFIX + "/users/coffreFort/uploadFile";

    //public static final String TOKEN = "Bearer UH3aqTaFFP4xhGSaOTAkSoyHx8ndAXC5";
    public static final String TOKEN = "Bearer sH3aqTaFFP4xhGSaOTAWSoyHx8ndAXC5";
}
