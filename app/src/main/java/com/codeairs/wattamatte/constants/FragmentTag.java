package com.codeairs.wattamatte.constants;

public class FragmentTag {

    public static final String PERSONAL_INFO = "com.codeairs.wattamatte.fragments.completeprofile.PersonalInfoFragment";
    public static final String CHOOSE_INTERESTS = "com.codeairs.wattamatte.fragments.completeprofile.ChooseInterestFragment";
    public static final String CHOOSE_STYLES = "com.codeairs.wattamatte.fragments.completeprofile.ChooseStylesFragment";
    public static final String CHOOSE_ROOM_MATE_INTERESTS = "com.codeairs.wattamatte.fragments.completeprofile.ChooseRoommateInterestsFragment";
    public static final String CHOOSE_ROOM_MATE_STYLES = "com.codeairs.wattamatte.fragments.ChooseRoommateStylesFragment";
    public static final String CHOOSE_PROFILE_PIC = "com.codeairs.wattamatte.fragments.completeprofile.ChooseProfilePicFragment";
    public static final String COMPLETE_ROOM_DETAILS = "com.codeairs.wattamatte.fragments.completeprofile.CompleteRoomDetailsFragment";

    public static final String SAFE_COMPLETION = "com.codeairs.wattamatte.fragments.safe.SafeCompletionFragment";
    public static final String PIECE_IDENTITY = "com.codeairs.wattamatte.fragments.safe.PieceIdentityFragment";
    public static final String ADDRESS_PROOF = "com.codeairs.wattamatte.fragments.safe.AddressProofFragment";
    public static final String SUPPORT_CAF = "com.codeairs.wattamatte.fragments.safe.SupportCAFFragment";
    public static final String STUDENT_CARD = "com.codeairs.wattamatte.fragments.safe.StudentCardFragment";

    public static final String GUARANTOR = "com.codeairs.wattamatte.fragments.safe.GuarantorFragment";
    public static final String GUARANTOR_STEP1 = "com.codeairs.wattamatte.fragments.safe.GuarantorStep1Fragment";
    public static final String GUARANTOR_STEP2 = "com.codeairs.wattamatte.fragments.safe.GuarantorStep2Fragment";
    public static final String GUARANTOR_STEP3 = "com.codeairs.wattamatte.fragments.safe.GuarantorStep3Fragment";

}
