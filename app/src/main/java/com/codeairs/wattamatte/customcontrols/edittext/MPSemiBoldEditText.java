package com.codeairs.wattamatte.customcontrols.edittext;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatEditText;
import android.util.AttributeSet;

public class MPSemiBoldEditText extends AppCompatEditText {

    public MPSemiBoldEditText(Context context) {
        super(context);
        applyCustomFont(context);
    }

    public MPSemiBoldEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        applyCustomFont(context);
    }

    public MPSemiBoldEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        applyCustomFont(context);
    }

    private void applyCustomFont(Context context) {
        Typeface tf = Typeface.createFromAsset(context.getAssets(), "fonts/Metropolis-SemiBold.otf");
        setTypeface(tf);
    }
}
