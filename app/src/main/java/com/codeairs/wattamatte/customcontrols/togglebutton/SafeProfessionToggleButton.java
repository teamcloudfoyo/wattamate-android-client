package com.codeairs.wattamatte.customcontrols.togglebutton;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.TextView;
import com.codeairs.wattamatte.R;
import com.nex3z.togglebuttongroup.button.CompoundToggleButton;

public class SafeProfessionToggleButton extends CompoundToggleButton {

    private TextView button;

    public SafeProfessionToggleButton(Context context) {
        this(context, null);
    }

    public SafeProfessionToggleButton(Context context, AttributeSet attrs) {
        super(context, attrs);

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.layout_safe_profession_toggle_button, this, true);

        button = findViewById(R.id.btn);

        TypedArray a = context.getTheme().obtainStyledAttributes(attrs,
                R.styleable.SafeProfessionToggleButton, 0, 0);
        try {
            CharSequence text = a.getText(R.styleable.SafeProfessionToggleButton_spText);
            button.setText(text);

            refreshState(isChecked());
        } finally {
            a.recycle();
        }
    }

    @Override
    public void toggle() {
        if (isChecked()) {
            return;
        }
        super.toggle();
        refreshState(isChecked());
    }

    @Override
    public void setChecked(boolean checked) {
        super.setChecked(checked);
        refreshState(checked);
    }

    public void setText(int resId) {
        button.setText(resId);
        refreshState(isChecked());
    }

    private void refreshState(boolean checked) {
        if (checked) {
            button.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.radicalRed));
            button.setTextColor(ContextCompat.getColor(getContext(), android.R.color.white));
        } else {
            button.setTextColor(ContextCompat.getColor(getContext(), R.color.stPatricksBlue));
            button.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.white));
        }
    }

}
