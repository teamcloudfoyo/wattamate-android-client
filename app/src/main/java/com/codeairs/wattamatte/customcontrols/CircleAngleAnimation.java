package com.codeairs.wattamatte.customcontrols;

import android.view.animation.Animation;
import android.view.animation.Transformation;

public class CircleAngleAnimation extends Animation {

    private Circle circle;

    private float oldAngle;
    private float newAngle;
    private int color;

    boolean angular2;

    public CircleAngleAnimation(Circle circle, int newAngle, int color) {
        this.oldAngle = circle.getAngle();
        this.newAngle = newAngle;
        this.circle = circle;
        this.color = color;
    }

    public CircleAngleAnimation(Circle circle, int newAngle) {
        this.oldAngle = circle.getAngle2();
        this.newAngle = newAngle;
        this.circle = circle;
        angular2 = true;
    }

    @Override
    protected void applyTransformation(float interpolatedTime, Transformation transformation) {
        float angle = oldAngle + ((newAngle - oldAngle) * interpolatedTime);

        if (angular2) {
            circle.drawSecondArc(angle);
        } else {
            circle.setAngle(angle);
            circle.setColor(color);
        }
        circle.requestLayout();
    }
}