package com.codeairs.wattamatte.customcontrols.textview;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

public class MPSemiBoldTextView extends android.support.v7.widget.AppCompatTextView {

    public MPSemiBoldTextView(Context context) {
        super(context);
        applyCustomFont(context);
    }

    public MPSemiBoldTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        applyCustomFont(context);
    }

    public MPSemiBoldTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        applyCustomFont(context);
    }

    private void applyCustomFont(Context context) {
        Typeface tf = Typeface.createFromAsset(context.getAssets(), "fonts/Metropolis-SemiBold.otf");
        setTypeface(tf);
    }
}
