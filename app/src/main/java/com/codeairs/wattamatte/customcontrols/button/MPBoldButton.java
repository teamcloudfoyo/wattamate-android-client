package com.codeairs.wattamatte.customcontrols.button;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatButton;
import android.util.AttributeSet;

public class MPBoldButton extends AppCompatButton {

    public MPBoldButton(Context context) {
        super(context);
        applyCustomFont(context);
    }

    public MPBoldButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        applyCustomFont(context);
    }

    public MPBoldButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        applyCustomFont(context);
    }

    private void applyCustomFont(Context context) {
        Typeface tf = Typeface.createFromAsset(context.getAssets(), "fonts/Metropolis-Bold.otf");
        setTypeface(tf);
    }
}
