package com.codeairs.wattamatte.customcontrols.togglebutton;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.codeairs.wattamatte.R;
import com.nex3z.togglebuttongroup.button.CompoundToggleButton;

public class InterestToggleButton extends CompoundToggleButton {

    private CardView card;
    private ImageView img;
    private TextView tv;
    private LinearLayout layout;

    private int bgColor, selectedBgColor;

    public InterestToggleButton(Context context) {
        this(context, null);
    }

    public InterestToggleButton(Context context, AttributeSet attrs) {
        super(context, attrs);

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.layout_interest_toggle_button, this, true);

        img = findViewById(R.id.img);
        tv = findViewById(R.id.tv);
        card = findViewById(R.id.card);
        layout = findViewById(R.id.layout);

        TypedArray a = context.getTheme().obtainStyledAttributes(attrs,
                R.styleable.InterestToggleButton, 0, 0);
        try {
            Drawable front = a.getDrawable(R.styleable.InterestToggleButton_clipart);
            img.setImageDrawable(front);

            CharSequence text = a.getText(R.styleable.InterestToggleButton_itgText);
            tv.setText(text);

            bgColor = a.getColor(R.styleable.InterestToggleButton_bgColor, 0xff000000);
            selectedBgColor = a.getColor(R.styleable.InterestToggleButton_selectedBgColor, 0xff000000);

            refreshState(isChecked());

        } finally {
            a.recycle();
        }
    }

    @Override
    public void toggle() {
        if (isChecked()) {
            return;
        }
        super.toggle();
    }

    @Override
    public void setChecked(boolean checked) {
        super.setChecked(checked);
        refreshState(checked);
    }

    public void setImage(int resId) {
        img.setImageResource(resId);
        refreshState(isChecked());
    }

    public void setText(int resId) {
        tv.setText(resId);
        refreshState(isChecked());
    }

    public void setBgColor(int color) {
        bgColor = color;
        refreshState(isChecked());
    }

    public void setSelectedBgColor(int color) {
        selectedBgColor = color;
        refreshState(isChecked());
    }

    private void refreshState(boolean checked) {
        if (checked) {
            if (img.getDrawable() != null)
                img.getDrawable().setAlpha(255);
            layout.setBackgroundColor(selectedBgColor);
            tv.setTextColor(ContextCompat.getColor(getContext(), android.R.color.white));
            tv.setAlpha(1);
        } else {
            if (img.getDrawable() != null)
                img.getDrawable().setAlpha(80);
            layout.setBackgroundColor(bgColor);
            tv.setTextColor(ContextCompat.getColor(getContext(), android.R.color.black));
            tv.setAlpha(0.3f);
        }
    }

}
