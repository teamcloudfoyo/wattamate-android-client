package com.codeairs.wattamatte.customcontrols;

import android.content.Context;
import android.graphics.*;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.View;
import com.codeairs.wattamatte.R;
import com.codeairs.wattamatte.utils.AndroidUtils;

public class Circle extends View {

    private static final int START_ANGLE_POINT = 90;

    private Paint paint;

    private Paint paint2;

    private RectF rect;

    private float angle;
    private float angle2;
    private int color = ContextCompat.getColor(getContext(), R.color.darkSlateBlue);

    private int strokeWidth = AndroidUtils.dpToPx(getContext(), 3);

    public Circle(Context context, AttributeSet attrs) {
        super(context, attrs);

        init();
    }

    public void init() {

        paint = new Paint();
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(strokeWidth);
        //Circle color
        paint.setColor(color);
        paint.setStrokeCap(Paint.Cap.ROUND);
        paint.setStrokeJoin(Paint.Join.ROUND);

        //size 200x200 example

        rect = new RectF(strokeWidth, strokeWidth,
                AndroidUtils.dpToPx(getContext(), 43) + strokeWidth,
                AndroidUtils.dpToPx(getContext(), 43) + strokeWidth);

        //Initial Angle (optional, it can be zero)
        angle = angle2 = 0;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawOval(rect, mOvalPaint);
        canvas.drawArc(rect, START_ANGLE_POINT, angle, false, paint);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int parentWidth = MeasureSpec.getSize(widthMeasureSpec);
        int parentHeight = MeasureSpec.getSize(heightMeasureSpec);
        this.setMeasuredDimension(parentWidth, parentHeight);
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    private final Paint mOvalPaint = new Paint() {
        {
            setStyle(Style.STROKE);
            setColor(Color.TRANSPARENT);
            setAntiAlias(true);
            setStrokeWidth(AndroidUtils.dpToPx(getContext(), 2.9f));
            setStrokeCap(Paint.Cap.ROUND);
            setStrokeJoin(Paint.Join.ROUND);
        }
    };

    public float getAngle() {
        return angle;
    }

    public float getAngle2() {
        return angle2;
    }

    public void setAngle(float angle) {
        this.angle = angle;
    }

    public void setColor(int color) {
        this.color = color;
        paint.setColor(color);
    }

    public void drawSecondArc(float angle2) {
        this.angle2 = angle2;
    }

    public void setPathColor(int color) {
        mOvalPaint.setColor(color);
    }
}