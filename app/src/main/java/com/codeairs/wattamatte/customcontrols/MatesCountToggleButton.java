package com.codeairs.wattamatte.customcontrols;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.GradientDrawable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.TextView;
import com.codeairs.wattamatte.R;
import com.nex3z.togglebuttongroup.button.CompoundToggleButton;

public class MatesCountToggleButton extends CompoundToggleButton {

    private TextView textView;

    public MatesCountToggleButton(Context context) {
        this(context, null);
    }

    public MatesCountToggleButton(Context context, AttributeSet attrs) {
        super(context, attrs);

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.layout_mates_count_toggle_button, this, true);

        textView = findViewById(R.id.text);

        TypedArray a = context.getTheme().obtainStyledAttributes(attrs,
                R.styleable.MatesCountToggleButton, 0, 0);
        try {
            CharSequence t = a.getText(R.styleable.MatesCountToggleButton_tgText);
            textView.setText(t);

            refreshState(isChecked());
        } finally {
            a.recycle();
        }
    }

    @Override
    public void setChecked(boolean checked) {
        super.setChecked(checked);
        refreshState(checked);
    }

    public String getText() {
        if (textView.getText() == null)
            return null;
        return textView.getText().toString();
    }

    private void refreshState(boolean checked) {
        GradientDrawable shape = new GradientDrawable();

        if (checked) {
            textView.setTextColor(ContextCompat.getColor(getContext(), android.R.color.white));

            shape.setCornerRadius(12);
            shape.setColor(ContextCompat.getColor(getContext(), R.color.mediumTurquoise));
            shape.setStroke(4, ContextCompat.getColor(getContext(), R.color.mediumTurquoise));
        } else {
            textView.setTextColor(ContextCompat.getColor(getContext(), android.R.color.black));

            shape.setCornerRadius(12);
            shape.setColor(android.R.color.transparent);
            shape.setStroke(4, ContextCompat.getColor(getContext(), R.color.mediumTurquoise));
        }
        setBackground(shape);
    }

}
