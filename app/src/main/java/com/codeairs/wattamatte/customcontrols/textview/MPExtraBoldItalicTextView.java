package com.codeairs.wattamatte.customcontrols.textview;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

public class MPExtraBoldItalicTextView extends android.support.v7.widget.AppCompatTextView {

    public MPExtraBoldItalicTextView(Context context) {
        super(context);
        applyCustomFont(context);
    }

    public MPExtraBoldItalicTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        applyCustomFont(context);
    }

    public MPExtraBoldItalicTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        applyCustomFont(context);
    }

    private void applyCustomFont(Context context) {
        Typeface tf = Typeface.createFromAsset(context.getAssets(), "fonts/Metropolis-ExtraBoldItalic.otf");
        setTypeface(tf);
    }
}
