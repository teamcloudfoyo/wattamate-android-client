package com.codeairs.wattamatte.customcontrols.togglebutton;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import com.codeairs.wattamatte.R;
import com.nex3z.togglebuttongroup.button.CompoundToggleButton;

public class InterestTextToggleButton extends CompoundToggleButton {

    private int bgColor;
    private int borderColor;

    public InterestTextToggleButton(Context context) {
        this(context, null);
    }

    public InterestTextToggleButton(Context context, AttributeSet attrs) {
        super(context, attrs);

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.layout_interest_toggle_button_text, this, true);


        TypedArray a = context.getTheme().obtainStyledAttributes(attrs,
                R.styleable.InterestToggleButton, 0, 0);
        try {
            bgColor = a.getColor(R.styleable.InterestToggleButton_bgColor, 0xff000000);
            borderColor = a.getColor(R.styleable.InterestToggleButton_borderColor, 0xff000000);

            setBackgroundColor(bgColor);

            refreshState(isChecked());

        } finally {
            a.recycle();
        }
    }

    @Override
    public void toggle() {
        if (isChecked()) {
            return;
        }
        super.toggle();
    }

    @Override
    public void setChecked(boolean checked) {
        super.setChecked(checked);
        refreshState(checked);
    }

    public void setBorderColor(int color) {
        borderColor = color;
        refreshState(isChecked());
    }

    public void setBgColor(int color) {
        bgColor = color;
        refreshState(isChecked());
    }

    private void refreshState(boolean checked) {
        GradientDrawable shape = new GradientDrawable();

        if (checked) {
            shape.setCornerRadius(6);
            shape.setColor(bgColor);
            shape.setStroke(12, borderColor);
            shape.setAlpha(255);
        } else {
            shape.setCornerRadius(6);
            shape.setColor(bgColor);
            shape.setStroke(12, 0xFFFFFFFF);
            shape.setAlpha(80);
        }
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
            setBackgroundDrawable(shape);
        } else {
            setBackground(shape);
        }
    }

}
