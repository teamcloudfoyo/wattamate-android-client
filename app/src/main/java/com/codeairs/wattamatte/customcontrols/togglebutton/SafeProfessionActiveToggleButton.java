package com.codeairs.wattamatte.customcontrols.togglebutton;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.codeairs.wattamatte.R;
import com.codeairs.wattamatte.models.SafeVault.SafeUser;
import com.nex3z.togglebuttongroup.button.CompoundToggleButton;

import java.util.ArrayList;


public class SafeProfessionActiveToggleButton extends CompoundToggleButton {

    private TextView text;
    private CardView listCard;
    private ImageView arrow;
    private ListAdapter adapter;
    private RecyclerView recyclerView;

    private OnItemClickListener listener;

    public SafeProfessionActiveToggleButton(Context context) {
        this(context, null);
    }

    public SafeProfessionActiveToggleButton(Context context, AttributeSet attrs) {
        super(context, attrs);

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.layout_safe_profession_active_toggle_button, this, true);

        text = findViewById(R.id.text);
        text.setText(R.string.active);
        arrow = findViewById(R.id.arrow);
        listCard = findViewById(R.id.card2);
        recyclerView = findViewById(R.id.list);
        refreshState(isChecked());
    }

    public void setSubCategories(ArrayList<SafeUser> safeUserList) {
        adapter = new ListAdapter(safeUserList);
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void toggle() {
        if (isChecked()) {
            return;
        }
        super.toggle();
    }

    @Override
    public void setChecked(boolean checked) {
        super.setChecked(checked);
        refreshState(checked);
    }

    public void setText(int resId) {
        text.setText(resId);
        refreshState(isChecked());
    }

    private void refreshState(boolean checked) {
        if (checked) {
            listCard.setVisibility(View.VISIBLE);
            arrow.setVisibility(View.VISIBLE);
        } else {
            listCard.setVisibility(View.GONE);
            arrow.setVisibility(View.GONE);
        }
    }

    public int getSelectedPosition() {
        return adapter.getSelectedPosition();
    }

    private class ListAdapter extends RecyclerView.Adapter<ListAdapter.ViewHolder> {

        ArrayList<SafeUser> safeUserList = new ArrayList<>();
        int lastCheckedPosition = -1;

        ListAdapter(ArrayList<SafeUser> safeUserList) {
            this.safeUserList = safeUserList;
        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_profession_active, parent, false);
            return new ListAdapter.ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
            SafeUser safeUser = safeUserList.get(position);
            holder.mView.setText(safeUser.getCdName());
            if (position == lastCheckedPosition) {
                holder.mView.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.radicalRed));
                holder.mView.setTextColor(ContextCompat.getColor(getContext(), R.color.white));
            } else {
                holder.mView.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.white));
                holder.mView.setTextColor(ContextCompat.getColor(getContext(), R.color.stPatricksBlue));
            }

            holder.mView.setOnClickListener(v -> {
                int pos = holder.getAdapterPosition();
                lastCheckedPosition = pos;
                notifyDataSetChanged();
                if (listener != null)
                    listener.onItemClick(pos);
            });
        }

        @Override
        public int getItemCount() {
            return safeUserList.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder {

            final TextView mView;

            public ViewHolder(View itemView) {
                super(itemView);
                mView = (TextView) itemView;
            }
        }

        private int getSelectedPosition() {
            return lastCheckedPosition;
        }
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    public interface OnItemClickListener {
        void onItemClick(int pos);
    }
}