package com.codeairs.wattamatte.customcontrols.edittext;

import android.content.Context;
import android.graphics.Typeface;
import android.support.design.widget.TextInputEditText;
import android.util.AttributeSet;

public class MPRegularTextInputEditText extends TextInputEditText {

    public MPRegularTextInputEditText(Context context) {
        super(context);
        applyCustomFont(context);
    }

    public MPRegularTextInputEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        applyCustomFont(context);
    }

    public MPRegularTextInputEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        applyCustomFont(context);
    }

    private void applyCustomFont(Context context) {
        Typeface tf = Typeface.createFromAsset(context.getAssets(), "fonts/Metropolis-Regular.otf");
        setTypeface(tf);
    }
}
