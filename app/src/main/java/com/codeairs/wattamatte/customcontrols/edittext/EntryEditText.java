package com.codeairs.wattamatte.customcontrols.edittext;

import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatEditText;
import android.util.AttributeSet;
import com.codeairs.wattamatte.R;

public class EntryEditText extends AppCompatEditText {

    public EntryEditText(Context context) {
        super(context);
        applyCustomFont(context);
    }

    public EntryEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        applyCustomFont(context);
    }

    public EntryEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        applyCustomFont(context);
    }

    private void applyCustomFont(Context context) {
        Typeface tf = Typeface.createFromAsset(context.getAssets(), "fonts/Metropolis-Regular.otf");
        setTypeface(tf);
    }

    @Override
    public Drawable getBackground() {
        return ContextCompat.getDrawable(getContext(), R.drawable.dsb_round_border);
    }
}
