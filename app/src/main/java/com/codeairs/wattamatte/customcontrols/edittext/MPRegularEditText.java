package com.codeairs.wattamatte.customcontrols.edittext;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatEditText;
import android.util.AttributeSet;

public class MPRegularEditText extends AppCompatEditText {

    public MPRegularEditText(Context context) {
        super(context);
        applyCustomFont(context);
    }

    public MPRegularEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        applyCustomFont(context);
    }

    public MPRegularEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        applyCustomFont(context);
    }

    private void applyCustomFont(Context context) {
        Typeface tf = Typeface.createFromAsset(context.getAssets(), "fonts/Metropolis-Regular.otf");
        setTypeface(tf);
    }
}
