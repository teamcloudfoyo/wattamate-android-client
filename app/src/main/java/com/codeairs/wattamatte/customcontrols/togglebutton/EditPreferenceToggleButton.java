package com.codeairs.wattamatte.customcontrols.togglebutton;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.TextView;
import com.codeairs.wattamatte.R;
import com.codeairs.wattamatte.utils.AndroidUtils;
import com.nex3z.togglebuttongroup.button.CompoundToggleButton;

public class EditPreferenceToggleButton extends CompoundToggleButton {

    private TextView text;

    private int bgColor;
    private int borderColor;
    private int cornerRadius = AndroidUtils.dpToPx(getContext(), 16);

    public EditPreferenceToggleButton(Context context) {
        this(context, null);
    }

    public EditPreferenceToggleButton(Context context, AttributeSet attrs) {
        super(context, attrs);

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.layout_preference_toggle, this, true);

        text = findViewById(R.id.text);

        TypedArray a = context.getTheme().obtainStyledAttributes(attrs,
                R.styleable.EditPreferenceToggleButton, 0, 0);
        try {
            CharSequence t = a.getText(R.styleable.EditPreferenceToggleButton_epText);
            text.setText(t);

            Typeface tf = Typeface.createFromAsset(context.getAssets(), "fonts/Metropolis-Medium.otf");
            text.setTypeface(tf);

            try {
                bgColor = a.getColor(R.styleable.EditPreferenceToggleButton_epBgColor, 0xff000000);
            } catch (Exception e) {
                bgColor = ContextCompat.getColor(getContext(), R.color.radicalRed);
            }
            try {
                borderColor = a.getColor(R.styleable.EditPreferenceToggleButton_epBorderColor, 0xff000000);
            } catch (Exception e) {
                borderColor = ContextCompat.getColor(getContext(), R.color.darkSlateBlue);
            }

//            cornerRadius = a.getInt(R.styleable.EditPreferenceToggleButton_cornerRadius, 0);
            refreshState(isChecked());

        } finally {
            a.recycle();
        }
    }

    public EditPreferenceToggleButton(Context context, int id, String text, boolean isChecked) {
        super(context);

        setId(id);
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.layout_preference_toggle, this, true);
        this.text = findViewById(R.id.text);

        bgColor = ContextCompat.getColor(getContext(), R.color.radicalRed);
        borderColor = ContextCompat.getColor(getContext(), R.color.darkSlateBlue);
        setText(text);
        setChecked(isChecked);
    }

    @Override
    public void toggle() {
        if (isChecked()) {
            return;
        }
        super.toggle();
    }

    @Override
    public void setChecked(boolean checked) {
        super.setChecked(checked);
        refreshState(checked);
    }

    public void setBorderColor(int color) {
        borderColor = color;
        refreshState(isChecked());
    }

    public void setBgColor(int color) {
        bgColor = color;
        refreshState(isChecked());
    }

    public void setText(String text) {
        this.text.setText(text);
    }

    private void refreshState(boolean checked) {
        GradientDrawable shape = new GradientDrawable();

        if (checked) {
            shape.setCornerRadius(cornerRadius);
            shape.setColor(bgColor);
//            shape.setStroke(0, borderColor);
            text.setTextColor(ContextCompat.getColor(getContext(), R.color.white));
        } else {
            shape.setCornerRadius(cornerRadius);
            shape.setColor(android.R.color.transparent);
            shape.setStroke(2, borderColor);
            text.setTextColor(borderColor);
        }
        setBackground(shape);
    }
}