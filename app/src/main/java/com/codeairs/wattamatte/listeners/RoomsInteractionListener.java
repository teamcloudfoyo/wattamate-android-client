package com.codeairs.wattamatte.listeners;

import com.codeairs.wattamatte.models.rooms.Property;

import java.util.List;

public interface RoomsInteractionListener {

    String getSearchKey();

    void clearSearchKey();

    List<Property> getRoomsToDisplay();

    List<String> getFavoriteRoomsIds();

    void removeFavorite(String id);

    void addFavorite(String id);

}
