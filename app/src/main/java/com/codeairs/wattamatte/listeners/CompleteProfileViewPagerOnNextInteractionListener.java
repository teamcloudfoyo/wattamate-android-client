package com.codeairs.wattamatte.listeners;

public interface CompleteProfileViewPagerOnNextInteractionListener {

    void handleNextButtonClick(int page);

    void disableSwipeToNextPage();

    void enableSwipeToNextPage();
}