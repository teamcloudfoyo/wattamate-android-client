package com.codeairs.wattamatte.listeners;

import com.codeairs.wattamatte.models.Tenant.Tenant;

public interface TenantInteractionListener {

    void goToTenantDetails(Tenant item, Boolean checked);

    void showSnackBar(String msg);

    void openChat(Tenant tenant);

}
