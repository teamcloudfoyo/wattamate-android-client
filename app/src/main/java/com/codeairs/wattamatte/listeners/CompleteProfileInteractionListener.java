package com.codeairs.wattamatte.listeners;

import android.net.Uri;
import com.codeairs.wattamatte.models.Place;

import java.util.Date;
import java.util.List;

/**
 * this is not the interaction listeners for the next buttons inside pager,
 * they've their own interaction listener CompleteProfileViewPagerOnNextInteractionListener
 */
public interface CompleteProfileInteractionListener {

    void handleOnNext(String fragmentTag);

    void handleOnNameChange(String name);

    void handleOnDobChange(String dob);

    void handleOnProfessionIdChange(int professionId);

    void handleOnProfessionChange(String profession);

    void handleGenderChange(int genderId);

    void handleIsSmokerChange(int isSmoker);

    void handleHavePetsChange(int havePets);

    void handleHaveFurnitureChange(int haveFurniture);

    void handleHaveChildrenChange(int haveChildren);

    void handleProfilePicChange(Uri uri);

    String getName();

    String getDob();

    int getProfessionId();

    String getProfession();

    int getGender();

    int getIsSmoker();

    int getHaveChildren();

    int getHaveFurniture();

    int getHavePets();

    Uri getProfilePic();

    void showLoading(String message);

    void hideLoading();

    int getBudgetMin();

    void setBudgetMin(int value);

    void setBudgetChanged();

    boolean hasBudgetChanged();

    int getBudgetMax();

    void setBudgetMax(int value);

    Place getLocation();

    void setLocation(Place location);

    List<String> getMatesCount();

    void addMatesCount(String count);

    void removeMatesCount(String count);

    Date getEntryDate();

    void setEntryDate(Date date);

    String getFBProfilePic();

    boolean getDidUploadPic();

    void setDidUploadPic(boolean status);

}