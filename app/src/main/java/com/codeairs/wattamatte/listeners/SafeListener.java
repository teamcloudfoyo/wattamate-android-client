package com.codeairs.wattamatte.listeners;

import com.codeairs.wattamatte.models.SafeVault.InitializeSafe.Document;
import com.codeairs.wattamatte.models.SafeVault.InitializeSafe.SafeInitializer;
import com.codeairs.wattamatte.models.SafeVault.MySafe;

public interface SafeListener {

    void openPieceIdentity();

    void openAddressProof();

    void openSupportCAF();

    void openStudentCard();

    void openGuarantor();

    MySafe getMySafe();

    void setMySafe(MySafe mySafe);

    void updateSafeInitializer();

    void updateSafeCompletionStatus();

    Document getStudentCard();

    Document getCAFHelpDocument();

    Document getPieceIdentityDocument();

    Document getAddressProofDocument();

    Document getGuarantorIdentityPiece();

    /*Document getGuarantorAccomodationCertificate();

    Document getGuarantorHomeProof();*/

}
