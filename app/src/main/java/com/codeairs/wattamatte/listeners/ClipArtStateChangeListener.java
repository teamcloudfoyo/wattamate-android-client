package com.codeairs.wattamatte.listeners;

public interface ClipArtStateChangeListener {

    void handleStateChange();
}
