package com.codeairs.wattamatte.listeners;

public interface MainInteractionListener {

    void showLoading();

    void hideLoading();

    void goToTab(int tabNo);

    //Rest is notification based
    boolean isNotificationReceived();

    boolean isChatNotification();

    boolean isContactRequestNotification();

    String getUserNotified();

    void clearNotificationDatas();

}
