package com.codeairs.wattamatte.listeners;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import com.codeairs.wattamatte.R;
import com.codeairs.wattamatte.customcontrols.Circle;
import com.codeairs.wattamatte.customcontrols.CircleAngleAnimation;
import com.codeairs.wattamatte.utils.AndroidUtils;

public class CompleteProfileNextArrowListener implements View.OnClickListener, Animation.AnimationListener {

    private Context mContext;
    private Circle mCircle;

    private int mCircleColor;
    private View mInput;
    private View mLine;
    private Activity mActivity;
    private CompleteProfileNextArrowAnimCompleteListener mListener;

    public CompleteProfileNextArrowListener(Activity activity, Context context, Circle circle, int circleColor,
                                            View input, View line,
                                            CompleteProfileNextArrowAnimCompleteListener listener) {
        mActivity = activity;
        mContext = context;
        mCircle = circle;
        mCircleColor = circleColor;
        mInput = input;
        mLine = line;
        mListener = listener;
    }

    @Override
    public void onClick(View v) {
        AndroidUtils.hideSoftKeyboard(mActivity);

        doCircleFirstSweep();
        doCircleSecondSweep();
        moveLineOutOfScreen();
        hideLine();
    }

    private void doCircleFirstSweep() {
        new Handler().postDelayed(() -> {
            mCircle.setAngle(0);
            mCircle.setPathColor(ContextCompat.getColor(mContext, R.color.transparent));
            mCircle.setColor(mCircleColor);
            mCircle.invalidate();

            CircleAngleAnimation animation = new CircleAngleAnimation(mCircle, -360,
                    mCircleColor);
            animation.setDuration(400);
            mCircle.startAnimation(animation);
        }, 500);
    }

    private void doCircleSecondSweep() {
        new Handler().postDelayed(() -> {
            mCircle.setAngle(0);
            mCircle.setPathColor(mCircleColor);
            mCircle.setColor(ContextCompat.getColor(mContext, R.color.white));
            mCircle.invalidate();

            CircleAngleAnimation animation = new CircleAngleAnimation(mCircle, -360,
                    ContextCompat.getColor(mContext, R.color.white));
            animation.setDuration(400);

            mCircle.startAnimation(animation);
        }, 900);
    }

    private void moveLineOutOfScreen() {
        new Handler().postDelayed(() -> {
            mInput.setVisibility(View.INVISIBLE);
            Animation animation = AnimationUtils.loadAnimation(mContext,
                    R.anim.slide_out_left_right);
            animation.setAnimationListener(this);
            mLine.startAnimation(animation);
        }, 800);
    }

    private void hideLine() {
        new Handler().postDelayed(() -> mLine.setBackgroundColor(
                ContextCompat.getColor(mContext, android.R.color.transparent)),
                1150);
    }

    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {
        mListener.onComplete();
    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }

    public interface CompleteProfileNextArrowAnimCompleteListener {

        void onComplete();
    }
}


