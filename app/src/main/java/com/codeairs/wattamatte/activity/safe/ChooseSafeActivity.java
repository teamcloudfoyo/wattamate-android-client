package com.codeairs.wattamatte.activity.safe;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;

import com.codeairs.wattamatte.AppController;
import com.codeairs.wattamatte.R;
import com.codeairs.wattamatte.utils.AndroidUtils;

public class ChooseSafeActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AndroidUtils.changeStatusBarToWhite(getWindow(), this);
        setContentView(R.layout.activity_choose_safe);
        initControls();
    }

    private void initControls() {
        initToolbar();
        initFingerPrintImage();
        findViewById(R.id.fingerprint).setOnClickListener(this);
        findViewById(R.id.password).setOnClickListener(this);
    }

    private void initToolbar() {
        Toolbar toolbar = AndroidUtils.addToolbarWithBack(this);
        toolbar.setNavigationOnClickListener(v -> onBackPressed());
    }

    private void initFingerPrintImage() {
        ImageView img = findViewById(R.id.img);
        img.setColorFilter(ContextCompat.getColor(this, R.color.white), PorterDuff.Mode.SRC_IN);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fingerprint:
                startActivity(new Intent(this, FingerPrintActivity.class));
                return;
            case R.id.password:
                startActivity(new Intent(this, PasswordActivity.class));
                return;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppController.setForeground();
    }

    @Override
    protected void onPause() {
        super.onPause();
        AppController.setBackground();
    }
}
