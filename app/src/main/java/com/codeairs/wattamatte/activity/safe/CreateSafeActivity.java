package com.codeairs.wattamatte.activity.safe;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import com.codeairs.wattamatte.AppController;
import com.codeairs.wattamatte.R;
import com.codeairs.wattamatte.utils.AndroidUtils;

public class CreateSafeActivity extends AppCompatActivity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AndroidUtils.changeStatusBarColor(getWindow(), this, R.color.bgSafe);
        setContentView(R.layout.activity_create_safe);
        initControls();
    }

    private void initControls() {
        initToolbar();
        initCreateSafeButton();
    }

    private void initToolbar() {
        Toolbar toolbar = AndroidUtils.addToolbarWithBack(this);
        toolbar.setNavigationOnClickListener(v -> onBackPressed());
    }

    private void initCreateSafeButton() {
        findViewById(R.id.btn).setOnClickListener(v ->
                startActivity(new Intent(this, ChooseSafeActivity.class)));
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppController.setForeground();
    }

    @Override
    protected void onPause() {
        super.onPause();
        AppController.setBackground();
    }
}
