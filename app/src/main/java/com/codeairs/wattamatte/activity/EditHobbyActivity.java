package com.codeairs.wattamatte.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import com.android.volley.Request;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.codeairs.wattamatte.AppController;
import com.codeairs.wattamatte.R;
import com.codeairs.wattamatte.adapters.StyleRecyclerViewAdapter;
import com.codeairs.wattamatte.constants.ApiPaths;
import com.codeairs.wattamatte.fragments.ProfileFragment;
import com.codeairs.wattamatte.listeners.ClipArtStateChangeListener;
import com.codeairs.wattamatte.models.Tenant.TenantHobby;
import com.codeairs.wattamatte.models.Tenant.TenantKeyFeatureMate;
import com.codeairs.wattamatte.models.Tenant.TenantKeyFeatureMine;
import com.codeairs.wattamatte.models.completeprofiledetails.*;
import com.codeairs.wattamatte.models.profile.MyProfile;
import com.codeairs.wattamatte.utils.*;
import com.codeairs.wattamatte.utils.internet.NetworkUtils;
import com.codeairs.wattamatte.utils.internet.VolleySingleton;
import com.google.common.collect.ImmutableMap;
import com.google.gson.Gson;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class EditHobbyActivity extends AppCompatActivity
        implements ClipArtStateChangeListener, View.OnClickListener {

    public static final String PARAM_1 = "EditHobbyActivity.PARAM_1";

    private List<Hobby> allHobbies;
    private MyProfile profile;
    private Button nextButton;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_hobby);

        profile = new Gson().fromJson(getIntent().getStringExtra(PARAM_1), MyProfile.class);
        initControls();
        fetchHobbies();
    }

    @Override
    public void handleStateChange() {
        CompleteProfileHelper.handleClipArtStateChange(allHobbies, nextButton);
    }

    private void initControls() {
        nextButton = findViewById(R.id.nextButton);
        nextButton.setText(R.string.update);
        nextButton.setOnClickListener(this);

        recyclerView = findViewById(R.id.list);

        AndroidUtils.setText(findViewById(R.id.content), R.id.title, getString(R.string.choose_three_1));
    }

    private void fetchHobbies() {
        StringRequest request = new StringRequest(Request.Method.GET, ApiPaths.HOBBY,
                response -> {
                    Log.d("hobby.response", response);
                    allHobbies = JsonUtils.fromJsonList(response, Hobby.class).data;
                    markAlreadySelected();

                    recyclerView.setLayoutManager(new GridLayoutManager(this, 3));
                    recyclerView.addItemDecoration(new GridSpacingItemDecoration(3,
                            AndroidUtils.dpToPx(this, 0), false, 0));
                    recyclerView.setAdapter(new StyleRecyclerViewAdapter(allHobbies, this,
                            this, recyclerView.getHeight() / 4));
                },
                error -> {

                }
        ) {

            @Override
            public Map<String, String> getHeaders() {
                return NetworkUtils.authenticatedHeader(EditHobbyActivity.this);
            }
        };
        VolleySingleton.getInstance(this).addToRequestQueue(request);
    }

    private void markAlreadySelected() {
        for (TenantHobby tHobby : profile.hobbies)
            for (Hobby hobby : allHobbies)
                if (tHobby.hobby.id.equals(hobby.id))
                    hobby.setChecked(true);
    }

    private JSONObject getPostBody() throws JSONException {
        List<HobbyRequest> hobbyRequests = new ArrayList<>();
        for (Hobby hobby : allHobbies) {
            if (hobby.isChecked) {
                hobbyRequests.add(new HobbyRequest(hobby.id, hobby.name));
            }
        }

        List<KeyFeatureMine> mine = new ArrayList<>();
        for (TenantKeyFeatureMine feature : profile.features.features.mine) {
            mine.add(new KeyFeatureMine(feature.id.id, feature.name));
        }

        List<KeyFeatureMate> mate = new ArrayList<>();
        for (TenantKeyFeatureMate feature : profile.features.features.mate) {
            mate.add(new KeyFeatureMate(feature.id.id, feature.name));
        }

        return new JSONObject(new Gson().toJson(ImmutableMap.<String, Object>builder()
                .put("userId", SharedPrefManager.getUserId(this))
                .put("hobbies", hobbyRequests)
                .put("keyFeatures", ImmutableMap.<String, Object>builder()
                        .put("mine", mine)
                        .put("mate", mate)
                        .build()
                ).build()));
    }

    @Override
    public void onClick(View v) {
        JsonObjectRequest request = null;
        try {
            Log.d("json", getPostBody().toString());
            request = NetworkUtils.authenticatedPutRequest(
                    this, ApiPaths.UPDATE_HOBBY_FEATURE, getPostBody(),
                    response -> {
                        Log.d("UpdateHobby.Response", response.toString());
                        ProfileFragment.needsRefresh = true;
                        onBackPressed();
                    }, error -> {

                    });
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (request != null) {
            request.setShouldCache(false);
            VolleySingleton.getInstance(this).addToRequestQueue(request);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppController.setForeground();
    }

    @Override
    protected void onPause() {
        super.onPause();
        AppController.setBackground();
    }
}
