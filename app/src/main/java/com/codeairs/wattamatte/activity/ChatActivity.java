package com.codeairs.wattamatte.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.JsonObjectRequest;
import com.codeairs.wattamatte.AppController;
import com.codeairs.wattamatte.R;
import com.codeairs.wattamatte.adapters.MessageListAdapter;
import com.codeairs.wattamatte.constants.ApiPaths;
import com.codeairs.wattamatte.models.ApiResponse;
import com.codeairs.wattamatte.models.Tenant.Tenant;
import com.codeairs.wattamatte.models.contacts.Message;
import com.codeairs.wattamatte.utils.DateUtils;
import com.codeairs.wattamatte.utils.JsonUtils;
import com.codeairs.wattamatte.utils.SharedPrefManager;
import com.codeairs.wattamatte.utils.internet.NetworkUtils;
import com.codeairs.wattamatte.utils.internet.VolleySingleton;
import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Socket;
import com.google.common.collect.ImmutableMap;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.codeairs.wattamatte.constants.ApiPaths.CHAT_GET_MSGS;

public class ChatActivity extends AppCompatActivity implements View.OnClickListener, View.OnLayoutChangeListener {

    static final String PARAM_DETAILS = "com.codeairs.wattamatte.activity.ChatActivity.PARAM_DETAILS";
    private static final String TAG = ChatActivity.class.getSimpleName();

    private Tenant tenant;

    private MessageListAdapter mMessageAdapter;
    private RecyclerView mMessageRecycler;
    private TextView message;
    private ImageButton send;

    private Map<String, String> requestMap;
    private List<Message> messages = new ArrayList<>();
    private String me; // userId of account holder


    private Socket mSocket;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        tenant = new Gson().fromJson(getIntent().getStringExtra(PARAM_DETAILS), Tenant.class);
        me = SharedPrefManager.getUserId(this);

        initRequestMap();
        initControls();
        fetchMessages();

        AppController appController = AppController.getInstance();
        mSocket = appController.getmSocket();
        mSocket.on(getSocketListenKey(), newMsgListener);
        mSocket.connect();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mSocket.off(getSocketListenKey());
    }

    private String getSocketListenKey() {
        return MessageFormat.format("tchat-{0}-{1}",
                tenant.userId, me);
    }

    private Emitter.Listener newMsgListener = (Object... args) -> {
        if (args[0] != null) {
            try {
                JSONObject messageJson = new JSONObject(args[0].toString());
                JSONArray array = messageJson.getJSONArray("result");
                for (int i = 0; i < array.length(); i++) {
                    JSONObject jsonObject = array.getJSONObject(i);
                    messages.add(new Message(tenant.userId, me, jsonObject.getString("message"),
                            null, new Date(), false));
                }
                runOnUiThread(() -> mMessageAdapter.notifyDataSetChanged());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    @Override
    protected void onStop() {
        super.onStop();
    }

//    @Override
//    public boolean dispatchTouchEvent(MotionEvent ev) {
//        View v = getCurrentFocus();
//
//        if ((ev.getAction() == MotionEvent.ACTION_UP || ev.getAction() == MotionEvent.ACTION_MOVE) &&
//                v instanceof EditText &&
//                !v.getClass().getName().startsWith("android.webkit.")) {
//            int screenCoordinates[] = new int[2];
//            v.getLocationOnScreen(screenCoordinates);
//            float y = ev.getRawY() + v.getTop() - screenCoordinates[1];
//
//            if (y < v.getTop() || y > v.getBottom())
//                AndroidUtils.hideSoftKeyboard(this);
//        }
//        return super.dispatchTouchEvent(ev);
//    }

    private void initRequestMap() {
        requestMap = new HashMap<>();
        requestMap.put("userOneId", me);
        requestMap.put("userTwoId", tenant.userId);
    }

    private void initControls() {
        initPic();

        TextView name = findViewById(R.id.name);
        name.setText(tenant.firstName);
        name.setOnClickListener(v -> gotoTenantProfile());

        initChatBox();
        initList();
    }

    private void initList() {
        mMessageRecycler = findViewById(R.id.reyclerview_message_list);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setReverseLayout(true);

        mMessageRecycler.setLayoutManager(new LinearLayoutManager(this));
        mMessageAdapter = new MessageListAdapter(this, messages, tenant);
        mMessageRecycler.setAdapter(mMessageAdapter);
        mMessageRecycler.addOnLayoutChangeListener(this);
    }

    private void initPic() {
        /*String pic = tenant.pictureThumb.getName();
        if (pic != null && pic.contains("/home/api/data/uploads/"))
            pic = pic.replace("/home/api/data/uploads/", ApiPaths.PATH_PREFIX + "/");
            ImageView img = findViewById(R.id.img);
            Picasso.get().load(pic).placeholder(R.drawable.holder).into(img);*/
        ImageView img = findViewById(R.id.img);
        if (tenant.pictureThumb != null && tenant.pictureThumb.getId() != null)
        {
            AppController.getImageLoader().DisplayThumbImage(tenant.pictureThumb.getId(), img, R.drawable.holder);
            Log.d(TAG,"Thumb Id : "+tenant.pictureThumb.getId());
        }
        else
            AppController.getPicasso().load(R.drawable.holder).into(img);
        img.setOnClickListener(v -> gotoTenantProfile());
    }

    private void initChatBox() {
        message = findViewById(R.id.edittext_chatbox);
        message.addTextChangedListener(new ChatBoxTextWatcher());

        send = findViewById(R.id.button_chatbox_send);
        send.setEnabled(false);
        send.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        String msg = message.getText().toString();
        requestMap.put("message", msg);
        JsonObjectRequest request = NetworkUtils.authenticatedPostRequest(
                this, ApiPaths.SEND_CHAT_MSG, new JSONObject(requestMap),
                response -> {
                    Log.d("ChatMsgSend.Response", response.toString());
                },
                error -> {

                });
        VolleySingleton.getInstance(this).addToRequestQueue(request);

        messages.add(new Message(me, tenant.userId, msg, null, new Date(), false));
        mMessageAdapter.notifyItemChanged(messages.size() - 1);
        message.setText(null);
    }

    @Override
    public void onLayoutChange(View v, int left, int top, int right, int bottom,
                               int oldLeft, int oldTop, int oldRight, int oldBottom) {
        int size = mMessageRecycler.getAdapter().getItemCount();
        if (bottom < oldBottom && size > 5) {
            mMessageRecycler.postDelayed(() -> mMessageRecycler.smoothScrollToPosition(
                    size - 1), 100);
        }
    }

    class ChatBoxTextWatcher implements TextWatcher {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            send.setEnabled(!TextUtils.isEmpty(message.getText()) && (message.getText().toString().trim().length() > 0));
        }

    }

    private void fetchMessages() {
        JSONObject requestBody = getRequestBody(DateUtils.subtractDays(
                new Date(), -7), DateUtils.subtractDays(new Date(), 1));
        JsonObjectRequest request = NetworkUtils.authenticatedPostRequest(
                this, CHAT_GET_MSGS, requestBody,
                response -> {
                    Log.d("ChatMsgs.Response", response.toString());
                    ApiResponse<List<Message>> res = JsonUtils.fromJsonList(
                            response.toString(), Message.class);
                    if (!res.success) {
                        return;
                    }
                    messages.clear();
                    messages.addAll(res.data);
                    removeEmptyMessages();
                    mMessageAdapter.notifyDataSetChanged();
                }, error -> {

                }
        );
        request.setShouldCache(true);
        VolleySingleton.getInstance(this).addToRequestQueue(request);
    }

    private JSONObject getRequestBody(Date from, Date to) {
        return new JSONObject(ImmutableMap.<String, Object>builder()
                .put("userOneId", me)
                .put("userTwoId", tenant.userId)
                .put("fromDate", DateUtils.toFormat(from, DateUtils.PRIMARY_DATE_FORMAT))
                .put("toDate", DateUtils.toFormat(to, DateUtils.PRIMARY_DATE_FORMAT))
                .put("isRead", 0)
                .put("nbMessages", 50)
                .build());
    }

    private void removeEmptyMessages() {
        List<Message> list = new ArrayList<>();
        for (Message message : messages) {
            if (!TextUtils.isEmpty(message.msg))
                list.add(message);
        }
        messages.clear();
        messages.addAll(list);
    }

    private void gotoTenantProfile() {
        Intent intent = new Intent(this, TenantDetailsActivity.class);
        intent.putExtra(TenantDetailsActivity.PARAM_DETAILS, new Gson().toJson(tenant));
        startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppController.setForeground();
    }

    @Override
    protected void onPause() {
        super.onPause();
        AppController.setBackground();
    }
}
