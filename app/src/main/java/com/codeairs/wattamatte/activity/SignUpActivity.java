package com.codeairs.wattamatte.activity;

import android.content.Intent;
import android.os.Build;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import cn.pedant.SweetAlert.SweetAlertDialog;
import com.android.volley.*;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.codeairs.wattamatte.AppController;
import com.codeairs.wattamatte.R;
import com.codeairs.wattamatte.constants.ApiPaths;
import com.codeairs.wattamatte.constants.Constants;
import com.codeairs.wattamatte.models.ApiResponse;
import com.codeairs.wattamatte.models.entry.FBGraphRequest;
import com.codeairs.wattamatte.models.entry.FBLogin;
import com.codeairs.wattamatte.models.entry.SignUp;
import com.codeairs.wattamatte.utils.AndroidUtils;
import com.codeairs.wattamatte.utils.JsonUtils;
import com.codeairs.wattamatte.utils.SharedPrefManager;
import com.codeairs.wattamatte.utils.UIHelper;
import com.codeairs.wattamatte.utils.internet.NetworkUtils;
import com.codeairs.wattamatte.utils.internet.VolleySingleton;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.common.collect.ImmutableMap;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.Map;

public class SignUpActivity extends AppCompatActivity {

    private static final String TAG = SignUpActivity.class.getSimpleName();
    private TextInputLayout emailLayout, passLayout, confirmPassLayout;
    private EditText email, pass, confirmPass;
    private Button register;
    private SweetAlertDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        initControls();
    }

    private void initControls() {
        initToolbar();
        initTextFields();
        register = findViewById(R.id.register);
        register.setOnClickListener(v -> attemptSignUp());
        initDialog();
    }

    private void initToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        }
        initBackButton();
    }

    private void initBackButton() {
        findViewById(R.id.back).setOnClickListener(view -> {
            AndroidUtils.hideSoftKeyboard(this);
            onBackPressed();
        });
    }

    private void initTextFields() {
        emailLayout = findViewById(R.id.emailLayout);
        passLayout = findViewById(R.id.etPasswordLayout);
        confirmPassLayout = findViewById(R.id.etConfirmPasswordLayout);
        email = findViewById(R.id.email);
        pass = findViewById(R.id.password);
        confirmPass = findViewById(R.id.confirmPassword);

        email.addTextChangedListener(emailTextWatcher);
        pass.addTextChangedListener(passTextWatcher);
        confirmPass.addTextChangedListener(confirmPassTextWatcher);
    }

    private void initDialog() {
        pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(ContextCompat.getColor(this, R.color.darkSlateBlue));
        pDialog.setTitleText("Loading");
        pDialog.setCancelable(false);
    }

    private TextWatcher emailTextWatcher = new TextWatcher() {

        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (emailLayout.isErrorEnabled())
                UIHelper.removeError(emailLayout, email);
        }

        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        public void afterTextChanged(Editable s) {
        }
    };

    private TextWatcher passTextWatcher = new TextWatcher() {

        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (passLayout.isErrorEnabled())
                UIHelper.removeError(passLayout, pass);
        }

        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        public void afterTextChanged(Editable s) {
        }
    };

    private TextWatcher confirmPassTextWatcher = new TextWatcher() {

        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (confirmPassLayout.isErrorEnabled())
                UIHelper.removeError(confirmPassLayout, confirmPass);
        }

        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        public void afterTextChanged(Editable s) {
        }
    };

    private void attemptSignUp() {
        if (!validate()) {
            return;
        }
        UIHelper.disableViews(email, pass, confirmPass, register);
        pDialog.show();


        doSignUp();
    }

    private boolean validate() {
        boolean status = true;
        if (TextUtils.isEmpty(email.getText())) {
            UIHelper.setError(emailLayout, email, getString(R.string.error_no_email));
            status = false;
        }
        if (TextUtils.isEmpty(pass.getText())) {
            UIHelper.setError(passLayout, pass, getString(R.string.error_field_empty));
            status = false;
        }
        if (TextUtils.isEmpty(pass.getText())) {
            UIHelper.setError(confirmPassLayout, confirmPass, getString(R.string.error_field_empty));
            status = false;
        }
        if (!UIHelper.isValidEmail(email.getText())) {
            UIHelper.setError(emailLayout, email, getString(R.string.error_invalid_email));
            status = false;
        }
        if (!TextUtils.equals(pass.getText(), confirmPass.getText())) {
            UIHelper.setError(confirmPassLayout, confirmPass, getString(R.string.pass_no_match));
            status = false;
        }
        return status;
    }

    private void doSignUp() {
        JSONObject requestBody = new JSONObject();
        try {
            requestBody.put("email", email.getText().toString().trim());
            requestBody.put("password", pass.getText().toString().trim());

            JSONObject support = new JSONObject();
            support.put("os", Build.VERSION.RELEASE);
            support.put("device", "android");
            support.put("phoneName", Build.MODEL);

            requestBody.put("support", support);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d("Login.Json", requestBody.toString());

        StringRequest request = new StringRequest(Request.Method.POST, ApiPaths.SIGN_UP,
                this::handleResponse, this::handleError) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() {
                try {
                    return requestBody.toString() == null ? null : requestBody.toString().getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody.toString(), "utf-8");
                    return null;
                }
            }

            @Override
            public Map<String, String> getHeaders() {
                return ImmutableMap.<String, String>builder()
                        .put("Authorization", ApiPaths.TOKEN)
                        .build();
            }
        };
        request.setShouldCache(false);
        VolleySingleton.getInstance(this).addToRequestQueue(request);
    }

    private void handleResponse(String response) {
        Log.d(TAG,"SignUp.Response : "+ response);
        pDialog.dismiss();
        ApiResponse<SignUp> res = JsonUtils.fromJson(response.toString(), SignUp.class);
        if (!res.success) {
            UIHelper.enableViews(email, pass, confirmPass, register);
            if (res.data != null && res.data.msg != null && !res.data.msg.isEmpty())
                UIHelper.showErrorAlert(this, res.data.msg.get(0));
            return;
        }

        if (!TextUtils.isEmpty(res.data.userId)) {
            SharedPrefManager.saveUserStatus(this, Constants.USER_STATUS_NEEDS_VERIFICATION);
            SharedPrefManager.saveUserId(this, res.data.userId);
            startActivity(new Intent(this, VerifyPhoneActivity.class));
            finish();
        }
    }

    private void handleError(VolleyError error) {
        pDialog.dismiss();
        UIHelper.enableViews(email, pass, confirmPass, register);
        Log.e("LOG_VOLLEY", error.toString());
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppController.setForeground();
    }

    @Override
    protected void onPause() {
        super.onPause();
        AppController.setBackground();
    }
}
