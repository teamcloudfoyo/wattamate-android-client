package com.codeairs.wattamatte.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputFilter;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.codeairs.wattamatte.AppController;
import com.codeairs.wattamatte.R;
import com.codeairs.wattamatte.adapters.PlacesAutoCompleteAdapter;
import com.codeairs.wattamatte.constants.ApiPaths;
import com.codeairs.wattamatte.constants.Constants;
import com.codeairs.wattamatte.constants.Identifiers;
import com.codeairs.wattamatte.fragments.RoomMatesFragment;
import com.codeairs.wattamatte.models.ApiResponse;
import com.codeairs.wattamatte.models.Place;
import com.codeairs.wattamatte.models.Tenant.Tenant;
import com.codeairs.wattamatte.models.Tenant.TenantFilter;
import com.codeairs.wattamatte.models.completeprofiledetails.Address;
import com.codeairs.wattamatte.models.profile.MyProfile;
import com.codeairs.wattamatte.utils.AndroidUtils;
import com.codeairs.wattamatte.utils.CommonUtils;
import com.codeairs.wattamatte.utils.InputFilterMinMax;
import com.codeairs.wattamatte.utils.JsonUtils;
import com.codeairs.wattamatte.utils.OnFocusChangeListenerMinMax;
import com.codeairs.wattamatte.utils.SharedPrefManager;
import com.codeairs.wattamatte.utils.internet.NetworkUtils;
import com.codeairs.wattamatte.utils.internet.PlaceDetailsAPI;
import com.codeairs.wattamatte.utils.internet.VolleySingleton;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.gson.Gson;
import com.nex3z.togglebuttongroup.SingleSelectToggleGroup;
import com.shawnlin.numberpicker.NumberPicker;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.Locale;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;
import mehdi.sakout.fancybuttons.FancyButton;

public class EditTenantPreferenceActivity extends AppCompatActivity {

    private static final String TAG = EditTenantPreferenceActivity.class.getSimpleName();
    private View content;
    private ProgressBar progressBar;
    private AutoCompleteTextView location;
    private EditText budgetMin, budgetMax;
    private SingleSelectToggleGroup toggleGroupSmoker, toggleGroupAnimals, toggleGroupProfession, toggleGroupChildren;
    private NumberPicker daysPicker, monthsPicker;
    private FancyButton apply;

    private MyProfile profile;
    private String dontCare;
    private String selectedLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AndroidUtils.changeStatusBarToWhite(getWindow(), this);
        setContentView(R.layout.activity_edit_tenant_preference);
        initControls();
    }

    private void initControls() {
        initToolbar();
        content = findViewById(R.id.content);
        progressBar = findViewById(R.id.progressBar);
        budgetMin = content.findViewById(R.id.budgetMin);
        budgetMax = content.findViewById(R.id.budgetMax);
        monthsPicker = content.findViewById(R.id.monthPicker);
        daysPicker = content.findViewById(R.id.daysPicker);
        toggleGroupSmoker = content.findViewById(R.id.toggleGroupSmoker);
        toggleGroupAnimals = content.findViewById(R.id.toggleGroupAnimals);
        toggleGroupProfession = content.findViewById(R.id.toggleGroupProfession);
        toggleGroupChildren = content.findViewById(R.id.toggleGroupChildren);

        getProfileDetails();

        dontCare = CommonUtils.getLocaleStringResource(
                new Locale("fr"), R.string.dont_care, this);

        apply = findViewById(R.id.applyFilter);
        apply.setOnClickListener(v -> invokePost());
    }

    private void initToolbar() {
        Toolbar toolbar = AndroidUtils.addToolbarWithBack(this);
        toolbar.setNavigationOnClickListener(v -> onBackPressed());
    }

    private void initLocationField() {
        location = content.findViewById(R.id.etLocation);
        location.setText(CommonUtils.getValueFromAdditionalInfos(
                profile.additionInfos, Identifiers.LOCATION));

        location.setAdapter(new PlacesAutoCompleteAdapter(this, R.layout.autocomplete_list_item));
        location.setOnItemClickListener((parent, view1, position, id) -> {
            Place place = (Place) parent.getItemAtPosition(position);
            location.setText(place.description);
            new PlaceDetailsTask().execute(place.placeId);
        });
    }

    class PlaceDetailsTask extends AsyncTask<String, Void, Address> {

        protected Address doInBackground(String... urls) {
            try {
                PlaceDetailsAPI api = new PlaceDetailsAPI();
                return api.getDetails(urls[0]);
            } catch (Exception e) {
                return null;
            }
        }

        protected void onPostExecute(Address address) {
            selectedLocation = address.city;
        }

    }

    private void getProfileDetails() {
        content.setVisibility(View.INVISIBLE);
        progressBar.setVisibility(View.VISIBLE);
        Log.d(TAG,"Profile Url : "+ApiPaths.USER_PROFILE+"/"+ SharedPrefManager.getUserId(this));
        StringRequest request = new StringRequest(Request.Method.GET,
                ApiPaths.USER_PROFILE +"/"+ SharedPrefManager.getUserId(this),
                response -> {
                    Log.d(TAG,"Response : "+response);
                    ApiResponse<MyProfile> features = JsonUtils.fromJson(response, MyProfile.class);
                    if (features.success) {
                        profile = features.data;
                        populateData();
                    }
                    content.setVisibility(View.VISIBLE);
                    progressBar.setVisibility(View.GONE);
                },
                error -> {

                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return ImmutableMap.<String, String>builder()
                        .put("userId", SharedPrefManager.getUserId(getApplicationContext()))
                        .put("Authorization", ApiPaths.TOKEN)
                        .build();
            }
        };
        VolleySingleton.getInstance(this).addToRequestQueue(request);
    }

    private void populateData() {
        initLocationField();
        initBudgetField(budgetMin, Identifiers.BUDGET_MIN, Constants.BUDGET_MIN, true, budgetMax);
        initBudgetField(budgetMax, Identifiers.BUDGET_MAX, Constants.BUDGET_MAX, false, budgetMin);

        initToggle(toggleGroupSmoker, Identifiers.SMOKER, new String[]{"true", "false", dontCare},
                R.id.smokerYes, R.id.smokerNo, R.id.smokerNoCare);

        initToggle(toggleGroupAnimals, Identifiers.PETS, new String[]{"true", "false", dontCare},
                R.id.animalYes, R.id.animalNo, R.id.animalNoCare);

        initToggle(toggleGroupChildren, Identifiers.CHILDREN, new String[]{"true", "false", dontCare},
                R.id.childrenYes, R.id.childrenNo, R.id.childrenNoCare);

        initProfessionToggle();
    }

    private void initBudgetField(EditText et, String identifier,
                                 int value, boolean isMin, EditText otherField) {
        et.setText(CommonUtils.getValueFromAdditionalInfos(
                profile.additionInfos, identifier));
        et.setFilters(new InputFilter[]{new InputFilterMinMax(
                Constants.BUDGET_MIN, Constants.BUDGET_MAX)});
        et.setOnFocusChangeListener(new OnFocusChangeListenerMinMax(
                value, isMin, otherField));
    }

    private void initToggle(SingleSelectToggleGroup tg, String identifier, String[] values, int... redIds) {
        String value = CommonUtils.getValueFromAdditionalInfos(
                profile.additionInfos, identifier);
        initToggleSelection(tg, value, redIds);
//        tg.setOnCheckedChangeListener((group, checkedId) -> {
//            handleAdditionInfoCheckedChange(checkedId, identifier, redIds, values);
//            save();
//        });
    }

    private void initToggleSelection(SingleSelectToggleGroup group, String value, int... resIds) {
        try {
            boolean val = Boolean.valueOf(value);
            if (val)
                group.check(resIds[0]);
            else
                group.check(resIds[1]);
        } catch (Exception e) {
            group.check(resIds[2]);
        }
    }

//    private void handleAdditionInfoCheckedChange(int checkedId, String identifier, int[] ids, String[] values) {
//        for (int i = 0; i < ids.length; i++) {
//            int id = ids[i];
//            String value = values[i];
//            if (id == checkedId) {
//                updateAdditionalInfo(identifier, value);
//                break;
//            }
//        }
//    }

    private void initProfessionToggle() {
        switch (profile.post) {
            case "salarié":
            case "Salarié":
                toggleGroupProfession.check(R.id.professionYes);
            case "Étudiant":
                toggleGroupProfession.check(R.id.professionNo);
                break;
            default:
                toggleGroupProfession.check(R.id.professionNoCare);
        }
//        toggleGroupProfession.setOnCheckedChangeListener((group, checkedId) -> {
//            switch (checkedId) {
//                case R.id.professionYes:
//                    profile.job = "salarié";
//                    break;
//                case R.id.professionNo:
//                    profile.job = "Étudiant";
//                    break;
//            }
//            save();
//        });
    }

    private TenantFilter getPostObject() {
        TenantFilter filter = new TenantFilter();
        filter.userId = SharedPrefManager.getUserId(this);
        if (!TextUtils.isEmpty(budgetMin.getText()))
            filter.budgetMin = Float.parseFloat(budgetMin.getText().toString());
        if (!TextUtils.isEmpty(budgetMax.getText()))
            filter.budgetMax = Float.parseFloat(budgetMax.getText().toString());
        filter.moisEntree = monthsPicker.getValue();
        filter.joursEntree = daysPicker.getValue();
        filter.fumeurs = getSelectedSmokerValue();
        filter.animaux = getSelectedPetsValue();
        filter.enfants = getSelectedChildrenValue();
        filter.profession = getSelectedProfessionValue();
        if (!TextUtils.isEmpty(selectedLocation))
            filter.cities = Lists.newArrayList(selectedLocation);
        else
            filter.cities = Lists.newArrayList("");
        return filter;
    }

    private Integer getSelectedSmokerValue() {
        switch (toggleGroupSmoker.getCheckedId()) {
            case R.id.smokerYes:
                return 0;
            case R.id.smokerNo:
                return 1;
            case R.id.smokerNoCare:
                return 2;
        }
        return null;
    }

    private Integer getSelectedPetsValue() {
        switch (toggleGroupAnimals.getCheckedId()) {
            case R.id.animalYes:
                return 0;
            case R.id.animalNo:
                return 1;
            case R.id.animalNoCare:
                return 2;
        }
        return null;
    }

    private Integer getSelectedChildrenValue() {
        switch (toggleGroupChildren.getCheckedId()) {
            case R.id.childrenYes:
                return 0;
            case R.id.childrenNo:
                return 1;
            case R.id.childrenNoCare:
                return 2;
        }
        return null;
    }

    private Integer getSelectedProfessionValue() {
        switch (toggleGroupProfession.getCheckedId()) {
            case R.id.professionYes:
                return 0;
            case R.id.professionNo:
                return 1;
            case R.id.professionNoCare:
                return 2;
        }
        return null;
    }

    private void invokePost() {
        try {
            AndroidUtils.disableView(apply);
            JSONObject json = new JSONObject(new Gson().toJson(getPostObject()));
            Log.d("tenantFilter.Json", json.toString());
            JsonObjectRequest request = NetworkUtils.authenticatedPostRequest(this,
                    ApiPaths.FILTER_USERS, json, this::handleResponse, this::handleError);
            request.setShouldCache(false);
            VolleySingleton.getInstance(this).addToRequestQueue(request);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void handleResponse(JSONObject response) {
        Log.d("tenantFilter.Response", response.toString());
        ApiResponse<List<Tenant>> res = JsonUtils.fromJsonList(response.toString(), Tenant.class);
        if (!res.success) {
            AndroidUtils.enableView(apply);
            new SweetAlertDialog(this)
                    .setTitleText(res.error.message)
                    .show();
            return;
        }
        RoomMatesFragment.setFilterResults(res.data);
        Intent intent = new Intent();
        intent.putExtra("filterResult", true);
        setResult(Activity.RESULT_OK, intent);
        finish();
    }

    private void handleError(VolleyError error) {

    }

    @Override
    protected void onResume() {
        super.onResume();
        AppController.setForeground();
    }

    @Override
    protected void onPause() {
        super.onPause();
        AppController.setBackground();
    }
}
