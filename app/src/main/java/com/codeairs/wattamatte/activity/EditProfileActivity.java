package com.codeairs.wattamatte.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputFilter;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.codeairs.wattamatte.AppController;
import com.codeairs.wattamatte.R;
import com.codeairs.wattamatte.adapters.PlacesAutoCompleteAdapter;
import com.codeairs.wattamatte.constants.ApiPaths;
import com.codeairs.wattamatte.constants.Constants;
import com.codeairs.wattamatte.constants.Identifiers;
import com.codeairs.wattamatte.constants.RequestCode;
import com.codeairs.wattamatte.fragments.ProfileFragment;
import com.codeairs.wattamatte.models.ApiResponse;
import com.codeairs.wattamatte.models.Place;
import com.codeairs.wattamatte.models.completeprofiledetails.Address;
import com.codeairs.wattamatte.models.profile.EditProfile;
import com.codeairs.wattamatte.models.profile.MyProfile;
import com.codeairs.wattamatte.utils.AndroidUtils;
import com.codeairs.wattamatte.utils.CommonUtils;
import com.codeairs.wattamatte.utils.DateUtils;
import com.codeairs.wattamatte.utils.InputFilterMinMax;
import com.codeairs.wattamatte.utils.JsonUtils;
import com.codeairs.wattamatte.utils.OnFocusChangeListenerMinMax;
import com.codeairs.wattamatte.utils.SharedPrefManager;
import com.codeairs.wattamatte.utils.internet.ImagePostUtility;
import com.codeairs.wattamatte.utils.internet.NetworkUtils;
import com.codeairs.wattamatte.utils.internet.PhotoMultipartRequest;
import com.codeairs.wattamatte.utils.internet.PlaceDetailsAPI;
import com.codeairs.wattamatte.utils.internet.VolleySingleton;
import com.google.gson.Gson;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.theartofdev.edmodo.cropper.CropImage;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import cn.pedant.SweetAlert.SweetAlertDialog;
import id.zelory.compressor.Compressor;

public class EditProfileActivity extends AppCompatActivity implements View.OnClickListener, ImagePostUtility.OnImagePostedCallback {

    private static final String TAG = EditProfileActivity.class.getSimpleName();
    public static final String PROFILE_DETAILS = "com.codeairs.wattamatte.activity.EditProfileActivity";

    private static int REQUEST_CODE;

    private MyProfile profile;
    private EditText firstName, lastName, dob, profession, budgetMin, budgetMax, aboutMe;
    private AutoCompleteTextView searchCity, city;
    private CircularImageView changePic;

    private SweetAlertDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AndroidUtils.changeStatusBarToWhite(getWindow(), this);
        setContentView(R.layout.activity_edit_profile);
        profile = new Gson().fromJson(getIntent()
                .getStringExtra(PROFILE_DETAILS), MyProfile.class);
        initControls();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        CropImage.ActivityResult result = CropImage.getActivityResult(data);
        if (resultCode == RESULT_OK) {
            Uri resultUri = result.getUri();
            Log.d(TAG, "Image Cropping Success : " + resultUri);
            changePic.setImageURI(resultUri);
            uploadPic(resultUri);
        } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
            Exception error = result.getError();
            Log.d(TAG, "Image Cropping Failure : " + error.getMessage());
        }
    }

    private void initControls() {
        initToolbar();
        initChangePicButton();
        initEditTexts();
        initDialog();
        findViewById(R.id.save).setOnClickListener(v -> postResults());
        initContacts();
    }

    private void initContacts() {
        ImageView facebook = findViewById(R.id.contact).findViewById(R.id.contactFB);
        if (SharedPrefManager.getLoginType(this) == Constants.LOGIN_TYPE_FACEBOOK) {
            facebook.setImageResource(R.drawable.facebook_active);
        } else {
            facebook.setImageResource(R.drawable.facebook_inactive);
        }
    }

    private void initToolbar() {
        Toolbar toolbar = AndroidUtils.addToolbarWithBack(this);
        toolbar.setNavigationOnClickListener(v -> onBackPressed());
    }

    private void initChangePicButton() {
        changePic = findViewById(R.id.circleImage);
        ImageButton plus = findViewById(R.id.plusButton);

        if (profile.picture != null && profile.picture.getId() != null) {
            AppController.getImageLoader().DisplayImage(profile.picture.getId(), changePic, R.drawable.ph_profile_pic);
            plus.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_pencil_white_bg_dsb));
            changePic.setBorderColor(ContextCompat.getColor(this, R.color.darkSlateBlue));
        } else
            AppController.getPicasso().load(R.drawable.ph_profile_pic).into(changePic);

        changePic.setOnClickListener(this);
        plus.setOnClickListener(this);
    }

    private void initDialog() {
        pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(ContextCompat.getColor(this, R.color.darkSlateBlue));
        pDialog.setTitleText("Saving, Please wait...");
        pDialog.setCancelable(false);
    }

    private void initEditTexts() {
        firstName = findViewById(R.id.firstname);
        firstName.setText(profile.firstName);

//        lastName = findViewById(R.id.name);
//        lastName.setText(profile.lastName);

        dob = findViewById(R.id.dob);
        dob.setText(DateUtils.toFormat(profile.birthDate, DateUtils.PRIMARY_DATE_FORMAT));

//        city = findViewById(R.id.city);
//        city.setText(profile.address.city);

        profession = findViewById(R.id.profession);
        if (profile.post.equals("salarié")) {
            profession.setHint("Profession");
            profession.setText(profile.job);
        } else if (profile.post.equals("En recherche")) {
            profession.setHint("Université");
            profession.setText(profile.university);
        }

        searchCity = findViewById(R.id.searchCity);
        searchCity.setText(CommonUtils.getValueFromAdditionalInfos(
                profile.additionInfos, Identifiers.LOCATION));

        String bMin = CommonUtils.getValueFromAdditionalInfos(
                profile.additionInfos, Identifiers.BUDGET_MIN);

        String bMax = CommonUtils.getValueFromAdditionalInfos(
                profile.additionInfos, Identifiers.BUDGET_MAX);

        budgetMin = findViewById(R.id.budgetMin);
        budgetMin.setText(bMin);

        budgetMax = findViewById(R.id.budgetMax);
        budgetMax.setText(bMax);

        aboutMe = findViewById(R.id.aboutMe);
        aboutMe.setText(profile.about);

        setUpBudgetFilters();

        initSearchCity();
//        initCityOfResidence();


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgButton:
            case R.id.plusButton:
                handleChangePic();
                break;
        }
    }

    private void handleChangePic() {
        REQUEST_CODE = RequestCode.PersonalInfoProfilePic.id();
        CommonUtils.startImagePicker(this);
    }

    private void initSearchCity() {
        searchCity.setAdapter(new PlacesAutoCompleteAdapter(this, R.layout.autocomplete_list_item));
        searchCity.setOnItemClickListener((parent, view1, position, id) -> {
            Place place = (Place) parent.getItemAtPosition(position);
            searchCity.setText(place.location);
        });
    }

    private void initCityOfResidence() {
        city.setAdapter(new PlacesAutoCompleteAdapter(this, R.layout.autocomplete_list_item));
        city.setOnItemClickListener((parent, view1, position, id) -> {
            Place place = (Place) parent.getItemAtPosition(position);
            city.setText(place.location);
            new PlaceDetailsTask().execute(place.placeId);
        });
    }

    class PlaceDetailsTask extends AsyncTask<String, Void, Address> {

        protected Address doInBackground(String... urls) {
            try {
                PlaceDetailsAPI api = new PlaceDetailsAPI();
                return api.getDetails(urls[0]);
            } catch (Exception e) {
                return null;
            }
        }

        protected void onPostExecute(Address address) {
//            Address cityAddress = address;
        }

    }

    private void setUpBudgetFilters() {
        budgetMin.setFilters(new InputFilter[]{new InputFilterMinMax(
                Constants.BUDGET_MIN, Constants.BUDGET_MAX)});
        budgetMax.setFilters(new InputFilter[]{new InputFilterMinMax(
                Constants.BUDGET_MIN, Constants.BUDGET_MAX)});
        budgetMin.setOnFocusChangeListener(new OnFocusChangeListenerMinMax(
                Constants.BUDGET_MIN, true, budgetMax));
        budgetMax.setOnFocusChangeListener(new OnFocusChangeListenerMinMax(
                Constants.BUDGET_MAX, false, budgetMin));
    }

    private EditProfile getPostObject() {
        EditProfile edit = new EditProfile();
        edit.userId = SharedPrefManager.getUserId(this);
        edit.dateOfBirth = dob.getText().toString();
        edit.firstName = firstName.getText().toString();
        edit.lastName = profile.lastName; // lastName.getText().toString();
        edit.job = profession.getText().toString();
        edit.villeRecherche = searchCity.getText().toString();
        edit.about = aboutMe.getText().toString();
        edit.registrationToken = SharedPrefManager.getFirebaseToken(this);
        edit.budgetMin = Integer.valueOf(budgetMin.getText().toString());
        edit.budgetMax = Integer.valueOf(budgetMax.getText().toString());
        edit.address = new Address(profile.address.city); // new Address(city.getText().toString());
        return edit;
    }

    private void postResults() {
        try {
            if (!(Integer.parseInt(budgetMin.getText().toString()) >= Constants.BUDGET_MIN && Integer.parseInt(budgetMax.getText().toString()) <= Constants.BUDGET_MAX)) {
                Toast.makeText(this, getResources().getString(R.string.minimum_budget), Toast.LENGTH_SHORT).show();
                return;
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        String json = new Gson().toJson(getPostObject());
        Log.d(TAG, "Profile Url : " + ApiPaths.USER_PROFILE);
        Log.d(TAG, "Profile JSON Posting : " + json);
        pDialog.show();
        try {
            JSONObject body = new JSONObject(json);
            JsonObjectRequest request = NetworkUtils.authenticatedPutRequest(
                    this, ApiPaths.USER_PROFILE, body,
                    this::handleResponse, this::handleError);
            request.setShouldCache(false);
            VolleySingleton.getInstance(this).addToRequestQueue(request);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void handleResponse(JSONObject response) {
        Log.d(TAG, "Profile Response" + response.toString());
        pDialog.dismiss();
        ApiResponse<MyProfile> res = JsonUtils.fromJson(response.toString(), MyProfile.class);
        if (!res.success) {
            new SweetAlertDialog(this)
                    .setTitleText(res.error.message)
                    .show();
        }
        ProfileFragment.needsRefresh = true;
        onBackPressed();
//        Toast.makeText(this, R.string.update_success, Toast.LENGTH_SHORT).show();
    }

    private void handleError(VolleyError error) {
        pDialog.dismiss();
        Toast.makeText(this, "Trouble connecting to your wattamatte servers. " + "Please check your internet connection", Toast.LENGTH_LONG).show();
        if (error != null && error.getMessage() != null)
            Log.d(TAG, "Profile Picture Error Response : " + error.getMessage());
        if (error.networkResponse == null) {
            return;
        }

        String body;
        //get status code here
        String statusCode = String.valueOf(error.networkResponse.statusCode);
        //get response body and parse with appropriate encoding
        if (error.networkResponse.data != null) {
            try {
                body = new String(error.networkResponse.data, "UTF-8");
                Log.d(TAG, "Profile Error Response : " + body);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
    }

    private void uploadPic(Uri resultUri) {
        pDialog.show();
        File compressedImageFile;
        File actualFile = new File(resultUri.getPath());
        try {
            compressedImageFile = new Compressor(this).compressToFile(actualFile);
        } catch (IOException e) {
            e.printStackTrace();
            compressedImageFile = actualFile;
        }
        PhotoMultipartRequest imageUploadReq = new PhotoMultipartRequest(
                SharedPrefManager.getUserId(this),
                ApiPaths.PROFILE_UPLOAD_PIC,
                (Response.Listener<String>) response -> {
                    if (!TextUtils.isEmpty(response))
                        Log.d("profilePic.Response", response);
                    pDialog.hide();
                    AppController.getImageLoader().clearCache();
                }, error -> handleError(error), compressedImageFile);
        VolleySingleton.getInstance(this).addToRequestQueue(imageUploadReq);
    }

    @Override
    public void onImageUploaded(int statusCode, String respone) {
        pDialog.hide();
        Log.d(TAG, "ProfilePicture Response : " + respone);
    }

    @Override
    public void onImageUploadError(int statusCode, String errorResponse) {
        pDialog.hide();
        Log.d(TAG, "ProfilePicture Error Response : " + errorResponse);
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppController.setForeground();
    }

    @Override
    protected void onPause() {
        super.onPause();
        AppController.setBackground();
    }
}
