package com.codeairs.wattamatte.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.codeairs.wattamatte.AppController;
import com.codeairs.wattamatte.R;
import com.codeairs.wattamatte.adapters.BadgesRecyclerViewAdapter;
import com.codeairs.wattamatte.adapters.FavRoomsRecyclerViewAdapter;
import com.codeairs.wattamatte.adapters.ProfileClipartAdapter;
import com.codeairs.wattamatte.constants.ApiPaths;
import com.codeairs.wattamatte.constants.Constants;
import com.codeairs.wattamatte.constants.Identifiers;
import com.codeairs.wattamatte.models.ApiResponse;
import com.codeairs.wattamatte.models.Item;
import com.codeairs.wattamatte.models.SuccessMsg;
import com.codeairs.wattamatte.models.Tenant.Tenant;
import com.codeairs.wattamatte.models.contacts.Contact;
import com.codeairs.wattamatte.models.rooms.Property;
import com.codeairs.wattamatte.utils.AndroidUtils;
import com.codeairs.wattamatte.utils.CommonUtils;
import com.codeairs.wattamatte.utils.JsonUtils;
import com.codeairs.wattamatte.utils.SharedPrefManager;
import com.codeairs.wattamatte.utils.SpacesItemDecoration;
import com.codeairs.wattamatte.utils.internet.NetworkUtils;
import com.codeairs.wattamatte.utils.internet.VolleySingleton;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;
import mehdi.sakout.fancybuttons.FancyButton;

import static com.codeairs.wattamatte.utils.CommonUtils.getAnimalsImgResId;
import static com.codeairs.wattamatte.utils.CommonUtils.getChildrenImgResId;
import static com.codeairs.wattamatte.utils.CommonUtils.getProfessionImgResId;
import static com.codeairs.wattamatte.utils.CommonUtils.getSmokerImgRedId;

public class TenantDetailsActivity extends AppCompatActivity
        implements AppBarLayout.OnOffsetChangedListener, View.OnClickListener {

    static final String PARAM_DETAILS = "com.codeairs.wattamatte.activity.TenantDetailsActivity.PARAM_DETAILS";
    static final String PARAM_FAV = "com.codeairs.wattamatte.activity.TenantDetailsActivity.PARAM_FAV";
    private static final String TAG = TenantDetailsActivity.class.getSimpleName();

    private int scrollRange = -1;

    private View content;
    private Button seeMore;
    private View moreItems;
    private ToggleButton favTG;
    private TextView nameTitle;
    private SweetAlertDialog pDialog;

    private CoordinatorLayout layout;
    private FancyButton contact;

    private Tenant tenant;
    private Boolean isFav;
    private String userId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AndroidUtils.setStatusBarAppearance(getWindow(), true,
                android.R.color.transparent);
        setContentView(R.layout.activity_tenant_details);

        Intent intent = getIntent();

        if (intent.hasExtra(Constants.BUNDLE_USER_ID)) {
            userId = intent.getStringExtra(Constants.BUNDLE_USER_ID);
            getProfileDetails(userId);
            return;
        }

        tenant = new Gson().fromJson(intent.getStringExtra(PARAM_DETAILS), Tenant.class);

        if (intent.hasExtra(PARAM_FAV))
            isFav = intent.getBooleanExtra(PARAM_FAV, false);

        initControls();
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
        if (scrollRange == -1)
            scrollRange = appBarLayout.getTotalScrollRange();

        if (scrollRange + verticalOffset < 60) {
            AndroidUtils.changeStatusBarIconColor(getWindow(), true);
            nameTitle.setVisibility(View.VISIBLE);
        } else {
            AndroidUtils.changeStatusBarIconColor(getWindow(), false);
            nameTitle.setVisibility(View.GONE);
        }
    }

    private void getProfileDetails(String userId) {
        Log.d(TAG, "Profile Url : " + ApiPaths.USER_PROFILE + "/" + userId);
        SweetAlertDialog progressDialog = CommonUtils.getDialog(TenantDetailsActivity.this, "Loading");
        SweetAlertDialog retryDialog = CommonUtils.getRetryDialog(TenantDetailsActivity.this, "Some internet connectivity issues!!", "Retry", "Go back");
        retryDialog.setConfirmClickListener(sweetAlertDialog -> {
            retryDialog.dismiss();
            getProfileDetails(userId);
        });
        retryDialog.setCancelClickListener(sweetAlertDialog -> onBackPressed());
        progressDialog.show();
        StringRequest request = new StringRequest(Request.Method.GET,
                ApiPaths.USER_PROFILE + "/" + userId,
                response -> {
                    Log.d(TAG, "User Response : " + response);
                    ApiResponse<Tenant> features = JsonUtils.fromJson(response, Tenant.class);
                    if (features.success) {
                        tenant = features.data;
                        initControls();
                    }
                    content.setVisibility(View.VISIBLE);
                    progressDialog.dismissWithAnimation();
                },
                error -> {
                    progressDialog.dismissWithAnimation();
                    retryDialog.show();
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return ImmutableMap.<String, String>builder()
                        .put("userId", SharedPrefManager.getUserId(getApplicationContext()))
                        .put("Authorization", ApiPaths.TOKEN)
                        .build();
            }
        };
        VolleySingleton.getInstance(this).addToRequestQueue(request);
    }

    private void initControls() {
        AppBarLayout appBarLayout = findViewById(R.id.app_bar);
        appBarLayout.addOnOffsetChangedListener(this);

        layout = findViewById(R.id.layout);
        nameTitle = findViewById(R.id.nameTitle);

        content = findViewById(R.id.content);
        seeMore = findViewById(R.id.seeMore);
        seeMore.setOnClickListener(this);
        moreItems = findViewById(R.id.moreItems);
        initContacts();
        initTextsViews();
        initPic();
        initBadges();
        initInterests();
        initStyles();
        initMateStyles();
        initFavoritesToggle();
        if (isFav == null)
            fetchFavorites();
        fetchFavoriteRooms();
        fetchContacts();
        pDialog = CommonUtils.getDialog(this, getString(R.string.please_wait));
    }

    private void initFavoritesToggle() {
        favTG = findViewById(R.id.favoritesToggleButton);
        if (isFav != null)
            favTG.setChecked(isFav);
        favTG.setOnCheckedChangeListener((buttonView, isChecked) -> {
            JsonObjectRequest request = NetworkUtils.toggleTenantFavoritesRequest(
                    this, tenant.userId, isChecked,
                    response -> {
                        Log.d(TAG, "FavToggle.Response : " + response.toString());
                        // do nothing
                    }, error -> {
                        Log.d(TAG, "FavToggle.ErrorResponse : " + "Error ");
                    });
            VolleySingleton.getInstance(this).addToRequestQueue(request);
        });
    }

    private void fetchFavorites() {
        favTG.setVisibility(View.INVISIBLE);
        Log.d(TAG, "Favourite Fav Tenant Details Url : " + ApiPaths.USER_GET_ALL_FAVORITES + tenant.userId);
        StringRequest request = NetworkUtils.authenticatedGetRequest(getApplicationContext(), ApiPaths.USER_GET_ALL_FAVORITES + tenant.userId, this::handleFavouriteUserResponse, this::handleFavouriteUserError);
        request.setShouldCache(false);
        VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(request);
    }

    private void handleFavouriteUserResponse(String response) {
        Log.d(TAG, "Favourite Users Response : " + response);
        favTG.setChecked(false);
        ApiResponse<List<Tenant>> res = JsonUtils.fromJsonList(response, Tenant.class);
        if (res.success) {
            for (Tenant tenant : res.data) {
                if (TextUtils.equals(tenant.userId, this.tenant.userId)) {
                    favTG.setChecked(true);
                    break;
                }
            }
            favTG.setVisibility(View.VISIBLE);
        }
    }

    private void handleFavouriteUserError(VolleyError error) {
        Log.d(TAG, "Favourite Users Error : " + error.getMessage());
    }

    private void initContactButton(boolean isFriend) {
        contact = content.findViewById(R.id.contactRequest);
        contact.setVisibility(View.VISIBLE);
        if (isFriend) {
            changeContactButtonToMessage();
            return;
        }

        contact.setOnClickListener(v -> {
            pDialog.show();
            JsonObjectRequest request = NetworkUtils.sendContactRequest(
                    this, tenant.userId,
                    response -> {
                        Log.d("contactRequest", response.toString());
                        pDialog.dismiss();
                        ApiResponse<SuccessMsg> res = JsonUtils.fromJson(response.toString(), SuccessMsg.class);
                        if (!res.success && !res.succes) {
                            Snackbar.make(layout, res.error.message, Snackbar.LENGTH_SHORT).show();
                        } else {
                            Snackbar.make(layout, res.data.msg, Snackbar.LENGTH_SHORT).show();
//                            changeContactButtonToMessage();
                        }
                    },
                    error -> {

                    });
            VolleySingleton.getInstance(this).addToRequestQueue(request);
        });
    }

    private void changeContactButtonToMessage() {
        contact.setText(getString(R.string.message));
        contact.setIconResource("");
        contact.setOnClickListener(gotoChatListener);
    }

    private View.OnClickListener gotoChatListener = view -> {
        Intent intent = new Intent(this, ChatActivity.class);
        intent.putExtra(ChatActivity.PARAM_DETAILS, new Gson().toJson(tenant));
        startActivity(intent);
    };

    private void initPic() {
        ImageView iv = findViewById(R.id.expandedImage);
        if (tenant != null && tenant.picture != null && tenant.picture.getId() != null)
            AppController.getImageLoader().DisplayImage(tenant.picture.getId(), iv, R.drawable.splash_logo);
        ConstraintLayout contentLayout = content.findViewById(R.id.contentLayout);
        ViewTreeObserver vto = contentLayout.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                contentLayout.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                int height = contentLayout.getMeasuredHeight();
                int screenHeight = AndroidUtils.getScreenHeight(TenantDetailsActivity.this);
                AndroidUtils.setViewHeight(iv, screenHeight - height);
            }
        });
    }

//    private void setTextViewColor(Bitmap bitmap) {
//        Palette.from(bitmap).generate(palette -> {
//            Palette.Swatch vibrantSwatch = palette.getVibrantSwatch();
//
//            int defaultValue = ContextCompat.getColor(this, R.color.white);
//            int vibrant = palette.getVibrantColor(defaultValue);
//            int vibrantLight = palette.getLightVibrantColor(defaultValue);
//            int vibrantDark = palette.getDarkVibrantColor(defaultValue);
//            int muted = palette.getMutedColor(defaultValue);
//            int mutedLight = palette.getLightMutedColor(defaultValue);
//            int mutedDark = palette.getDarkMutedColor(defaultValue);
//            if (vibrantSwatch != null) {
//                name.setTextColor(vibrantSwatch.getBodyTextColor());
//                job.setTextColor(vibrantSwatch.getBodyTextColor());
//            }
//
//        });
//    }

//    public Palette createPaletteSync(Bitmap bitmap) {
//        Palette p = Palette.from(bitmap).generate();
//        return p;
//    }

    private void initTextsViews() {
        TextView name = findViewById(R.id.name);
        name.setText(CommonUtils.getNameWithAge(tenant));
        TextView job = findViewById(R.id.job);
        job.setText(tenant.job);
        nameTitle.setText(tenant.firstName);

        AndroidUtils.setText(content, R.id.location,
                CommonUtils.getValueFromAdditionalInfos(tenant.additionInfos, Identifiers.LOCATION));
        AndroidUtils.setText(content, R.id.budget, CommonUtils.getBudget(tenant));
        AndroidUtils.setText(content, R.id.aboutMe, tenant.about);
    }

    private void initBadges() {
        RecyclerView recyclerView = initList(R.id.badges, R.drawable.ic_badges_red_20dp,
                getString(R.string.badges), 16, 16);
        recyclerView.setAdapter(new BadgesRecyclerViewAdapter(Lists.newArrayList(
                new Item(tenant.job, getProfessionImgResId(tenant.job)),
                new Item(getString(R.string.children), getChildrenImgResId(tenant.additionInfos)),
                new Item(getString(R.string.cigarettes), getSmokerImgRedId(tenant.additionInfos)),
                new Item(getString(R.string.animals), getAnimalsImgResId(tenant.additionInfos))
        ), this));
    }

    private void initInterests() {
        List<Item> items = CommonUtils.getHobbyItems(tenant.hobbies);
        RecyclerView recyclerView = initList(R.id.interests, R.drawable.ic_like_red_20dp,
                getString(R.string.interest), 0, 12);
        recyclerView.setAdapter(new ProfileClipartAdapter(items, this));
    }

    private void initStyles() {
        List<Item> items = CommonUtils.getMineFeatureItems(tenant.features);
        RecyclerView recyclerView = initList(R.id.styles, R.drawable.ic_boy_red_20dp,
                getString(R.string.styles), 0, 12);
        recyclerView.setAdapter(new ProfileClipartAdapter(items, this));
    }

    private void initMateStyles() {
        List<Item> items = CommonUtils.getMateFeatureItems(tenant.features);
        RecyclerView recyclerView = initList(R.id.mateStyles, R.drawable.ic_bulb_red_20dp,
                getString(R.string.mate_styles), 0, 12);
        recyclerView.setAdapter(new ProfileClipartAdapter(items, this));
    }

    private void initFavRooms(List<Property> properties) {
        content.findViewById(R.id.favRooms).setVisibility(View.VISIBLE);
        RecyclerView recyclerView = initList(R.id.favRooms, R.drawable.ic_condo_red_20dp,
                MessageFormat.format(getString(R.string.favorites_common), properties.size()),
                0, 12);
        recyclerView.setAdapter(new FavRoomsRecyclerViewAdapter(properties, this));
    }

    private RecyclerView initList(int layoutResId, int iconResId,
                                  String title, int paddingPx, int paddingEdgesPx) {
        View badges = content.findViewById(layoutResId);

        badges.findViewById(R.id.edit).setVisibility(View.GONE);

        ImageView icon = badges.findViewById(R.id.icon);
        icon.setImageResource(iconResId);

        TextView tv = badges.findViewById(R.id.tv);
        tv.setText(title);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this,
                LinearLayoutManager.HORIZONTAL, false);

        RecyclerView recyclerView = badges.findViewById(R.id.list);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addItemDecoration(new SpacesItemDecoration(paddingPx, paddingEdgesPx));
        recyclerView.setAdapter(new FavRoomsRecyclerViewAdapter(null, this));

        return recyclerView;
    }

    private void initContacts() {
        ImageView facebook = findViewById(R.id.contact).findViewById(R.id.contactFB);
        if (tenant.facebook != null) {
            facebook.setImageResource(R.drawable.facebook_active);
        } else {
            facebook.setImageResource(R.drawable.facebook_inactive);
        }
    }

    private void fetchFavoriteRooms() {
        Log.d(TAG, "Favourite property Url : " + ApiPaths.PROPERTY_GET_ALL_FAVORITES + tenant.userId);
        StringRequest request = new StringRequest(Request.Method.GET,
                ApiPaths.PROPERTY_GET_ALL_FAVORITES + tenant.userId,
                response -> {
                    Log.d(TAG, "Favourite property response : " + response);
                    List<Property> properties = new ArrayList<>();
                    try {
                        JSONObject res = new JSONObject(response);
                        JSONArray data = res.getJSONArray("data");
                        for (int i = 0; i < data.length(); i++) {
                            JSONObject roomObject = data.getJSONObject(i);
                            properties.add(new Gson().fromJson(roomObject.toString(), Property.class));
                            Log.d(TAG,"Favourite Room : "+ properties.get(properties.size()-1).name);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    initFavRooms(properties);
                },
                error -> {
                }) {

            @Override
            public Map<String, String> getHeaders() {
                return NetworkUtils.authenticatedHeader(TenantDetailsActivity.this);
            }

        };
        request.setShouldCache(true);
        VolleySingleton.getInstance(this).addToRequestQueue(request);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.seeMore:
                handleSeeMoreClick();
                break;
        }
    }

    private void handleSeeMoreClick() {
        seeMore.setVisibility(View.GONE);
        moreItems.setVisibility(View.VISIBLE);
    }

    @Override
    public void onBackPressed() {
        Intent returnIntent = new Intent();
        returnIntent.putExtra("userId", tenant.userId);
        returnIntent.putExtra("favStatus", favTG.isChecked());
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }

    private List<Property> filterFavRooms(List<Property> myProperties) {
        List<Property> favoritesInCommon = new ArrayList<>();
        for (Property myProperty : myProperties)
            for (Object hisFavRoomId : tenant.favoriteRooms) {
                Log.d(TAG, "Filter favourite Rooms : " + hisFavRoomId.toString());
                if (hisFavRoomId instanceof LinkedTreeMap) {
                    LinkedTreeMap<String, Object> map = (LinkedTreeMap<String, Object>) hisFavRoomId;
                    if (TextUtils.equals(myProperty.id, map.get("_id").toString()))
                        favoritesInCommon.add(myProperty);
                } else {
                    if (TextUtils.equals(myProperty.id, hisFavRoomId.toString()))
                        favoritesInCommon.add(myProperty);
                }
            }

        return favoritesInCommon;

    }

    private void fetchContacts() {
        JsonObjectRequest request = NetworkUtils.getContactsRequest(this,
                response -> {
                    Log.d(TAG, "contacts.res : " + response.toString());
                    ApiResponse<List<Contact>> res = JsonUtils.fromJsonList(response.toString(), Contact.class);
                    if (!res.success) {
                        //TODO: handle
                    }
                    for (Contact contact : res.data) {
                        if (TextUtils.equals(contact.user.userId, tenant.userId)) {
                            initContactButton(true);
                            return;
                        }
                    }
                    initContactButton(false);
                }, error -> {
                    Log.d(TAG, "contacts.res Error");
                });
        VolleySingleton.getInstance(this).addToRequestQueue(request);
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppController.setForeground();
    }

    @Override
    protected void onPause() {
        super.onPause();
        AppController.setBackground();
    }
}
