package com.codeairs.wattamatte.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.SparseArray;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.codeairs.wattamatte.AppController;
import com.codeairs.wattamatte.R;
import com.codeairs.wattamatte.constants.Constants;
import com.codeairs.wattamatte.fragments.ChatFragment;
import com.codeairs.wattamatte.fragments.ConversationFragment;
import com.codeairs.wattamatte.fragments.ProfileFragment;
import com.codeairs.wattamatte.fragments.RoomMatesFragment;
import com.codeairs.wattamatte.fragments.RoomsFragment;
import com.codeairs.wattamatte.fragments.RoomsListFragment;
import com.codeairs.wattamatte.fragments.dummy.DummyContent;
import com.codeairs.wattamatte.listeners.MainInteractionListener;
import com.codeairs.wattamatte.listeners.TenantInteractionListener;
import com.codeairs.wattamatte.models.NotificationData;
import com.codeairs.wattamatte.models.Tenant.Tenant;
import com.codeairs.wattamatte.models.rooms.Property;
import com.codeairs.wattamatte.utils.AndroidUtils;
import com.codeairs.wattamatte.utils.CommonUtils;
import com.codeairs.wattamatte.utils.SharedPrefManager;
import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Socket;
import com.google.gson.Gson;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;

import java.util.List;

import q.rorbin.badgeview.Badge;
import q.rorbin.badgeview.QBadgeView;

import static com.codeairs.wattamatte.activity.RoomDetailsActivity.PARAM_FAV;
import static com.codeairs.wattamatte.activity.RoomDetailsActivity.PARAM_ID;

public class MainActivity extends AppCompatActivity
        implements MainInteractionListener, RoomsListFragment.OnListFragmentInteractionListener, ViewPager.OnPageChangeListener,
        TenantInteractionListener, ConversationFragment.OnListConversationsListener, PermissionListener {

    private static final String TAG = "MainActivity";

    private static final int BOTTOM_NAVIGATION_ICON_TOP_MARGIN = 20;
    public static final int REQUEST_CODE_FILTER_ROOMS = 1001;
    public static final int REQUEST_CODE_TENANT_FAV_CHANGE = 1002;
    public static final int REQUEST_CODE_ROOM_FAV_CHANGE = 1003;
    public static final int REQUEST_CODE_ROOM_FAV_CHANGE_MY_PROFILE = 1004;
    public static final int REQUEST_CODE_FILTER_TENANTS = 1005;

    private boolean isNotificationReceived = false;
    private String notifiedUserId = "";
    private String notificationType = "";

    private CoordinatorLayout layout;
    private ViewPager viewPager;
    private BottomNavigationViewEx navigation;
    private MenuItem prevMenuItem;
    private ProgressBar progressBar;
    private ViewPagerAdapter pagerAdapter;
    private Intent notificationIntent;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = item -> {
        switch (item.getItemId()) {
            case R.id.navigation_mates:
                viewPager.setCurrentItem(0, false);
                break;
            case R.id.navigation_rooms:
                viewPager.setCurrentItem(1, false);
                break;
            case R.id.navigation_chat:
                viewPager.setCurrentItem(2, false);
                break;
            case R.id.navigation_profile:
                viewPager.setCurrentItem(3, false);
                break;
        }
        return true;
    };

    private Socket mSocket;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        AppController appController = AppController.getInstance();
        mSocket = appController.getmSocket();
        mSocket.on(Socket.EVENT_CONNECT_ERROR, onConnectError);
        mSocket.on(Socket.EVENT_CONNECT_TIMEOUT, onConnectError);
        mSocket.on(Socket.EVENT_CONNECT, onConnect);
        mSocket.on(Socket.EVENT_DISCONNECT, onDisconnect);
        mSocket.on("notif-" + SharedPrefManager.getUserId(this), onChatNotification);
        mSocket.on("tcontact-" + SharedPrefManager.getUserId(this), onContactRequest);
        mSocket.on("tfav-" + SharedPrefManager.getUserId(this), onFavMarked);
        mSocket.connect();

        notificationIntent = getIntent();
        adjustStatusBar();
        initControls();
        checkStoragePermission();
        checkIfNotificationTrigeredOpening();
    }

    @Override
    protected void onPause() {
        super.onPause();
        //mSocket.disconnect();
        Log.d(TAG, "App controller Background");
        AppController.setBackground();
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppController.setForeground();
    }

    @Override
    protected void onSaveInstanceState(Bundle oldInstanceState) {
        super.onSaveInstanceState(oldInstanceState);
        oldInstanceState.clear();
    }

    private void checkStoragePermission() {
        TedPermission.with(this)
                .setPermissionListener(this)
                .setDeniedTitle("Permission Required")
                .setDeniedMessage("Storage permission is required to display images properly. If you deny this permission, images may fail to load!!")
                .setPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .check();
    }

    private Badge addBadgeAt(int position, int number) {
        // add badge
        return new QBadgeView(this)
                .setBadgeNumber(number)
                .setBadgeBackgroundColor(ContextCompat.getColor(this, R.color.radicalRed))
                .setBadgeTextColor(ContextCompat.getColor(this, R.color.radicalRed))
                .setGravityOffset(30, 9, true)
                .setBadgeTextSize(4, true)
                .bindTarget(navigation.getBottomNavigationItemView(position))
                .setOnDragStateChangedListener(new Badge.OnDragStateChangedListener() {
                    @Override
                    public void onDragStateChanged(int dragState, Badge badge, View targetView) {
//                        if (Badge.OnDragStateChangedListener.STATE_SUCCEED == dragState)
//                            Toast.makeText(BadgeViewActivity.this, R.string.tips_badge_removed, Toast.LENGTH_SHORT).show();
                    }
                });
    }

    public Emitter.Listener onConnect = args -> {
        Log.d(TAG, "Socket Connected!");
        Log.d(TAG, "length: " + args.length);
        if (args == null) {
            return;
        }
        for (Object arg : args) {
            Log.d(TAG, arg.toString());
        }
    };

    private Emitter.Listener onConnectError = args -> runOnUiThread(() -> {
        Log.d(TAG, "Socket Connection Error");
    });

    private Emitter.Listener onContactRequest = args -> runOnUiThread(() -> {
        addBadgeAt(2, 1);
        Log.d(TAG, "Badge Added for Contact Request");
    });

    private Emitter.Listener onChatNotification = args -> runOnUiThread(() -> {
        addBadgeAt(2, 1);
        Log.d(TAG, "Badge Added for Chat Notification");
    });

    private Emitter.Listener onDisconnect = args -> runOnUiThread(() -> {
        Log.d(TAG, "Socket Disconnected");
    });

    private Emitter.Listener onFavMarked = args -> runOnUiThread(() -> {
        Log.d(TAG, "Badge Added for Favourites Marked");
        if (args[0] != null) {
            Log.d(TAG, "Badge Added for Favourites Marked : The json data is : " + args[0]);
            addBadgeAt(3, 1);
//            try {
//                JSONObject messageJson = new JSONObject(args[0].toString());
//                JSONArray result = messageJson.getJSONArray("result");
//                for (int i = 0; i < result.length(); i++) {
//                    JSONObject jsonObject = result.getJSONObject(i);
//                    MyProfile user =  new Gson().fromJson(jsonObject
//                            .getJSONObject("user").toString(), MyProfile.class);
//
//                    messages.add(new Message(tenant.userId, me, jsonObject.getString("message"),
//                            null, new Date(), false));
//                }
//                runOnUiThread(() -> mMessageAdapter.notifyDataSetChanged());
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
        } else
            Log.d(TAG, "Badge Added for Favourites Marked : The json data is null");
    });

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
//            case REQUEST_CODE_FILTER_TENANTS:
            case REQUEST_CODE_FILTER_ROOMS:
            case REQUEST_CODE_TENANT_FAV_CHANGE:
            case REQUEST_CODE_ROOM_FAV_CHANGE:
            case REQUEST_CODE_ROOM_FAV_CHANGE_MY_PROFILE:
                passActivityResultToActiveFragment(requestCode, resultCode, data);
                break;
        }
    }

    private void passActivityResultToActiveFragment(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            int pos;
            if (requestCode == REQUEST_CODE_FILTER_ROOMS || requestCode == REQUEST_CODE_ROOM_FAV_CHANGE)
                pos = 1;
            else if (requestCode == REQUEST_CODE_ROOM_FAV_CHANGE_MY_PROFILE)
                pos = 3;
            else
                pos = 0;

            Fragment fragment = pagerAdapter.getRegisteredFragment(pos);
            if (null != fragment)
                fragment.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void adjustStatusBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR |
                            View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, android.R.color.transparent));
        }
    }

    private void initControls() {
        layout = findViewById(R.id.layout);
        initBottomNavigation();
        initViewPager();
        progressBar = findViewById(R.id.progressBar);
    }

    private void initBottomNavigation() {
        navigation = findViewById(R.id.navigation);
        navigation.setSelectedItemId(R.id.navigation_mates);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        navigation.enableShiftingMode(false);
        navigation.enableItemShiftingMode(false);
        navigation.setIconMarginTop(0, BOTTOM_NAVIGATION_ICON_TOP_MARGIN);
        navigation.setIconMarginTop(1, BOTTOM_NAVIGATION_ICON_TOP_MARGIN);
        navigation.setIconMarginTop(2, BOTTOM_NAVIGATION_ICON_TOP_MARGIN);
        navigation.setIconMarginTop(3, BOTTOM_NAVIGATION_ICON_TOP_MARGIN);
        navigation.setIconSize(32, 32);
    }

    private void initViewPager() {
        viewPager = findViewById(R.id.viewpager);
        pagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(pagerAdapter);
        viewPager.addOnPageChangeListener(this);
    }

    @Override
    public void onListFragmentInteraction(Property room, boolean isFavorite) {
        Intent intent = new Intent(this, RoomDetailsActivity.class);
        intent.putExtra(PARAM_ID, room.id);
        intent.putExtra(PARAM_FAV, isFavorite);
        startActivityForResult(intent, REQUEST_CODE_ROOM_FAV_CHANGE);
    }

    @Override
    public void goToTenantDetails(Tenant item, Boolean fav) {
        Intent intent = new Intent(this, TenantDetailsActivity.class);
        intent.putExtra(TenantDetailsActivity.PARAM_DETAILS, new Gson().toJson(item));
        intent.putExtra(TenantDetailsActivity.PARAM_FAV, fav);
        startActivityForResult(intent, REQUEST_CODE_TENANT_FAV_CHANGE);
    }

    @Override
    public void openChat(Tenant tenant) {
        Intent intent = new Intent(this, ChatActivity.class);
        intent.putExtra(ChatActivity.PARAM_DETAILS, new Gson().toJson(tenant));
        startActivity(intent);
    }

    @Override
    public void showSnackBar(String msg) {
        Snackbar snackbar = Snackbar.make(layout, msg, Snackbar.LENGTH_LONG);

        CoordinatorLayout.LayoutParams layoutParams = (CoordinatorLayout.LayoutParams) snackbar.getView().getLayoutParams();
        layoutParams.setAnchorId(R.id.navigation); // Id for your bottomNavBar or TabLayout
        layoutParams.anchorGravity = Gravity.TOP;
        layoutParams.gravity = Gravity.TOP;
        snackbar.getView().setLayoutParams(layoutParams);

        snackbar.show();
    }

    @Override
    public void onListConversationInteraction(DummyContent.DummyItem item) {
        startActivity(new Intent(this, TenantDetailsActivity.class));
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        if (prevMenuItem != null) {
            prevMenuItem.setChecked(false);
        } else {
            navigation.getMenu().getItem(0).setChecked(false);
        }
        navigation.getMenu().getItem(position).setChecked(true);
        prevMenuItem = navigation.getMenu().getItem(position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void showLoading() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void goToTab(int tabNo) {
        switch (tabNo) {
            case 0:
                navigation.setSelectedItemId(R.id.navigation_mates);
                break;
            case 1:
                navigation.setSelectedItemId(R.id.navigation_rooms);
                break;
            case 2:
                navigation.setSelectedItemId(R.id.navigation_chat);
                break;
            case 3:
                navigation.setSelectedItemId(R.id.navigation_profile);
                break;
        }
    }

    private class ViewPagerAdapter extends FragmentPagerAdapter {

        SparseArray<Fragment> registeredFragments = new SparseArray<>();

        ViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return new RoomMatesFragment();
                case 1:
                    return new RoomsFragment();
                case 2:
                    return new ChatFragment();
                default:
                    return new ProfileFragment();
            }
        }

        @Override
        public int getCount() {
            return 4;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            Fragment fragment = (Fragment) super.instantiateItem(container, position);
            registeredFragments.put(position, fragment);
            return fragment;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            registeredFragments.remove(position);
            super.destroyItem(container, position, object);
        }

        public Fragment getRegisteredFragment(int position) {
            return registeredFragments.get(position);
        }
    }

    // for the first two fragments
    public static void adjustToolbarHeight(View view, Context context) {
        ConstraintLayout parentConstraintLayout = view.findViewById(R.id.cLayout);

        ConstraintSet constraintSet = new ConstraintSet();
        constraintSet.clone(parentConstraintLayout);

        constraintSet.connect(R.id.search, ConstraintSet.TOP, ConstraintSet.PARENT_ID, ConstraintSet.TOP,
                AndroidUtils.dpToPx(context, 8) + AndroidUtils.getStatusBarHeight(context));

        constraintSet.applyTo(parentConstraintLayout);

        CollapsingToolbarLayout.LayoutParams params = new CollapsingToolbarLayout.LayoutParams(
                CollapsingToolbarLayout.LayoutParams.MATCH_PARENT,
                AndroidUtils.getStatusBarHeight(context)
        );
        Toolbar toolbar = view.findViewById(R.id.toolbar);
        toolbar.setLayoutParams(params);
    }

    @Override
    public void onPermissionGranted() {

    }

    @Override
    public void onPermissionDenied(List<String> deniedPermissions) {

    }

    @Override
    public boolean isNotificationReceived() {
        return isNotificationReceived;
    }

    @Override
    public boolean isChatNotification() {
        return (notificationType.equalsIgnoreCase(Constants.NOTIFICATION_MESSAGING) || notificationType.equalsIgnoreCase(Constants.NOTIFICATION_MESSAGING_FRENCH));
    }

    @Override
    public boolean isContactRequestNotification() {
        return (notificationType.equalsIgnoreCase(Constants.NOTIFICATION_CONTACT_REQUEST) || notificationType.equalsIgnoreCase(Constants.NOTIFICATION_CONTACT_REQUEST_FRENCH));
    }

    @Override
    public String getUserNotified() {
        return notifiedUserId;
    }

    @Override
    public void clearNotificationDatas() {
        isNotificationReceived = false;
        notifiedUserId = "";
        notificationType = "";
    }

    private void checkIfNotificationTrigeredOpening() {
        CommonUtils.dumpIntent(notificationIntent);
        if (!(notificationIntent.hasExtra(Constants.NOTIFICATION_BUNDLE) || notificationIntent.hasExtra("title"))) {
            return;
        }
        NotificationData notificationData = null;
        if (notificationIntent.hasExtra(Constants.NOTIFICATION_BUNDLE)) {
            notificationData = new Gson().fromJson(notificationIntent.getStringExtra(Constants.NOTIFICATION_BUNDLE), NotificationData.class);
        } else if (notificationIntent.hasExtra("title")) {
            notificationData = CommonUtils.getModelFromNotificationIntent(notificationIntent);
        }
        isNotificationReceived = true;
        notifiedUserId = notificationData.getNotifiedUserId();
        notificationType = notificationData.getNotificationType();
        if (notificationData.isChatNotification()) {
            goToTab(2);
        } else if (notificationData.isContactRequestNotification()) {
            goToTab(2);
        } else if (notificationData.isFavouriteNotification()) {
            Intent intent = new Intent(this, TenantDetailsActivity.class);
            intent.putExtra(Constants.BUNDLE_USER_ID, notifiedUserId);
            startActivityForResult(intent, REQUEST_CODE_TENANT_FAV_CHANGE);
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        notificationIntent = intent;
        checkIfNotificationTrigeredOpening();
    }
}