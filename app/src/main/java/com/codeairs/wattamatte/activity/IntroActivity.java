package com.codeairs.wattamatte.activity;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.codeairs.wattamatte.AppController;
import com.codeairs.wattamatte.R;
import com.viewpagerindicator.CirclePageIndicator;

public class IntroActivity extends AppCompatActivity implements View.OnClickListener {

    private ViewPager viewPager;
    private Button nextButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);
        initToolbar();
        initViewPager();
        initNextButton();
        findViewById(R.id.skipButton).setOnClickListener(this);
    }

    private void initToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
    }

    private void initViewPager() {
        viewPager = findViewById(R.id.viewPager);
        viewPager.setAdapter(new CustomPagerAdapter(this));

        CirclePageIndicator titleIndicator = findViewById(R.id.pageIndicator);
        titleIndicator.setViewPager(viewPager);
    }

    private void initNextButton() {
        nextButton = findViewById(R.id.nextButton);
        nextButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.nextButton:
                handleNextButtonClick();
                break;
            case R.id.skipButton:
                goToLogin();
                break;
        }
    }

    private void handleNextButtonClick() {
        if (viewPager.getCurrentItem() == 2) {
            goToLogin();
            return;
        } else {
            viewPager.setCurrentItem(viewPager.getCurrentItem() + 1, true);
        }
        if (viewPager.getCurrentItem() == 2) {
            nextButton.setText(R.string.lets_go);
        }
    }

    private void goToLogin() {
        finish();
        startActivity(new Intent(this, LoginActivity.class));
    }

    class CustomPagerAdapter extends PagerAdapter {

        private Context mContext;

        CustomPagerAdapter(Context context) {
            mContext = context;
        }

        @NonNull
        @Override
        public Object instantiateItem(@NonNull ViewGroup collection, int position) {
            LayoutInflater inflater = LayoutInflater.from(mContext);
            ViewGroup layout = (ViewGroup) inflater.inflate(R.layout.layout_intro_pager_item, collection, false);
            ImageView iv = layout.findViewById(R.id.image);
            TextView tv = layout.findViewById(R.id.text);
            switch (position) {
                case 0:
                    tv.setText(R.string.intro1_text);
                    iv.setImageResource(R.drawable.illustration1);
                    break;
                case 1:
                    tv.setText(R.string.intro2_text);
                    iv.setImageResource(R.drawable.illustration2);
                    break;
                case 2:
                    tv.setText(R.string.intro3_text);
                    iv.setImageResource(R.drawable.illustration3);
                    break;
            }
            collection.addView(layout);
            return layout;
        }

        @Override
        public void destroyItem(@NonNull ViewGroup collection, int position, @NonNull Object view) {
            collection.removeView((View) view);
        }

        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
            return view == object;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return "1";
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppController.setForeground();
    }

    @Override
    protected void onPause() {
        super.onPause();
        AppController.setBackground();
    }

}
