package com.codeairs.wattamatte.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.codeairs.wattamatte.AppController;
import com.codeairs.wattamatte.R;
import com.codeairs.wattamatte.adapters.ImageAdapter;
import com.codeairs.wattamatte.adapters.RoomDetailAdapter;
import com.codeairs.wattamatte.constants.ApiPaths;
import com.codeairs.wattamatte.fragments.dialogs.EmptyRentalRecordDialogFragment;
import com.codeairs.wattamatte.models.ApiResponse;
import com.codeairs.wattamatte.models.SuccessMsg;
import com.codeairs.wattamatte.models.profile.MyProfile;
import com.codeairs.wattamatte.models.rooms.ContactMailRequest;
import com.codeairs.wattamatte.models.rooms.RoomDetails;
import com.codeairs.wattamatte.utils.AndroidUtils;
import com.codeairs.wattamatte.utils.CommonUtils;
import com.codeairs.wattamatte.utils.JsonUtils;
import com.codeairs.wattamatte.utils.SharedPrefManager;
import com.codeairs.wattamatte.utils.UIHelper;
import com.codeairs.wattamatte.utils.internet.NetworkUtils;
import com.codeairs.wattamatte.utils.internet.VolleySingleton;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.rd.PageIndicatorView;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;

import static com.codeairs.wattamatte.activity.AskQuestionActivity.PARAM_PROFILE;
import static com.codeairs.wattamatte.activity.ImageSlideShowActivity.PARAM_IMAGES;
import static com.codeairs.wattamatte.activity.ImageSlideShowActivity.PARAM_START_POS;
import static com.codeairs.wattamatte.activity.ImageSlideShowActivity.PARAM_TITLE;
import static com.codeairs.wattamatte.utils.AndroidUtils.createDrawableFromView;

public class RoomDetailsActivity extends AppCompatActivity
        implements OnMapReadyCallback, LocationListener {

    public static final String PARAM_ID = "com.codeairs.wattamatte.activity.RoomDetailsActivity.id";
    public static final String PARAM_FAV = "com.codeairs.wattamatte.activity.RoomDetailsActivity.Fav";
    public static final String TAG = RoomDetailsActivity.class.getSimpleName();

    private CoordinatorLayout layout;
    private AppBarLayout mAppBarLayout;
    private View content;
    private ProgressBar progressBar;
    private ConstraintLayout toolbarChild;
    private PageIndicatorView pageIndicatorView;
    private ViewPager pager;
    private FloatingActionButton favoritesToggleButton;
    private RoomDetails property;

    private String roomId;

    private GoogleMap mMap;
    private LocationManager locationManager;

    private String[] generals = {
            "{0} Pièces", "Surface de {0}m2", "Situé au {0}{1} étage"
    };

    private String[] insides = {
            "{0} chambre(s)", "{0} Salle(s) de Bain", "{0} Toilette séparé",
            "Cuisine Américaine",
//            "Parking privé", "Handicapé", "Chauffage collectif"
    };

    private String[] others = {
            "Proche Commerce", "Métro Gratte ciel à 5min"
    };

    private boolean isFavorite;
    private MyProfile profile;

    private SweetAlertDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AndroidUtils.setStatusBarAppearance(getWindow(), true, android.R.color.transparent);
        setContentView(R.layout.activity_room_details);

        roomId = getIntent().getStringExtra(PARAM_ID);
        isFavorite = getIntent().getBooleanExtra(PARAM_FAV, false);

        initControls();
        fetch();
        fetchProfile();
        setUpRoomLocation();
    }

    private void initControls() {
        layout = findViewById(R.id.layout);
        mAppBarLayout = findViewById(R.id.app_bar);
        content = findViewById(R.id.roomDetails);
        progressBar = findViewById(R.id.progressBar);
        toolbarChild = findViewById(R.id.toolbarChild);
        Toolbar toolbar = findViewById(R.id.toolbar);
        AndroidUtils.setViewHeight(toolbar, AndroidUtils.getStatusBarHeight(this));
        pager = findViewById(R.id.viewpager);
        initFavToggle();
        findViewById(R.id.applyRequest).setOnClickListener(v -> sendContactMail(1));
        findViewById(R.id.appointmentRequest).setOnClickListener(v -> sendContactMail(2));
        findViewById(R.id.contactRequest).setOnClickListener(v -> {
            Intent intent = new Intent(this, AskQuestionActivity.class);
            intent.putExtra(AskQuestionActivity.PARAM_ID, roomId);
            intent.putExtra(PARAM_PROFILE, new Gson().toJson(profile));
            startActivity(intent);
        });
        pDialog = CommonUtils.getDialog(this, getString(R.string.please_wait));
    }

    private void fetchProfile() {
        Log.d(TAG, "Profile Fetching Url : " + ApiPaths.USER_PROFILE + "/" + SharedPrefManager.getUserId(this));
        StringRequest request = new StringRequest(Request.Method.GET,
                ApiPaths.USER_PROFILE + "/" + SharedPrefManager.getUserId(this),
                response -> {
                    if (response != null) Log.d(TAG, "Response : " + response);
                    ApiResponse<MyProfile> features = JsonUtils.fromJson(response, MyProfile.class);
                    if (features.success) {
                        profile = features.data;
                    }
                }, error -> {
        }) {
            @Override
            public Map<String, String> getHeaders() {
                return NetworkUtils.authenticatedHeader(RoomDetailsActivity.this);
            }
        };
        request.setShouldCache(true);
        VolleySingleton.getInstance(this).addToRequestQueue(request);
    }

    private void setUpRoomLocation() {
        SupportMapFragment mapFragment = (SupportMapFragment)
                getSupportFragmentManager().findFragmentById(R.id.g_map);
        if (mapFragment != null) {
            locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
            mapFragment.getMapAsync(this);
        }
    }

    private void initFavToggle() {
        favoritesToggleButton = findViewById(R.id.favoritesToggleButton);
        resetFavoriteIcon();
        favoritesToggleButton.setOnClickListener(view -> {
            isFavorite = !isFavorite;
            resetFavoriteIcon();
            JsonObjectRequest request = NetworkUtils.toggleRoomFavoritesRequest(
                    this, roomId, isFavorite,
                    response -> {
                        Log.d(TAG, "Toggle Favourite Room Response : " + response.toString());
                    },
                    error -> {

                    });
            VolleySingleton.getInstance(this).addToRequestQueue(request);
        });
    }

    private void resetFavoriteIcon() {
        if (isFavorite) {
            favoritesToggleButton.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(),
                    R.drawable.ic_heart_filled_24dp));
        } else {
            favoritesToggleButton.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(),
                    R.drawable.ic_heart_unfilled_24dp));
        }
    }

    private void initList(int listRedId, String[] list) {
        RecyclerView recyclerView = content.findViewById(listRedId);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(new RoomDetailAdapter(Arrays.asList(list)));
    }

    private void initApply(String propertyId) {
        Button btn = content.findViewById(R.id.apply);
        btn.setOnClickListener(v -> {
            showEmptyRentenalRecordDialog(propertyId);
        });
        btn.setVisibility(View.INVISIBLE);
    }

    private void showEmptyRentenalRecordDialog(String propertyId) {
        FragmentManager manager = getSupportFragmentManager();
        EmptyRentalRecordDialogFragment fragment = EmptyRentalRecordDialogFragment.newInstance(propertyId);
        fragment.setStyle(DialogFragment.STYLE_NORMAL, R.style.DialogFragmentTheme);
        fragment.show(manager, "empty");
    }

    private void fetch() {
        Log.d(TAG, "Room Details Url :" + ApiPaths.PROPERTY_BY_ID + roomId);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, ApiPaths.PROPERTY_BY_ID + roomId,
                this::handleResponse, this::handleError) {

            @Override
            public Map<String, String> getHeaders() {
                return NetworkUtils.authenticatedHeader(RoomDetailsActivity.this);
            }
        };
        stringRequest.setShouldCache(false);
        VolleySingleton.getInstance(this).addToRequestQueue(stringRequest);
    }

    private void handleResponse(String response) {
        Log.d(TAG, "Room Details Response : " + response);

        progressBar.setVisibility(View.GONE);
        mAppBarLayout.setVisibility(View.VISIBLE);
        content.setVisibility(View.VISIBLE);

        ApiResponse<RoomDetails> res = JsonUtils.fromJson(response, RoomDetails.class);
        if (!res.success) {
            return;
        }

        property = res.data;

        AndroidUtils.setText(toolbarChild, R.id.name, property.name + " - " + property.surface + "m2");
        AndroidUtils.setText(toolbarChild, R.id.location, property.address.city);
        AndroidUtils.setText(toolbarChild, R.id.price, property.price + "€ C.C");
        AndroidUtils.setText(toolbarChild, R.id.tv1, property.pieces + "p");
        AndroidUtils.setText(toolbarChild, R.id.tv2, property.roomCount + "ch");
        AndroidUtils.setText(toolbarChild, R.id.tv3, property.surface + "m²");

        initPager(property);

        setUpGeneralInfo(property);
        setUpInsides(property);
        setUpOtherDetails(property);
        setUpRentInfo(property);

        markRoomLocationOnMap(property);
        initApply(property.id);

        Log.d("height", toolbarChild.getMeasuredHeight() + "");
        AndroidUtils.setMargins(content, 0, toolbarChild.getMeasuredHeight(), 0, 0);
    }

    private void handleError(VolleyError error) {
        Toast.makeText(this, "Trouble connecting to your wattamatte servers. " +
                "Please check your internet connection", Toast.LENGTH_LONG).show();
    }

    private void initPager(RoomDetails property) {
        pager.setAdapter(new ImageAdapter(this, property.pictures,
                pos -> {
                    Intent intent = new Intent(this, ImageSlideShowActivity.class);
                    intent.putExtra(PARAM_IMAGES, new Gson().toJson(property.pictures));
                    intent.putExtra(PARAM_TITLE, property.name);
                    intent.putExtra(PARAM_START_POS, pos);
                    startActivity(intent);
                }));

        pageIndicatorView = findViewById(R.id.pageIndicatorView);
        pageIndicatorView.setCount(property.pictures.size());
        pageIndicatorView.setSelection(0);
        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                pageIndicatorView.setSelected(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void setUpGeneralInfo(RoomDetails details) {
        generals[0] = MessageFormat.format(generals[0], details.pieces);
        generals[1] = MessageFormat.format(generals[1], details.surface);
        int floorNumber = Integer.valueOf(details.floorNumber);
        generals[2] = MessageFormat.format(generals[2], details.floorNumber,
                floorNumber == 1 ? "er" : "ème") + (floorNumber != 1 ? "" : "");
        initList(R.id.listGeneral, generals);
    }

    private void setUpInsides(RoomDetails details) {
        List<String> list = new ArrayList<>();
        list.add(MessageFormat.format(insides[0], details.roomCount));
        try {
            if (Integer.valueOf(details.showerRoomCount) > 0)
                list.add(MessageFormat.format(insides[1], details.showerRoomCount));
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            if (Integer.valueOf(details.bathRoomCount) > 0)
                list.add(MessageFormat.format(insides[2], details.bathRoomCount));
        } catch (Exception e) {
            e.printStackTrace();
        }
        list.add(details.kitchen);


//        insides[0] = MessageFormat.format(insides[0], details.roomCount);
//        insides[1] = MessageFormat.format(insides[2], details.bathRoomCount);
//        insides[2] = MessageFormat.format(insides[3], details.showerRoomCount);
//        insides[3] = details.kitchen;
//        insides[5] = details.staySurface;
        initList(R.id.listInside, list.toArray(new String[0]));
    }

    private void setUpOtherDetails(RoomDetails details) {
        others = new String[]{details.description};
        initList(R.id.listOther, others);
    }

    private void setUpRentInfo(RoomDetails property) {
        String info = getString(R.string.rent_info_dummy);
        info = MessageFormat.format(info, property.price,
                property.charge, property.fees, property.guarantee);
        AndroidUtils.setText(findViewById(R.id.roomDetails), R.id.description, info);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        setupMap();
        //paris coordinates
        LatLng seattle = new LatLng(48.8566, 2.3522);
        mMap.addMarker(new MarkerOptions().position(seattle).title("paris"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(seattle));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(14.0f));
    }

    @Override
    public void onLocationChanged(Location location) {
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 10);
        mMap.animateCamera(cameraUpdate);
        locationManager.removeUpdates(this);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    private void setupMap() {

        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mMap.setMyLocationEnabled(true);

        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        Criteria criteria = new Criteria();

        String provider = locationManager.getBestProvider(criteria, true);

        Location myLocation = locationManager.getLastKnownLocation(provider);
        mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
        double latitude = myLocation.getLatitude();
        double longitude = myLocation.getLongitude();
        LatLng latLng = new LatLng(latitude, longitude);

//        // Show the current location in Google Map
//        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
//
//        // Zoom in the Google Map
//        mMap.animateCamera(CameraUpdateFactory.zoomTo(20));
        mMap.addMarker(new MarkerOptions().position(latLng).title("You are here!"));
    }

    private void markRoomLocationOnMap(RoomDetails property) {
        LatLng latLng = new LatLng(Double.valueOf(property.address.latitude),
                Double.valueOf(property.address.longitude));

        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.layout_map_marker_oval, null);

        mMap.addMarker(new MarkerOptions()
                .position(latLng)
                .title(property.address.address)
                .icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(this, view))));

        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(14.0f));
    }

    @Override
    public void onBackPressed() {
        Intent returnIntent = new Intent();
        returnIntent.putExtra("propertyId", roomId);
        returnIntent.putExtra("favStatus", isFavorite);
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }


    private void sendContactMail(int type) {
        ContactMailRequest body = new ContactMailRequest(roomId, SharedPrefManager.getUserId(this),
                CommonUtils.getEmail(profile), profile.firstName, profile.phone, getContenu(type));
        Gson gson = new GsonBuilder().setDateFormat("dd/MM/yyyy").create();
        String requestBody = gson.toJson(body);
        Log.d(TAG, "Send Contact Mail Url : " + getApiPath(type));
        Log.d(TAG, "Send Contact Mail Body : " + requestBody);
        pDialog.show();
        try {
            JsonObjectRequest request = NetworkUtils.authenticatedPostRequest(
                    this, getApiPath(type), new JSONObject(requestBody),
                    response -> {
                        Log.d(TAG, "Send Contact Mail Response : " + response.toString());
                        pDialog.dismiss();
                        try {
                            ApiResponse<SuccessMsg> res = JsonUtils.fromJson(response.toString(), SuccessMsg.class);
                            if (res.success) {
                                /*if (type == 1)
                                    showEmptyRentenalRecordDialog(property.id);*/
                                Snackbar.make(layout, getString(R.string.room_contact_success), Snackbar.LENGTH_SHORT).show();
                            } else {
                                UIHelper.showErrorAlert(this, res.error.message);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    },
                    error -> {
                        pDialog.dismiss();
                        Log.d(TAG, "Send Contact Mail Error Response : " + error.getMessage());
                    });
            VolleySingleton.getInstance(this).addToRequestQueue(request);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private String getApiPath(int type) {
        switch (type) {
            case 1:
                return ApiPaths.MAIL_POSTULER;
            case 2:
                return ApiPaths.MAIL_REQUEST_APPOINTMENT;
            default:
                return ApiPaths.MAIL_CONTACT_WATTA;
        }
    }

    private String getContenu(int type) {
        switch (type) {
            case 1:
                return CommonUtils.getLocaleStringResource(Locale.FRANCE, R.string.apply, this);
            case 2:
                return CommonUtils.getLocaleStringResource(Locale.FRANCE, R.string.request_appointment, this);
            default:
                return CommonUtils.getLocaleStringResource(Locale.FRANCE, R.string.contacts, this);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppController.setForeground();
    }

    @Override
    protected void onPause() {
        super.onPause();
        AppController.setBackground();
    }
}
