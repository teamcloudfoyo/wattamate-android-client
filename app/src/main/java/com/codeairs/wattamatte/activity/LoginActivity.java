package com.codeairs.wattamatte.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.codeairs.wattamatte.AppController;
import com.codeairs.wattamatte.R;
import com.codeairs.wattamatte.constants.ApiPaths;
import com.codeairs.wattamatte.constants.Constants;
import com.codeairs.wattamatte.models.ApiResponse;
import com.codeairs.wattamatte.models.Login;
import com.codeairs.wattamatte.models.entry.FBGraphRequest;
import com.codeairs.wattamatte.models.entry.FBLogin;
import com.codeairs.wattamatte.utils.AndroidUtils;
import com.codeairs.wattamatte.utils.JsonUtils;
import com.codeairs.wattamatte.utils.SharedPrefManager;
import com.codeairs.wattamatte.utils.UIHelper;
import com.codeairs.wattamatte.utils.internet.NetworkUtils;
import com.codeairs.wattamatte.utils.internet.VolleySingleton;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.common.collect.ImmutableMap;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener, FacebookCallback<LoginResult> {

    private static final String TAG = "CheckingSocialMedia";
    private EditText email, password;
    private Button login;
    private SweetAlertDialog pDialog;
    private ImageButton fbButton;
    private CallbackManager callbackManager;
    private AccessToken mAccessToken;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AndroidUtils.setStatusBarAppearance(getWindow(), true, Color.WHITE);
        setContentView(R.layout.activity_login);
        initControls();
    }

    private void initControls() {
        login = findViewById(R.id.login);
        login.setOnClickListener(this);
        findViewById(R.id.register).setOnClickListener(this);
        email = findViewById(R.id.email);
        password = findViewById(R.id.password);
        initDialog();
        initFBButton();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.login:
                handleLoginTouch();
                break;
            case R.id.register:
                startActivity(new Intent(this, SignUpActivity.class));
        }
    }

    private void initFBButton() {
        callbackManager = CallbackManager.Factory.create();

        LoginButton fbLoginButton = findViewById(R.id.fb_login_button);
        fbLoginButton.setReadPermissions("public_profile", "email", "user_birthday", "user_gender");
        fbButton = findViewById(R.id.fbButton);
        fbButton.setOnClickListener(view -> fbLoginButton.performClick());

        fbLoginButton.registerCallback(callbackManager, this);
    }

    @Override
    public void onSuccess(LoginResult loginResult) {
        mAccessToken = loginResult.getAccessToken();
        getUserProfile(mAccessToken);
    }

    @Override
    public void onCancel() {

    }

    @Override
    public void onError(FacebookException error) {

    }

    private void getUserProfile(AccessToken currentAccessToken) {
        GraphRequest request = GraphRequest.newMeRequest(
                currentAccessToken, (object, response) -> {
                    Log.i(TAG, "Facebook Object : " + object.toString());
                    FBGraphRequest fbRes = new Gson().fromJson(object.toString(), FBGraphRequest.class);
                    authFacebook(fbRes);
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id, name, email, picture.width(200), first_name, last_name, birthday, gender");
        request.setParameters(parameters);
        request.executeAsync();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void initDialog() {
        pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(ContextCompat.getColor(this, R.color.darkSlateBlue));
        pDialog.setTitleText("Loading");
        pDialog.setCancelable(false);
    }

    private void handleLoginTouch() {
        if (!validate()) {
            return;
        }
        UIHelper.disableViews(email, password, login);
        pDialog.show();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("email", email.getText().toString());
            jsonObject.put("password", password.getText().toString());
            jsonObject.put("registrationToken", SharedPrefManager.getFirebaseToken(getApplicationContext()));
            doLogin(jsonObject.toString());
        } catch (JSONException e) {
            Log.e("SignUp", e.toString());
        }
    }

    private boolean validate() {
        boolean status = true;
        if (TextUtils.isEmpty(email.getText())) {
            email.setError(getString(R.string.email_needed));
            status = false;
        }
        if (TextUtils.isEmpty(password.getText())) {
            password.setError(getString(R.string.pass_needed));
            status = false;
        }
        return status;
    }

    private void doLogin(String json) {
        Log.i(TAG, "JSON: " + json);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiPaths.LOGIN,
                this::handleResponse, this::handleError) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() {
                try {
                    return json == null ? null : json.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", json, "utf-8");
                    return null;
                }
            }

            @Override
            public Map<String, String> getHeaders() {
                return ImmutableMap.<String, String>builder()
                        .put("Authorization", ApiPaths.TOKEN)
                        .build();
            }
        };
        stringRequest.setShouldCache(false);
        VolleySingleton.getInstance(this).addToRequestQueue(stringRequest);
    }

    private void handleResponse(String response) {
        Log.i(TAG, "Login.Response : " + response);
        pDialog.dismiss();
        ApiResponse<Login> res = JsonUtils.fromJson(response, Login.class);
        if (!res.success) {
            UIHelper.enableViews(email, password, login);
            if (res.data != null && res.data.msg != null && !res.data.msg.isEmpty())
                UIHelper.showErrorAlert(this, res.data.msg.get(0));
            return;
        }

        if (TextUtils.isEmpty(res.data.userId)) {
            UIHelper.showErrorAlert(this, "Invalid User"); // this is not likely to happen
            return;
        }

        SharedPrefManager.saveLoginType(this, Constants.LOGIN_TYPE_NORMAL);
        SharedPrefManager.saveUserId(this, res.data.userId);
        SharedPrefManager.saveProfilePic(this, res.data.profile);
        SharedPrefManager.saveProfilePicThumb(this, res.data.small);
        navigateAccordingly(res.data.step);
    }

    private void handleError(VolleyError error) {
        pDialog.dismiss();
        UIHelper.enableViews(email, password, login);
        Toast.makeText(this, "Trouble connecting to your wattamatte servers. " +
                "Please check your internet connection", Toast.LENGTH_LONG).show();
    }

    private void authFacebook(FBGraphRequest graphRequest) {
        pDialog.show();

        JSONObject requestBody = new JSONObject();
        try {
            JSONObject profile = new JSONObject();
            profile.put("id", graphRequest.id);
            profile.put("firstName", graphRequest.firstName);
            profile.put("lastName", graphRequest.lastName);
            profile.put("email", graphRequest.email);

            requestBody.put("profile", profile);
            requestBody.put("token", mAccessToken.getToken());
            requestBody.put("registrationToken", SharedPrefManager.getFirebaseToken(getApplicationContext()));
            Log.i(TAG, "Facebook Login Post Json : " + requestBody);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.i(TAG, "Facebook Login Url : " + ApiPaths.FACEBOOK_AUTH);
        JsonObjectRequest request = NetworkUtils.authenticatedBasicPostRequest(getApplicationContext(),
                ApiPaths.FACEBOOK_AUTH, requestBody,
                response -> {
                    Log.d(TAG, "Facebook Login Api Response : " + response.toString());
                    pDialog.dismiss();
                    ApiResponse<FBLogin> res = JsonUtils.fromJson(response.toString(), FBLogin.class);
                    if (res.success) {
                        SharedPrefManager.saveLoginType(this, Constants.LOGIN_TYPE_FACEBOOK);
                        SharedPrefManager.saveUserId(this, res.data.userId);
                        SharedPrefManager.saveFBGraph(this, graphRequest);
                        SharedPrefManager.saveProfilePicThumb(this,res.data.small);
                        SharedPrefManager.saveProfilePic(this,res.data.profile);
                        navigateAccordingly(res.data.step);
                    }
                },
                error -> {
                    pDialog.dismiss();
                    Log.i(TAG,"FBError"+ error.toString());
                    NetworkResponse networkResponse = error.networkResponse;
                    if (networkResponse != null) {
                        Log.i(TAG,"FBError Status code"+ String.valueOf(networkResponse.statusCode));
                    }
                });
        request.setShouldCache(false);
        VolleySingleton.getInstance(this).addToRequestQueue(request);
    }

    private void navigateAccordingly(int step) {
        Intent intent;
        switch (step) {
            case 1:
                intent = new Intent(this, VerifyPhoneActivity.class);
                SharedPrefManager.saveUserStatus(this, Constants.USER_STATUS_NEEDS_VERIFICATION);
                break;
            case 2:
                intent = new Intent(this, VerifyPhoneActivity.class);
                SharedPrefManager.saveUserStatus(this, Constants.USER_STATUS_NEEDS_VERIFICATION);
                break;
            case 3:
                intent = new Intent(this, CompleteProfileDetailsActivity.class);
                SharedPrefManager.saveUserStatus(this, Constants.USER_STATUS_NEEDS_COMPLETE_PROFILE_DETAILS);
                break;
            default:
                intent = new Intent(this, MainActivity.class);
                SharedPrefManager.saveUserStatus(this, Constants.USER_STATUS_REGISTER_COMPLETE);
        }
        startActivity(intent);
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppController.setForeground();
    }

    @Override
    protected void onPause() {
        super.onPause();
        AppController.setBackground();
    }
}