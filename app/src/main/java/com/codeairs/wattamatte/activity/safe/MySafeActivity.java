package com.codeairs.wattamatte.activity.safe;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.android.volley.toolbox.StringRequest;
import com.codeairs.wattamatte.AppController;
import com.codeairs.wattamatte.R;
import com.codeairs.wattamatte.constants.ApiPaths;
import com.codeairs.wattamatte.constants.FragmentTag;
import com.codeairs.wattamatte.fragments.safe.AddressProofFragment;
import com.codeairs.wattamatte.fragments.safe.GuarantorFragment;
import com.codeairs.wattamatte.fragments.safe.PieceIdentityFragment;
import com.codeairs.wattamatte.fragments.safe.SafeCompletionFragment;
import com.codeairs.wattamatte.fragments.safe.StudentCardFragment;
import com.codeairs.wattamatte.fragments.safe.SupportCAFFragment;
import com.codeairs.wattamatte.listeners.SafeListener;
import com.codeairs.wattamatte.models.SafeVault.InitializeSafe.Document;
import com.codeairs.wattamatte.models.SafeVault.InitializeSafe.SafeInitializer;
import com.codeairs.wattamatte.models.SafeVault.MySafe;
import com.codeairs.wattamatte.utils.AndroidUtils;
import com.codeairs.wattamatte.utils.CommonUtils;
import com.codeairs.wattamatte.utils.SharedPrefManager;
import com.codeairs.wattamatte.utils.internet.NetworkUtils;
import com.codeairs.wattamatte.utils.internet.VolleySingleton;
import com.google.gson.Gson;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class MySafeActivity extends AppCompatActivity implements SafeListener {

    private static final String TAG = MySafeActivity.class.getSimpleName();
    private MySafe mySafe = new MySafe();
    private SafeInitializer safeInitializer;
    private SweetAlertDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AndroidUtils.changeStatusBarColor(getWindow(), this, R.color.mediumTurquoise);
        setContentView(R.layout.activity_my_safe);
        loadInitialFragment();
        loadSafeInitializers();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            getSupportFragmentManager().findFragmentById(R.id.frame)
                    .onActivityResult(requestCode, resultCode, data);
        }
    }

    private void loadSafeInitializers() {
        pDialog = CommonUtils.getDialog(MySafeActivity.this, getString(R.string.please_wait));
        pDialog.show();
        Log.d(TAG, "Safe Initializer Api : " + ApiPaths.SAFE_INITIALIZER + SharedPrefManager.getUserId(getApplicationContext()));
        StringRequest request = NetworkUtils.authenticatedGetRequest(this, ApiPaths.SAFE_INITIALIZER + SharedPrefManager.getUserId(getApplicationContext()),
                response -> {
                    pDialog.dismissWithAnimation();
                    Log.d(TAG, "Safe Initializer Response : "+ response);
                    safeInitializer = new Gson().fromJson(response, SafeInitializer.class);
                    if (!safeInitializer.getSuccess()) {
                        return;
                    }
                },
                error -> {
                    pDialog.dismissWithAnimation();
                });
        VolleySingleton.getInstance(this).addToRequestQueue(request);
    }

    private void loadInitialFragment() {
        final FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.frame, new SafeCompletionFragment(), FragmentTag.SAFE_COMPLETION).commit();
    }

    @Override
    public void openPieceIdentity() {
        if (safeInitializer != null && safeInitializer.getIdentityPiece() != null)
            addFragmentToFrame(new PieceIdentityFragment(), FragmentTag.PIECE_IDENTITY);
        else
            loadSafeInitializers();
    }

    @Override
    public void openAddressProof() {
        if (safeInitializer != null && safeInitializer.getAddressProof() != null)
            addFragmentToFrame(new AddressProofFragment(), FragmentTag.ADDRESS_PROOF);
        else
            loadSafeInitializers();
    }

    @Override
    public void openSupportCAF() {
        if (safeInitializer != null && safeInitializer.getCAFHelp() != null)
            addFragmentToFrame(new SupportCAFFragment(), FragmentTag.SUPPORT_CAF);
        else
            loadSafeInitializers();
    }

    @Override
    public void openStudentCard() {
        if (safeInitializer != null && safeInitializer.getStudentCard() != null)
            addFragmentToFrame(new StudentCardFragment(), FragmentTag.STUDENT_CARD);
        else
            loadSafeInitializers();
    }

    @Override
    public void openGuarantor() {
        addFragmentToFrame(new GuarantorFragment(), FragmentTag.GUARANTOR);
    }

    @Override
    public MySafe getMySafe() {
        return mySafe;
    }

    @Override
    public void setMySafe(MySafe mySafe) {
        this.mySafe = mySafe;
    }

    @Override
    public void updateSafeInitializer() {
        loadSafeInitializers();
    }

    @Override
    public void updateSafeCompletionStatus() {
        SafeCompletionFragment safeCompletionFragment = (SafeCompletionFragment) getSupportFragmentManager().findFragmentByTag(FragmentTag.SAFE_COMPLETION);
        GuarantorFragment guarantorFragment = (GuarantorFragment) getSupportFragmentManager().findFragmentByTag(FragmentTag.GUARANTOR);
        if (safeCompletionFragment != null) {
            safeCompletionFragment.updateSafeCompletionStatus(mySafe);
        }
        if (guarantorFragment != null) {
            guarantorFragment.updateSafeCompletionStatus(mySafe);
        }
    }

    @Override
    public Document getStudentCard() {
        return safeInitializer.getStudentCard();
    }

    @Override
    public Document getCAFHelpDocument() {
        return safeInitializer.getCAFHelp();
    }

    @Override
    public Document getPieceIdentityDocument() {
        return safeInitializer.getIdentityPiece();
    }

    @Override
    public Document getAddressProofDocument() {
        return safeInitializer.getAddressProof();
    }

    @Override
    public Document getGuarantorIdentityPiece() {
        return safeInitializer.getGarantIdentityPiece();
    }

    private void addFragmentToFrame(Fragment fragment, String tag) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .add(R.id.frame, fragment, tag)
                .addToBackStack(tag)
                .commit();
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppController.setForeground();
    }

    @Override
    protected void onPause() {
        super.onPause();
        AppController.setBackground();
    }
}
