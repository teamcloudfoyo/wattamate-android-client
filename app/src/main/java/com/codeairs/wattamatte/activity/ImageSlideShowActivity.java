package com.codeairs.wattamatte.activity;

import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.codeairs.wattamatte.AppController;
import com.codeairs.wattamatte.R;
import com.codeairs.wattamatte.constants.ApiPaths;
import com.codeairs.wattamatte.utils.TouchImageView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.rd.PageIndicatorView;
import com.squareup.picasso.Picasso;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class ImageSlideShowActivity extends AppCompatActivity {

    public static final String PARAM_IMAGES = "ImageSlideShowActivity.PARAM_IMAGES";
    public static final String PARAM_TITLE = "ImageSlideShowActivity.PARAM_TITLE";
    public static final String PARAM_START_POS = "ImageSlideShowActivity.PARAM_START_POS";

    private List<String> images;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_slide_show);

        String title = getIntent().getStringExtra(PARAM_TITLE);
        if (!TextUtils.isEmpty(title)) {
            Toolbar toolbar = findViewById(R.id.toolbar);
            toolbar.setTitle(title);
        }

        Type listType = new TypeToken<ArrayList<String>>() {
        }.getType();
        images = new Gson().fromJson(getIntent().getStringExtra(PARAM_IMAGES), listType);
        int startPos = getIntent().getIntExtra(PARAM_START_POS, 0);

        ViewPager pager = findViewById(R.id.viewPager);
        pager.setAdapter(new SlideShowAdapter());
        pager.setCurrentItem(startPos);

        PageIndicatorView pageIndicatorView = findViewById(R.id.pageIndicatorView);
        pageIndicatorView.setCount(images.size());
        pageIndicatorView.setSelection(startPos);
        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                pageIndicatorView.setSelected(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    class SlideShowAdapter extends PagerAdapter {

        @Override
        public int getCount() {
            return images.size();
        }

        @NonNull
        @Override
        public Object instantiateItem(@NonNull ViewGroup collection, int position) {
            TouchImageView img = new TouchImageView(ImageSlideShowActivity.this);
            img.setLayoutParams(new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
            img.setScaleType(ImageView.ScaleType.FIT_CENTER);
            img.setAdjustViewBounds(true);
            AppController.getPicasso().load(ApiPaths.IMG_PATH_PREFIX + images.get(position)).into(img);

            collection.addView(img);
            return img;
        }

        public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
            return view == object;
        }

        @Override
        public void destroyItem(@NonNull ViewGroup collection, int position, @NonNull Object view) {
            collection.removeView((View) view);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppController.setForeground();
    }

    @Override
    protected void onPause() {
        super.onPause();
        AppController.setBackground();
    }
}
