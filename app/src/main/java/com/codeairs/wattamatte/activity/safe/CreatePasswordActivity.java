package com.codeairs.wattamatte.activity.safe;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.toolbox.JsonObjectRequest;
import com.codeairs.wattamatte.AppController;
import com.codeairs.wattamatte.R;
import com.codeairs.wattamatte.constants.ApiPaths;
import com.codeairs.wattamatte.models.ApiResponse;
import com.codeairs.wattamatte.models.SafeVault.SafeResponse;
import com.codeairs.wattamatte.utils.AndroidUtils;
import com.codeairs.wattamatte.utils.CommonUtils;
import com.codeairs.wattamatte.utils.JsonUtils;
import com.codeairs.wattamatte.utils.SharedPrefManager;
import com.codeairs.wattamatte.utils.internet.NetworkUtils;
import com.codeairs.wattamatte.utils.internet.VolleySingleton;
import com.google.common.collect.ImmutableMap;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import cn.pedant.SweetAlert.SweetAlertDialog;

import static com.codeairs.wattamatte.constants.Constants.SAFE_LOG_TAG;

public class CreatePasswordActivity extends AppCompatActivity {

    private EditText pass, confirmPass;
    private Button start;
    private SweetAlertDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AndroidUtils.changeStatusBarToWhite(getWindow(), this);
        setContentView(R.layout.activity_create_password);
        initControls();
    }

    private void initControls() {
        initToolbar();

        start = findViewById(R.id.start);
        AndroidUtils.disableView(start);

        pass = findViewById(R.id.password);
        confirmPass = findViewById(R.id.confirmPassword);
        TextWatcher textWatcher = new PassWatcher();
        pass.addTextChangedListener(textWatcher);
        confirmPass.addTextChangedListener(textWatcher);

        start.setOnClickListener(v -> handleStart());
        pDialog = CommonUtils.getDialog(this, getString(R.string.please_wait));
    }

    private void initToolbar() {
        Toolbar toolbar = AndroidUtils.addToolbarWithBack(this);
        toolbar.setNavigationOnClickListener(v -> onBackPressed());
    }

    private class PassWatcher implements TextWatcher {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            if (!TextUtils.isEmpty(pass.getText()) && !TextUtils.isEmpty(confirmPass.getText())
                    && TextUtils.equals(pass.getText(), confirmPass.getText())) {
                AndroidUtils.enableView(start);
            } else {
                AndroidUtils.disableView(start);
            }
        }
    }

    private void handleStart() {
        try {
            pDialog.show();
            JSONObject body = new JSONObject(new Gson().toJson(
                    ImmutableMap.<String, String>builder()
                            .put("userId", SharedPrefManager.getUserId(this))
                            .put("password", pass.getText().toString())
                            .build()
            ));
            Log.d(SAFE_LOG_TAG, "login Url : " + ApiPaths.CREATE_SAFE_PASS);
            JsonObjectRequest request = NetworkUtils.authenticatedPostRequest(
                    this, ApiPaths.CREATE_SAFE_PASS, body,
                    response -> {
                        pDialog.dismiss();
                        Log.d(SAFE_LOG_TAG, "Response after Login : " + response.toString());
                        ApiResponse<SafeResponse> res = JsonUtils.fromJson(response.toString(), SafeResponse.class);
                        if (res.success) {
                            startActivity(new Intent(this, SelectProfessionActivity.class));
                        } else
                            Toast.makeText(this, "" + res.error.message, Toast.LENGTH_SHORT).show();
                    }, error -> {
                        pDialog.dismiss();
                        Toast.makeText(this, getResources().getString(R.string.try_again_later), Toast.LENGTH_SHORT).show();
                    });
            request.setShouldCache(false);
            VolleySingleton.getInstance(this).addToRequestQueue(request);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppController.setForeground();
    }

    @Override
    protected void onPause() {
        super.onPause();
        AppController.setBackground();
    }
}
