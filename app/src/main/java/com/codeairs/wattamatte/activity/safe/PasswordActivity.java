package com.codeairs.wattamatte.activity.safe;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.JsonObjectRequest;
import com.codeairs.wattamatte.AppController;
import com.codeairs.wattamatte.R;
import com.codeairs.wattamatte.constants.ApiPaths;
import com.codeairs.wattamatte.models.ApiResponse;
import com.codeairs.wattamatte.models.SafeVault.SafeResponse;
import com.codeairs.wattamatte.utils.AndroidUtils;
import com.codeairs.wattamatte.utils.CommonUtils;
import com.codeairs.wattamatte.utils.JsonUtils;
import com.codeairs.wattamatte.utils.SharedPrefManager;
import com.codeairs.wattamatte.utils.internet.NetworkUtils;
import com.codeairs.wattamatte.utils.internet.VolleySingleton;
import com.google.common.collect.ImmutableMap;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import cn.pedant.SweetAlert.SweetAlertDialog;

import static com.codeairs.wattamatte.constants.Constants.SAFE_LOG_TAG;

public class PasswordActivity extends AppCompatActivity {

    private SweetAlertDialog pDialog;
    private Button btn;
    private TextView createPassword;
    private EditText password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AndroidUtils.changeStatusBarColor(getWindow(), this, R.color.bgSafe);
        setContentView(R.layout.activity_password);
        initControls();
    }

    private void initControls() {
        initToolbar();
        pDialog = CommonUtils.getDialog(this, getString(R.string.please_wait));

        password = findViewById(R.id.password);
        createPassword = findViewById(R.id.createPassword);
        password.addTextChangedListener(new PassTextWatcher());

        btn = findViewById(R.id.btn);
        AndroidUtils.disableView(btn);
        btn.setOnClickListener(v -> handleButtonClick());
        createPassword.setOnClickListener(v -> goToPasswordCreation());
    }

    private void initToolbar() {
        Toolbar toolbar = AndroidUtils.addToolbarWithBack(this);
        toolbar.setNavigationOnClickListener(v -> onBackPressed());
    }

    private void goToPasswordCreation() {
        startActivity(new Intent(PasswordActivity.this, CreatePasswordActivity.class));
    }

    private void handleButtonClick() {
        pDialog.show();
        try {
            JSONObject body = new JSONObject(new Gson().toJson(
                    ImmutableMap.<String, String>builder()
                            .put("userId", SharedPrefManager.getUserId(this))
                            .put("password", password.getText().toString())
                            .build()
            ));
            JsonObjectRequest request = NetworkUtils.authenticatedPostRequest(
                    this, ApiPaths.SAFE_CONNECT_WITH_PASS, body,
                    response -> {
                        pDialog.dismiss();
                        Log.d("createSafePass", response.toString());
                        Log.d(SAFE_LOG_TAG, "Response after Login : " + response.toString());
                        ApiResponse<SafeResponse> res = JsonUtils.fromJson(response.toString(), SafeResponse.class);
                        if (res.success) {
                            startActivity(new Intent(this, SelectProfessionActivity.class));
                        } else
                            Toast.makeText(this, "" + res.error.message, Toast.LENGTH_SHORT).show();
                    }, error -> {

                    });
            request.setShouldCache(false);
            VolleySingleton.getInstance(this).addToRequestQueue(request);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private class PassTextWatcher implements TextWatcher {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            if (!TextUtils.isEmpty(password.getText()))
                AndroidUtils.enableView(btn);
            else
                AndroidUtils.disableView(btn);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppController.setForeground();
    }

    @Override
    protected void onPause() {
        super.onPause();
        AppController.setBackground();
    }
}
