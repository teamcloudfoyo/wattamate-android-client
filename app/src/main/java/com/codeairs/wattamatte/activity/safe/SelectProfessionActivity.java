package com.codeairs.wattamatte.activity.safe;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.codeairs.wattamatte.AppController;
import com.codeairs.wattamatte.R;
import com.codeairs.wattamatte.constants.ApiPaths;
import com.codeairs.wattamatte.customcontrols.togglebutton.SafeProfessionActiveToggleButton;
import com.codeairs.wattamatte.fragments.dialogs.StudentDialogFragment;
import com.codeairs.wattamatte.models.ApiResponse;
import com.codeairs.wattamatte.models.SafeVault.SafeResponse;
import com.codeairs.wattamatte.models.SafeVault.SafeUser;
import com.codeairs.wattamatte.utils.AndroidUtils;
import com.codeairs.wattamatte.utils.CommonUtils;
import com.codeairs.wattamatte.utils.JsonUtils;
import com.codeairs.wattamatte.utils.SharedPrefManager;
import com.codeairs.wattamatte.utils.internet.NetworkUtils;
import com.codeairs.wattamatte.utils.internet.VolleySingleton;
import com.nex3z.togglebuttongroup.SingleSelectToggleGroup;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class SelectProfessionActivity extends AppCompatActivity implements SafeProfessionActiveToggleButton.OnItemClickListener {

    private SingleSelectToggleGroup toggleGroup;
    private SafeProfessionActiveToggleButton professionToggle;
    private Button next;
    private SweetAlertDialog pDialog;
    private List<SafeUser> safeUsers;
    private SafeUser student, others, active;

    private ArrayList<SafeUser> subCategories;
    private String typeActif;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AndroidUtils.changeStatusBarToWhite(getWindow(), this);
        setContentView(R.layout.activity_select_profession);
        initControls();
        getUserTypes();
    }

    private void initControls() {
        initToolbar();
        initNext();

        toggleGroup = findViewById(R.id.tgGroup);
        toggleGroup.setOnCheckedChangeListener(checkedChangeListener);
        professionToggle = findViewById(R.id.tg2);
        professionToggle.setOnItemClickListener(pos -> AndroidUtils.enableView(next));
        pDialog = CommonUtils.getDialog(this, getString(R.string.please_wait));
    }

    private void initToolbar() {
        Toolbar toolbar = AndroidUtils.addToolbarWithBack(this);
        toolbar.setNavigationOnClickListener(v -> onBackPressed());
    }

    private void initNext() {
        next = findViewById(R.id.next);
        AndroidUtils.disableView(next);
        findViewById(R.id.next).setOnClickListener(v -> handleNext());
    }

    private SingleSelectToggleGroup.OnCheckedChangeListener checkedChangeListener =
            (group, checkedId) -> {
                if (checkedId == R.id.tg2) {
                    if (professionToggle.getSelectedPosition() == -1)
                        AndroidUtils.disableView(next);
                    else {
                        AndroidUtils.enableView(next);
                    }
                    return;
                }
                AndroidUtils.enableView(next);
            };

    private void handleNext() {
        pDialog.show();

        try {
            JSONObject body = new JSONObject();
            body.put("userId", SharedPrefManager.getUserId(this));
            body.put("type", getSelectedType());
            if (getSelectedType().equalsIgnoreCase(active.getId()))
                body.put("typeActif", typeActif);
            JsonObjectRequest request = NetworkUtils.authenticatedPostRequest(
                    this, ApiPaths.SAFE_PROFESSION_TYPE, body,
                    response -> {
                        pDialog.dismiss();
                        ApiResponse<SafeResponse> res = JsonUtils.fromJson(response.toString(), SafeResponse.class);
                        if (res.success) {
                            if (getSelectedType().equalsIgnoreCase(student.getId())) {
                                StudentDialogFragment fragment = StudentDialogFragment.newInstance(subCategories);
                                fragment.setStyle(DialogFragment.STYLE_NORMAL, R.style.DialogFragmentTheme);
                                fragment.show(getSupportFragmentManager(), "chooseStudentSubType");
                            } else {
                                startActivity(new Intent(SelectProfessionActivity.this, WelcomeToSafeActivity.class));
                                finish();
                            }
                        } else
                            Toast.makeText(getApplicationContext(), "" + res.error.message, Toast.LENGTH_SHORT).show();
                        Log.d("createSafePass", response.toString());
                    }, error -> {
                        pDialog.dismiss();
                    });
            request.setShouldCache(false);
            VolleySingleton.getInstance(this).addToRequestQueue(request);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void getUserTypes() {
        pDialog.show();
        StringRequest request = NetworkUtils.authenticatedGetRequest(this, ApiPaths.SAFE_USERS_TYPES,
                response -> {
                    pDialog.dismiss();
                    ApiResponse<List<SafeUser>> res = JsonUtils.fromJsonList(response, SafeUser.class);
                    if (!res.success) {
                        return;
                    }
                    safeUsers = res.data;
                    subCategories = new ArrayList<>();
                    for (SafeUser safeUser : safeUsers) {
                        if (safeUser.getCdCategory() == 1)
                            subCategories.add(safeUser);
                        if (safeUser.getCdName().equalsIgnoreCase("Etudiant"))
                            student = safeUser;
                        if (safeUser.getCdName().equalsIgnoreCase("autre"))
                            others = safeUser;
                        if (safeUser.getCdName().equalsIgnoreCase("actif"))
                            active = safeUser;
                    }
                    professionToggle.setSubCategories(subCategories);
                    professionToggle.setOnItemClickListener(SelectProfessionActivity.this);
                },
                error -> {
                    pDialog.dismiss();
                    Toast.makeText(this, getResources().getString(R.string.try_again_later), Toast.LENGTH_SHORT).show();
                });
        VolleySingleton.getInstance(this).addToRequestQueue(request);
        request.setShouldCache(false);
        VolleySingleton.getInstance(this).addToRequestQueue(request);
    }

    private String getSelectedType() {
        switch (toggleGroup.getCheckedId()) {
            case R.id.tg1:
                SharedPrefManager.saveSafeUserType(getApplicationContext(),"3");
                return student.getId();
            case R.id.tg2:
                SharedPrefManager.saveSafeUserType(getApplicationContext(),"1");
                return active.getId();
            case R.id.tg3:
                SharedPrefManager.saveSafeUserType(getApplicationContext(),"1");
                return others.getId();
            default:
                return "";
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppController.setForeground();
    }

    @Override
    protected void onPause() {
        super.onPause();
        AppController.setBackground();
    }

    @Override
    public void onItemClick(int pos) {
        typeActif = subCategories.get(pos).getId();
        AndroidUtils.enableView(next);
    }
}
