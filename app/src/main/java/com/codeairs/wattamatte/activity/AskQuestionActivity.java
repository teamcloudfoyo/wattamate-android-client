package com.codeairs.wattamatte.activity;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.codeairs.wattamatte.AppController;
import com.codeairs.wattamatte.R;
import com.codeairs.wattamatte.constants.ApiPaths;
import com.codeairs.wattamatte.models.ApiResponse;
import com.codeairs.wattamatte.models.SuccessMsg;
import com.codeairs.wattamatte.models.profile.MyProfile;
import com.codeairs.wattamatte.models.rooms.ContactMailRequest;
import com.codeairs.wattamatte.utils.*;
import com.codeairs.wattamatte.utils.internet.NetworkUtils;
import com.codeairs.wattamatte.utils.internet.VolleySingleton;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;

public class AskQuestionActivity extends AppCompatActivity implements View.OnClickListener {

    public static final String PARAM_ID = "com.codeairs.wattamatte.activity.AskQuestionActivity.id";
    public static final String PARAM_PROFILE = "com.codeairs.wattamatte.activity.AskQuestionActivity.profile";

    private String roomId;

    private EditText input;
    private Button send;
    private TextView successMsg;
    private ProgressBar progressBar;

    private MyProfile profile;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ask_question);
        roomId = getIntent().getStringExtra(PARAM_ID);
        profile = new Gson().fromJson(getIntent().getStringExtra(PARAM_PROFILE), MyProfile.class);
        initControls();
    }

    private void initControls() {
        initClose();
        initSend();
        initInput();
        progressBar = findViewById(R.id.progressBar);
        successMsg = findViewById(R.id.successMsg);
    }

    private void initClose() {
        findViewById(R.id.close).setOnClickListener(v -> onBackPressed());
    }

    private void initSend() {
        send = findViewById(R.id.send);
        send.setOnClickListener(this);
    }

    private void initInput() {
        input = findViewById(R.id.input);
        input.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (TextUtils.isEmpty(s))
                    AndroidUtils.disableView(send);
                else
                    AndroidUtils.enableView(send);
            }
        });
    }

    @Override
    public void onBackPressed() {
        AndroidUtils.hideSoftKeyboard(this);
        new Handler().postDelayed(super::onBackPressed, 500);
    }

    @Override
    public void onClick(View v) {
        ContactMailRequest body = new ContactMailRequest(roomId, SharedPrefManager.getUserId(this),
                CommonUtils.getEmail(profile), profile.firstName, profile.phone,
                CommonUtils.getLocaleStringResource(Locale.FRANCE, R.string.contacts, this));
        body.contenu = input.getText().toString();
        body.object = "Contactez nous";

        Gson gson = new GsonBuilder().setDateFormat("dd/MM/yyyy").create();
        String requestBody = gson.toJson(body);

        Log.d("contact.Json", requestBody);
        send.setVisibility(View.INVISIBLE);
        progressBar.setVisibility(View.VISIBLE);
        JsonObjectRequest request = null;
        try {
            request = NetworkUtils.authenticatedPostRequest(this,
                    ApiPaths.MAIL_CONTACT_WATTA, new JSONObject(requestBody),
                    this::handleResponse, this::handleError);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        VolleySingleton.getInstance(this).addToRequestQueue(request);
    }

    private void handleResponse(JSONObject response) {
        Log.d("sendContactMail.res", response.toString());
        progressBar.setVisibility(View.INVISIBLE);
        try {
            ApiResponse<SuccessMsg> res = JsonUtils.fromJson(
                    response.toString(), SuccessMsg.class);
            if (res.success) {
                runOnUiThread(() -> AndroidUtils.hideSoftKeyboard(this));
                getWindow().setBackgroundDrawableResource(R.color.amber);
                input.setVisibility(View.GONE);
                successMsg.setVisibility(View.VISIBLE);
            } else {
                send.setVisibility(View.VISIBLE);
                UIHelper.showErrorAlert(this, res.error.message);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void handleError(VolleyError error) {

    }

    @Override
    protected void onResume() {
        super.onResume();
        AppController.setForeground();
    }

    @Override
    protected void onPause() {
        super.onPause();
        AppController.setBackground();
    }
}
