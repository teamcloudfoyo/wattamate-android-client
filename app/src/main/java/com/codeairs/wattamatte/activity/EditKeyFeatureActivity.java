package com.codeairs.wattamatte.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.android.volley.Request;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.codeairs.wattamatte.AppController;
import com.codeairs.wattamatte.R;
import com.codeairs.wattamatte.adapters.StyleRecyclerViewAdapter;
import com.codeairs.wattamatte.constants.ApiPaths;
import com.codeairs.wattamatte.fragments.ProfileFragment;
import com.codeairs.wattamatte.listeners.ClipArtStateChangeListener;
import com.codeairs.wattamatte.models.Tenant.TenantHobby;
import com.codeairs.wattamatte.models.Tenant.TenantKeyFeatureMate;
import com.codeairs.wattamatte.models.Tenant.TenantKeyFeatureMine;
import com.codeairs.wattamatte.models.completeprofiledetails.*;
import com.codeairs.wattamatte.models.profile.MyProfile;
import com.codeairs.wattamatte.utils.*;
import com.codeairs.wattamatte.utils.internet.NetworkUtils;
import com.codeairs.wattamatte.utils.internet.VolleySingleton;
import com.google.common.collect.ImmutableMap;
import com.google.gson.Gson;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class EditKeyFeatureActivity extends AppCompatActivity
        implements ClipArtStateChangeListener, View.OnClickListener {

    public static final String TAG = EditKeyFeatureActivity.class.getSimpleName();
    public static final String PARAM_1 = "EditKeyFeatureActivity.PARAM_1";
    public static final String PARAM_2 = "EditKeyFeatureActivity.PARAM_2";

    private MyProfile profile;
    private Button nextButton;
    private RecyclerView recyclerView;
    private List<Feature> features;
    private boolean mine;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_edit_hobby);

        profile = new Gson().fromJson(getIntent().getStringExtra(PARAM_1), MyProfile.class);
        Log.d("editKeyFeature","is Profile null :"+String.valueOf(profile == null));
        Log.d("editKeyFeature","is Profile Features null :"+String.valueOf(profile.features == null));
        mine = getIntent().getBooleanExtra(PARAM_2, true);
        initControls();
        fetchKeyFeatures();
    }

    @Override
    public void handleStateChange() {
        CompleteProfileHelper.handleClipArtStateChange(features, nextButton);
    }

    private void initControls() {
        TextView title = findViewById(R.id.title);
        title.setText(mine ? R.string.describe_you : R.string.your_ideal_mate);
        AndroidUtils.setText(findViewById(R.id.content), R.id.title,
                getString(mine ? R.string.choose_three_2 : R.string.choose_three_3));

        nextButton = findViewById(R.id.nextButton);
        nextButton.setText(R.string.update);
        nextButton.setOnClickListener(this);

        recyclerView = findViewById(R.id.list);
    }

    private void fetchKeyFeatures() {
        StringRequest request = new StringRequest(Request.Method.GET, ApiPaths.FEATURES,
                response -> {
                    Log.d(TAG,"Edit Feature Response : "+ response);
                    features = JsonUtils.fromJsonList(response, Feature.class).data;
                    markAlreadySelected();

                    recyclerView.setLayoutManager(new GridLayoutManager(this, 3));
                    recyclerView.addItemDecoration(new GridSpacingItemDecoration(3,
                            AndroidUtils.dpToPx(this, 0), false, 0));
                    recyclerView.setAdapter(new StyleRecyclerViewAdapter(features, this,
                            this, recyclerView.getHeight() / 4));
                },
                error -> {
                }
        ) {

            @Override
            public Map<String, String> getHeaders() {
                return NetworkUtils.authenticatedHeader(EditKeyFeatureActivity.this);
            }
        };
        VolleySingleton.getInstance(this).addToRequestQueue(request);
    }

    private void markAlreadySelected() {
        if (mine) {
            for (TenantKeyFeatureMine mine : profile.features.features.mine)
                for (Feature feature : features)
                    if (mine.id.id.equals(feature.id))
                        feature.setChecked(true);
        } else {
            for (TenantKeyFeatureMate mate : profile.features.features.mate)
                for (Feature feature : features)
                    if (mate.id.id.equals(feature.id))
                        feature.setChecked(true);
        }
    }

    private JSONObject getPostBody() throws JSONException {
        List<HobbyRequest> hobbyRequests = new ArrayList<>();
        for (TenantHobby hobby : profile.hobbies) {
            hobbyRequests.add(new HobbyRequest(hobby.hobby.id, hobby.name));
        }

        List<KeyFeatureMine> mine = new ArrayList<>();
        List<KeyFeatureMate> mate = new ArrayList<>();
        if (this.mine) {
            for (Feature feature : features)
                if (feature.isChecked) {
                    mine.add(new KeyFeatureMine(feature.id, feature.name));
                }
            for (TenantKeyFeatureMate feature : profile.features.features.mate) {
                mate.add(new KeyFeatureMate(feature.id.id, feature.name));
            }
        } else {
            for (TenantKeyFeatureMine feature : profile.features.features.mine) {
                mine.add(new KeyFeatureMine(feature.id.id, feature.name));
            }
            for (Feature feature : features)
                if (feature.isChecked) {
                    mate.add(new KeyFeatureMate(feature.id, feature.name));
                }
        }

        return new JSONObject(new Gson().toJson(ImmutableMap.<String, Object>builder()
                .put("userId", SharedPrefManager.getUserId(this))
                .put("hobbies", hobbyRequests)
                .put("keyFeatures", ImmutableMap.<String, Object>builder()
                        .put("mine", mine)
                        .put("mate", mate)
                        .build()
                ).build()));
    }

    @Override
    public void onClick(View v) {
        JsonObjectRequest request = null;
        try {
            Log.d(TAG,"Edit Feature Post Json : "+getPostBody().toString());
            request = NetworkUtils.authenticatedPutRequest(
                    this, ApiPaths.UPDATE_HOBBY_FEATURE, getPostBody(),
                    response -> {
                        Log.d(TAG,"Edit Feature Response : "+ response.toString());
                        ProfileFragment.needsRefresh = true;
                        onBackPressed();
                    }, error -> {
                        Log.d(TAG,"Edit Feature Error Response : "+ error.getMessage());
                    });
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (request != null) {
            request.setShouldCache(false);
            VolleySingleton.getInstance(this).addToRequestQueue(request);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppController.setForeground();
    }

    @Override
    protected void onPause() {
        super.onPause();
        AppController.setBackground();
    }
}
