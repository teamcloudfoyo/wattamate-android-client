package com.codeairs.wattamatte.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.codeairs.wattamatte.AppController;
import com.codeairs.wattamatte.R;
import com.codeairs.wattamatte.constants.ApiPaths;
import com.codeairs.wattamatte.constants.Constants;
import com.codeairs.wattamatte.constants.FragmentTag;
import com.codeairs.wattamatte.fragments.completeprofile.ChooseInterestFragment;
import com.codeairs.wattamatte.fragments.completeprofile.ChooseProfilePicFragment;
import com.codeairs.wattamatte.fragments.completeprofile.ChooseRoomMateStylesFragment;
import com.codeairs.wattamatte.fragments.completeprofile.ChooseStylesFragment;
import com.codeairs.wattamatte.fragments.completeprofile.PersonalInfoFragment;
import com.codeairs.wattamatte.fragments.completeprofile.completeroomdetails.CompleteRoomDetailsFragment;
import com.codeairs.wattamatte.listeners.CompleteProfileInteractionListener;
import com.codeairs.wattamatte.models.Place;
import com.codeairs.wattamatte.models.completeprofiledetails.Feature;
import com.codeairs.wattamatte.models.completeprofiledetails.Hobby;
import com.codeairs.wattamatte.models.completeprofiledetails.ProfileDetails;
import com.codeairs.wattamatte.models.entry.FBGraphRequest;
import com.codeairs.wattamatte.utils.CommonUtils;
import com.codeairs.wattamatte.utils.CompleteProfileHelper;
import com.codeairs.wattamatte.utils.JsonUtils;
import com.codeairs.wattamatte.utils.SharedPrefManager;
import com.codeairs.wattamatte.utils.UIHelper;
import com.codeairs.wattamatte.utils.internet.VolleySingleton;
import com.google.common.collect.ImmutableMap;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class CompleteProfileDetailsActivity extends AppCompatActivity
        implements CompleteProfileInteractionListener {

    private static final String TAG = "CompleteProfileDetails";

    public static final String PARAM_FB_LOGIN_RESPONSE = "PARAM_FB_LOGIN_RESPONSE";

    public static int REQUEST_CODE;

    private Button backButton;
    private ProgressBar progressBar;
    private TextView title;
    private SweetAlertDialog pDialog;

    public List<Hobby> hobbies;
    public List<Feature> features;
    public List<Feature> mateFeatures;

    private final CountDownLatch countDownLatch = new CountDownLatch(2);
    private boolean apiError = false;

    private ProfileDetails profileDetails = new ProfileDetails();

    private FBGraphRequest fbObject;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complete_profile_details);

        if (SharedPrefManager.getLoginType(this) == Constants.LOGIN_TYPE_FACEBOOK) {
            try {
                fbObject = SharedPrefManager.getFBGraph(this);
                profileDetails.name = fbObject.name;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        initControls();
        populate();
        Log.d(TAG, "Phone Number : " + SharedPrefManager.getPhoneNo(getApplicationContext()));

        if (Build.VERSION.SDK_INT >= 23) {
            int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
            if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
            }
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        //No call for super(). Bug on API Level > 11.
    }

    private void initControls() {
        initToolbar();
        progressBar = findViewById(R.id.progressBar);
        initDialog();
    }

    private void initToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        }
        title = findViewById(R.id.title);
        initBackButton();
    }

    private void initBackButton() {
        backButton = findViewById(R.id.back);
        backButton.setOnClickListener(view -> onBackPressed());
        backButton.setVisibility(View.GONE);
    }

    private void initDialog() {
        pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(ContextCompat.getColor(this, R.color.darkSlateBlue));
        pDialog.setTitleText("Please wait...");
        pDialog.setCancelable(false);
    }

    private void loadPersonalInfo() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .add(R.id.frame, new PersonalInfoFragment())
                .commit();
    }

    @Override
    public void handleOnNext(String fragmentTag) {
        Fragment fragment = null;
        String tag = null;
        switch (fragmentTag) {
            case FragmentTag.PERSONAL_INFO:
                fragment = new ChooseProfilePicFragment();
                tag = FragmentTag.CHOOSE_PROFILE_PIC;
                backButton.setText(null);
                title.setText(null);
                break;
            case FragmentTag.CHOOSE_INTERESTS:
                fragment = new ChooseStylesFragment();
                tag = FragmentTag.CHOOSE_STYLES;
                backButton.setText(null);
                title.setText(R.string.describe_you);
                break;
            case FragmentTag.CHOOSE_STYLES:
                fragment = new ChooseRoomMateStylesFragment();
                tag = FragmentTag.CHOOSE_ROOM_MATE_STYLES;
                backButton.setText(null);
                title.setText(R.string.your_ideal_mate);
                break;
            case FragmentTag.CHOOSE_ROOM_MATE_STYLES:
                fragment = new CompleteRoomDetailsFragment();
                tag = FragmentTag.COMPLETE_ROOM_DETAILS;
                backButton.setText(null);
                title.setText(null);
                break;
            case FragmentTag.CHOOSE_PROFILE_PIC:
                fragment = new ChooseInterestFragment();
                tag = FragmentTag.CHOOSE_INTERESTS;
                backButton.setText(null);
                title.setText(R.string.what_you_like);
                break;
            case FragmentTag.COMPLETE_ROOM_DETAILS:
                postResults();
                return;
        }

        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .add(R.id.frame, fragment, tag)
                .addToBackStack(tag)
                .commit();
        backButton.setVisibility(View.VISIBLE);
    }

    @Override
    public void handleOnNameChange(String name) {
        profileDetails.name = name;
    }

    @Override
    public void handleOnDobChange(String dob) {
        profileDetails.dob = dob;
    }

    @Override
    public void handleOnProfessionIdChange(int professionId) {
        profileDetails.professionId = professionId;
    }

    @Override
    public void handleOnProfessionChange(String profession) {
        profileDetails.profession = profession;
    }

    @Override
    public void handleGenderChange(int genderId) {
        profileDetails.gender = genderId;
    }

    @Override
    public void handleIsSmokerChange(int isSmoker) {
        profileDetails.isSmoker = isSmoker;
    }

    @Override
    public void handleHavePetsChange(int havePets) {
        profileDetails.havePets = havePets;
    }

    @Override
    public void handleHaveFurnitureChange(int haveFurniture) {
        profileDetails.haveFurniture = haveFurniture;
    }

    @Override
    public void handleHaveChildrenChange(int haveChildren) {
        profileDetails.haveChildren = haveChildren;
    }

    @Override
    public void handleProfilePicChange(Uri uri) {
        profileDetails.profilePicUri = uri;
    }

    @Override
    public String getName() {
        return profileDetails.name;
    }

    @Override
    public String getDob() {
        return profileDetails.dob;
    }

    @Override
    public int getProfessionId() {
        return profileDetails.professionId;
    }

    @Override
    public String getProfession() {
        return profileDetails.profession;
    }

    @Override
    public int getGender() {
        return profileDetails.gender;
    }

    @Override
    public int getIsSmoker() {
        return profileDetails.isSmoker;
    }

    @Override
    public int getHaveChildren() {
        return profileDetails.haveChildren;
    }

    @Override
    public int getHaveFurniture() {
        return profileDetails.haveFurniture;
    }

    @Override
    public int getHavePets() {
        return profileDetails.havePets;
    }

    @Override
    public Uri getProfilePic() {
        return profileDetails.profilePicUri;
    }

    @Override
    public void showLoading(String message) {
        pDialog.show();
    }

    @Override
    public void hideLoading() {
        pDialog.dismiss();
    }

    @Override
    public int getBudgetMin() {
        return profileDetails.minBudget;
    }

    @Override
    public void setBudgetMin(int value) {
        profileDetails.minBudget = value;
    }

    @Override
    public void setBudgetChanged() {
        profileDetails.budgetChanged = true;
    }

    @Override
    public boolean hasBudgetChanged() {
        return profileDetails.budgetChanged;
    }

    @Override
    public int getBudgetMax() {
        return profileDetails.maxBudget;
    }

    @Override
    public void setBudgetMax(int value) {
        profileDetails.maxBudget = value;
    }

    @Override
    public Place getLocation() {
        return profileDetails.location;
    }

    @Override
    public void setLocation(Place location) {
        profileDetails.location = location;
    }

    @Override
    public List<String> getMatesCount() {
        return profileDetails.matesCount;
    }

    @Override
    public void addMatesCount(String count) {
        if (!profileDetails.matesCount.contains(count))
            profileDetails.matesCount.add(count);
    }

    @Override
    public void removeMatesCount(String count) {
        if (profileDetails.matesCount.contains(count))
            profileDetails.matesCount.remove(count);
    }

    @Override
    public Date getEntryDate() {
        return profileDetails.entryDate;
    }

    @Override
    public void setEntryDate(Date date) {
        profileDetails.entryDate = date;
    }

    @Override
    public String getFBProfilePic() {
        return (fbObject.picture != null && fbObject.picture.data != null)
                ? fbObject.picture.data.url : null;
    }

    @Override
    public boolean getDidUploadPic() {
        return profileDetails.didUploadPic;
    }

    @Override
    public void setDidUploadPic(boolean status) {
        profileDetails.didUploadPic = status;
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            String tag = getSupportFragmentManager().getBackStackEntryAt(
                    getSupportFragmentManager().getBackStackEntryCount() - 1).getName();
            switch (tag) {
                case FragmentTag.CHOOSE_INTERESTS:
                    title.setText(null);
                    backButton.setVisibility(View.VISIBLE);
                    break;
                case FragmentTag.CHOOSE_STYLES:
                    title.setText(R.string.what_you_like);
                    backButton.setVisibility(View.VISIBLE);
                    break;
                case FragmentTag.CHOOSE_ROOM_MATE_STYLES:
                    title.setText(R.string.describe_you);
                    backButton.setVisibility(View.VISIBLE);
                    break;
                case FragmentTag.CHOOSE_PROFILE_PIC:
                    title.setText(null);
                    backButton.setVisibility(View.GONE);
                    break;
                case FragmentTag.COMPLETE_ROOM_DETAILS:
                    CompleteRoomDetailsFragment fragment = (CompleteRoomDetailsFragment)
                            getSupportFragmentManager().findFragmentByTag(tag);
                    int currentPage = fragment.getCurrentPageNo();
                    if (currentPage == 0) {
                        title.setText(R.string.your_ideal_mate);
                        break;
                    }
                    fragment.goToPreviousPage();
                    return;
                default:
                    backButton.setVisibility(View.VISIBLE);
            }
        }
        super.onBackPressed();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // sending the choose profile pic image result to the fragment
        getSupportFragmentManager().findFragmentById(R.id.frame)
                .onActivityResult(requestCode, resultCode, data);
    }

    private void populate() {

        progressBar.setVisibility(View.VISIBLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

        getHobbies();
        getNatures();

        final Handler mainThreadHandler = new Handler(Looper.getMainLooper());
        new Thread(() -> {
            try {
                countDownLatch.await();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            mainThreadHandler.post(() -> {
                if (apiError) {

                } else {
                    loadPersonalInfo();

                    progressBar.setVisibility(View.GONE);
                    getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                }
            });
        }).start();
    }

    private void getHobbies() {
        StringRequest request = new StringRequest(Request.Method.GET, ApiPaths.HOBBY,
                response -> {
                    Log.d(TAG, "Hobby response : " + response);
                    hobbies = JsonUtils.fromJsonList(response, Hobby.class).data;
                    CommonUtils.getActiveHobbies(hobbies);
                    countDownLatch.countDown();
                },
                error -> {
                    apiError = true;
                    countDownLatch.countDown();
                }
        ) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return ImmutableMap.<String, String>builder()
                        .put("Authorization", ApiPaths.TOKEN)
                        .build();
            }
        };
        VolleySingleton.getInstance(this).addToRequestQueue(request);
    }

    private void getNatures() {
        StringRequest request = new StringRequest(Request.Method.GET, ApiPaths.FEATURES,
                response -> {
                    Log.d(TAG, "Feature response : " + response);
                    features = JsonUtils.fromJsonList(response, Feature.class).data;
                    CommonUtils.getActiveFeatures(features);
                    mateFeatures = JsonUtils.fromJsonList(response, Feature.class).data;
                    countDownLatch.countDown();
                },
                error -> {
                    apiError = true;
                    countDownLatch.countDown();
                }
        ) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return ImmutableMap.<String, String>builder()
                        .put("Authorization", ApiPaths.TOKEN)
                        .build();
            }
        };
        VolleySingleton.getInstance(this).addToRequestQueue(request);
    }

    private void postResults() {
        String json = new Gson().toJson(CompleteProfileHelper.convert(
                profileDetails, hobbies, features, mateFeatures, this));
        Log.d(TAG, "Signup JSON: " + json);
        pDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                ApiPaths.PATH_PREFIX + "/user/signup/profile",
                this::handleResponse, this::handleError) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() {
                try {
                    return json == null ? null : json.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", json, "utf-8");
                    return null;
                }
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return ImmutableMap.<String, String>builder()
                        .put("userId", SharedPrefManager.getUserId(getApplicationContext()))
                        .put("Authorization", ApiPaths.TOKEN)
                        .build();
            }

        };
        stringRequest.setShouldCache(false);
        VolleySingleton.getInstance(this).addToRequestQueue(stringRequest);
    }

    private void handleResponse(String response) {
        Log.d(TAG, "CPD.Response : " + response);
        pDialog.dismiss();
        try {
            JSONObject jsonObject = new JSONObject(response);
            if (jsonObject.getBoolean("success")) {
                SharedPrefManager.saveUserStatus(this, Constants.USER_STATUS_REGISTER_COMPLETE);
                startActivity(new Intent(CompleteProfileDetailsActivity.this,
                        WelcomeActivity.class));
                finish();
            } else {
                if (jsonObject.has("error")) {
                    JSONObject error = jsonObject.getJSONObject("error");
                    UIHelper.showErrorAlert(this, error.getString("message"));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    private void handleError(VolleyError error) {
        pDialog.dismiss();

        Toast.makeText(this, "Trouble connecting to your wattamatte servers. " +
                "Please check your internet connection", Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppController.setForeground();
    }

    @Override
    protected void onPause() {
        super.onPause();
        AppController.setBackground();
    }
}
