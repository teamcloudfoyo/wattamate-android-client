package com.codeairs.wattamatte.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.codeairs.wattamatte.AppController;
import com.codeairs.wattamatte.R;

public class TestActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppController.setForeground();
    }

    @Override
    protected void onPause() {
        super.onPause();
        AppController.setBackground();
    }
}
