package com.codeairs.wattamatte.activity;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ImageView;

import com.codeairs.wattamatte.AppController;
import com.codeairs.wattamatte.R;
import com.codeairs.wattamatte.constants.Constants;
import com.codeairs.wattamatte.models.NotificationData;
import com.codeairs.wattamatte.utils.AndroidUtils;
import com.codeairs.wattamatte.utils.CommonUtils;
import com.codeairs.wattamatte.utils.SharedPrefManager;
import com.facebook.AccessToken;
import com.google.gson.Gson;

import static com.codeairs.wattamatte.constants.Constants.LOGIN_TYPE_FACEBOOK;
import static com.codeairs.wattamatte.constants.Constants.USER_STATUS_NEEDS_COMPLETE_PROFILE_DETAILS;
import static com.codeairs.wattamatte.constants.Constants.USER_STATUS_NEEDS_VERIFICATION;
import static com.codeairs.wattamatte.constants.Constants.USER_STATUS_NEW;

public class SplashActivity extends AppCompatActivity {

    private static int SPLASH_TIMEOUT = 2000; // 2 seconds
    private static final String TAG = SplashActivity.class.getSimpleName();
    private Intent notificationIntent;
    private NotificationData notificationData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AndroidUtils.setTransparentStatusBar(getWindow());
        setContentView(R.layout.activity_splash);

        ImageView img = findViewById(R.id.img);
        img.setColorFilter(ContextCompat.getColor(this, R.color.white), PorterDuff.Mode.SRC_IN);
        notificationIntent = getIntent();
        CommonUtils.dumpIntent(notificationIntent);
        if (SharedPrefManager.getLoginType(this) == LOGIN_TYPE_FACEBOOK && !isFBLoggedIn()) {
            SharedPrefManager.getEditor(this).clear().commit();
        }

        new Handler().postDelayed(() -> {
            Intent intent;
            int userStatus = SharedPrefManager.getUserStatus(this);
            switch (userStatus) {
                case USER_STATUS_NEW:
                    intent = new Intent(this, IntroActivity.class);
                    break;
                case USER_STATUS_NEEDS_VERIFICATION:
                    intent = new Intent(this, VerifyPhoneActivity.class);
                    break;
                case USER_STATUS_NEEDS_COMPLETE_PROFILE_DETAILS:
                    intent = new Intent(this, CompleteProfileDetailsActivity.class);
                    break;
                default: {
                    intent = new Intent(this, MainActivity.class);
                    if (notificationIntent != null && notificationIntent.hasExtra(Constants.NOTIFICATION_BUNDLE)) {
                        intent.putExtra(Constants.NOTIFICATION_BUNDLE, notificationIntent.getStringExtra(Constants.NOTIFICATION_BUNDLE));
                    } else if (notificationIntent.hasExtra("title")) {
                        notificationData = CommonUtils.getModelFromNotificationIntent(notificationIntent);
                        intent.putExtra(Constants.NOTIFICATION_BUNDLE, new Gson().toJson(notificationData));
                    } else {
                        intent = new Intent(this, MainActivity.class);
                    }
                }
            }
            startActivity(intent);
            finish();
        }, SPLASH_TIMEOUT);
    }

    public boolean isFBLoggedIn() {
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        return accessToken != null;
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppController.setForeground();
    }

    @Override
    protected void onPause() {
        super.onPause();
        AppController.setBackground();
    }
}
