package com.codeairs.wattamatte.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.InputFilter;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ProgressBar;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.codeairs.wattamatte.AppController;
import com.codeairs.wattamatte.R;
import com.codeairs.wattamatte.adapters.PlacesAutoCompleteAdapter;
import com.codeairs.wattamatte.constants.ApiPaths;
import com.codeairs.wattamatte.constants.Constants;
import com.codeairs.wattamatte.customcontrols.togglebutton.EditPreferenceToggleButton;
import com.codeairs.wattamatte.fragments.RoomsFragment;
import com.codeairs.wattamatte.fragments.RoomsListFragment;
import com.codeairs.wattamatte.models.ApiResponse;
import com.codeairs.wattamatte.models.Place;
import com.codeairs.wattamatte.models.rooms.Room;
import com.codeairs.wattamatte.models.rooms.RoomFilter;
import com.codeairs.wattamatte.models.rooms.preferency.Amenity;
import com.codeairs.wattamatte.models.rooms.preferency.Heating;
import com.codeairs.wattamatte.models.rooms.preferency.PreferenceCategory;
import com.codeairs.wattamatte.utils.*;
import com.codeairs.wattamatte.utils.internet.NetworkUtils;
import com.codeairs.wattamatte.utils.internet.VolleySingleton;
import com.google.common.collect.Lists;
import com.google.gson.Gson;
import com.nex3z.togglebuttongroup.MultiSelectToggleGroup;
import com.nex3z.togglebuttongroup.SingleSelectToggleGroup;
import mehdi.sakout.fancybuttons.FancyButton;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.CountDownLatch;


public class EditRoomPreferencesActivity extends AppCompatActivity {

    private List<Heating> heatings;
    private List<Amenity> amenities;
    private List<PreferenceCategory> categories;

    private final CountDownLatch countDownLatch = new CountDownLatch(3);
    private boolean apiError = false;

    private View content;
    private ProgressBar progressBar;
    private AutoCompleteTextView etLocation;
    private EditText budgetMin, budgetMax, surfaceMin, surfaceMax;

    private SingleSelectToggleGroup toggleGroupRooms, toggleGroupRentalType,
            toggleGroupFurniture, toggleGroupCuisine;
    private MultiSelectToggleGroup toggleGroupHeating, toggleGroupAmenity, toggleGroupCategory;
    private MultiSelectToggleGroup.LayoutParams tgLayoutParams;
    private FancyButton filter;

    private String location;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AndroidUtils.changeStatusBarToWhite(getWindow(), this);
        setContentView(R.layout.activity_edit_room_preferences);
        initControls();
        fetch();
    }

    private void initControls() {
        initToolbar();
        content = findViewById(R.id.content);
        progressBar = findViewById(R.id.progressBar);
        toggleGroupCategory = content.findViewById(R.id.toggleGroupPieces);
        toggleGroupHeating = content.findViewById(R.id.toggleGroupHeating);
        toggleGroupAmenity = content.findViewById(R.id.toggleGroupAmenity);

        tgLayoutParams = new MultiSelectToggleGroup.LayoutParams(
                (AndroidUtils.getScreenWidth(this) - AndroidUtils.dpToPx(this, 80)) / 3,
                AndroidUtils.dpToPx(this, 32)
        );

        initBudgetFields();

        surfaceMin = findViewById(R.id.surfaceMin);
        surfaceMax = findViewById(R.id.surfaceMax);
        initLocationField();
        toggleGroupRooms = findViewById(R.id.toggleGroupRooms);
        toggleGroupRentalType = findViewById(R.id.toggleGroupRentalType);
        toggleGroupFurniture = findViewById(R.id.toggleGroupFurniture);
        toggleGroupCuisine = findViewById(R.id.toggleGroupCuisine);
        filter = findViewById(R.id.applyFilter);
        filter.setOnClickListener(v -> invokePost());
    }

    private void initToolbar() {
        Toolbar toolbar = AndroidUtils.addToolbarWithBack(this);
        toolbar.setNavigationOnClickListener(v -> onBackPressed());
    }

    private void initLocationField() {
        etLocation = findViewById(R.id.etLocation);
        etLocation.setAdapter(new PlacesAutoCompleteAdapter(this, R.layout.autocomplete_list_item));
        etLocation.setOnItemClickListener((parent, view1, position, id) -> {
            Place place = (Place) parent.getItemAtPosition(position);
            etLocation.setText(place.description);
            location = place.location;
        });
    }

    private void initBudgetFields() {
        budgetMin = findViewById(R.id.budgetMin);
        budgetMax = findViewById(R.id.budgetMax);

        budgetMin.setFilters(new InputFilter[]{new InputFilterMinMax(
                Constants.BUDGET_MIN, Constants.BUDGET_MAX)});
        budgetMax.setFilters(new InputFilter[]{new InputFilterMinMax(
                Constants.BUDGET_MIN, Constants.BUDGET_MAX)});
        budgetMin.setOnFocusChangeListener(new OnFocusChangeListenerMinMax(
                Constants.BUDGET_MIN, true, budgetMax));
        budgetMax.setOnFocusChangeListener(new OnFocusChangeListenerMinMax(
                Constants.BUDGET_MAX, false, budgetMin));
    }

    private void fetch() {
        progressBar.setVisibility(View.VISIBLE);

        fetchAmenities();
        fetchHeatings();
        fetchCategories();

        final Handler mainThreadHandler = new Handler(Looper.getMainLooper());
        new Thread(() -> {
            try {
                countDownLatch.await();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            mainThreadHandler.post(() -> {
                if (apiError) {
                    return;
                }
                progressBar.setVisibility(View.GONE);
                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                content.setVisibility(View.VISIBLE);
            });
        }).start();
    }

    private void fetchAmenities() {
        StringRequest request = NetworkUtils.authenticatedGetRequest(this, ApiPaths.AMENITIES,
                response -> {
                    ApiResponse<List<Amenity>> res = JsonUtils.fromJsonList(response, Amenity.class);
                    if (!res.success) {
                        return;
                    }
                    amenities = res.data;
                    for (int i = 0; i < amenities.size(); i++) {
                        Amenity amenity = amenities.get(i);
                        toggleGroupAmenity.addView(new EditPreferenceToggleButton(
                                this, i, amenity.name, false), tgLayoutParams);
                    }
                    countDownLatch.countDown();
                },
                error -> {
                    apiError = true;
                });
        VolleySingleton.getInstance(this).addToRequestQueue(request);
    }

    private void fetchHeatings() {
        StringRequest request = NetworkUtils.authenticatedGetRequest(this, ApiPaths.HEATINGS,
                response -> {
                    ApiResponse<List<Heating>> res = JsonUtils.fromJsonList(response, Heating.class);
                    if (!res.success) {
                        return;
                    }
                    heatings = res.data;
                    for (int i = 0; i < heatings.size(); i++) {
                        Heating heating = heatings.get(i);
                        toggleGroupHeating.addView(new EditPreferenceToggleButton(
                                this, i, heating.name, false), tgLayoutParams);
                    }
                    countDownLatch.countDown();
                },
                error -> {
                    apiError = true;
                });
        VolleySingleton.getInstance(this).addToRequestQueue(request);
    }

    private void fetchCategories() {
        StringRequest request = NetworkUtils.authenticatedGetRequest(this, ApiPaths.CATEGORY,
                response -> {
                    ApiResponse<List<PreferenceCategory>> res = JsonUtils.fromJsonList(
                            response, PreferenceCategory.class);
                    if (!res.success) {
                        return;
                    }
                    categories = res.data;
                    for (int i = 0; i < categories.size(); i++) {
                        PreferenceCategory category = categories.get(i);
                        toggleGroupCategory.addView(new EditPreferenceToggleButton(
                                this, i, category.name, false), tgLayoutParams);
                    }
                    countDownLatch.countDown();
                },
                error -> {
                    apiError = true;
                });
        VolleySingleton.getInstance(this).addToRequestQueue(request);
    }

    private Integer getSelectedNumberOfRooms() {
        switch (toggleGroupRooms.getCheckedId()) {
            case R.id.tgRoom1:
                return 1;
            case R.id.tgRoom2:
                return 2;
            case R.id.tgRoom3:
                return 3;
            case R.id.tgRoom4:
                return 4;
        }
        return null;
    }

    private String getSelectedRentalType() {
        switch (toggleGroupRentalType.getCheckedId()) {
            case R.id.tgRentalType1:
                return CommonUtils.getLocaleStringResource(
                        new Locale("fr"), R.string.former, this);
            case R.id.tgRentalType2:
                return CommonUtils.getLocaleStringResource(
                        new Locale("fr"), R.string.nine, this);
            default:
                return CommonUtils.getLocaleStringResource(
                        new Locale("fr"), R.string.renovated, this);
        }
    }

    private String getSelectedFurnitureType() {
        switch (toggleGroupFurniture.getCheckedId()) {
            case R.id.tgFurnitureType1:
                return CommonUtils.getLocaleStringResource(
                        new Locale("fr"), R.string.vacuum, this);
            default:
                return CommonUtils.getLocaleStringResource(
                        new Locale("fr"), R.string.furniture, this);
        }
    }

    private String getSelectedKitchenType() {
        switch (toggleGroupCuisine.getCheckedId()) {
            case R.id.tgKitchenType1:
                return CommonUtils.getLocaleStringResource(
                        new Locale("fr"), R.string.american, this);
            case R.id.tgKitchenType2:
                return CommonUtils.getLocaleStringResource(
                        new Locale("fr"), R.string.equipped, this);
            default:
                return CommonUtils.getLocaleStringResource(
                        new Locale("fr"), R.string.separate, this);

        }
    }

    private List<String> getSelectedAmenities() {
        List<String> ids = new ArrayList<>();
        for (int id : toggleGroupAmenity.getCheckedIds()) {
            ids.add(amenities.get(id).id);
        }
        if (ids.isEmpty())
            return null;
        return ids;
    }

    private List<String> getSelectedHeatings() {
        List<String> ids = new ArrayList<>();
        for (int id : toggleGroupHeating.getCheckedIds()) {
            ids.add(heatings.get(id).id);
        }
        if (ids.isEmpty())
            return null;
        return ids;
    }

    private List<String> getSelectedCategory() {
        List<String> ids = new ArrayList<>();
        for (int id : toggleGroupCategory.getCheckedIds()) {
            ids.add(categories.get(id).id);
        }
        if (ids.isEmpty())
            return null;
        return ids;
    }

    private JSONObject getPostRequestBody() throws JSONException {
        RoomFilter filter = new RoomFilter();
        filter.userId = SharedPrefManager.getUserId(this);
        if (!TextUtils.isEmpty(location)) {
            filter.cities = Lists.newArrayList(location);
        } else {
            filter.cities = Lists.newArrayList("");
        }
        if (!TextUtils.isEmpty(budgetMin.getText()))
            filter.budgetMin = Float.valueOf(budgetMin.getText().toString());
        if (!TextUtils.isEmpty(budgetMax.getText()))
            filter.budgetMax = Float.valueOf(budgetMax.getText().toString());
        if (!TextUtils.isEmpty(surfaceMin.getText()))
            filter.surfaceMin = Float.valueOf(surfaceMin.getText().toString());
        if (!TextUtils.isEmpty(surfaceMax.getText()))
            filter.surfaceMax = Float.valueOf(surfaceMax.getText().toString());
        filter.category = getSelectedCategory();
        filter.roomCount = getSelectedNumberOfRooms();
        filter.status = getSelectedFurnitureType();
        filter.standingType = getSelectedRentalType();
        filter.kitchen = getSelectedKitchenType();
        filter.amenities = getSelectedAmenities();
        filter.heatings = getSelectedHeatings();
        String json = new Gson().toJson(filter);
        Log.d("Json", json);
        return new JSONObject(json);
    }

    private void invokePost() {
        AndroidUtils.disableView(filter);
        progressBar.setVisibility(View.VISIBLE);
        try {
            JsonObjectRequest request = NetworkUtils.authenticatedPostRequest(
                    this, ApiPaths.FILTER_PROPERTY, getPostRequestBody(),
                    this::handleResponse, error -> {
                        progressBar.setVisibility(View.GONE);
                        AndroidUtils.enableView(filter);
                    });
            request.setShouldCache(false);
            VolleySingleton.getInstance(this).addToRequestQueue(request);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void handleResponse(JSONObject response) {
        Log.d("response", response.toString());

        ApiResponse<List<Room>> res = JsonUtils.fromJsonList(response.toString(), Room.class);
        if (!res.success) {
            // handle
            return;
        }

        progressBar.setVisibility(View.GONE);
        RoomsListFragment.needsRefresh = true;

        RoomsFragment.filterResults = CommonUtils.getProperties(res.data);
        Intent intent = new Intent();
        intent.putExtra("filterResult", true);
        setResult(Activity.RESULT_OK, intent);
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppController.setForeground();
    }

    @Override
    protected void onPause() {
        super.onPause();
        AppController.setBackground();
    }
}
