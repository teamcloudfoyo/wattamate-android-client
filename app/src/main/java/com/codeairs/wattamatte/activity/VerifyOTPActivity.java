package com.codeairs.wattamatte.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.*;
import cn.pedant.SweetAlert.SweetAlertDialog;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.codeairs.wattamatte.AppController;
import com.codeairs.wattamatte.R;
import com.codeairs.wattamatte.constants.ApiPaths;
import com.codeairs.wattamatte.constants.Constants;
import com.codeairs.wattamatte.models.ApiResponse;
import com.codeairs.wattamatte.models.entry.SignUp;
import com.codeairs.wattamatte.models.entry.VerifyOTP;
import com.codeairs.wattamatte.utils.*;
import com.codeairs.wattamatte.utils.internet.NetworkUtils;
import com.codeairs.wattamatte.utils.internet.VolleySingleton;
import com.dpizarro.pinview.library.PinView;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

public class VerifyOTPActivity extends AppCompatActivity {

    static final String PARAM_PHONE_NO = "com.codeairs.wattamatte.activity.VerifyOTPActivity.PARAM_PHONE_NO";

    private PinView pinView;
    private Button next;
    private SweetAlertDialog pDialog;
    private EditText editTextPhone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_otp);

        initControls();
    }

    private void initControls() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        }
        initBackButton();
        initPhoneNo();
        initResend();
        initPinView();
        initDialog();
        initContinueButton();
    }

    private void initBackButton() {
        Button back = findViewById(R.id.back);
        back.setOnClickListener(view -> {
            AndroidUtils.hideSoftKeyboard(VerifyOTPActivity.this);
            onBackPressed();
        });
        back.setVisibility(View.INVISIBLE);
    }

    private void initResend() {
        Button resend = findViewById(R.id.otpNotGet);
        resend.setOnClickListener(v -> {
            pDialog.setTitleText(R.string.resending_otp);
            pDialog.show();
            reSendOTP();
        });
    }

    private void initPhoneNo() {
        String phoneNo = getIntent().getStringExtra(PARAM_PHONE_NO);
        editTextPhone = findViewById(R.id.phoneNo);
        editTextPhone.setText(phoneNo);

        ImageButton edit = findViewById(R.id.edit);
        edit.setOnClickListener(v -> {
            editTextPhone.setEnabled(true);
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null)
                imm.showSoftInput(editTextPhone, InputMethodManager.SHOW_IMPLICIT);
        });
    }

    private void initDialog() {
        pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(ContextCompat.getColor(this, R.color.darkSlateBlue));
        pDialog.setTitleText(R.string.please_wait);
        pDialog.setCancelable(false);
    }

    private void initPinView() {
        pinView = findViewById(R.id.pinView);

        // by default PinView library has number pad, but the requirement is alphanumeric keyboard
//        for (int i = 0; i < pinView.getChildCount(); i++) {
//            try {
//                LinearLayout ll = (LinearLayout) pinView.getChildAt(i);
//                for (int j = 0; j < ll.getChildCount(); j++) {
//                    LinearLayout ll2 = (LinearLayout) ll.getChildAt(j);
//                    for (int k = 0; k < ll2.getChildCount(); k++) {
//                        EditText et = (EditText) ll2.getChildAt(k);
//                        et.setInputType(InputType.TYPE_CLASS_TEXT);
//                    }
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
        pinView.setOnCompleteListener((completed, pinResults) -> {
            if (completed) {
                Keyboard.toggle(this);
            }
        });
    }

    private void initContinueButton() {
        next = findViewById(R.id.nextButton);
        next.setOnClickListener(view -> {
            String pin = pinView.getPinResults();
            if (!validate(pin)) {
                return;
            }
            UIHelper.disableViews(next);
            pDialog.setTitleText(R.string.please_wait);
            pDialog.show();
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("code", pin);
                jsonObject.put("userId", SharedPrefManager.getUserId(this));
                attemptVerify(jsonObject);
            } catch (JSONException e) {
                Log.e("SignUp", e.toString());
            }
        });
    }

    private boolean validate(String pin) {
        if (TextUtils.isEmpty(pin) || pin.length() != 4) {
            UIHelper.showErrorAlert(this, "Enter otp");
            return false;
        }
        return true;
    }

    private void attemptVerify(JSONObject json) {
        JsonObjectRequest stringRequest = new JsonObjectRequest(Request.Method.POST,
                ApiPaths.VERIFY_OTP, json,
                this::handleResponse, this::handleError) {

            @Override
            public Map<String, String> getHeaders() {
                return NetworkUtils.authenticatedHeader(getApplicationContext());
            }

        };
        stringRequest.setShouldCache(false);
        VolleySingleton.getInstance(this).addToRequestQueue(stringRequest);
    }

    private void handleResponse(JSONObject response) {
        Log.d("VerifyOtp.Response", response.toString());
        pDialog.dismiss();
        ApiResponse<VerifyOTP> res = JsonUtils.fromJson(response.toString(), VerifyOTP.class);
        if (!res.success) {
            UIHelper.enableViews(next);
            if (res.data != null && !TextUtils.isEmpty(res.data.msg))
                UIHelper.showErrorAlert(this, res.data.msg);
            return;
        }

        SharedPrefManager.saveUserStatus(this, Constants.USER_STATUS_NEEDS_COMPLETE_PROFILE_DETAILS);
        SharedPrefManager.savePhoneNo(this, editTextPhone.getText().toString());
        startActivity(new Intent(this, CompleteProfileDetailsActivity.class));
        finish();
    }

    private void handleError(VolleyError error) {
        pDialog.dismiss();
    }


    private void reSendOTP() {
        AndroidUtils.hideSoftKeyboard(this);
        if (TextUtils.isEmpty(editTextPhone.getText())) {
            new SweetAlertDialog(this)
                    .setTitleText(R.string.enter_valid_ph)
                    .show();
            return;
        }
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("phoneNumber", editTextPhone.getText().toString());
            jsonObject.put("userId", SharedPrefManager.getUserId(this));
            JsonObjectRequest stringRequest = new JsonObjectRequest(Request.Method.POST,
                    ApiPaths.VERIFY_PHONE, jsonObject,
                    this::handleResendResponse, this::handleResendError) {

                @Override
                public Map<String, String> getHeaders() {
                    return NetworkUtils.authenticatedHeader(getApplicationContext());
                }
            };
            stringRequest.setShouldCache(false);
            VolleySingleton.getInstance(this).addToRequestQueue(stringRequest);
        } catch (JSONException e) {
            Log.e("SignUp", e.toString());
        }
    }

    private void handleResendResponse(JSONObject response) {
        pDialog.dismiss();
        ApiResponse<SignUp> res = JsonUtils.fromJson(response.toString(), SignUp.class);
        if (!res.success) {
            if (res.data != null && res.data.msg != null && !res.data.msg.isEmpty())
                UIHelper.showErrorAlert(this, res.data.msg.get(0));
            else if (res.error != null)
                UIHelper.showErrorAlert(this, res.error.message);
            return;
        }
        editTextPhone.setEnabled(false);
        new SweetAlertDialog(this)
                .setTitleText(R.string.otp_sent)
                .show();
    }

    private void handleResendError(VolleyError error) {
        pDialog.dismiss();
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppController.setForeground();
    }

    @Override
    protected void onPause() {
        super.onPause();
        AppController.setBackground();
    }

}
