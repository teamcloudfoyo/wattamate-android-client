package com.codeairs.wattamatte.activity.safe;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import com.codeairs.wattamatte.AppController;
import com.codeairs.wattamatte.R;
import com.codeairs.wattamatte.utils.AndroidUtils;

public class FingerPrintActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AndroidUtils.changeStatusBarColor(getWindow(), this, R.color.bgSafe);
        setContentView(R.layout.activity_finger_print);
        initControls();
    }

    private void initControls() {
        initToolbar();
    }

    private void initToolbar() {
        Toolbar toolbar = AndroidUtils.addToolbarWithBack(this);
        toolbar.setNavigationOnClickListener(v -> onBackPressed());
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppController.setForeground();
    }

    @Override
    protected void onPause() {
        super.onPause();
        AppController.setBackground();
    }
}
