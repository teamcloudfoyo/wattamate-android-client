package com.codeairs.wattamatte.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.Toolbar;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import cn.pedant.SweetAlert.SweetAlertDialog;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.codeairs.wattamatte.AppController;
import com.codeairs.wattamatte.R;
import com.codeairs.wattamatte.constants.ApiPaths;
import com.codeairs.wattamatte.models.ApiResponse;
import com.codeairs.wattamatte.models.entry.SignUp;
import com.codeairs.wattamatte.utils.AndroidUtils;
import com.codeairs.wattamatte.utils.JsonUtils;
import com.codeairs.wattamatte.utils.SharedPrefManager;
import com.codeairs.wattamatte.utils.UIHelper;
import com.codeairs.wattamatte.utils.internet.NetworkUtils;
import com.codeairs.wattamatte.utils.internet.VolleySingleton;
import com.rilixtech.CountryCodePicker;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

import static com.codeairs.wattamatte.activity.VerifyOTPActivity.PARAM_PHONE_NO;

public class VerifyPhoneActivity extends AppCompatActivity {

    private CountryCodePicker ccp;
    private AppCompatEditText edtPhoneNumber;
    private Button verify;
    private SweetAlertDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_phone);

        initToolbar();
        initPhoneNumberFields();
        initNextButton();
        initDialog();
    }

    private void initToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        }
        initBackButton();
    }

    private void initBackButton() {
        Button back = findViewById(R.id.back);
        back.setOnClickListener(view -> {
            AndroidUtils.hideSoftKeyboard(this);
            onBackPressed();
        });
        back.setVisibility(View.INVISIBLE);
    }

    private void initPhoneNumberFields() {
        ccp = findViewById(R.id.ccp);
        edtPhoneNumber = findViewById(R.id.phoneNo);
        edtPhoneNumber.addTextChangedListener(new PhoneNumberFormattingTextWatcher());

        ccp.registerPhoneNumberTextView(edtPhoneNumber);

        edtPhoneNumber.setHint("6 12 34 56 78");
    }

    private void initDialog() {
        pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(ContextCompat.getColor(this, R.color.darkSlateBlue));
        pDialog.setTitleText(getString(R.string.please_wait));
        pDialog.setCancelable(false);
    }

    private void initNextButton() {
        verify = findViewById(R.id.nextButton);
        verify.setOnClickListener(view -> {
            if (!validate()) {
                return;
            }

            UIHelper.disableViews(edtPhoneNumber, verify);
            pDialog.show();
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("phoneNumber", "+" + ccp.getSelectedCountryCode() + getUnformattedNumber());
                jsonObject.put("userId", SharedPrefManager.getUserId(this));
                attemptVerify(jsonObject);
            } catch (JSONException e) {
                Log.e("SignUp", e.toString());
            }
        });
    }

    private boolean validate() {
        if (TextUtils.isEmpty(edtPhoneNumber.getText())) {
            UIHelper.showErrorAlert(this, getString(R.string.enter_valid_ph_no));
            return false;
        }
        return true;
    }

    private void attemptVerify(JSONObject json) {
        Log.d("JSON", json.toString());
        JsonObjectRequest stringRequest = new JsonObjectRequest(Request.Method.POST,
                ApiPaths.VERIFY_PHONE, json,
                this::handleResponse, this::handleError) {

            @Override
            public Map<String, String> getHeaders() {
                return NetworkUtils.authenticatedHeader(getApplicationContext());
            }
        };
        stringRequest.setShouldCache(false);
        VolleySingleton.getInstance(this).addToRequestQueue(stringRequest);
    }

    private void handleResponse(JSONObject response) {
        Log.d("VerifyPhone.Response", response.toString());
        pDialog.dismiss();
        ApiResponse<SignUp> res = JsonUtils.fromJson(response.toString(), SignUp.class);
        if (!res.success) {
            UIHelper.enableViews(edtPhoneNumber, verify);
            if (res.data != null && res.data.msg != null && !res.data.msg.isEmpty())
                UIHelper.showErrorAlert(this, res.data.msg.get(0));
            else if (res.error != null)
                UIHelper.showErrorAlert(this, res.error.message);
            return;
        }

        Intent intent = new Intent(this, VerifyOTPActivity.class);
        intent.putExtra(PARAM_PHONE_NO, "+" + ccp.getSelectedCountryCode() + getUnformattedNumber());
        startActivity(intent);
        finish();
    }

    private void handleError(VolleyError error) {

    }

    private String getUnformattedNumber() {
        String phoneNumber = edtPhoneNumber.getText().toString();
        phoneNumber = phoneNumber.replace("(", "");
        phoneNumber = phoneNumber.replace(")", "");
        phoneNumber = phoneNumber.replace("-", "");
        phoneNumber = phoneNumber.replace(" ", "");
        return phoneNumber;
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppController.setForeground();
    }

    @Override
    protected void onPause() {
        super.onPause();
        AppController.setBackground();
    }
}
