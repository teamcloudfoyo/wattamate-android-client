package com.codeairs.wattamatte.models.SafeVault.InitializeSafe;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Form {

    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("champ")
    @Expose
    private String champ;
    @SerializedName("for")
    @Expose
    private List<Integer> forWhome = null;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getChamp() {
        return champ;
    }

    public void setChamp(String champ) {
        this.champ = champ;
    }

    public List<Integer> getForWhome() {
        return forWhome;
    }

    public void setForWhome(List<Integer> forWhome) {
        this.forWhome = forWhome;
    }
}