package com.codeairs.wattamatte.models.rooms.preferency;

import com.google.gson.annotations.SerializedName;

public class Heating {

    @SerializedName("_id")
    public String id;

    @SerializedName("hc_reference")
    public String reference;

    @SerializedName("hc_name")
    public String name;

    @SerializedName("hc_status")
    public int status;

}
