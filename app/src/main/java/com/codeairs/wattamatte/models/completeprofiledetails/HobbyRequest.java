package com.codeairs.wattamatte.models.completeprofiledetails;

import com.google.gson.annotations.SerializedName;

public class HobbyRequest {

    @SerializedName("h_id")
    public String id;

    public String name;

    public HobbyRequest(String id, String name) {
        this.id = id;
        this.name = name;
    }
}
