package com.codeairs.wattamatte.models.profile;

import com.codeairs.wattamatte.models.completeprofiledetails.Address;

public class EditProfile {

    public String userId;
    public String dateOfBirth;
    public String firstName;
    public String lastName;
    public String job;
    public String villeRecherche;
    public String about;
    public String registrationToken;
    public int budgetMin;
    public int budgetMax;
    public Address address;

}
