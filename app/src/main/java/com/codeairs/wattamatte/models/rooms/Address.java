package com.codeairs.wattamatte.models.rooms;

import com.google.gson.annotations.SerializedName;

public class Address {

    @SerializedName("ad_state")
    public String state;

    @SerializedName("ad_city")
    public String city;

    @SerializedName("ad_country")
    public String country;

    @SerializedName("ad_zip")
    public String zip;

    @SerializedName("ad_address")
    public String address;

    @SerializedName("ad_longitude")
    public String longitude;

    @SerializedName("ad_latitude")
    public String latitude;

    @SerializedName("ad_neighborhood")
    public String neighborhood;

}
