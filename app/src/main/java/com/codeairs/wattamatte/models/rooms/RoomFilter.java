package com.codeairs.wattamatte.models.rooms;

import java.util.List;

public class RoomFilter {

    public String userId;

    public List<String> cities;

    public Float budgetMin;

    public Float budgetMax;

    public Float surfaceMin;

    public Float surfaceMax;

    public List<String> category;

    public Integer roomCount;

    public String status;

    public String standingType;

    public String kitchen;

    public List<String> amenities;

    public List<String> heatings;

}
