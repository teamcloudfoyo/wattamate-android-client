package com.codeairs.wattamatte.models.SafeVault;

public class CAFHelp {

    private String filePath;
    private String beneficaryNumber;
    private String amount;

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getBeneficaryNumber() {
        return beneficaryNumber;
    }

    public void setBeneficaryNumber(String beneficaryNumber) {
        this.beneficaryNumber = beneficaryNumber;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
}
