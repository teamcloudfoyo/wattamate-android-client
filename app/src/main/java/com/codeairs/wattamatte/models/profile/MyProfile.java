package com.codeairs.wattamatte.models.profile;

import com.codeairs.wattamatte.models.Tenant.PictureDetails;
import com.codeairs.wattamatte.models.Tenant.TenantHobby;
import com.codeairs.wattamatte.models.Tenant.TenantKeyFeatures;
import com.codeairs.wattamatte.models.completeprofiledetails.AdditionInfo;
import com.codeairs.wattamatte.models.rooms.Address;
import com.google.gson.annotations.SerializedName;

import java.util.Date;
import java.util.List;

public class MyProfile {

    @SerializedName("_id")
    public String id;

//    public Date updatedAt;
//
//    public Date createdAt;

    @SerializedName("u_first_name")
    public String firstName;

    @SerializedName("u_last_name")
    public String lastName;

    @SerializedName("u_sex")
    public String sex;

    @SerializedName("u_fk_additional_info")
    public List<AdditionInfo> additionInfos;

    @SerializedName("u_fk_hobbies")
    public List<TenantHobby> hobbies;

    @SerializedName("kf_fk_user_id")
    public TenantKeyFeatures features;

    @SerializedName("u_picture")
    public PictureDetails picture;

    @SerializedName("u_picture_small")
    public PictureDetails pictureThumb;

    @SerializedName("u_job")
    public String job;

    @SerializedName("u_university")
    public String university;

    @SerializedName("u_post")
    public String post;

    @SerializedName("u_phone")
    public String phone;

    @SerializedName("u_birthdate")
    public Date birthDate;

    @SerializedName("u_address")
    public Address address;

    @SerializedName("u_about")
    public String about;

    @SerializedName("local")
    public AccountDetail accountDetail;

    @SerializedName("facebook")
    public FBDetail fbDetail;

}
