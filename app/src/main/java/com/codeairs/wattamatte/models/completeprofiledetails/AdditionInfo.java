package com.codeairs.wattamatte.models.completeprofiledetails;

import com.google.gson.annotations.SerializedName;

public class AdditionInfo {

    @SerializedName("ai_id")
    public String id;

    public String question;
    public Object response;

    public AdditionInfo(String id, String question, Object response) {
        this.id = id;
        this.question = question;
        this.response = response;
    }
}
