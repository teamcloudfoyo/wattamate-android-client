package com.codeairs.wattamatte.models.rooms.preferency;

import com.google.gson.annotations.SerializedName;

public class Amenity {

    @SerializedName("_id")
    public String id;

    @SerializedName("am_reference")
    public String reference;

    @SerializedName("am_name")
    public String name;

    @SerializedName("am_icon")
    public String icon;

    @SerializedName("am_status")
    public int status;

}
