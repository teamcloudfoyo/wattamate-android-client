package com.codeairs.wattamatte.models.entry;

import com.google.gson.annotations.SerializedName;

public class FBGraphRequest {

    public String id;

    public String name;

    public String email;

    @SerializedName("first_name")
    public String firstName;

    @SerializedName("last_name")
    public String lastName;

    public FBPic picture;

}
