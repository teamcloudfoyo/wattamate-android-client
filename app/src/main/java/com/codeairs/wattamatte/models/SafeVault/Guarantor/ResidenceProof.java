package com.codeairs.wattamatte.models.SafeVault.Guarantor;

public class ResidenceProof {
    private String filePath;
    private boolean lessThan3Months;
    private boolean isInvoiceOfGasWaterInternetTelephone;

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public boolean isLessThan3Months() {
        return lessThan3Months;
    }

    public void setLessThan3Months(boolean lessThan3Months) {
        this.lessThan3Months = lessThan3Months;
    }

    public boolean isInvoiceOfGasWaterInternetTelephone() {
        return isInvoiceOfGasWaterInternetTelephone;
    }

    public void setInvoiceOfGasWaterInternetTelephone(boolean invoiceOfGasWaterInternetTelephone) {
        isInvoiceOfGasWaterInternetTelephone = invoiceOfGasWaterInternetTelephone;
    }
}
