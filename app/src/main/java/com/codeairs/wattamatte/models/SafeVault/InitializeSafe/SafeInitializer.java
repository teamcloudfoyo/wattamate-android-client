package com.codeairs.wattamatte.models.SafeVault.InitializeSafe;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SafeInitializer {
    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("docsUser")
    @Expose
    private List<Document> userDocs = null;
    @SerializedName("docsGarant")
    @Expose
    private List<Document> garantDocs = null;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public List<Document> getUserDocs() {
        return userDocs;
    }

    public void setUserDocs(List<Document> userDocs) {
        this.userDocs = userDocs;
    }

    public List<Document> getGarantDocs() {
        return garantDocs;
    }

    public void setGarantDocs(List<Document> garantDocs) {
        this.garantDocs = garantDocs;
    }

    public Document getIdentityPiece() {
        return getDocument("Pièce d'identité");
    }

    public Document getAddressProof() {
        return getDocument("Justificatif de domicile");
    }

    public Document getCAFHelp() {
        return getDocument("Aide CAF");
    }

    public Document getStudentCard() {
        return getDocument("Carte étudiant");
    }

    public Document getGarantIdentityPiece() {
        return getGarantDocument("Pièce d'identité");
    }

    public Document getDocument(String documentType) {
        for (Document document : getUserDocs()) {
            if (document.getDName().equalsIgnoreCase(documentType)) {
                return document;
            }
        }
        return null;
    }

    public Document getGarantDocument(String documentType) {
        for (Document document : getGarantDocs()) {
            if (document.getDName().equalsIgnoreCase(documentType)) {
                return document;
            }
        }
        return null;
    }
}
