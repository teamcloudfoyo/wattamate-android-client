package com.codeairs.wattamatte.models.Tenant;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class KeyFeatureId {

    @SerializedName("_id")
    public String id;

    @SerializedName("f_name")
    public String name;

    @SerializedName("f_icon")
    public List<String> photo;
}
