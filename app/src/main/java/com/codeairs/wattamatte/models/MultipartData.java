package com.codeairs.wattamatte.models;

public class MultipartData {
    private String key;
    private String value;

    public MultipartData(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }
}
