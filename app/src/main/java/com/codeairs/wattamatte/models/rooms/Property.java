package com.codeairs.wattamatte.models.rooms;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Property {

    @SerializedName("_id")
    public String id;

    @SerializedName("p_name")
    public String name;

    @SerializedName("p_reference")
    public String reference;

    @SerializedName("p_price")
    public String price;

    @SerializedName("p_address")
    public Address address;

    @SerializedName("p_pictures")
    public List<String> pictures;

    @SerializedName("p_description")
    public String description;

    @SerializedName("p_pieces")
    public String pieces;

    @SerializedName("p_surface")
    public String surface;

    @SerializedName("p_floor_number")
    public String floorNumber;

    @SerializedName("p_room_count")
    public String roomCount;

    @SerializedName("p_bathroom_count")
    public String bathRoomCount;

    @SerializedName("p_count_person")
    public int personCount;

}
