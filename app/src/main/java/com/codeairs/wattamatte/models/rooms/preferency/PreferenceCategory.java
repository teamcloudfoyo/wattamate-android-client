package com.codeairs.wattamatte.models.rooms.preferency;

import com.google.gson.annotations.SerializedName;

public class PreferenceCategory {

    @SerializedName("_id")
    public String id;

    @SerializedName("ca_reference")
    public String reference;

    @SerializedName("ca_name")
    public String name;

    @SerializedName("ca_icon")
    public String icon;

}
