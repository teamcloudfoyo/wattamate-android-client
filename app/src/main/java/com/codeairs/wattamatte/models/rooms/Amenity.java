package com.codeairs.wattamatte.models.rooms;

import com.google.gson.annotations.SerializedName;

public class Amenity {
    //FYI this class from now on, not used since json structure has changed. Now String is used instead of Object Aminity.
    @SerializedName("val")
    public String value;

    @SerializedName("am_id")
    public String id;

}
