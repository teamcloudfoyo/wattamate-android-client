package com.codeairs.wattamatte.models.Tenant;

import com.codeairs.wattamatte.models.profile.Hobby;
import com.google.gson.annotations.SerializedName;

public class TenantHobby {

    @SerializedName("_id")
    public String id;

    public String name;

    @SerializedName("h_id")
    public Hobby hobby;

}
