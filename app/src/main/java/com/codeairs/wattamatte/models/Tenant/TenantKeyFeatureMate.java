package com.codeairs.wattamatte.models.Tenant;

import com.google.gson.annotations.SerializedName;

public class TenantKeyFeatureMate {

    @SerializedName("ma_id")
    public KeyFeatureId id;

    @SerializedName("ma_name")
    public String name;

    @SerializedName("_id")
    public String mainId;

}
