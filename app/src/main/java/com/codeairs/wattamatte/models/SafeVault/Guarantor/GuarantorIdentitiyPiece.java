package com.codeairs.wattamatte.models.SafeVault.Guarantor;

public class GuarantorIdentitiyPiece {
    private String proofType;
    private String filePath;
    private String firstName;
    private String lastName;
    private String parentageLink;

    public String getProofType() {
        return proofType;
    }

    public void setProofType(String proofType) {
        this.proofType = proofType;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getParentageLink() {
        return parentageLink;
    }

    public void setParentageLink(String parentageLink) {
        this.parentageLink = parentageLink;
    }
}
