package com.codeairs.wattamatte.models.Tenant;

import com.google.gson.annotations.SerializedName;

public class PictureDetails {
    @SerializedName("name")
    private String name;
    @SerializedName("_id")
    private String id;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
