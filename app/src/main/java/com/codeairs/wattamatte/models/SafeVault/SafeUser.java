package com.codeairs.wattamatte.models.SafeVault;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SafeUser implements Parcelable {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("cd_name")
    @Expose
    private String cdName;
    @SerializedName("cd_category")
    @Expose
    private Integer cdCategory;

    public boolean isSelected = false;

    protected SafeUser(Parcel in) {
        id = in.readString();
        cdName = in.readString();
        if (in.readByte() == 0) {
            cdCategory = null;
        } else {
            cdCategory = in.readInt();
        }
        isSelected = in.readByte() != 0;
    }

    public static final Creator<SafeUser> CREATOR = new Creator<SafeUser>() {
        @Override
        public SafeUser createFromParcel(Parcel in) {
            return new SafeUser(in);
        }

        @Override
        public SafeUser[] newArray(int size) {
            return new SafeUser[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCdName() {
        return cdName;
    }

    public void setCdName(String cdName) {
        this.cdName = cdName;
    }

    public Integer getCdCategory() {
        return cdCategory;
    }

    public void setCdCategory(Integer cdCategory) {
        this.cdCategory = cdCategory;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(cdName);
        if (cdCategory == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(cdCategory);
        }
        parcel.writeByte((byte) (isSelected ? 1 : 0));
    }
}
