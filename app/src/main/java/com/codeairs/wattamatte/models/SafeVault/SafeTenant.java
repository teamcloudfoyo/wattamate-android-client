package com.codeairs.wattamatte.models.SafeVault;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SafeTenant {
    @Expose
    private User user;
    @SerializedName("coffre")
    @Expose
    private String coffre;
    @SerializedName("profession")
    @Expose
    private String profession;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getCoffre() {
        return coffre;
    }

    public void setCoffre(String coffre) {
        this.coffre = coffre;
    }

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }
}
