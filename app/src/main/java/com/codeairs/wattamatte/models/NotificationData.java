package com.codeairs.wattamatte.models;

import com.codeairs.wattamatte.constants.Constants;

public class NotificationData {

    private String notificationType = "";
    private String notificationDescription = "";
    private String notifiedUserId = "";
    private String currentUserId = "";
    private String chatMessage = "";

    public String getNotificationType() {
        return notificationType;
    }

    public void setNotificationType(String notificationType) {
        this.notificationType = notificationType;
    }

    public String getNotifiedUserId() {
        return notifiedUserId;
    }

    public void setNotifiedUserId(String notifiedUserId) {
        this.notifiedUserId = notifiedUserId;
    }

    public String getCurrentUserId() {
        return currentUserId;
    }

    public void setCurrentUserId(String currentUserId) {
        this.currentUserId = currentUserId;
    }

    public String getChatMessage() {
        return chatMessage;
    }

    public void setChatMessage(String chatMessage) {
        this.chatMessage = chatMessage;
    }

    public boolean isChatNotification() {
        return (getNotificationType().equalsIgnoreCase(Constants.NOTIFICATION_MESSAGING)||getNotificationType().equalsIgnoreCase(Constants.NOTIFICATION_MESSAGING_FRENCH));
    }

    public boolean isFavouriteNotification() {
        return (getNotificationType().equalsIgnoreCase(Constants.NOTIFICATION_FAVOURITE)||getNotificationType().equalsIgnoreCase(Constants.NOTIFICATION_FAVOURITE_FRENCH));
    }

    public boolean isContactRequestNotification() {
        return (getNotificationType().equalsIgnoreCase(Constants.NOTIFICATION_CONTACT_REQUEST)||getNotificationType().equalsIgnoreCase(Constants.NOTIFICATION_CONTACT_REQUEST_FRENCH));
    }

    public String getNotificationDescription() {
        return notificationDescription;
    }

    public void setNotificationDescription(String notificationDescription) {
        this.notificationDescription = notificationDescription;
    }
}
