package com.codeairs.wattamatte.models.SafeVault.Guarantor;

public class Guarantor {
    private GuarantorIdentitiyPiece identitiyPiece;
    private AccomodationCertificate accomodationCertificate;
    private ResidenceProof residenceProof;

    public GuarantorIdentitiyPiece getIdentitiyPiece() {
        return identitiyPiece;
    }

    public void setIdentitiyPiece(GuarantorIdentitiyPiece identitiyPiece) {
        this.identitiyPiece = identitiyPiece;
    }

    public AccomodationCertificate getAccomodationCertificate() {
        return accomodationCertificate;
    }

    public void setAccomodationCertificate(AccomodationCertificate accomodationCertificate) {
        this.accomodationCertificate = accomodationCertificate;
    }

    public ResidenceProof getResidenceProof() {
        return residenceProof;
    }

    public void setResidenceProof(ResidenceProof residenceProof) {
        this.residenceProof = residenceProof;
    }
}
