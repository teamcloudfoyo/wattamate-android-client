package com.codeairs.wattamatte.models.Tenant;

import com.google.gson.annotations.SerializedName;

public class TenantKeyFeatureMine {

    @SerializedName("mi_id")
    public KeyFeatureId id;

    @SerializedName("mi_name")
    public String name;

    @SerializedName("_id")
    public String mainId;

}
