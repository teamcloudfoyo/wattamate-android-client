package com.codeairs.wattamatte.models.rooms;

import java.util.Date;

public class ContactMailRequest {

    public String userId;

    public String emailFrom;

    public String object = "Vous avez reçu un nouveau dossier pour l’annonce “titre annonce”\n" +
            "Félicitations ! Vous avez reçu un nouveau dossier ! Allez jeter un coup d’oeil :clin_d'œil:\n" +
            "Ceci est un message automatique, merci de ne pas y répondre.";

    public String name;

    public String phoneNumber;

    public String contenu;

    public String propertyId;

    public Date date = new Date();

    public ContactMailRequest(String propertyId, String userId, String emailFrom,
                              String name, String phoneNumber, String contenu) {
        this.propertyId = propertyId;
        this.userId = userId;
        this.emailFrom = emailFrom;
        this.name = name;
        this.phoneNumber = phoneNumber;
        this.contenu = contenu;
    }
}
