package com.codeairs.wattamatte.models.completeprofiledetails;

import java.util.List;
import java.util.Map;

public class CompleteProfileDetails {

    public String userId, dateOfBirth, firstName, lastName, civility, sex, job, phone, university, post, registrationToken;

    public Address address;

    public List<HobbyRequest> hobbies;

    public List<AdditionInfo> additionalInfo;

    public Map<String, Object> keyFeatures;
}
