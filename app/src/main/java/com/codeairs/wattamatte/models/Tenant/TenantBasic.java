package com.codeairs.wattamatte.models.Tenant;

import com.google.gson.annotations.SerializedName;

public class TenantBasic {

    @SerializedName("_id")
    public String userId;

    @SerializedName("u_first_name")
    public String firstName;

    @SerializedName("u_last_name")
    public String lastName;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
