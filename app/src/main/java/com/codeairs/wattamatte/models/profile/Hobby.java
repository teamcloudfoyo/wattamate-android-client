package com.codeairs.wattamatte.models.profile;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Hobby {

    @SerializedName("_id")
    public String id;

    public String name;

    @SerializedName("h_photo")
    public List<String> photo;

}
