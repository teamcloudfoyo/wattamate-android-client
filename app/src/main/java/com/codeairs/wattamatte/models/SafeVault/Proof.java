package com.codeairs.wattamatte.models.SafeVault;

public class Proof {

    String proofId;
    String proofName;

    public Proof(String proofId, String proofName) {
        this.proofId = proofId;
        this.proofName = proofName;
    }

    public String getProofId() {
        return proofId;
    }

    public void setProofId(String proofId) {
        this.proofId = proofId;
    }

    public String getProofName() {
        return proofName;
    }

    public void setProofName(String proofName) {
        this.proofName = proofName;
    }
}
