package com.codeairs.wattamatte.models.Tenant;

import java.util.List;

public class TenantFilter {

    public String userId;

    public List<String> cities;

    public Float budgetMin;

    public Float budgetMax;

    public Integer moisEntree;

    public Integer joursEntree;

    public Integer fumeurs;

    public Integer animaux;

    public Integer profession;

    public Integer enfants;

}
