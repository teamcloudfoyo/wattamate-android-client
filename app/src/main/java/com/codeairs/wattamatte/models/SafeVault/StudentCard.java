package com.codeairs.wattamatte.models.SafeVault;

public class StudentCard {

    private String filePath;
    private String establishment;

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getEstablishment() {
        return establishment;
    }

    public void setEstablishment(String establishment) {
        this.establishment = establishment;
    }
}
