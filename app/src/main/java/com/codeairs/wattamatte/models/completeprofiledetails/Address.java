package com.codeairs.wattamatte.models.completeprofiledetails;

public class Address {

    public String address, country, city, state;
    public int zip;

    public Address() {
    }

    public Address(String address, int zip, String country, String city, String state) {
        this.address = address;
        this.zip = zip;
        this.country = country;
        this.city = city;
        this.state = state;
    }

    public Address(String city) {
        this.city = city;
    }
}
