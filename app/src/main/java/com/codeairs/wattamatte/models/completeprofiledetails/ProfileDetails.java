package com.codeairs.wattamatte.models.completeprofiledetails;

import android.net.Uri;
import com.codeairs.wattamatte.models.Place;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ProfileDetails {

    public String name;
    public String dob;
    public int professionId = 3;
    public String profession;
    public int gender;
    public int isSmoker = 2;
    public int havePets = 2;
    public int haveChildren = 2;
    public int haveFurniture = 3;
    public Uri profilePicUri;
    public int minBudget = 50;
    public int maxBudget = 2000;
    public boolean budgetChanged;
    public Place location;
    public List<String> matesCount = new ArrayList<>();
    public Date entryDate;
    public boolean didUploadPic;

}
