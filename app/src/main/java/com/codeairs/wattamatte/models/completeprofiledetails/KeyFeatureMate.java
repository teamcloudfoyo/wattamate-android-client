package com.codeairs.wattamatte.models.completeprofiledetails;

import com.google.gson.annotations.SerializedName;

public class KeyFeatureMate {

    @SerializedName("ma_id")
    public String id;

    @SerializedName("ma_name")
    public String name;

    public KeyFeatureMate(String id, String name) {
        this.id = id;
        this.name = name;
    }

}
