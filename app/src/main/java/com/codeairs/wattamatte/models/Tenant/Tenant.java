package com.codeairs.wattamatte.models.Tenant;

import android.graphics.Bitmap;

import com.codeairs.wattamatte.models.completeprofiledetails.AdditionInfo;
import com.codeairs.wattamatte.models.rooms.Address;
import com.google.gson.annotations.SerializedName;

import java.util.Date;
import java.util.List;

public class Tenant {

    @SerializedName("_id")
    public String userId;

    @SerializedName("u_first_name")
    public String firstName;

    @SerializedName("u_last_name")
    public String lastName;

    @SerializedName("u_sex")
    public String sex;

    @SerializedName("u_address")
    public Address address;

    @SerializedName("u_birthdate")
    public Date dob;

    @SerializedName("u_fk_hobbies")
    public List<TenantHobby> hobbies;

    @SerializedName("u_fk_additional_info")
    public List<AdditionInfo> additionInfos;

    @SerializedName("kf_fk_user_id")
    public TenantKeyFeatures features;

    @SerializedName("u_post")
    public String job;

    @SerializedName("u_picture")
    public PictureDetails picture;

    @SerializedName("u_picture_small")
    public PictureDetails pictureThumb;

    @SerializedName("u_favourite_properties")
    public List<Object> favoriteRooms;

    @SerializedName("u_about")
    public String about;

    public FBLoginDetails facebook;

    public boolean isPagenationView = false;
}
