package com.codeairs.wattamatte.models.profile;

import com.codeairs.wattamatte.models.completeprofiledetails.KeyFeatureMate;
import com.codeairs.wattamatte.models.completeprofiledetails.KeyFeatureMine;

import java.util.List;

public class ProfileFeatures {

    public List<KeyFeatureMine> mine;
    public List<KeyFeatureMate> mate;

}
