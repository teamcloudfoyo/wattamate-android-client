package com.codeairs.wattamatte.models.profile;

import com.google.gson.annotations.SerializedName;

public class Features {

    @SerializedName("_id")
    public String id;

    @SerializedName("kf_fk_features")
    public ProfileFeatures features;

}
