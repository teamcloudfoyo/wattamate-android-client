package com.codeairs.wattamatte.models.SafeVault;

import com.google.gson.annotations.SerializedName;

public class SafeStatus {
    @SerializedName("success")
    private Boolean success;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }
}
