package com.codeairs.wattamatte.models;

import com.codeairs.wattamatte.models.completeprofiledetails.Address;

public class Place {

    public String description;

    public String location;

    public String placeId;

    public Address address;

    public Place(String description, String location, String placeId) {
        this.description = description;
        this.location = location;
        this.placeId = placeId;
    }
}
