package com.codeairs.wattamatte.models;

import com.google.gson.annotations.SerializedName;

public class Item {

    public int id;
    public String idString;
    public String name;
    public int imgRes;

    @SerializedName("image_url")
    public String imageUrl;

    public boolean isChecked;

    public Item(String idString, String name, String imageUrl) {
        this.idString = idString;
        this.name = name;
        this.imageUrl = imageUrl;
    }

    public Item(String name, int imgRes) {
        this.name = name;
        this.imgRes = imgRes;
    }
}
