package com.codeairs.wattamatte.models.Tenant;

import com.google.gson.annotations.SerializedName;

public class TenantKeyFeatures {

    @SerializedName("_id")
    public String id;

    @SerializedName("kf_fk_features")
    public TenantProfileFeatures features;

}
