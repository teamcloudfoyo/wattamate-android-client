package com.codeairs.wattamatte.models.completeprofiledetails;

import com.google.gson.annotations.SerializedName;

public class KeyFeatureMine {

    @SerializedName("mi_id")
    public String id;

    @SerializedName("mi_name")
    public String name;

    public KeyFeatureMine(String id, String name) {
        this.id = id;
        this.name = name;
    }
}
