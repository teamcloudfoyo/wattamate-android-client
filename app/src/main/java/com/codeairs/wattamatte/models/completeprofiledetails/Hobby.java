package com.codeairs.wattamatte.models.completeprofiledetails;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Hobby extends ProfileItem {

    @SerializedName("_id")
    public String id;

//    public Date updatedAt;
//
//    public Date createdAt;

    @SerializedName("h_name")
    public String name;

    @SerializedName("h_photo")
    public List<String> photo;

    @SerializedName("h_displayed")
    private Boolean hDisplayed;

    @Override
    public String getId() {
        return id;
    }

    @Override
    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public List<String> getImage() {
        return photo;
    }

    @Override
    public void setImage(List<String> photo) {
        this.photo = photo;
    }

    public Boolean gethDisplayed() {
        return hDisplayed;
    }

    public void sethDisplayed(Boolean hDisplayed) {
        this.hDisplayed = hDisplayed;
    }
}
