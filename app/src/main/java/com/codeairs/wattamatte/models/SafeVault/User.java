package com.codeairs.wattamatte.models.SafeVault;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class User {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("u_first_name")
    @Expose
    private String uFirstName;
    @SerializedName("u_post")
    @Expose
    private String uPost;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUFirstName() {
        return uFirstName;
    }

    public void setUFirstName(String uFirstName) {
        this.uFirstName = uFirstName;
    }

    public String getUPost() {
        return uPost;
    }

    public void setUPost(String uPost) {
        this.uPost = uPost;
    }

}