package com.codeairs.wattamatte.models.SafeVault.InitializeSafe;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Document {
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("d_name")
    @Expose
    private String dName;
    @SerializedName("d_type")
    @Expose
    private List<DocumentType> dType = null;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDName() {
        return dName;
    }

    public void setDName(String dName) {
        this.dName = dName;
    }

    public List<DocumentType> getDType() {
        return dType;
    }

    public void setDType(List<DocumentType> dType) {
        this.dType = dType;
    }
}
