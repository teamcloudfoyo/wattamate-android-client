package com.codeairs.wattamatte.models.SafeVault;

import android.util.Log;

import com.codeairs.wattamatte.models.SafeVault.Guarantor.Guarantor;

public class MySafe {

    private IdentitiyPiece myIdentity;
    private AddressProof myAddressProof;
    private Guarantor guarantor;
    private CAFHelp cafHelp;
    private StudentCard studentCard;

    public IdentitiyPiece getMyIdentity() {
        return myIdentity;
    }

    public void setMyIdentity(IdentitiyPiece myIdentity) {
        this.myIdentity = myIdentity;
    }

    public AddressProof getMyAddressProof() {
        return myAddressProof;
    }

    public void setMyAddressProof(AddressProof myAddressProof) {
        this.myAddressProof = myAddressProof;
    }

    public Guarantor getGuarantor() {
        return guarantor;
    }

    public void setGuarantor(Guarantor guarantor) {
        this.guarantor = guarantor;
    }

    public CAFHelp getCafHelp() {
        return cafHelp;
    }

    public void setCafHelp(CAFHelp cafHelp) {
        this.cafHelp = cafHelp;
    }

    public StudentCard getStudentCard() {
        return studentCard;
    }

    public void setStudentCard(StudentCard studentCard) {
        this.studentCard = studentCard;
    }

    public boolean isGuarantorCompleted() {
        if (getGuarantor() == null) return false;
        if (getGuarantor().getResidenceProof() != null && getGuarantor().getAccomodationCertificate() != null && getGuarantor().getIdentitiyPiece() != null)
            return true;
        return false;
    }

    public float getCompletionPercentage() {
        float total = 6;
        int completed = 1;
        if (myIdentity != null)
            completed++;
        if (myAddressProof != null)
            completed++;
        if (isGuarantorCompleted())
            completed++;
        if (cafHelp != null)
            completed++;
        if (studentCard != null)
            completed++;
        return  (completed / total) * 100;
    }
}
