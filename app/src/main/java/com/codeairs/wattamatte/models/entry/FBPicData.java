package com.codeairs.wattamatte.models.entry;

import com.google.gson.annotations.SerializedName;

public class FBPicData {

    public int height;

    public int width;

    @SerializedName("is_silhouette")
    public boolean isSilhouette;

    public String url;

}
