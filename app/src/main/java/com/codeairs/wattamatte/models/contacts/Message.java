package com.codeairs.wattamatte.models.contacts;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class Message {

    @SerializedName("_id")
    public String id;

    @SerializedName("c_fk_user_id")
    public String fromUserId;

    @SerializedName("c_fk_chatted_user_id")
    public String toUserId;

    @SerializedName("c_message")
    public String msg;

    @SerializedName("c_note")
    public String note;

    @SerializedName("c_date")
    public Date date;

    @SerializedName("c_is_readed")
    public boolean isRead;


    public Message(String fromUserId, String toUserId, String msg, String note, Date date, boolean isRead) {
        this.fromUserId = fromUserId;
        this.toUserId = toUserId;
        this.msg = msg;
        this.note = note;
        this.date = date;
        this.isRead = isRead;
    }
}
