package com.codeairs.wattamatte.models.completeprofiledetails;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Feature extends ProfileItem {

    @SerializedName("_id")
    public String id;

//    public Date updatedAt;
//
//    public Date createdAt;

    @SerializedName("f_name")
    public String name;

    @SerializedName("f_icon")
    public List<String> photo;

    @SerializedName("f_displayed")
    private Boolean fDisplayed;

    @Override
    public String getId() {
        return id;
    }

    @Override
    @SerializedName("f_name")
    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public List<String> getImage() {
        return photo;
    }

    @Override
    public void setImage(List<String> photo) {
        this.photo = photo;
    }

    public List<String> getPhoto() {
        return photo;
    }

    public void setPhoto(List<String> photo) {
        this.photo = photo;
    }

    public Boolean getfDisplayed() {
        return fDisplayed;
    }

    public void setfDisplayed(Boolean fDisplayed) {
        this.fDisplayed = fDisplayed;
    }
}
