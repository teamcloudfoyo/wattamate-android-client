package com.codeairs.wattamatte.models.SafeVault.Guarantor;

public class AccomodationCertificate {
    private String filePath;

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }
}
