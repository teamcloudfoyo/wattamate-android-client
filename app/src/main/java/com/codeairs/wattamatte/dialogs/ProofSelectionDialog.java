package com.codeairs.wattamatte.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import com.codeairs.wattamatte.R;
import com.codeairs.wattamatte.adapters.ProofSelectionAdapter;
import com.codeairs.wattamatte.adapters.SafeDocumentTypeAdapter;
import com.codeairs.wattamatte.models.SafeVault.InitializeSafe.DocumentType;
import com.codeairs.wattamatte.models.SafeVault.Proof;

import java.util.List;

import static android.support.v7.widget.LinearLayoutManager.HORIZONTAL;
import static android.support.v7.widget.LinearLayoutManager.VERTICAL;

public class ProofSelectionDialog extends Dialog implements SafeDocumentTypeAdapter.SafeDocumentTypeSelectedCallback {

    Button cancel;
    RecyclerView proofSelectorRecyclerview;
    private String proofName = "";
    private SafeDocumentTypeAdapter adapter;
    private List<DocumentType> documentTypes;
    DocumentType documentType;
    private OnProofSelectedCallback callback;

    public ProofSelectionDialog(@NonNull Context context, OnProofSelectedCallback callback, List<DocumentType> documentTypes) {
        super(context);
        this.documentTypes = documentTypes;
        this.callback = callback;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        setCancelable(false);
        setCanceledOnTouchOutside(false);
        setContentView(R.layout.dialog_proof_selection);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        getWindow().setAttributes(lp);
        proofSelectorRecyclerview = findViewById(R.id.proofSelectorRecyclerview);
        cancel = findViewById(R.id.cancel);
        adapter = new SafeDocumentTypeAdapter(ProofSelectionDialog.this, documentTypes);
        proofSelectorRecyclerview.setLayoutManager(new LinearLayoutManager(getContext()));
        DividerItemDecoration customDivider = new DividerItemDecoration(getContext(), VERTICAL);
        customDivider.setDrawable(ContextCompat.getDrawable(getContext(), R.drawable.proof_item_divider));
        proofSelectorRecyclerview.addItemDecoration(customDivider);

        proofSelectorRecyclerview.setAdapter(adapter);
        cancel.setOnClickListener(cancelClicked);
    }

    View.OnClickListener cancelClicked = view -> dismiss();

    @Override
    public void onSafeDocumentTypeSelected(DocumentType documentType) {
        callback.onProofSelected(documentType);
        dismiss();
    }

    public interface OnProofSelectedCallback {
        void onProofSelected(DocumentType documentType);
    }
}
