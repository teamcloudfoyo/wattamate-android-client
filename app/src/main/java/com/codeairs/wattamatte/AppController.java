package com.codeairs.wattamatte;

import android.support.multidex.MultiDexApplication;
import android.util.Log;

import com.codeairs.wattamatte.constants.ApiPaths;
import com.codeairs.wattamatte.utils.internet.NetworkUtils;
import com.codeairs.wattamatte.utils.lazylist.ImageLoader;
import com.crashlytics.android.Crashlytics;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;
import com.squareup.picasso.Picasso;

import java.net.URISyntaxException;

import io.fabric.sdk.android.Fabric;


public class AppController extends MultiDexApplication {
    private static final String TAG = AppController.class.getSimpleName();
    /**
     * A singleton instance of the application class for easy access in other places
     */
    private static AppController sInstance;
    private static Picasso picasso;
    private static final String URL = ApiPaths.PATH_PREFIX;
    private static final String SOCKET_PATH = "/socket.io";
    private static ImageLoader imageLoader;
    public static boolean isForeground = false;

    private Socket mSocket;


    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        // initialize the singleton
        sInstance = this;
        enableImageCaching();

        try {
            IO.Options options = new IO.Options();
            options.path = SOCKET_PATH;
            mSocket = IO.socket(URL, options);

        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

    public static void setForeground() {
        Log.d(TAG, "App controller Foreground");
        isForeground = true;
    }

    public static void setBackground() {
        isForeground = false;
        Log.d(TAG, "App controller background");
    }

    public static boolean isForeground() {
        return isForeground;
    }

    public static synchronized AppController getInstance() {
        return sInstance;
    }


    private void enableImageCaching() {
        picasso = NetworkUtils.getAuthenticatedPicasso(getInstance());
        picasso.setLoggingEnabled(true);
        Picasso.setSingletonInstance(picasso);
        imageLoader = new ImageLoader(getInstance());
        imageLoader.clearCache();
    }

    public static Picasso getPicasso() {
        return picasso;
    }

    public Socket getmSocket() {
        return mSocket;
    }

    public static ImageLoader getImageLoader() {
        return imageLoader;
    }
}
