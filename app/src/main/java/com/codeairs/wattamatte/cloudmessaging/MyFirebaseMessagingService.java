package com.codeairs.wattamatte.cloudmessaging;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.codeairs.wattamatte.AppController;
import com.codeairs.wattamatte.R;
import com.codeairs.wattamatte.activity.MainActivity;
import com.codeairs.wattamatte.activity.SplashActivity;
import com.codeairs.wattamatte.constants.Constants;
import com.codeairs.wattamatte.models.NotificationData;
import com.codeairs.wattamatte.utils.CommonUtils;
import com.codeairs.wattamatte.utils.SharedPrefManager;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Map;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // [START_EXCLUDE]
        // There are two types of messages data messages and notification messages. Data messages are handled
        // here in onMessageReceived whether the app is in the foreground or background. Data messages are the type
        // traditionally used with GCM. Notification messages are only received here in onMessageReceived when the app
        // is in the foreground. When the app is in the background an automatically generated notification is displayed.
        // When the user taps on the notification they are returned to the app. Messages containing both notification
        // and data payloads are treated as notification messages. The Firebase console always sends notification
        // messages. For more see: https://firebase.google.com/docs/cloud-messaging/concept-options
        // [END_EXCLUDE]

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG, "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());
        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
            sendNotification(remoteMessage);
        }
    }

    private void sendNotification(RemoteMessage remoteMessage) {
        int userStatus = SharedPrefManager.getUserStatus(this);
        Log.d(TAG, "AppController Notification App Current Status isForeground : " + AppController.isForeground());
        if (!(userStatus == Constants.USER_STATUS_REGISTER_COMPLETE))
            return;

        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        NotificationData notificationData = new NotificationData();
        Gson gson = new Gson();
        String title = remoteMessage.getNotification().getTitle();
        String messageBody = remoteMessage.getNotification().getBody();
        notificationData.setNotificationType(title);
        Map<String, String> messageData = new HashMap<>();
        if (remoteMessage.getData().size() > 0) {
            messageData = remoteMessage.getData();
            notificationData.setNotificationType(CommonUtils.getValueFromKey(messageData, "title"));
            notificationData.setNotificationDescription(CommonUtils.getValueFromKey(messageData, "body"));
            CommonUtils.copyValuesToIntent(messageData,intent);
        }
        intent.setAction("com.codeairs.wattamatte");
        notificationData.setNotificationType(title);
        notificationData.setNotificationDescription(messageBody);
        if (notificationData.isFavouriteNotification()) {
            String userId = CommonUtils.getValueFromKey(messageData, "userId");
            notificationData.setNotifiedUserId(userId);
        } else if (notificationData.isChatNotification()) {
            String userId = CommonUtils.getValueFromKey(messageData, "userOneId");
            notificationData.setNotifiedUserId(userId);
        } else if (notificationData.isContactRequestNotification()) {
            String userId = CommonUtils.getValueFromKey(messageData, "userId");
            notificationData.setNotifiedUserId(userId);
        }
        intent.putExtra(Constants.NOTIFICATION_BUNDLE, gson.toJson(notificationData));
        int uniqueInt = (int) (System.currentTimeMillis() & 0xfffffff);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, uniqueInt, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        String channelId = getString(R.string.default_notification_channel_id);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, channelId)
                        .setSmallIcon(R.mipmap.ic_launcher_round)
                        .setContentTitle(notificationData.getNotificationType())
                        .setContentText(notificationData.getNotificationDescription())
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(channelId,
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(channel);
        }

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }
}