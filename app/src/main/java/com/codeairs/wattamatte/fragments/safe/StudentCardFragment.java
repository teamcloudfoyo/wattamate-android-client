package com.codeairs.wattamatte.fragments.safe;


import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.codeairs.wattamatte.R;
import com.codeairs.wattamatte.constants.ApiPaths;
import com.codeairs.wattamatte.customcontrols.edittext.MPRegularEditText;
import com.codeairs.wattamatte.listeners.SafeListener;
import com.codeairs.wattamatte.models.ApiResponse;
import com.codeairs.wattamatte.models.MultipartData;
import com.codeairs.wattamatte.models.SafeVault.InitializeSafe.Document;
import com.codeairs.wattamatte.models.SafeVault.Safe;
import com.codeairs.wattamatte.models.SafeVault.StudentCard;
import com.codeairs.wattamatte.utils.CommonUtils;
import com.codeairs.wattamatte.utils.JsonUtils;
import com.codeairs.wattamatte.utils.SharedPrefManager;
import com.codeairs.wattamatte.utils.internet.MultipartRequest;
import com.codeairs.wattamatte.utils.internet.VolleySingleton;
import com.google.gson.Gson;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import id.zelory.compressor.Compressor;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class StudentCardFragment extends BaseSafeFragment {
    private static final String TAG = StudentCardFragment.class.getSimpleName();
    private TextView addProof;
    private MPRegularEditText establishment;
    private ImageView proofImage;
    private Button validate;
    private File imageFile;
    private SweetAlertDialog pDialog;
    private SafeListener mListener;
    private Document document;

    public StudentCardFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_student_card, container, false);
        addProof = view.findViewById(R.id.text);
        establishment = view.findViewById(R.id.establishment);
        proofImage = view.findViewById(R.id.img1);
        validate = view.findViewById(R.id.nextButton);
        validate.setOnClickListener(validateClick);
        addProof.setOnClickListener(captureImageClicked);
        proofImage.setOnClickListener(captureImageClicked);
        document = mListener.getStudentCard();
        Log.d(TAG,"Student Json : "+new Gson().toJson(document));
        return view;
    }

    View.OnClickListener captureImageClicked = view -> CropImage.activity()
            .setGuidelines(CropImageView.Guidelines.ON)
            .setCropShape(CropImageView.CropShape.RECTANGLE)
            .setAspectRatio(3370, 2125)
            .start(getContext(), StudentCardFragment.this);

    View.OnClickListener validateClick = view -> handleValidateClick();

    private void handleValidateClick() {
        boolean isValid = true;
        if (TextUtils.isEmpty(establishment.getText().toString())) {
            establishment.setError("Required Field");
            isValid = false;
        }
        if (imageFile == null) {
            Toast.makeText(getContext(), "Please capture Document photo", Toast.LENGTH_SHORT).show();
            isValid = false;
        }

        if (isValid)
            postIdentityPiece();
    }

    private void postIdentityPiece() {
        List<MultipartData> multipartData = new ArrayList<>();
        multipartData.add(new MultipartData("userId", SharedPrefManager.getUserId(getContext())));
        multipartData.add(new MultipartData("type", SharedPrefManager.getSafeUserType(getContext())));
        multipartData.add(new MultipartData("docId", document.getId()));
        multipartData.add(new MultipartData("etablissement", establishment.getText().toString()));
        for (MultipartData data : multipartData) {
            Log.d(TAG, data.getKey() + " : " + data.getValue());
        }
        pDialog = CommonUtils.getDialog(getContext(), getString(R.string.please_wait));
        pDialog.show();
        MultipartRequest imageUploadReq = new MultipartRequest(
                SharedPrefManager.getUserId(getContext()),
                ApiPaths.SAFE_UPLOAD_DOCUMENT,
                (Response.Listener<String>) response -> {
                    handleResponse(response);
                }, this::handleError, multipartData, imageFile);
        VolleySingleton.getInstance(getContext()).addToRequestQueue(imageUploadReq);
    }

    private void handleResponse(String response) {
        pDialog.dismiss();
        ApiResponse<Safe> safe = JsonUtils.fromJson(response, Safe.class);
        if (safe.success) {
            Toast.makeText(getContext(), safe.data.getMsg(), Toast.LENGTH_SHORT).show();
            goToUpdatedCompletonFragment();
        } else
            Toast.makeText(getContext(), safe.error.message, Toast.LENGTH_SHORT).show();
    }

    private void handleError(VolleyError error) {
        pDialog.dismiss();
        Toast.makeText(getContext(), getResources().getString(R.string.try_again_later), Toast.LENGTH_SHORT).show();
    }

    private void goToUpdatedCompletonFragment() {
        mListener.getMySafe().setStudentCard(new StudentCard());
        mListener.updateSafeCompletionStatus();
        if (getActivity() != null)
            getActivity().getSupportFragmentManager().popBackStackImmediate();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof SafeListener)
            mListener = (SafeListener) context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            Toast.makeText(getContext(), "Image Captured", Toast.LENGTH_SHORT).show();
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                imageFile = new File(resultUri.getPath());
                try {
                    imageFile = new Compressor(getContext()).compressToFile(imageFile);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                proofImage.setImageURI(resultUri);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }
}
