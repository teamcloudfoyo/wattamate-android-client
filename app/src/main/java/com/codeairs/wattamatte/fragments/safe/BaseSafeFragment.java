package com.codeairs.wattamatte.fragments.safe;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;

import com.baoyz.actionsheet.ActionSheet;
import com.codeairs.wattamatte.R;
import com.codeairs.wattamatte.utils.AndroidUtils;
import com.codeairs.wattamatte.utils.CommonUtils;

public class BaseSafeFragment extends Fragment implements ActionSheet.ActionSheetListener{

    public ImageView image;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getActivity() != null)
            AndroidUtils.changeStatusBarToWhite(getActivity().getWindow(), getContext());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (getActivity() != null)
            AndroidUtils.changeStatusBarColor(getActivity().getWindow(), getContext(), R.color.mediumTurquoise);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getActivity() != null)
            AndroidUtils.changeStatusBarToWhite(getActivity().getWindow(), getContext());
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initToolbar(view);
    }

    private void initToolbar(View view) {
        Toolbar toolbar = AndroidUtils.addToolbarWithBack((AppCompatActivity) getActivity(), view);
        toolbar.setNavigationOnClickListener(v -> getActivity().onBackPressed());
    }

    public static void displayDialog() {

    }

    public void handleCardClick() {
        CommonUtils.startCardImagePicker(getActivity());
//        ActionSheet.createBuilder(getContext(), getFragmentManager())
//                .setCancelButtonTitle("Cancel")
//                .setOtherButtonTitles("Camera", "Gallery")
//                .setCancelableOnTouchOutside(true)
//                .setListener(this).show();
    }

    @Override
    public void onDismiss(ActionSheet actionSheet, boolean isCancel) {
        // do nothing
    }

    @Override
    public void onOtherButtonClick(ActionSheet actionSheet, int index) {
        CommonUtils.startCardImagePicker(getActivity());
    }
}
