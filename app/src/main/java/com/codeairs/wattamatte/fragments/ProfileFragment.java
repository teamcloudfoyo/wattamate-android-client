package com.codeairs.wattamatte.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.codeairs.wattamatte.AppController;
import com.codeairs.wattamatte.R;
import com.codeairs.wattamatte.activity.EditHobbyActivity;
import com.codeairs.wattamatte.activity.EditKeyFeatureActivity;
import com.codeairs.wattamatte.activity.EditProfileActivity;
import com.codeairs.wattamatte.activity.EditTenantPreferenceActivity;
import com.codeairs.wattamatte.activity.LoginActivity;
import com.codeairs.wattamatte.activity.safe.CreatePasswordActivity;
import com.codeairs.wattamatte.activity.safe.CreateSafeActivity;
import com.codeairs.wattamatte.activity.safe.PasswordActivity;
import com.codeairs.wattamatte.adapters.BadgesRecyclerViewAdapter;
import com.codeairs.wattamatte.adapters.FavRoomsRecyclerViewAdapter;
import com.codeairs.wattamatte.adapters.ProfileClipartAdapter;
import com.codeairs.wattamatte.constants.ApiPaths;
import com.codeairs.wattamatte.constants.Constants;
import com.codeairs.wattamatte.constants.Identifiers;
import com.codeairs.wattamatte.listeners.MainInteractionListener;
import com.codeairs.wattamatte.models.ApiResponse;
import com.codeairs.wattamatte.models.Item;
import com.codeairs.wattamatte.models.SafeVault.SafeResponse;
import com.codeairs.wattamatte.models.SafeVault.SafeStatus;
import com.codeairs.wattamatte.models.profile.MyProfile;
import com.codeairs.wattamatte.models.rooms.Property;
import com.codeairs.wattamatte.utils.AndroidUtils;
import com.codeairs.wattamatte.utils.CommonUtils;
import com.codeairs.wattamatte.utils.DateUtils;
import com.codeairs.wattamatte.utils.JsonUtils;
import com.codeairs.wattamatte.utils.SharedPrefManager;
import com.codeairs.wattamatte.utils.SpacesItemDecoration;
import com.codeairs.wattamatte.utils.internet.NetworkUtils;
import com.codeairs.wattamatte.utils.internet.VolleySingleton;
import com.facebook.login.LoginManager;
import com.google.common.collect.Lists;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;
import mehdi.sakout.fancybuttons.FancyButton;

import static com.codeairs.wattamatte.activity.EditHobbyActivity.PARAM_1;
import static com.codeairs.wattamatte.activity.MainActivity.REQUEST_CODE_ROOM_FAV_CHANGE_MY_PROFILE;
import static com.codeairs.wattamatte.utils.CommonUtils.getAnimalsImgResId;
import static com.codeairs.wattamatte.utils.CommonUtils.getChildrenImgResId;
import static com.codeairs.wattamatte.utils.CommonUtils.getProfessionImgResId;
import static com.codeairs.wattamatte.utils.CommonUtils.getSmokerImgRedId;

public class ProfileFragment extends Fragment {

    private static final String TAG = ProfileFragment.class.getSimpleName();
    public static boolean needsRefresh = false;
    private View profileLayout;
    private MyProfile profile;
    private TextView name, job;
    private MainInteractionListener mListener;
    private CoordinatorLayout layout;
    private ImageView pic;
    private SweetAlertDialog pDialog;

    private Context mContext;

    private boolean invalidateCachedPic;

    private FavRoomsRecyclerViewAdapter favRoomsRecyclerViewAdapter;
    private List<Property> favRooms;

    public ProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_profile, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        name = view.findViewById(R.id.name);
        pic = view.findViewById(R.id.expandedImage);
        job = view.findViewById(R.id.job);
        layout = view.findViewById(R.id.layout);
        profileLayout = view.findViewById(R.id.profileLayout);
        initToolbar(view);
        initEditButton(view);
        initRentalLocationsEdit();
        getProfileDetails();
        getFavoriteUsers();
        initLogoutButton(view);
        initDialog();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            if (getActivity() != null) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    getActivity().getWindow().getDecorView().setSystemUiVisibility(
                            View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
                }
            }
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        if (context instanceof MainInteractionListener) {
            mListener = (MainInteractionListener) context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (needsRefresh) {
            needsRefresh = false;
            invalidateCachedPic = true;
            getProfileDetails();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CODE_ROOM_FAV_CHANGE_MY_PROFILE && data != null) {
            String propertyId = data.getStringExtra("propertyId");
            boolean status = data.getBooleanExtra("favStatus", false);

            if (!status) {
                for (int i = 0; i < favRooms.size(); i++) {
                    Property property = favRooms.get(i);
                    if (propertyId.equals(property.id)) {
                        favRooms.remove(i);
                        break;
                    }
                }
                favRoomsRecyclerViewAdapter.notifyDataSetChanged();
            }
        }
    }

    private void initDialog() {
        pDialog = new SweetAlertDialog(mContext, SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(ContextCompat.getColor(mContext, R.color.darkSlateBlue));
        pDialog.setTitleText("Loading");
        pDialog.setCancelable(false);
    }

    private void initEditButton(View view) {
        FloatingActionButton edit = view.findViewById(R.id.editFAB);
        edit.setOnClickListener(v -> {
                    Intent intent = new Intent(mContext, EditProfileActivity.class);
                    intent.putExtra(EditProfileActivity.PROFILE_DETAILS, new Gson().toJson(profile));
                    startActivity(intent);
                }
        );
    }

    private void initLogoutButton(View view) {
        FancyButton logout = view.findViewById(R.id.logout);
        Log.d(TAG, "LogOut.Url : " + ApiPaths.LOGOUT);
        logout.setOnClickListener(v -> {
            pDialog.show();
            JsonObjectRequest request = NetworkUtils.authenticatedPostRequest(
                    getContext(), ApiPaths.LOGOUT, CommonUtils.getUserIdJson(getContext()),
                    response -> {
                        try {
                            pDialog.dismiss();
                            if (response != null && response.getBoolean("success")) {
                                Log.d(TAG, "LogOut.Response : " + response);
                                if (SharedPrefManager.getLoginType(mContext) == Constants.LOGIN_TYPE_FACEBOOK) {
                                    LoginManager.getInstance().logOut();
                                }
                                String fcmToken = SharedPrefManager.getFirebaseToken(mContext);
                                SharedPrefManager.getEditor(mContext).clear().commit();
                                SharedPrefManager.saveFirebaseToken(mContext, fcmToken);
                                startActivity(new Intent(mContext, LoginActivity.class));
                                if (getActivity() != null) {
                                    getActivity().finish();
                                }
                            } else
                                Toast.makeText(mContext, getResources().getString(R.string.try_again_later), Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                            pDialog.dismiss();
                        }
                    },
                    error -> {
                        Toast.makeText(mContext, getResources().getString(R.string.try_again_later), Toast.LENGTH_SHORT).show();
                        pDialog.dismiss();
                    });
            VolleySingleton.getInstance(getContext()).addToRequestQueue(request);
        });
    }

    private void initToolbar(View view) {
        adjustToolbarHeight(view);

        AppBarLayout appBarLayout = view.findViewById(R.id.app_bar);
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {

            int scrollRange = -1;

            @Override
            public void onOffsetChanged(final AppBarLayout appBarLayout, int verticalOffset) {
                if (getActivity() == null)
                    return;
                Window window = getActivity().getWindow();
                if (scrollRange == -1)
                    scrollRange = appBarLayout.getTotalScrollRange();

                if (scrollRange + verticalOffset < 60) {
                    AndroidUtils.changeStatusBarIconColor(window, true);
                } else {
                    AndroidUtils.changeStatusBarIconColor(window, false);
                }
            }
        });
    }

    private void adjustToolbarHeight(View view) {
        Toolbar toolbar = view.findViewById(R.id.toolbar);
        CollapsingToolbarLayout.LayoutParams params = new CollapsingToolbarLayout.LayoutParams(
                CollapsingToolbarLayout.LayoutParams.MATCH_PARENT,
                AndroidUtils.getStatusBarHeight(mContext)
                        + AndroidUtils.getToolBarHeight(mContext));
        toolbar.setLayoutParams(params);
    }

    private void initBadges() {
        RecyclerView recyclerView = initList(R.id.badges, R.drawable.ic_badges_red_20dp,
                R.string.badges, 16, 16);
        recyclerView.setAdapter(new BadgesRecyclerViewAdapter(Lists.newArrayList(
                new Item(profile.post, getProfessionImgResId(profile.post)),
                new Item(getString(R.string.children), getChildrenImgResId(profile.additionInfos)),
                new Item(getString(R.string.cigarettes), getSmokerImgRedId(profile.additionInfos)),
                new Item(getString(R.string.animals), getAnimalsImgResId(profile.additionInfos))
        ), mContext));

        ImageButton edit = profileLayout.findViewById(R.id.badges).findViewById(R.id.edit);
        edit.setOnClickListener(v -> startActivity(new Intent(mContext,
                EditTenantPreferenceActivity.class)));
        edit.setVisibility(View.INVISIBLE);
    }

    private void initFavRooms() {
        RecyclerView recyclerView = initList(R.id.favRooms, R.drawable.ic_condo_red_20dp,
                R.string.favorites, 0, 12);
        favRoomsRecyclerViewAdapter = new FavRoomsRecyclerViewAdapter(favRooms, mContext);
        recyclerView.setAdapter(favRoomsRecyclerViewAdapter);
        profileLayout.findViewById(R.id.favRooms).setVisibility(View.VISIBLE);

        ImageButton edit = profileLayout.findViewById(R.id.favRooms).findViewById(R.id.edit);
        edit.setOnClickListener(v -> mListener.goToTab(1));
        edit.setVisibility(View.GONE);
    }

    private void initInterests() {
        List<Item> items = CommonUtils.getHobbyItems(profile.hobbies);
        RecyclerView recyclerView = initList(R.id.interests, R.drawable.ic_like_red_20dp,
                R.string.interest, 0, 12);
        recyclerView.setAdapter(new ProfileClipartAdapter(items, mContext));

        ImageButton edit = profileLayout.findViewById(R.id.interests).findViewById(R.id.edit);
        edit.setOnClickListener(v -> {
            Intent intent = new Intent(mContext, EditHobbyActivity.class);
            intent.putExtra(PARAM_1, new Gson().toJson(profile));
            startActivity(intent);
        });
    }

    private void initStyles() {
        List<Item> items = CommonUtils.getMineFeatureItems(profile.features);
        RecyclerView recyclerView = initList(R.id.styles, R.drawable.ic_boy_red_20dp,
                R.string.styles, 0, 12);
        recyclerView.setAdapter(new ProfileClipartAdapter(items, mContext));

        initEditButtonFeatureEdit(R.id.styles, true);
    }

    private void initMateStyles() {
        List<Item> items = CommonUtils.getMateFeatureItems(profile.features);
        RecyclerView recyclerView = initList(R.id.mateStyles, R.drawable.ic_bulb_red_20dp,
                R.string.my_mate, 0, 12);
        recyclerView.setAdapter(new ProfileClipartAdapter(items, mContext));

        initEditButtonFeatureEdit(R.id.mateStyles, false);
    }

    private void initEditButtonFeatureEdit(int id, boolean mine) {
        ImageButton edit = profileLayout.findViewById(id).findViewById(R.id.edit);
        edit.setOnClickListener(v -> {
            Intent intent = new Intent(mContext, EditKeyFeatureActivity.class);
            Log.d(TAG, "Passing Bundle Json : " + new Gson().toJson(profile));
            intent.putExtra(EditKeyFeatureActivity.PARAM_1, new Gson().toJson(profile));
            intent.putExtra(EditKeyFeatureActivity.PARAM_2, mine);
            startActivity(intent);
        });
    }

    private RecyclerView initList(int layoutResId, int iconResId,
                                  int titleResId, int paddingPx, int paddingEdgesPx) {
        View badges = profileLayout.findViewById(layoutResId);

        ImageView icon = badges.findViewById(R.id.icon);
        icon.setImageResource(iconResId);

        TextView tv = badges.findViewById(R.id.tv);
        tv.setText(titleResId);

        LinearLayoutManager layoutManager = new LinearLayoutManager(mContext,
                LinearLayoutManager.HORIZONTAL, false);

        RecyclerView recyclerView = badges.findViewById(R.id.list);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addItemDecoration(new SpacesItemDecoration(paddingPx, paddingEdgesPx));
        recyclerView.setAdapter(new FavRoomsRecyclerViewAdapter(null, mContext));

        return recyclerView;
    }

    private void initRentalLocationsEdit() {
        ImageButton button = profileLayout.findViewById(R.id.locations).findViewById(R.id.editImgBtn);
        button.setOnClickListener(v ->
        {
            pDialog.show();
            StringRequest request = NetworkUtils.authenticatedGetRequest(getContext(), ApiPaths.CHECK_SAFE_PASS + SharedPrefManager.getUserId(getContext()),
                    response -> {
                        Log.d(TAG, "Safe Response : " + response);
                        pDialog.dismiss();
                        SafeStatus res = new Gson().fromJson(response, SafeStatus.class);
                        if (res.getSuccess())
                            startActivity(new Intent(mContext, PasswordActivity.class));
                        else
                            startActivity(new Intent(mContext, CreateSafeActivity.class));
                    },
                    error -> {
                        Log.d(TAG, "Safe Error Response");
                        pDialog.dismiss();
                    });
            VolleySingleton.getInstance(getContext()).addToRequestQueue(request);
        });
    }

    private void getProfileDetails() {
        if (profile == null)
            layout.setVisibility(View.INVISIBLE);
        mListener.showLoading();
        Log.d(TAG, "userProfile Url : " + ApiPaths.USER_PROFILE);
        StringRequest request = new StringRequest(Request.Method.GET,
                ApiPaths.USER_PROFILE + "/" + SharedPrefManager.getUserId(mContext),
                this::handleResponse, error -> {

        }
        ) {
            @Override
            public Map<String, String> getHeaders() {
                return NetworkUtils.authenticatedHeader(mContext);
            }
        };
        request.setShouldCache(true);
        VolleySingleton.getInstance(mContext).addToRequestQueue(request);
    }

    private void handleResponse(String response) {
        Log.d(TAG, "MyProfile.Json : " + response);
        ApiResponse<MyProfile> features = JsonUtils.fromJson(response, MyProfile.class);
        if (features.success) {
            profile = features.data;
            populateData();
            SharedPrefManager.saveProfilePic(getContext(), profile.picture.getId());
            SharedPrefManager.saveProfilePicThumb(getContext(), profile.pictureThumb.getId());
        }
        layout.setVisibility(View.VISIBLE);
        mListener.hideLoading();
    }

    private void populateData() {
        String nameAge = MessageFormat.format("{0}, {1}",
                profile.firstName, DateUtils.getAge(profile.birthDate));
        name.setText(nameAge);
        job.setText(profile.job);
        initProfilePic();
        AndroidUtils.setText(profileLayout, R.id.location, CommonUtils.getValueFromAdditionalInfos(
                profile.additionInfos, Identifiers.LOCATION));
        AndroidUtils.setText(profileLayout, R.id.budget,
                CommonUtils.getValueFromAdditionalInfos(profile.additionInfos, Identifiers.BUDGET_MIN) + " € - "
                        + CommonUtils.getValueFromAdditionalInfos(profile.additionInfos, Identifiers.BUDGET_MAX) + " €");
        initInterests();
        initStyles();
        initMateStyles();
        initBadges();
    }

    //todo The Profile Picture is now empty. ie, the getName() is somewhat empty/null.
    private void initProfilePic() {
        if (profile != null && profile.picture != null && profile.picture.getId() != null) {
            if (invalidateCachedPic) {
                AppController.getImageLoader().DisplayImage(profile.picture.getId(), pic, R.drawable.splash_logo);
                //NetworkUtils.loadBitmap(profile.picture.getId(),pic);
                invalidateCachedPic = false;
            }
            AppController.getImageLoader().DisplayImage(profile.picture.getId(), pic, R.drawable.splash_logo);
        }
    }

    private void getFavoriteUsers() {
        Log.d(TAG, "FavProperty.Url : " + ApiPaths.PROPERTY_GET_ALL_FAVORITES + SharedPrefManager.getUserId(mContext));
        StringRequest request = new StringRequest(Request.Method.GET,
                ApiPaths.PROPERTY_GET_ALL_FAVORITES + SharedPrefManager.getUserId(mContext),
                this::handleFavResponse,
                error -> {
                }) {

            @Override
            public Map<String, String> getHeaders() {
                return NetworkUtils.authenticatedHeader(mContext);
            }

        };
        request.setShouldCache(true);
        VolleySingleton.getInstance(mContext).addToRequestQueue(request);
    }

    private void handleFavResponse(String response) {
        Log.d(TAG, "FavProperty.Response" + response);


        List<Property> properties = new ArrayList<>();
        try {
            JSONObject root = new JSONObject(response);
            if (root.getBoolean("success")) {
                JSONArray array = root.getJSONArray("data");
                favRooms = Lists.newArrayList();
                for (int i = 0; i < array.length(); i++) {
                    JSONObject property = array.getJSONObject(i);
                    favRooms.add(new Gson().fromJson(property.toString(), Property.class));
                }
                initFavRooms();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
