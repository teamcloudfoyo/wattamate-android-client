package com.codeairs.wattamatte.fragments.dialogs;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.JsonObjectRequest;
import com.codeairs.wattamatte.AppController;
import com.codeairs.wattamatte.R;
import com.codeairs.wattamatte.constants.ApiPaths;
import com.codeairs.wattamatte.models.ApiResponse;
import com.codeairs.wattamatte.models.SafeVault.SafeTenant;
import com.codeairs.wattamatte.models.SuccessMsg;
import com.codeairs.wattamatte.utils.AndroidUtils;
import com.codeairs.wattamatte.utils.JsonUtils;
import com.codeairs.wattamatte.utils.SharedPrefManager;
import com.codeairs.wattamatte.utils.UIHelper;
import com.codeairs.wattamatte.utils.internet.NetworkUtils;
import com.codeairs.wattamatte.utils.internet.VolleySingleton;
import com.google.common.collect.ImmutableMap;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class ChooseMatesDialogFragment extends DialogFragment {

    private static final String PARAM_USERS = "users";
    private static final String PARAM_PROPERTY_ID = "propertyId";

    private View inviteHelp;
    private Context mContext;
    private RecyclerView recyclerView;
    private Button validate;
    private SweetAlertDialog pDialog;

    private List<SafeTenant> users;
    private List<Boolean> selection;
    private String propertyId;
    private long mRequestStartTime;

    public ChooseMatesDialogFragment() {
    }

    static ChooseMatesDialogFragment newInstance(List<SafeTenant> users, String propertyId) {
        ChooseMatesDialogFragment fragment = new ChooseMatesDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putString(PARAM_USERS, new Gson().toJson(users));
        bundle.putString(PARAM_PROPERTY_ID, propertyId);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            users = new Gson().fromJson(getArguments().getString(PARAM_USERS),
                    new TypeToken<List<SafeTenant>>() {
                    }.getType());
            selection = new ArrayList<>();
            for (int i = 0; i < users.size(); i++)
                selection.add(false);
            propertyId = getArguments().getString(PARAM_PROPERTY_ID);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_choose_mates_list, container, false);
        initClose(view);
        initList(view);
        initDialog();
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        inviteHelp = view.findViewById(R.id.rect);
        initValidate(view);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    private void initDialog() {
        pDialog = new SweetAlertDialog(mContext, SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(ContextCompat.getColor(mContext, R.color.darkSlateBlue));
        pDialog.setTitleText("Loading");
        pDialog.setCancelable(false);
    }

    private void initClose(View view) {
        ImageButton btn = view.findViewById(R.id.close);
        btn.setOnClickListener(v -> dismiss());
    }

    private void initList(View view) {
        recyclerView = view.findViewById(R.id.list);
        recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        recyclerView.setAdapter(new ChooseMatesAdapter());
    }

    private void hideInviteHelp() {
        AndroidUtils.setViewHeight(inviteHelp, 0);
        AndroidUtils.setViewHeight(recyclerView, AndroidUtils.dpToPx(mContext, 324));
    }

    private void showInviteHelp() {
        AndroidUtils.setViewHeight(inviteHelp, AndroidUtils.dpToPx(mContext, 44));
        AndroidUtils.setViewHeight(recyclerView, AndroidUtils.dpToPx(mContext, 280));
    }

    private void initValidate(View view) {
        validate = view.findViewById(R.id.validate);
        validate.setVisibility(View.GONE);
        validate.setOnClickListener(v -> {
            dismiss();
            if (getActivity() == null)
                return;
            invokeApply(getActivity().getSupportFragmentManager());
        });
    }

    private void invokeApply(FragmentManager fm) {
        try {
            pDialog.show();
            List<SafeTenant> selectedUsers = getSelectedUsers();
            JSONObject requestBody = new JSONObject(new Gson().toJson(
                    ImmutableMap.<String, Object>builder()
                            .put("userId", SharedPrefManager.getUserId(mContext))
                            .put("propertyId", propertyId)
                            .put("arrayUsers ", getSelectedUserIds(selectedUsers))
                            .build()
            ));

            mRequestStartTime = System.currentTimeMillis(); // setting the request start time just before  send the request.
            JsonObjectRequest request = NetworkUtils.authenticatedPostRequest(
                    mContext, ApiPaths.POSTULER_APPLY, requestBody,
                    response -> handleResponse(response, selectedUsers, fm),
                    error -> {

                    });
            VolleySingleton.getInstance(mContext).addToRequestQueue(request);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void handleResponse(JSONObject response, List<SafeTenant> selectedUsers, FragmentManager fm) {
        Log.d("res", response.toString());
        pDialog.dismiss();

        ApiResponse<SuccessMsg> res = JsonUtils.fromJson(response.toString(), SuccessMsg.class);
        if (!res.success) {
            UIHelper.showErrorAlert(mContext, res.error.message);
            return;
        }
        dismiss();
        Toast.makeText(mContext, res.message, Toast.LENGTH_SHORT).show();
        long totalRequestTime = System.currentTimeMillis() - mRequestStartTime;
        ChooseMatesConfirmDialogFragment fragment = ChooseMatesConfirmDialogFragment.newInstance(
                selectedUsers, totalRequestTime);
        fragment.setStyle(DialogFragment.STYLE_NORMAL, R.style.DialogFragmentTheme);
        fragment.show(fm, "chooseMatesConfirm");
    }

    private List<String> getSelectedUserIds(List<SafeTenant> selectedUsers) {
        List<String> ids = new ArrayList<>();
        for (SafeTenant tenant : selectedUsers) {
            ids.add(tenant.getUser().getId());
        }
        return ids;
    }

    private List<SafeTenant> getSelectedUsers() {
        List<SafeTenant> selectedUsers = new ArrayList<>();
        for (int i = 0; i < selection.size(); i++) {
            if (selection.get(i)) {
                selectedUsers.add(users.get(i));
            }
        }
        return selectedUsers;
    }

    private class ChooseMatesAdapter extends RecyclerView.Adapter<ChooseMatesAdapter.ViewHolder> {

        ChooseMatesAdapter() {

        }

        @NonNull
        @Override
        public ChooseMatesAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.fragment_choose_mates, parent, false);
            return new ChooseMatesAdapter.ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull final ChooseMatesAdapter.ViewHolder holder, int position) {
            holder.mItem = users.get(position);
            String name = holder.mItem.getUser().getUFirstName();
            holder.name.setText(name);
            AppController.getPicasso().load(R.drawable.splash_logo).into(holder.img);
            /*if (holder.mItem != null && holder.mItem. != null && holder.mItem.picture.getName() != null)
                loadPicture(holder.mItem.picture.getName(), holder.img);*/
            if (holder.mItem != null && holder.mItem.getCoffre() != null)
                holder.status.setText(holder.mItem.getCoffre());
            if (holder.mItem != null && holder.mItem.getProfession() != null)
                holder.position.setText(holder.mItem.getProfession());
            holder.mView.setOnClickListener(v -> {
                selection.set(position, !selection.get(position));
                if (selection.get(position)) {
                    holder.layout.setBackgroundColor(ContextCompat.getColor(mContext, R.color.mediumTurquoise));
                    holder.status.setVisibility(View.GONE);
                    AndroidUtils.setMarginStart(holder.img, AndroidUtils.dpToPx(mContext, 20));
                    holder.name.setTextColor(ContextCompat.getColor(mContext, android.R.color.white));
                    holder.position.setTextColor(ContextCompat.getColor(mContext, android.R.color.white));
                } else {
                    holder.layout.setBackgroundColor(ContextCompat.getColor(mContext, R.color.white));
                    holder.status.setVisibility(View.VISIBLE);
                    AndroidUtils.setMarginStart(holder.img, AndroidUtils.dpToPx(mContext, 8));
                    holder.name.setTextColor(ContextCompat.getColor(mContext, android.R.color.black));
                    holder.position.setTextColor(ContextCompat.getColor(mContext, android.R.color.black));
                }
                if (anySelected()) {
                    hideInviteHelp();
                    validate.setVisibility(View.VISIBLE);
                } else {
                    showInviteHelp();
                    validate.setVisibility(View.GONE);
                }
            });
        }

        @Override
        public int getItemCount() {
            return users.size();
        }

        private boolean anySelected() {
            for (boolean b : selection) {
                if (b)
                    return true;
            }
            return false;
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            public final View mView;
            public final TextView name;
            public final ImageView img;
            public final TextView position;
            public final TextView status;
            public final RelativeLayout layout;
            public SafeTenant mItem;

            public ViewHolder(View view) {
                super(view);
                mView = view;
                layout = view.findViewById(R.id.layout);
                name = view.findViewById(R.id.name);
                img = view.findViewById(R.id.img);
                position = view.findViewById(R.id.position);
                status = view.findViewById(R.id.status);
            }
        }
    }
}
