package com.codeairs.wattamatte.fragments.completeprofile.completeroomdetails;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import com.codeairs.wattamatte.R;
import com.codeairs.wattamatte.customcontrols.togglebutton.InterestToggleButton;
import com.codeairs.wattamatte.listeners.CompleteProfileInteractionListener;
import com.codeairs.wattamatte.listeners.CompleteProfileViewPagerOnNextInteractionListener;
import com.codeairs.wattamatte.utils.AndroidUtils;
import com.codeairs.wattamatte.utils.UIHelper;
import com.nex3z.togglebuttongroup.SingleSelectToggleGroup;

public class HaveFurnitureFragment extends Fragment {

    private static final int ID_HAVE_FURNITURE = 101;
    private static final int ID_NOT_HAVE_FURNITURE = 102;
    private static final int ID_NOT_DETERMINED = 103;

    private CompleteProfileViewPagerOnNextInteractionListener viewPagerListener;
    private CompleteProfileInteractionListener mListener;

    private SingleSelectToggleGroup toggleGroup;

    private Button nextButton;

    public HaveFurnitureFragment() {
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.layout_complete_profile_toggle, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        AndroidUtils.setText(view, R.id.title, R.string.have_furniture);
        initNextButton(view);
        initToggleButton(view);
        initSelection();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (getParentFragment() instanceof CompleteProfileViewPagerOnNextInteractionListener) {
            viewPagerListener = (CompleteProfileViewPagerOnNextInteractionListener) getParentFragment();
        } else {
            throw new RuntimeException("The parent fragment must implement OnChildFragmentInteractionListener");
        }
        if (getContext() instanceof CompleteProfileInteractionListener) {
            mListener = (CompleteProfileInteractionListener) getContext();
        } else {
            throw new RuntimeException("Not implemented");
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser)
            new Handler().postDelayed(() -> {
                if (mListener.getHaveFurniture() == 3)
                    viewPagerListener.disableSwipeToNextPage();
                else
                    viewPagerListener.enableSwipeToNextPage();
            }, 300);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        viewPagerListener = null;
        mListener = null;
    }

    private void initNextButton(View view) {
        nextButton = view.findViewById(R.id.nextButton);
        AndroidUtils.disableView(nextButton);
        nextButton.setOnClickListener(btn -> {
            syncData();
            viewPagerListener.handleNextButtonClick(3);
        });
    }

    private void syncData() {
        switch (toggleGroup.getCheckedId()) {
            case ID_HAVE_FURNITURE:
                mListener.handleHaveFurnitureChange(1);
                break;
            case ID_NOT_HAVE_FURNITURE:
                mListener.handleHaveFurnitureChange(0);
                break;
            case ID_NOT_DETERMINED:
                mListener.handleHaveFurnitureChange(2);
                break;
        }
    }

    private void initSelection() {
        switch (mListener.getHaveFurniture()) {
            case 0:
                toggleGroup.check(ID_NOT_HAVE_FURNITURE);
                break;
            case 1:
                toggleGroup.check(ID_HAVE_FURNITURE);
                break;
            case 2:
                toggleGroup.check(ID_NOT_DETERMINED);
        }
    }

    private void initToggleButton(View view) {
        if (getContext() == null)
            return;

        toggleGroup = view.findViewById(R.id.toggleGroup);

        int screenWidth = AndroidUtils.getScreenWidth(getActivity());

        RelativeLayout.LayoutParams p1 = new RelativeLayout.LayoutParams(
                AndroidUtils.dpToPx(getContext(), 324),
                RelativeLayout.LayoutParams.WRAP_CONTENT
        );
        p1.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
        toggleGroup.setLayoutParams(p1);

        SingleSelectToggleGroup.LayoutParams params = new SingleSelectToggleGroup.LayoutParams(
                AndroidUtils.dpToPx(getContext(), 108),
                AndroidUtils.dpToPx(getContext(), 148)
        );

        InterestToggleButton btn1 = UIHelper.getInterestsToggleButton(
                getContext(), ID_HAVE_FURNITURE, R.color.white, R.color.mediumTurquoise, R.string.yes,
                R.drawable.tick_green);

        InterestToggleButton btn2 = UIHelper.getInterestsToggleButton(
                getContext(), ID_NOT_DETERMINED, R.color.white, R.color.mediumTurquoise, R.string.to_be_determined,
                R.drawable.neutral_face);

        InterestToggleButton btn3 = UIHelper.getInterestsToggleButton(
                getContext(), ID_NOT_HAVE_FURNITURE, R.color.white, R.color.mediumTurquoise, R.string.no,
                R.drawable.cross_red);

        toggleGroup.addView(btn1, 0, params);
        toggleGroup.addView(btn2, 1, params);
        toggleGroup.addView(btn3, 2, params);

        toggleGroup.setOnCheckedChangeListener((group, checkedId) -> {
            AndroidUtils.enableView(nextButton);
            viewPagerListener.enableSwipeToNextPage();
        });
    }
}