package com.codeairs.wattamatte.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.codeairs.wattamatte.R;
import com.codeairs.wattamatte.activity.EditTenantPreferenceActivity;
import com.codeairs.wattamatte.activity.MainActivity;
import com.codeairs.wattamatte.adapters.RoomMatesRecyclerViewAdapter;
import com.codeairs.wattamatte.constants.ApiPaths;
import com.codeairs.wattamatte.listeners.MainInteractionListener;
import com.codeairs.wattamatte.listeners.TenantInteractionListener;
import com.codeairs.wattamatte.models.ApiResponse;
import com.codeairs.wattamatte.models.Tenant.Tenant;
import com.codeairs.wattamatte.models.Tenant.TenantBasic;
import com.codeairs.wattamatte.models.contacts.Contact;
import com.codeairs.wattamatte.utils.AndroidUtils;
import com.codeairs.wattamatte.utils.CommonUtils;
import com.codeairs.wattamatte.utils.JsonUtils;
import com.codeairs.wattamatte.utils.SharedPrefManager;
import com.codeairs.wattamatte.utils.internet.NetworkUtils;
import com.codeairs.wattamatte.utils.internet.VolleySingleton;
import com.google.common.collect.Lists;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

import static com.codeairs.wattamatte.activity.MainActivity.REQUEST_CODE_FILTER_TENANTS;
import static com.codeairs.wattamatte.activity.MainActivity.REQUEST_CODE_TENANT_FAV_CHANGE;

public class RoomMatesFragment extends Fragment implements View.OnClickListener,
        RoomMatesRecyclerViewAdapter.RoomMatesInteractionListener {

    public static final String TAG = RoomMatesFragment.class.getSimpleName();
    public static List<Tenant> filterResults;

    private MainInteractionListener mListener;
    private TenantInteractionListener matesListener;

    private List<Tenant> tenants = new ArrayList<>();
    private RecyclerView recyclerView;
    private EditText search;

    private CountDownLatch countDownLatch = new CountDownLatch(3);
    private boolean apiError = false;

    //todo remove this statement. This is a temporary Fix for FavUser API Fails. Please Remove this initialization favUserIds
    private List<String> favUserIds = new ArrayList<>();
    private List<String> contacts;
    private View emptyView;

    private RoomMatesRecyclerViewAdapter adapter;

    private Context mContext;

    private View view;

    private StaggeredGridLayoutManager staggeredGridLayoutManager;
    private int visibleThreshold = 5;
    private int page = 0;
    private boolean isLoading = false;
    private static boolean isLastPage = false;

    public RoomMatesFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (view != null) {
            tenants.clear();
            hideEmptyView();
            recyclerView.setAdapter(null);
            countDownLatch = new CountDownLatch(3);
            fetch();
            return view;
        }

        view = inflater.inflate(R.layout.fragment_roommates_list, container, false);
        MainActivity.adjustToolbarHeight(view, getContext());
        initControls(view);
        fetch();
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        if (context instanceof MainInteractionListener) {
            mListener = (MainInteractionListener) context;
        }
        if (context instanceof TenantInteractionListener) {
            matesListener = (TenantInteractionListener) context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {
        startActivityForResult(new Intent(getContext(), EditTenantPreferenceActivity.class),
                MainActivity.REQUEST_CODE_FILTER_TENANTS);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            if (getActivity() != null) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    getActivity().getWindow().getDecorView().setSystemUiVisibility(
                            View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR |
                                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
                }
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CODE_FILTER_TENANTS && data != null) {
            if (data.getBooleanExtra("filterResult", false)) {
                clearSearchKey(false);
                tenants.clear();
                tenants.addAll(filterResults);
                filterResults = null;
                adapter.notifyDataSetChanged();
            }
        } else if (requestCode == REQUEST_CODE_TENANT_FAV_CHANGE) {
            String userId = data.getStringExtra("userId");
            boolean status = data.getBooleanExtra("favStatus", false);
            CommonUtils.addOrRemoveFavorite(favUserIds, userId, status);
            adapter.notifyDataSetChanged();
        }
    }

    private void initControls(View view) {
        initEmptyView(view);
        // Set the adapter
        recyclerView = view.findViewById(R.id.list);

        search = view.findViewById(R.id.search);
        search.addTextChangedListener(searchListener);

        view.findViewById(R.id.change).setOnClickListener(this);
    }

    private void initEmptyView(View view) {
        emptyView = view.findViewById(R.id.emptyView);
        ImageView iv = emptyView.findViewById(R.id.img);
        iv.setImageResource(R.drawable.empty_matching);
        AndroidUtils.setText(view, R.id.msg, R.string.tenants_empty);
    }

    //
    private void fetch() {
        mListener.showLoading();
        page = 0;
        isLastPage = false;
        isLoading = false;
        if (TextUtils.isEmpty(search.getText()))
            fetchTenants();
        else
            search(search.getText().toString());
        getFavoriteUsers();
        fetchContacts();

        final Handler mainThreadHandler = new Handler(Looper.getMainLooper());
        new Thread(() -> {
            try {
                Log.d(TAG, "CountDownLatch : Await()");
                countDownLatch.await();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            mainThreadHandler.post(() -> {
                Log.d(TAG, "CountDownLatch : Entered");
                if (apiError) {
                    mListener.hideLoading();
                    //todo Remove this populateData() which is a temporary Fix for FavUser API Fails.
                    populateData();
                    Log.d(TAG, "CountDownLatch : API Error");
                } else {
                    populateData();
                    mListener.hideLoading();
                    Log.d(TAG, "CountDownLatch : Populate Data");
                }
            });
        }).start();
    }

    private void fetchTenants() {
        if (getContext() == null)
            return;
        isLoading = true;
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("userId", SharedPrefManager.getUserId(getContext()));
            if (page == 0) {
                jsonObject.put("init", "true");
            } else {
                jsonObject.put("init", "false");
                jsonObject.put("page", page);
            }
            Log.d(TAG, "Tenants Url : " + ApiPaths.PEOPLES);
            Log.d(TAG, "Tenants Request : " + jsonObject.toString());
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST,
                    ApiPaths.PEOPLES, jsonObject, this::handleFetchTenantsResponse, this::handleError) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    return NetworkUtils.authenticatedHeader(getContext());
                }

            };
            request.setShouldCache(false);
            VolleySingleton.getInstance(getContext()).addToRequestQueue(request);
        } catch (JSONException e) {
            e.printStackTrace();
            isLoading = false;
        }
    }

    private void handleFetchTenantsResponse(JSONObject response) {
        Log.d(TAG, "Tenants Response : " + response.toString());
        isLoading = false;
        ApiResponse<List<Tenant>> res = JsonUtils.fromJsonList(response.toString(), Tenant.class);
        if (res.success) {
            if (page == 0)
                tenants.clear();
            page++;
            CommonUtils.addTenantsWithoutDuplication(tenants, res.data);
            //tenants.addAll(res.data);
            try {
                for (Tenant ten : tenants) {
                    Log.d(TAG, "Tenant : " + ten.userId);
                }
                for (String ten : favUserIds) {

                    Log.d(TAG, "Fav : " + ten);
                }
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
            if (res.index != null) {
                Log.d(TAG, "Res.Index Not Null");
                if (res.index.equals("last")) {
                    isLastPage = true;
                    Log.d(TAG, "Res.Index IS Last Page");
                } else {
                    tenants.add(CommonUtils.getPagenationTenant());
                    Log.d(TAG, "Res.Index Not Last Page");
                }
            } else {
                Log.d(TAG, "Res.Index Null");
                tenants.add(CommonUtils.getPagenationTenant());
            }
            recyclerView.addOnScrollListener(pagenationScrollListener);
            Log.d(TAG, "Tenants Size: " + tenants.size());
            if (countDownLatch.getCount() != 0) {
                countDownLatch.countDown();
            } else {
                if (recyclerView.getAdapter() != null) {
                    adapter.removePagenationIndicator(false);
                    adapter.notifyDataSetChanged(); //Now the Pagenation Remover will auto notify dataset changed
                } else
                    populateData();
            }
        }
    }

    private void handleError(VolleyError error) {
        //todo Remove the code which might cause nullpointer exception
        Log.d(TAG, "Tenants Error : " + error.getMessage());
        isLoading = false;
        countDownLatch.countDown();
        apiError = true;
        if (error.networkResponse == null) {
            return;
        }

        String body;
        //get status code here
        String statusCode = String.valueOf(error.networkResponse.statusCode);
        //get response body and parse with appropriate encoding
        if (error.networkResponse.data != null) {
            try {
                body = new String(error.networkResponse.data, "UTF-8");
                Log.d("Error.Response: ", body);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
    }

    private void getFavoriteUsers() {
        Log.d(TAG, "Favourite Users Url : " + ApiPaths.USER_GET_ALL_FAVORITES + SharedPrefManager.getUserId(getContext()));
        StringRequest request = NetworkUtils.authenticatedGetRequest(getContext(), ApiPaths.USER_GET_ALL_FAVORITES + SharedPrefManager.getUserId(getContext()), this::handleFavouriteUserResponse, this::handleFavouriteUserError);
        request.setShouldCache(false);
        VolleySingleton.getInstance(getContext()).addToRequestQueue(request);
    }

    private void handleFavouriteUserResponse(String response) {
        Log.d(TAG, "Favourite Users Response : " + response);
        countDownLatch.countDown();
        ApiResponse<List<TenantBasic>> features = JsonUtils.fromJsonList(response, TenantBasic.class);
        favUserIds = Lists.newArrayList();
        if (features.success) {
            for (TenantBasic tenant : features.data) {
                favUserIds.add(tenant.userId);
            }
        }
    }

    private void handleFavouriteUserError(VolleyError error) {
        countDownLatch.countDown();
        apiError = true;
        Log.d(TAG, "Favourite Users Error : " + error.getMessage());
    }

    private void fetchContacts() {
        Log.d(TAG, "Contacts Url : " + ApiPaths.CHATTED_USERS);
        Log.d(TAG, "Request : " + NetworkUtils.commonPostBody(mContext));
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST,
                ApiPaths.CHATTED_USERS, NetworkUtils.commonPostBody(mContext),
                response -> {
                    Log.d(TAG, "Contacts Response : " + response.toString());
                    countDownLatch.countDown();
                    ApiResponse<List<Contact>> res = JsonUtils.fromJsonList(response.toString(), Contact.class);
                    contacts = Lists.newArrayList();
                    if (res.success) {
                        for (Contact contact : res.data) {
                            contacts.add(contact.user.userId);
                        }
                    }
                },
                error -> {
                    countDownLatch.countDown();
                    apiError = true;
                    //todo Remove the code which might cause nullpointer exception
                    Log.d(TAG, "Contacts Error : " + error.getMessage());
                }) {

            @Override
            public Map<String, String> getHeaders() {
                return NetworkUtils.authenticatedHeader(getContext());
            }

        };
        request.setShouldCache(true);
        VolleySingleton.getInstance(getContext()).addToRequestQueue(request);
    }

    private void search(String key) {

        JSONObject body = NetworkUtils.commonPostBody(mContext);
        try {
            body.put("name", key);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "Url : " + ApiPaths.SEARCH_PEOPLES);
        Log.d(TAG, "Request : " + body);
        VolleySingleton.getInstance(mContext).cancelPendingRequests("tenantSearch");
        JsonObjectRequest request = NetworkUtils.authenticatedPostRequest(
                mContext, ApiPaths.SEARCH_PEOPLES, body,
                response -> {
                    Log.d(TAG, "Response : " + response.toString());
                    ApiResponse<List<Tenant>> res = JsonUtils.fromJsonList(response.toString(), Tenant.class);
                    if (res.success) {
                        page = 0;
                        isLastPage = false;
                        tenants.clear();
                        tenants.addAll(res.data);
                        if (countDownLatch.getCount() > 0) {
                            countDownLatch.countDown();
                        } else {
                            if (recyclerView.getAdapter() == null) {
                                populateData();
                            } else {
                                adapter.notifyDataSetChanged();
                            }
                        }
                    }
                },
                error -> {

                });
        VolleySingleton.getInstance(mContext).addToRequestQueue(request, "tenantSearch");
    }

    private void populateData() {
        staggeredGridLayoutManager = new StaggeredGridLayoutManager(2,
                LinearLayoutManager.VERTICAL);

        recyclerView.setLayoutManager(staggeredGridLayoutManager);
        Log.d(TAG, "Applying adapter : " + tenants.size());
        adapter = new RoomMatesRecyclerViewAdapter(tenants, matesListener, this,
                favUserIds, contacts, getContext());
        recyclerView.setAdapter(adapter);
    }

    private RecyclerView.OnScrollListener pagenationScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            int[] lastVisibleItemPositions = staggeredGridLayoutManager.findLastVisibleItemPositions(null);
            int lastVisibleItem = getLastVisibleItem(lastVisibleItemPositions);
            int totalItemCount = staggeredGridLayoutManager.getItemCount();
            if (!isLoading && (lastVisibleItem + visibleThreshold) > totalItemCount && !isLastPage) {
                if (TextUtils.isEmpty(search.getText())) {
                    fetchTenants();
                    Log.d(TAG, "OnScrollLostener Now Loading for Page" + page);
                }
            }
        }
    };

    private TextWatcher searchListener = new TextWatcher() {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            if (recyclerView.getAdapter() == null) {
                return;
            }
            tenants.clear();
            hideEmptyView();
            recyclerView.setAdapter(null);
            if (TextUtils.isEmpty(s)) {
                VolleySingleton.getInstance(mContext).cancelPendingRequests("tenantSearch");
                //todo Resetting the Tenants when the search field is cleared. This causes the pagenation to start from first.
                page = 0;
                isLoading = false;
                isLastPage = false;
                fetchTenants();
                return;
            }
            search(s.toString());
        }
    };

    private void clearSearchKey(boolean doCallback) {
        if (!doCallback) {
            search.removeTextChangedListener(searchListener);
            recyclerView.clearOnScrollListeners();
        }
        search.setText(null);
        if (!doCallback) {
            search.addTextChangedListener(searchListener);
        }
    }

    @Override
    public void hideEmptyView() {
        recyclerView.setVisibility(View.VISIBLE);
        emptyView.setVisibility(View.GONE);
    }

    @Override
    public void showEmptyView() {
        recyclerView.setVisibility(View.INVISIBLE);
        emptyView.setVisibility(View.VISIBLE);
    }

    private void addPagenation() {

    }

    private void clearPagenation() {
        recyclerView.clearOnScrollListeners();
    }

    public int getLastVisibleItem(int[] lastVisibleItemPositions) {
        int maxSize = 0;
        for (int i = 0; i < lastVisibleItemPositions.length; i++) {
            if (i == 0) {
                maxSize = lastVisibleItemPositions[i];
            } else if (lastVisibleItemPositions[i] > maxSize) {
                maxSize = lastVisibleItemPositions[i];
            }
        }
        return maxSize;
    }

    public static void setFilterResults(List<Tenant> filteredResult) {
        filterResults = filteredResult;
        isLastPage = true;
    }
}