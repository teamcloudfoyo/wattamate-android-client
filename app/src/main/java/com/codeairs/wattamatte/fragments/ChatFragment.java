package com.codeairs.wattamatte.fragments;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.codeairs.wattamatte.R;
import com.codeairs.wattamatte.listeners.MainInteractionListener;
import com.codeairs.wattamatte.utils.AndroidUtils;

public class ChatFragment extends Fragment {

    private static final String TAG = ChatFragment.class.getSimpleName();
    private MainInteractionListener mListener;
    private TabLayout tabLayout;
    private EditText search;

    public ChatFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_chat, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        search = view.findViewById(R.id.search);
        search.addTextChangedListener(new SearchListener());
        adjustToolbarHeight(view);
        initTabs(view);
        loadFragment(new ConversationFragment(), "chat");
        if (mListener.isNotificationReceived()) {
            Log.d(TAG,"There is a notification");
            if (mListener.isContactRequestNotification()) {
                tabLayout.getTabAt(1).select();
                mListener.clearNotificationDatas();
                Log.d(TAG,"There is a Contact request notification ");
            } else if (mListener.isChatNotification()) {
                tabLayout.getTabAt(0).select();
                Log.d(TAG,"There is a Chat Messaging notification ");
            }
        }else
            Log.d(TAG,"There is a No notification ");
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            if (getActivity() != null) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    getActivity().getWindow().getDecorView().setSystemUiVisibility(
                            View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
                }
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        search.setText(null);
        filter(null);
    }

    private void initTabs(View view) {
        tabLayout = view.findViewById(R.id.tabs);
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.chat)));
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.req_received)));
        tabLayout.setSelectedTabIndicatorHeight(0);
        changeTabsFont();
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                switch (tab.getPosition()) {
                    case 0:
                        search.setText(null);
                        loadFragment(new ConversationFragment(), "chat");
                        break;
                    case 1:
                        search.setText(null);
                        loadFragment(new ContactRequestFragment(), "request");
                        break;
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
    }

    private void changeTabsFont() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/Metropolis-Medium.otf");
        ViewGroup vg = (ViewGroup) tabLayout.getChildAt(0);
        int tabsCount = vg.getChildCount();
        for (int j = 0; j < tabsCount; j++) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
            int tabChildCount = vgTab.getChildCount();
            for (int i = 0; i < tabChildCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {
                    ((TextView) tabViewChild).setTypeface(tf);
                    ((TextView) tabViewChild).setTextSize(14);
                }
            }
        }
    }

    private void loadFragment(Fragment fragment, String tag) {
        final FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        transaction.replace(R.id.chatFrame, fragment, tag).commit();
    }

    private void adjustToolbarHeight(View view) {
        ConstraintLayout parentConstraintLayout = view.findViewById(R.id.cLayout);
        parentConstraintLayout.setPadding(0, AndroidUtils.dpToPx(getContext(),
                AndroidUtils.getStatusBarHeight(getContext())), 0, 0);
    }


    class SearchListener implements TextWatcher {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            filter(s);
        }

    }

    private void filter(Editable s) {
        if (tabLayout.getSelectedTabPosition() == 0) {
            ConversationFragment fragment = (ConversationFragment) getChildFragmentManager()
                    .findFragmentByTag("chat");
            if (fragment != null) {
                fragment.filterContacts(s);
            }
        } else {
            ContactRequestFragment fragment = (ContactRequestFragment) getChildFragmentManager()
                    .findFragmentByTag("request");
            if (fragment != null) {
                fragment.filterRequests(s);
            }
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof MainInteractionListener) {
            mListener = (MainInteractionListener) context;
        }
    }
}
