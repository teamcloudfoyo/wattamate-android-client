package com.codeairs.wattamatte.fragments.completeprofile.completeroomdetails;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.Button;
import com.codeairs.wattamatte.R;
import com.codeairs.wattamatte.customcontrols.MatesCountToggleButton;
import com.codeairs.wattamatte.listeners.CompleteProfileInteractionListener;
import com.codeairs.wattamatte.listeners.CompleteProfileViewPagerOnNextInteractionListener;
import com.nex3z.togglebuttongroup.MultiSelectToggleGroup;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChooseMatesCountFragment extends Fragment {

    private CompleteProfileViewPagerOnNextInteractionListener viewPagerListener;
    private CompleteProfileInteractionListener mListener;

    private Button nextButton;
    private MultiSelectToggleGroup toggleGroup;

    public ChooseMatesCountFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_choose_mates_count, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initNextButton(view);
        initToggleButton(view);
        initSelection();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (getParentFragment() instanceof CompleteProfileViewPagerOnNextInteractionListener) {
            viewPagerListener = (CompleteProfileViewPagerOnNextInteractionListener) getParentFragment();
        } else {
            throw new RuntimeException("The parent fragment must implement OnChildFragmentInteractionListener");
        }
        if (getContext() instanceof CompleteProfileInteractionListener)
            mListener = (CompleteProfileInteractionListener) getContext();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser)
            viewPagerListener.disableSwipeToNextPage();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        viewPagerListener = null;
    }

    private void initSelection() {
        List<String> matesCount = mListener.getMatesCount();
        if (matesCount != null && matesCount.size() > 0) {
            for (String count : matesCount) {
                switch (count) {
                    case "1":
                        toggleGroup.check(R.id.toggle1);
                        break;
                    case "2":
                        toggleGroup.check(R.id.toggle2);
                        break;
                    case "3":
                        toggleGroup.check(R.id.toggle3);
                        break;
                    case "4":
                        toggleGroup.check(R.id.toggle4);
                        break;
                    case "5":
                        toggleGroup.check(R.id.toggle5);
                        break;
                    case "6+":
                        toggleGroup.check(R.id.toggle6);
                        break;
                }
            }
            enableNext();
        }
    }

    private void initNextButton(View view) {
        nextButton = view.findViewById(R.id.nextButton);
        nextButton.getBackground().setAlpha(80);
        nextButton.setEnabled(false);
        nextButton.setOnClickListener(btn ->
                viewPagerListener.handleNextButtonClick(5));
    }

    private void enableNext() {
        nextButton.getBackground().setAlpha(255);
        nextButton.setEnabled(true);
        viewPagerListener.enableSwipeToNextPage();
    }

    private void disableNext() {
        nextButton.getBackground().setAlpha(80);
        nextButton.setEnabled(false);
        viewPagerListener.disableSwipeToNextPage();
    }

    private void initToggleButton(View view) {
        toggleGroup = view.findViewById(R.id.toggleGroup);

        toggleGroup.setOnCheckedChangeListener((group, checkedId, isChecked) -> {
            MatesCountToggleButton btn = view.findViewById(checkedId);
            if (isChecked) {
                mListener.addMatesCount(btn.getText());
            } else {
                mListener.removeMatesCount(btn.getText());
            }
            enableNext();
        });
    }

}
