package com.codeairs.wattamatte.fragments.completeprofile.completeroomdetails;


import android.app.DatePickerDialog;
import android.content.Context;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import com.codeairs.wattamatte.R;
import com.codeairs.wattamatte.customcontrols.Circle;
import com.codeairs.wattamatte.listeners.CompleteProfileInteractionListener;
import com.codeairs.wattamatte.listeners.CompleteProfileNextArrowListener;
import com.codeairs.wattamatte.listeners.CompleteProfileViewPagerOnNextInteractionListener;
import com.codeairs.wattamatte.utils.AndroidUtils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class ChooseEntryDateFragment extends Fragment {

    private Calendar entryDate;
    private EditText input;
    private ImageButton nextArrowButton;
    private Button nextButton;

    private CompleteProfileViewPagerOnNextInteractionListener viewPagerListener;
    private CompleteProfileInteractionListener mListener;
    private Context mContext;

    private DatePickerDialog.OnDateSetListener dateSetListener = (view, year, monthOfYear, dayOfMonth) -> {
        entryDate.set(Calendar.YEAR, year);
        entryDate.set(Calendar.MONTH, monthOfYear);
        entryDate.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        updateInput();
    };

    public ChooseEntryDateFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_choose_entry_date, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initNextButton(view);
        input = view.findViewById(R.id.editText);
        makeInputADatePicker();
        initNextArrowButton(view);
        initSelection();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        if (getParentFragment() instanceof CompleteProfileViewPagerOnNextInteractionListener)
            viewPagerListener = (CompleteProfileViewPagerOnNextInteractionListener) getParentFragment();
        if (getContext() instanceof CompleteProfileInteractionListener)
            mListener = (CompleteProfileInteractionListener) getContext();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
    }

    private void initNextButton(View view) {
        nextButton = view.findViewById(R.id.nextButton);
        nextButton.setOnClickListener(btn ->
                viewPagerListener.handleNextButtonClick(7));
        AndroidUtils.disableView(nextButton);
    }

    private void initNextArrowButton(View view) {
        nextArrowButton = view.findViewById(R.id.btn);
        nextArrowButton.setColorFilter(ContextCompat.getColor(mContext, R.color.radicalRed),
                PorterDuff.Mode.SRC_IN);

        AndroidUtils.disableView(nextArrowButton);

        View line = view.findViewById(R.id.underline);
        Circle circle = view.findViewById(R.id.circle);
        int circleColor = ContextCompat.getColor(mContext, R.color.radicalRed);

        nextArrowButton.setOnClickListener(new CompleteProfileNextArrowListener(
                getActivity(), mContext, circle, circleColor, input, line,
                () -> viewPagerListener.handleNextButtonClick(7)));
    }

    private void makeInputADatePicker() {
        entryDate = Calendar.getInstance();

        input.setKeyListener(null);
        input.setCursorVisible(false);
        input.setPressed(false);
        input.setFocusable(false);

        input.setOnClickListener(view -> new DatePickerDialog(mContext,
                R.style.DarkSlateDialog, dateSetListener, entryDate.get(Calendar.YEAR),
                entryDate.get(Calendar.MONTH), entryDate.get(Calendar.DAY_OF_MONTH)).show()
        );
    }

    private void updateInput() {
        String myFormat = "dd / MM / yyyy";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.getDefault());
        input.setText(sdf.format(entryDate.getTime()));
        mListener.setEntryDate(entryDate.getTime());
        AndroidUtils.enableView(nextArrowButton);
        AndroidUtils.enableView(nextButton);
    }

    private void initSelection() {
        if (mListener.getEntryDate() != null) {
            String myFormat = "dd / MM / yyyy";
            SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.getDefault());
            input.setText(sdf.format(mListener.getEntryDate()));
            AndroidUtils.enableView(nextArrowButton);
            AndroidUtils.enableView(nextButton);
        }
    }
}
