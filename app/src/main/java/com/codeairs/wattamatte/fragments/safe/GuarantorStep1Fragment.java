package com.codeairs.wattamatte.fragments.safe;


import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.codeairs.wattamatte.R;
import com.codeairs.wattamatte.constants.ApiPaths;
import com.codeairs.wattamatte.dialogs.ProofSelectionDialog;
import com.codeairs.wattamatte.listeners.SafeListener;
import com.codeairs.wattamatte.models.ApiResponse;
import com.codeairs.wattamatte.models.MultipartData;
import com.codeairs.wattamatte.models.SafeVault.Guarantor.Guarantor;
import com.codeairs.wattamatte.models.SafeVault.Guarantor.GuarantorIdentitiyPiece;
import com.codeairs.wattamatte.models.SafeVault.InitializeSafe.Document;
import com.codeairs.wattamatte.models.SafeVault.InitializeSafe.DocumentType;
import com.codeairs.wattamatte.models.SafeVault.Proof;
import com.codeairs.wattamatte.models.SafeVault.Safe;
import com.codeairs.wattamatte.utils.AndroidUtils;
import com.codeairs.wattamatte.utils.CommonUtils;
import com.codeairs.wattamatte.utils.JsonUtils;
import com.codeairs.wattamatte.utils.SharedPrefManager;
import com.codeairs.wattamatte.utils.internet.MultipartRequest;
import com.codeairs.wattamatte.utils.internet.VolleySingleton;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;

import static android.app.Activity.RESULT_OK;

public class GuarantorStep1Fragment extends BaseSafeFragment implements ProofSelectionDialog.OnProofSelectedCallback {

    private SafeListener mListener;
    private EditText firstName, lastName, parentageLink;
    private TextView addProof;
    private ImageView proofImage;
    private Button validate;
    private File imageFile;
    private SweetAlertDialog pDialog;
    Document document;
    private DocumentType documentType;

    public GuarantorStep1Fragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_guarantor_step1, container, false);
        firstName = view.findViewById(R.id.firstname);
        lastName = view.findViewById(R.id.name);
        parentageLink = view.findViewById(R.id.realtion);
        addProof = view.findViewById(R.id.text);
        proofImage = view.findViewById(R.id.img1);
        validate = view.findViewById(R.id.nextButton);
        validate.setOnClickListener(validateClick);
        proofImage.setOnClickListener(captureImageClicked);
        addProof.setOnClickListener(captureImageClicked);
        return view;
    }

    private void initProofSelectionDialog() {
        ProofSelectionDialog proofSelectionDialog = new ProofSelectionDialog(getActivity(), GuarantorStep1Fragment.this, document.getDType());
        proofSelectionDialog.show();
    }

    View.OnClickListener captureImageClicked = view -> initProofSelectionDialog();

    View.OnClickListener validateClick = view -> handleValidateClick();

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof SafeListener)
            mListener = (SafeListener) context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void handleValidateClick() {
        boolean isValid = true;
        if (TextUtils.isEmpty(firstName.getText().toString())) {
            firstName.setError("Required Field");
            isValid = false;
        }
        if (TextUtils.isEmpty(lastName.getText().toString())) {
            lastName.setError("Required Field");
            isValid = false;
        }
        if (TextUtils.isEmpty(parentageLink.getText().toString())) {
            parentageLink.setError("Required Field");
            isValid = false;
        }
        if (imageFile == null) {
            Toast.makeText(getContext(), "Please capture Document photo", Toast.LENGTH_SHORT).show();
            isValid = false;
        }
        if (isValid)
            postIdentityPiece();
    }

    private void postIdentityPiece() {
        List<MultipartData> multipartData = new ArrayList<>();
        multipartData.add(new MultipartData("userId", SharedPrefManager.getUserId(getContext())));
        multipartData.add(new MultipartData("type", "fr_sejour"));
        multipartData.add(new MultipartData("firstName", firstName.getText().toString()));
        multipartData.add(new MultipartData("lastName", lastName.getText().toString()));
        multipartData.add(new MultipartData("lienFiliation", parentageLink.getText().toString()));
        multipartData.add(new MultipartData("forHebergeur", "false"));
        pDialog = CommonUtils.getDialog(getContext(), getString(R.string.please_wait));
        pDialog.show();
        MultipartRequest imageUploadReq = new MultipartRequest(
                SharedPrefManager.getUserId(getContext()),
                ApiPaths.SAFE_IDENTITY_PIECE,
                (Response.Listener<String>) response -> {
                    handleResponse(response);
                }, this::handleError, multipartData, imageFile);
        VolleySingleton.getInstance(getContext()).addToRequestQueue(imageUploadReq);
    }

    private void handleResponse(String response) {
        pDialog.dismiss();
        ApiResponse<Safe> safe = JsonUtils.fromJson(response, Safe.class);
        if (safe.success) {
            Toast.makeText(getContext(), safe.data.getMsg(), Toast.LENGTH_SHORT).show();
            goToUpdatedCompletonFragment();
        } else
            Toast.makeText(getContext(), safe.error.message, Toast.LENGTH_SHORT).show();
    }

    private void handleError(VolleyError error) {
        Toast.makeText(getContext(), "Sunucuya bağlanılamadı!", Toast.LENGTH_LONG).show();
        pDialog.dismiss();
        goToUpdatedCompletonFragment();
        /*String body;
        String statusCode = String.valueOf(error.networkResponse.statusCode);
        if (error.networkResponse.data != null) {
            try {
                body = new String(error.networkResponse.data, "UTF-8");
                Log.d("error", body);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }*/
    }

    private void goToUpdatedCompletonFragment() {
        Guarantor guarantor;
        if (mListener.getMySafe().getGuarantor() == null)
            guarantor = new Guarantor();
        else
            guarantor = mListener.getMySafe().getGuarantor();
        guarantor.setIdentitiyPiece(new GuarantorIdentitiyPiece());
        mListener.getMySafe().setGuarantor(guarantor);
        mListener.updateSafeCompletionStatus();
        if (getActivity() != null)
            getActivity().getSupportFragmentManager().popBackStackImmediate();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (getActivity() != null)
            AndroidUtils.changeStatusBarToWhite(getActivity().getWindow(), getContext());
    }

    @Override
    public void onProofSelected(DocumentType documentType) {
        this.documentType = documentType;
        CropImage.activity()
                .setGuidelines(CropImageView.Guidelines.ON)
                .setCropShape(CropImageView.CropShape.RECTANGLE)
                .setAspectRatio(3370, 2125)
                .start(getContext(), GuarantorStep1Fragment.this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            Toast.makeText(getContext(), "Image Captured", Toast.LENGTH_SHORT).show();
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                imageFile = new File(resultUri.getPath());
                proofImage.setImageURI(resultUri);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }
}
