package com.codeairs.wattamatte.fragments.dialogs;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.toolbox.JsonObjectRequest;
import com.codeairs.wattamatte.R;
import com.codeairs.wattamatte.activity.safe.MySafeActivity;
import com.codeairs.wattamatte.activity.safe.WelcomeToSafeActivity;
import com.codeairs.wattamatte.constants.ApiPaths;
import com.codeairs.wattamatte.customcontrols.button.MPMediumButton;
import com.codeairs.wattamatte.models.ApiResponse;
import com.codeairs.wattamatte.models.SafeVault.SafeResponse;
import com.codeairs.wattamatte.utils.AndroidUtils;
import com.codeairs.wattamatte.utils.JsonUtils;
import com.codeairs.wattamatte.utils.SharedPrefManager;
import com.codeairs.wattamatte.utils.internet.NetworkUtils;
import com.codeairs.wattamatte.utils.internet.VolleySingleton;

import org.json.JSONException;
import org.json.JSONObject;

public class GarantasOwnerDialogFragmen extends DialogFragment {

    private Context mContext;
    private MPMediumButton yes, no;

    public GarantasOwnerDialogFragmen() {
        // Required empty public constructor
    }

    public static final GarantasOwnerDialogFragmen newInstance() {
        GarantasOwnerDialogFragmen fragment = new GarantasOwnerDialogFragmen();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_garant_as_owner, container, false);
        setCancelable(false);
        initViews(view);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    private void initViews(View view) {
        yes = view.findViewById(R.id.yes);
        no = view.findViewById(R.id.no);
        yes.setOnClickListener(view12 -> updateGuarantorasOwner(true));
        no.setOnClickListener(view12 -> updateGuarantorasOwner(false));
    }

    private void updateGuarantorasOwner(boolean status) {
        try {
            JSONObject body = new JSONObject();
            body.put("userId", SharedPrefManager.getUserId(getContext()));
            body.put("bool", status);
            AndroidUtils.disableView(no);
            AndroidUtils.disableView(yes);
            JsonObjectRequest request = NetworkUtils.authenticatedPostRequest(
                    getContext(), ApiPaths.SAFE_GARANT_AS_OWNER, body,
                    response -> {
                        ApiResponse<SafeResponse> res = JsonUtils.fromJson(response.toString(), SafeResponse.class);
                        if (res.success) {
                            dismiss();
                            startActivity(new Intent(getContext(), WelcomeToSafeActivity.class));
                        } else
                            Toast.makeText(mContext, "" + res.error.message, Toast.LENGTH_SHORT).show();
                        Log.d("GarantResponse", response.toString());
                    }, error -> {
                        AndroidUtils.enableView(no);
                        AndroidUtils.enableView(yes);
                        Toast.makeText(mContext, getResources().getString(R.string.try_again_later), Toast.LENGTH_SHORT).show();
                    });
            request.setShouldCache(false);
            VolleySingleton.getInstance(getContext()).addToRequestQueue(request);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}