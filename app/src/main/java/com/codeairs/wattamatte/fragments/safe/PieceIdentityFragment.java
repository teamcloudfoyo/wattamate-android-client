package com.codeairs.wattamatte.fragments.safe;


import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.codeairs.wattamatte.R;
import com.codeairs.wattamatte.constants.ApiPaths;
import com.codeairs.wattamatte.dialogs.ProofSelectionDialog;
import com.codeairs.wattamatte.listeners.SafeListener;
import com.codeairs.wattamatte.models.MultipartData;
import com.codeairs.wattamatte.models.SafeVault.IdentitiyPiece;
import com.codeairs.wattamatte.models.SafeVault.InitializeSafe.Document;
import com.codeairs.wattamatte.models.SafeVault.InitializeSafe.DocumentType;
import com.codeairs.wattamatte.models.SafeVault.InitializeSafe.Form;
import com.codeairs.wattamatte.models.SafeVault.Proof;
import com.codeairs.wattamatte.utils.CommonUtils;
import com.codeairs.wattamatte.utils.SharedPrefManager;
import com.codeairs.wattamatte.utils.internet.MultipartRequest;
import com.codeairs.wattamatte.utils.internet.VolleySingleton;
import com.google.gson.Gson;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import cn.pedant.SweetAlert.SweetAlertDialog;

import static android.app.Activity.RESULT_OK;

public class PieceIdentityFragment extends BaseSafeFragment implements ProofSelectionDialog.OnProofSelectedCallback {

    private static final String TAG = PieceIdentityFragment.class.getSimpleName();
    private File imageFile;
    private EditText firstname, lastname, dob, parentageLink;

    private TextView addProof;
    private ImageView cardBg;
    private Button validate;
    private List<Proof> proofSelectionList;
    private SweetAlertDialog pDialog;
    private SafeListener mListener;
    private Document document;
    private DocumentType documentType;
    private Calendar entryDate;

    public PieceIdentityFragment() {
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_piece_identity, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        view.findViewById(R.id.nextButton).setOnClickListener(v -> handleValidateClick());
        firstname = view.findViewById(R.id.firstname);
        lastname = view.findViewById(R.id.name);
        parentageLink = view.findViewById(R.id.parentageLink);
        dob = view.findViewById(R.id.dob);
        addProof = view.findViewById(R.id.text);
        cardBg = view.findViewById(R.id.img1);
        validate = view.findViewById(R.id.nextButton);
        validate.setOnClickListener(validateClick);
        addProof.setOnClickListener(captureImageClicked);
        cardBg.setOnClickListener(captureImageClicked);
        proofSelectionList = CommonUtils.getIdentityPieceList(getContext());
        document = mListener.getPieceIdentityDocument();
        entryDate = Calendar.getInstance();
        CommonUtils.addDatePickerFeature(dob, datePickerClick);
        Log.d(TAG, "SupportCAF Json : " + new Gson().toJson(document));
    }

    private void initProofSelectionDialog() {
        ProofSelectionDialog proofSelectionDialog = new ProofSelectionDialog(getActivity(), PieceIdentityFragment.this, document.getDType());
        proofSelectionDialog.show();
    }

    View.OnClickListener captureImageClicked = view -> initProofSelectionDialog();

    View.OnClickListener validateClick = view -> handleValidateClick();

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof SafeListener)
            mListener = (SafeListener) context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void handleValidateClick() {
        boolean isValid = true;
        if (TextUtils.isEmpty(firstname.getText().toString())) {
            firstname.setError("Required Field");
            isValid = false;
        }
        if (TextUtils.isEmpty(lastname.getText().toString())) {
            lastname.setError("Required Field");
            isValid = false;
        }
        if (dob.getVisibility() == View.VISIBLE && TextUtils.isEmpty(dob.getText().toString())) {
            dob.setError("Required Field");
            isValid = false;
        }
        if (parentageLink.getVisibility() == View.VISIBLE && TextUtils.isEmpty(parentageLink.getText().toString())) {
            parentageLink.setError("Required Field");
            isValid = false;
        }
        if (imageFile == null) {
            Toast.makeText(getContext(), "Please capture Document photo", Toast.LENGTH_SHORT).show();
            isValid = false;
        }
        if (isValid)
            postIdentityPiece();
    }

    private void postIdentityPiece() {
        pDialog = CommonUtils.getDialog(getContext(), getString(R.string.please_wait));
        pDialog.show();
        List<MultipartData> multipartData = new ArrayList<>();
        multipartData.add(new MultipartData("userId", SharedPrefManager.getUserId(getContext())));
        multipartData.add(new MultipartData("type", "fr_sejour"));
        multipartData.add(new MultipartData("firstName", firstname.getText().toString()));
        multipartData.add(new MultipartData("lastName", lastname.getText().toString()));
        if(dob.getVisibility() == View.VISIBLE)multipartData.add(new MultipartData("dateOfBirth", dob.getText().toString()));
        //todo
        if(parentageLink.getVisibility() == View.VISIBLE)multipartData.add(new MultipartData("parentageLink", parentageLink.getText().toString()));
        multipartData.add(new MultipartData("forHebergeur", "true"));
        MultipartRequest imageUploadReq = new MultipartRequest(
                SharedPrefManager.getUserId(getContext()),
                ApiPaths.SAFE_IDENTITY_PIECE,
                (Response.Listener<String>) response -> {
                    handleResponse(response);
                }, this::handleError, multipartData, imageFile);
        VolleySingleton.getInstance(getContext()).addToRequestQueue(imageUploadReq);
    }

    private void handleResponse(String response) {
        pDialog.dismiss();
        goToUpdatedCompletonFragment();
    }

    private void handleError(VolleyError error) {
        pDialog.dismiss();
        Toast.makeText(getContext(), "Sunucuya bağlanılamadı!", Toast.LENGTH_LONG).show();
    }

    private void goToUpdatedCompletonFragment() {
        mListener.getMySafe().setMyIdentity(new IdentitiyPiece());
        mListener.updateSafeCompletionStatus();
        if (getActivity() != null)
            getActivity().getSupportFragmentManager().popBackStackImmediate();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            Toast.makeText(getContext(), "Image Captured", Toast.LENGTH_SHORT).show();
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                imageFile = new File(resultUri.getPath());
                cardBg.setImageURI(resultUri);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }

    @Override
    public void onProofSelected(DocumentType documentType) {
        this.documentType = documentType;
        updateUI(documentType);
        CropImage.activity()
                .setGuidelines(CropImageView.Guidelines.ON)
                .setCropShape(CropImageView.CropShape.RECTANGLE)
                .setAspectRatio(3370, 2125)
                .start(getContext(), PieceIdentityFragment.this);
    }

    private void updateUI(DocumentType documentType) {
        int userType = Integer.parseInt(SharedPrefManager.getSafeUserType(getContext()));
        boolean isFound = true;
        for (Form forms : documentType.getForm()) {
            if (forms.getChamp().equalsIgnoreCase("Date de naissance")) {
                isFound = false;
                for (int forWhome : forms.getForWhome()) {
                    if (forWhome == userType)
                        isFound = true;
                }
                if (isFound)
                    dob.setVisibility(View.VISIBLE);
                else
                    dob.setVisibility(View.GONE);
            } else if (forms.getChamp().equalsIgnoreCase("Lien de filiation")) {
                isFound = false;
                for (int forWhome : forms.getForWhome()) {
                    if (forWhome == userType)
                        isFound = true;
                }
                if (isFound)
                    parentageLink.setVisibility(View.VISIBLE);
                else
                    parentageLink.setVisibility(View.GONE);
            }
        }
    }

    View.OnClickListener datePickerClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            new DatePickerDialog(getContext(),
                    R.style.DarkSlateDialog, dateSetListener, entryDate.get(Calendar.YEAR),
                    entryDate.get(Calendar.MONTH), entryDate.get(Calendar.DAY_OF_MONTH)).show();
        }
    };

    private DatePickerDialog.OnDateSetListener dateSetListener = (view, year, monthOfYear, dayOfMonth) -> {
        entryDate.set(Calendar.YEAR, year);
        entryDate.set(Calendar.MONTH, monthOfYear);
        entryDate.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        updateInput();
    };

    private void updateInput() {
        String myFormat = "dd / MM / yyyy";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.getDefault());
        dob.setText(sdf.format(entryDate.getTime()));
    }
}
