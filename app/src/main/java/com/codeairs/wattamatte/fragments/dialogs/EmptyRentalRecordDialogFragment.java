package com.codeairs.wattamatte.fragments.dialogs;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.Button;
import android.widget.ImageButton;
import com.android.volley.toolbox.JsonObjectRequest;
import com.codeairs.wattamatte.R;
import com.codeairs.wattamatte.constants.ApiPaths;
import com.codeairs.wattamatte.models.ApiResponse;
import com.codeairs.wattamatte.models.SafeVault.SafeTenant;
import com.codeairs.wattamatte.models.SafeVault.SafeUser;
import com.codeairs.wattamatte.models.Tenant.Tenant;
import com.codeairs.wattamatte.utils.JsonUtils;
import com.codeairs.wattamatte.utils.internet.NetworkUtils;
import com.codeairs.wattamatte.utils.internet.VolleySingleton;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class EmptyRentalRecordDialogFragment extends DialogFragment {

    private static final String TAG = EmptyRentalRecordDialogFragment.class.getSimpleName();

    private static final String PARAM_PROPERTY_ID = "propertyId";

    private Context mContext;

    private String propertyId;

    public EmptyRentalRecordDialogFragment() {
        // Required empty public constructor
    }

    public static final EmptyRentalRecordDialogFragment newInstance(String propertyId) {
        EmptyRentalRecordDialogFragment fragment =  new EmptyRentalRecordDialogFragment();
        Bundle bundle =  new Bundle();
        bundle.putString(PARAM_PROPERTY_ID, propertyId);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        propertyId = getArguments().getString(PARAM_PROPERTY_ID);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_empty_rental_record_dialog, container, false);
        initClose(view);
        initFillButton(view);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    private void initClose(View view) {
        ImageButton btn = view.findViewById(R.id.close);
        btn.setOnClickListener(v -> dismiss());
    }

    private void initFillButton(View view) {
        Button btn = view.findViewById(R.id.btn);
        btn.setOnClickListener(v -> {
            dismiss();
            if (getActivity() != null)
                getMatchedUsers(getActivity().getSupportFragmentManager());
        });
    }

    private void getMatchedUsers(FragmentManager fm) {
        Log.d(TAG,"Api Path : "+ ApiPaths.ROOM_USERS_MATCHED);
        JsonObjectRequest request = NetworkUtils.authenticatedPostRequest(mContext,
                ApiPaths.ROOM_USERS_MATCHED, NetworkUtils.commonPostBody(mContext),
                response -> {
                    Log.d(TAG,"Response : "+ response.toString());
                    ApiResponse<List<SafeTenant>> res = JsonUtils.fromJsonList(response.toString(), SafeTenant.class);
                    if (!res.success) {
                        //TODO: handle
                        return;
                    }
                    showUsers(res.data, fm);
                },
                error -> {

                });
        VolleySingleton.getInstance(mContext).addToRequestQueue(request);
    }

    private void showUsers(List<SafeTenant> users, FragmentManager fm) {
        ChooseMatesDialogFragment fragment = ChooseMatesDialogFragment.newInstance(users, propertyId);
        fragment.setStyle(DialogFragment.STYLE_NORMAL, R.style.DialogFragmentTheme);
        fragment.show(fm, "chooseMates");
    }
}
