package com.codeairs.wattamatte.fragments.safe;


import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.codeairs.wattamatte.R;
import com.codeairs.wattamatte.constants.ApiPaths;
import com.codeairs.wattamatte.customcontrols.edittext.MPRegularEditText;
import com.codeairs.wattamatte.dialogs.ProofSelectionDialog;
import com.codeairs.wattamatte.listeners.SafeListener;
import com.codeairs.wattamatte.models.ApiResponse;
import com.codeairs.wattamatte.models.MultipartData;
import com.codeairs.wattamatte.models.SafeVault.AddressProof;
import com.codeairs.wattamatte.models.SafeVault.InitializeSafe.Document;
import com.codeairs.wattamatte.models.SafeVault.InitializeSafe.DocumentType;
import com.codeairs.wattamatte.models.SafeVault.InitializeSafe.Form;
import com.codeairs.wattamatte.models.SafeVault.Proof;
import com.codeairs.wattamatte.models.SafeVault.Safe;
import com.codeairs.wattamatte.utils.CommonUtils;
import com.codeairs.wattamatte.utils.JsonUtils;
import com.codeairs.wattamatte.utils.SharedPrefManager;
import com.codeairs.wattamatte.utils.internet.MultipartRequest;
import com.codeairs.wattamatte.utils.internet.VolleySingleton;
import com.google.gson.Gson;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import cn.pedant.SweetAlert.SweetAlertDialog;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddressProofFragment extends BaseSafeFragment implements ProofSelectionDialog.OnProofSelectedCallback {
    private static final String TAG = AddressProofFragment.class.getSimpleName();
    private MPRegularEditText firstName, lastName, address;
    private TextView addProof;
    private ImageView proofImage;
    private SweetAlertDialog pDialog;
    private SafeListener mListener;
    private Button validate;
    private File imageFile;
    private List<Proof> proofSelectionList;
    private Document document;
    private DocumentType documentType;
    private Calendar entryDate;
    private CheckBox heberguer;

    public AddressProofFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_address_proof, container, false);
        addProof = view.findViewById(R.id.text);
        address = view.findViewById(R.id.dob);
        lastName = view.findViewById(R.id.name);
        firstName = view.findViewById(R.id.firstname);
        proofImage = view.findViewById(R.id.img1);
        validate = view.findViewById(R.id.nextButton);
        heberguer = view.findViewById(R.id.checkBox);
        validate.setOnClickListener(validateClick);
        proofImage.setOnClickListener(captureImageClicked);
        proofImage.setOnClickListener(captureImageClicked);
        validate.setOnClickListener(validateClick);
        document = mListener.getAddressProofDocument();
        Log.d(TAG,"AddressProof Json : "+new Gson().toJson(document));
        return view;
    }

    private void initProofSelectionDialog() {
        proofSelectionList = CommonUtils.getAddressProofList(getContext());
        ProofSelectionDialog proofSelectionDialog = new ProofSelectionDialog(getActivity(), AddressProofFragment.this, document.getDType());
        proofSelectionDialog.show();
    }

    View.OnClickListener captureImageClicked = view -> initProofSelectionDialog();

    View.OnClickListener validateClick = view -> handleValidateClick();

    private void handleValidateClick() {
        boolean isValid = true;
        if (TextUtils.isEmpty(firstName.getText().toString())) {
            firstName.setError("Required Field");
            isValid = false;
        }
        if (TextUtils.isEmpty(lastName.getText().toString())) {
            lastName.setError("Required Field");
            isValid = false;
        }
        if (TextUtils.isEmpty(address.getText().toString())) {
            address.setError("Required Field");
            isValid = false;
        }
        if (imageFile == null) {
            Toast.makeText(getContext(), "Please capture Document photo", Toast.LENGTH_SHORT).show();
            isValid = false;
        }
        if (isValid)
            postIdentityPiece();
    }

    private void postIdentityPiece() {
        List<MultipartData> multipartData = new ArrayList<>();
        multipartData.add(new MultipartData("userId", SharedPrefManager.getUserId(getContext())));
        multipartData.add(new MultipartData("type", "fr_loyer"));
        multipartData.add(new MultipartData("firstName", firstName.getText().toString()));
        multipartData.add(new MultipartData("lastName", lastName.getText().toString()));
        multipartData.add(new MultipartData("address", address.getText().toString()));
        multipartData.add(new MultipartData("forHebergeur", heberguer.isChecked()?"true":"false"));
        pDialog = CommonUtils.getDialog(getContext(), getString(R.string.please_wait));
        pDialog.show();
        MultipartRequest imageUploadReq = new MultipartRequest(
                SharedPrefManager.getUserId(getContext()),
                ApiPaths.SAFE_IDENTITY_PIECE,
                (Response.Listener<String>) response -> {
                    handleResponse(response);
                },this::handleError, multipartData, imageFile);
        VolleySingleton.getInstance(getContext()).addToRequestQueue(imageUploadReq);
    }

    private void handleResponse(String response) {
        pDialog.dismiss();
        ApiResponse<Safe> safe = JsonUtils.fromJson(response, Safe.class);
        if (safe.success) {
            Toast.makeText(getContext(), safe.data.getMsg(), Toast.LENGTH_SHORT).show();
            goToUpdatedCompletonFragment();
        } else
            Toast.makeText(getContext(), safe.error.message, Toast.LENGTH_SHORT).show();

    }

    private void handleError(VolleyError error) {
        pDialog.dismiss();
        Toast.makeText(getContext(), "Sunucuya bağlanılamadı!", Toast.LENGTH_LONG).show();
    }

    private void goToUpdatedCompletonFragment() {
        mListener.getMySafe().setMyAddressProof(new AddressProof());
        mListener.updateSafeCompletionStatus();
        if (getActivity() != null)
            getActivity().getSupportFragmentManager().popBackStackImmediate();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            Toast.makeText(getContext(), "Image Captured", Toast.LENGTH_SHORT).show();
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                imageFile = new File(resultUri.getPath());
                proofImage.setImageURI(resultUri);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof SafeListener)
            mListener = (SafeListener) context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onProofSelected(DocumentType documentType) {
        this.documentType = documentType;
        CropImage.activity()
                .setGuidelines(CropImageView.Guidelines.ON)
                .setCropShape(CropImageView.CropShape.RECTANGLE)
                .setAspectRatio(3370, 2125)
                .start(getContext(), AddressProofFragment.this);
    }
}
