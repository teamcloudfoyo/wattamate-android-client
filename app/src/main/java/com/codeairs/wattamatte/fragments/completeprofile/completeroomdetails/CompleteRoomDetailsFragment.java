package com.codeairs.wattamatte.fragments.completeprofile.completeroomdetails;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.codeairs.wattamatte.R;
import com.codeairs.wattamatte.constants.FragmentTag;
import com.codeairs.wattamatte.constants.SwipeDirection;
import com.codeairs.wattamatte.customcontrols.SwipeBlockableViewPager;
import com.codeairs.wattamatte.listeners.CompleteProfileInteractionListener;
import com.codeairs.wattamatte.listeners.CompleteProfileViewPagerOnNextInteractionListener;
import com.rd.PageIndicatorView;

public class CompleteRoomDetailsFragment extends Fragment
        implements ViewPager.OnPageChangeListener, CompleteProfileViewPagerOnNextInteractionListener {

    private CompleteProfileInteractionListener nextListener;

    private PageIndicatorView pageIndicatorView;
    private SwipeBlockableViewPager viewPager;

    public CompleteRoomDetailsFragment() {
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.layout_complete_profile_viewpager, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViewPager(view);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof CompleteProfileInteractionListener) {
            nextListener = (CompleteProfileInteractionListener) context;
        } else {
            throw new RuntimeException("The parent fragment must implement OnChildFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        nextListener = null;
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }

    @Override
    public void onPageSelected(int position) {
        changeViewPagerIndicatorColor(position);
        pageIndicatorView.setSelection(position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {
    }

    private void initViewPager(View view) {
        pageIndicatorView = view.findViewById(R.id.pageIndicatorView);
        pageIndicatorView.setCount(8); // specify total count of indicators
        pageIndicatorView.setSelection(0);

        viewPager = view.findViewById(R.id.viewPager);
        viewPager.setOffscreenPageLimit(1);

        CompleteRoomDetailsAdapter adapter = new CompleteRoomDetailsAdapter(getChildFragmentManager());
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(this);
    }

    public int getCurrentPageNo() {
        return viewPager.getCurrentItem();
    }

    public void goToPreviousPage() {
        viewPager.setCurrentItem(getCurrentPageNo() - 1);
    }

    private void changeViewPagerIndicatorColor(int position) {
        if (getContext() == null)
            return;

        switch (position) {
            case 0:
                pageIndicatorView.setSelectedColor(ContextCompat.getColor(getContext(), R.color.darkSlateBlue));
                break;
            case 1:
                pageIndicatorView.setSelectedColor(ContextCompat.getColor(getContext(), R.color.darkSlate50Transparent));
                break;
            case 2:
                pageIndicatorView.setSelectedColor(ContextCompat.getColor(getContext(), R.color.radicalRed));
                break;
            case 3:
                pageIndicatorView.setSelectedColor(ContextCompat.getColor(getContext(), R.color.mediumTurquoise));
                break;
            case 4:
                pageIndicatorView.setSelectedColor(ContextCompat.getColor(getContext(), R.color.corn));
                break;
            case 5:
                pageIndicatorView.setSelectedColor(ContextCompat.getColor(getContext(), R.color.mediumTurquoise));
                break;
            case 6:
                pageIndicatorView.setSelectedColor(ContextCompat.getColor(getContext(), R.color.stPatricksBlue));
                break;
            default:
                pageIndicatorView.setSelectedColor(ContextCompat.getColor(getContext(), R.color.radicalRed));
                break;
        }
        pageIndicatorView.invalidate();
    }

    @Override
    public void handleNextButtonClick(int page) {
        if (page != 7) {
            viewPager.setCurrentItem(page + 1);
            return;
        }
        nextListener.handleOnNext(FragmentTag.COMPLETE_ROOM_DETAILS);
    }

    @Override
    public void disableSwipeToNextPage() {
        viewPager.setAllowedSwipeDirection(SwipeDirection.Left);
    }

    @Override
    public void enableSwipeToNextPage() {
        viewPager.setAllowedSwipeDirection(SwipeDirection.All);
    }

    private static class CompleteRoomDetailsAdapter extends FragmentPagerAdapter {

        CompleteRoomDetailsAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return new IsSmokerFragment();
                case 1:
                    return new DoILikePetsFragment();
                case 2:
                    return new HaveChildrenFragment();
                case 3:
                    return new HaveFurnitureFragment();
                case 4:
                    return new ChooseLocationFragment();
                case 5:
                    return new ChooseMatesCountFragment();
                case 6:
                    return new ChooseBudgetFragment();
                default:
                    return new ChooseEntryDateFragment();
            }
        }

        @Override
        public int getCount() {
            return 8;
        }
    }

}