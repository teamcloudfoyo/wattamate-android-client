package com.codeairs.wattamatte.fragments;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Request;
import com.android.volley.toolbox.JsonObjectRequest;
import com.codeairs.wattamatte.R;
import com.codeairs.wattamatte.adapters.ConversationRecyclerViewAdapter;
import com.codeairs.wattamatte.constants.ApiPaths;
import com.codeairs.wattamatte.fragments.dummy.DummyContent.DummyItem;
import com.codeairs.wattamatte.listeners.MainInteractionListener;
import com.codeairs.wattamatte.listeners.TenantInteractionListener;
import com.codeairs.wattamatte.models.ApiResponse;
import com.codeairs.wattamatte.models.contacts.Contact;
import com.codeairs.wattamatte.utils.JsonUtils;
import com.codeairs.wattamatte.utils.internet.NetworkUtils;
import com.codeairs.wattamatte.utils.internet.VolleySingleton;

import org.json.JSONObject;

import java.util.List;
import java.util.Map;

public class ConversationFragment extends Fragment {

    private TenantInteractionListener mListener;

    private Context context;

    private RecyclerView recyclerView;

    private List<Contact> allContacts, filteredContacts;

    private ConversationRecyclerViewAdapter adapter;

    private MainInteractionListener mainInteractionListener;

    public ConversationFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_conversation_list, container, false);
        context = view.getContext();
        recyclerView = view.findViewById(R.id.list);
        fetchContacts();
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof TenantInteractionListener) {
            mListener = (TenantInteractionListener) context;
        }
        if (context instanceof MainInteractionListener)
            mainInteractionListener = (MainInteractionListener) context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnListConversationsListener {
        // TODO: Update argument type and name
        void onListConversationInteraction(DummyItem item);
    }

    private void fetchContacts() {
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST,
                ApiPaths.CHATTED_USERS, NetworkUtils.commonPostBody(context),
                this::handleResponse,
                error -> {
                }) {

            @Override
            public Map<String, String> getHeaders() {
                return NetworkUtils.authenticatedHeader(getContext());
            }

        };
        request.setShouldCache(true);
        VolleySingleton.getInstance(getContext()).addToRequestQueue(request);
    }

    private void handleResponse(JSONObject response) {
        Log.d("ContactRequest.Response", response.toString());
        ApiResponse<List<Contact>> res = JsonUtils.fromJsonList(response.toString(), Contact.class);
        if (!res.success) {
            //TODO: handle
        }

        allContacts = res.data;
        filteredContacts = JsonUtils.fromJsonList(response.toString(), Contact.class).data;

        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        adapter = new ConversationRecyclerViewAdapter(filteredContacts, mListener, context);
        recyclerView.setAdapter(adapter);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(),
                LinearLayoutManager.VERTICAL);
        dividerItemDecoration.setDrawable(ContextCompat.getDrawable(context, R.drawable.divider));
        recyclerView.addItemDecoration(dividerItemDecoration);
        //This is to handle Notification from FCM
        if (mainInteractionListener != null && mainInteractionListener.isNotificationReceived() && mainInteractionListener.isChatNotification()) {
            Log.d("ChatFragment", "Listener not Null");
            new Handler().postDelayed(() -> adapter.selectUser(mainInteractionListener.getUserNotified()), 500);
        } else
            Log.d("ChatFragment", "Listener Null");
    }


    public void filterContacts(Editable s) {
        if (filteredContacts == null)
            return;

        if (TextUtils.isEmpty(s)) {
            filteredContacts.clear();
            filteredContacts.addAll(allContacts);
            adapter.notifyDataSetChanged();
            return;
        }
        filteredContacts.clear();
        for (Contact contact : allContacts) {
            if ((contact.user.firstName + contact.user.lastName).toLowerCase().contains(s.toString().toLowerCase())) {
                filteredContacts.add(contact);
            }
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onAttachFragment(Fragment childFragment) {
        super.onAttachFragment(childFragment);
        getParentFragment().getActivity();
    }
}
