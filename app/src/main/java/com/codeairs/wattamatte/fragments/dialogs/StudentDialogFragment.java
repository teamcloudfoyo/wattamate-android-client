package com.codeairs.wattamatte.fragments.dialogs;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.toolbox.JsonObjectRequest;
import com.codeairs.wattamatte.R;
import com.codeairs.wattamatte.adapters.SafeUserTypeAdapter;
import com.codeairs.wattamatte.constants.ApiPaths;
import com.codeairs.wattamatte.customcontrols.button.MPRegularButton;
import com.codeairs.wattamatte.models.ApiResponse;
import com.codeairs.wattamatte.models.SafeVault.SafeResponse;
import com.codeairs.wattamatte.models.SafeVault.SafeUser;
import com.codeairs.wattamatte.models.SuccessMsg;
import com.codeairs.wattamatte.utils.AndroidUtils;
import com.codeairs.wattamatte.utils.JsonUtils;
import com.codeairs.wattamatte.utils.SharedPrefManager;
import com.codeairs.wattamatte.utils.internet.NetworkUtils;
import com.codeairs.wattamatte.utils.internet.VolleySingleton;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static android.support.v7.widget.LinearLayoutManager.VERTICAL;

public class StudentDialogFragment extends DialogFragment implements SafeUserTypeAdapter.StudentSubTypeSelectedCallback {

    private static final String PARAM_USERS = "propertyId";
    private Context mContext;
    private MPRegularButton cancel;
    private ProgressBar progress;
    private ArrayList<SafeUser> userSubTypes;
    private SafeUserTypeAdapter adapter;
    private RecyclerView recyclerView;

    public StudentDialogFragment() {
        // Required empty public constructor
    }

    public static final StudentDialogFragment newInstance(ArrayList<SafeUser> userSubTypes) {
        StudentDialogFragment fragment = new StudentDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList(PARAM_USERS, userSubTypes);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        userSubTypes = getArguments().getParcelableArrayList(PARAM_USERS);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_proof_selection, container, false);
        setCancelable(false);
        initViews(view);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    private void initViews(View view) {
        cancel = view.findViewById(R.id.cancel);
        recyclerView = view.findViewById(R.id.proofSelectorRecyclerview);
        progress = view.findViewById(R.id.progress);
        adapter = new SafeUserTypeAdapter(StudentDialogFragment.this, userSubTypes);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        DividerItemDecoration customDivider = new DividerItemDecoration(getContext(), VERTICAL);
        customDivider.setDrawable(ContextCompat.getDrawable(getContext(), R.drawable.proof_item_divider));
        recyclerView.addItemDecoration(customDivider);
        recyclerView.setAdapter(adapter);
        cancel.setOnClickListener(view1 -> {
            dismiss();
        });
    }

    private void getMatchedUsers(String type) {
        try {
            JSONObject body = new JSONObject();
            body.put("userId", SharedPrefManager.getUserId(getContext()));
            body.put("type", type);
            progress.setVisibility(View.VISIBLE);
            JsonObjectRequest request = NetworkUtils.authenticatedPostRequest(
                    getContext(), ApiPaths.SAFE_PROFESSION_GARANT, body,
                    response -> {
                        progress.setVisibility(View.GONE);
                        ApiResponse<SafeResponse> res = JsonUtils.fromJson(response.toString(), SafeResponse.class);
                        if(res.success){
                            dismiss();
                            showGarantOwnerDialog();
                        }else
                            Toast.makeText(mContext, ""+res.error.message, Toast.LENGTH_SHORT).show();
                        Log.d("createSafePass", response.toString());
                    }, error -> {
                        progress.setVisibility(View.GONE);
                        Toast.makeText(mContext, getResources().getString(R.string.try_again_later), Toast.LENGTH_SHORT).show();
                    });
            request.setShouldCache(false);
            VolleySingleton.getInstance(getContext()).addToRequestQueue(request);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onStudentSubTypeSelected(SafeUser subType) {
        Toast.makeText(mContext, "STudent Selected", Toast.LENGTH_SHORT).show();
        getMatchedUsers(subType.getId());
    }

    private void showGarantOwnerDialog() {
        GarantasOwnerDialogFragmen fragment = GarantasOwnerDialogFragmen.newInstance();
        fragment.setStyle(DialogFragment.STYLE_NORMAL, R.style.DialogFragmentTheme);
        if (getActivity() != null)
            fragment.show(getActivity().getSupportFragmentManager(), "chooseGarantAsOwner");
    }
}

