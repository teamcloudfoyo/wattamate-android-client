package com.codeairs.wattamatte.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.android.volley.Request;
import com.android.volley.toolbox.JsonObjectRequest;
import com.codeairs.wattamatte.R;
import com.codeairs.wattamatte.adapters.ContactRequestRecyclerViewAdapter;
import com.codeairs.wattamatte.constants.ApiPaths;
import com.codeairs.wattamatte.listeners.TenantInteractionListener;
import com.codeairs.wattamatte.models.ApiResponse;
import com.codeairs.wattamatte.models.Tenant.Tenant;
import com.codeairs.wattamatte.utils.JsonUtils;
import com.codeairs.wattamatte.utils.internet.NetworkUtils;
import com.codeairs.wattamatte.utils.internet.VolleySingleton;
import org.json.JSONObject;

import java.util.List;
import java.util.Map;

public class ContactRequestFragment extends Fragment {

    private TenantInteractionListener mListener;

    private Context context;

    private RecyclerView recyclerView;
    private ContactRequestRecyclerViewAdapter adapter;

    private List<Tenant> allRequests, filteredRequests;

    public ContactRequestFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_contactrequest_list, container, false);
        context = view.getContext();
        recyclerView = view.findViewById(R.id.list);
        fetchRequests();
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof TenantInteractionListener) {
            mListener = (TenantInteractionListener) context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void fetchRequests() {
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST,
                ApiPaths.CONTACT_REQUESTS, NetworkUtils.commonPostBody(context),
                this::handleResponse,
                error -> {
                }) {

            @Override
            public Map<String, String> getHeaders() {
                return NetworkUtils.authenticatedHeader(getContext());
            }

        };
        request.setShouldCache(true);
        VolleySingleton.getInstance(getContext()).addToRequestQueue(request);
    }

    private void handleResponse(JSONObject response) {
        Log.d("ContactRequest.Response", response.toString());

        ApiResponse<List<Tenant>> res = JsonUtils.fromJsonList(response.toString(), Tenant.class);
        if (!res.success) {
            //TODO: handle
            return;
        }
        allRequests = res.data;
        filteredRequests = JsonUtils.fromJsonList(response.toString(), Tenant.class).data;

        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        adapter = new ContactRequestRecyclerViewAdapter(filteredRequests, mListener, context);
        recyclerView.setAdapter(adapter);
    }

    public void filterRequests(Editable s) {
        if (filteredRequests == null)
            return;

        if (TextUtils.isEmpty(s)) {
            filteredRequests.clear();
            filteredRequests.addAll(allRequests);
            adapter.notifyDataSetChanged();
            return;
        }
        filteredRequests.clear();
        for (Tenant tenant : allRequests) {
            if ((tenant.firstName + tenant.lastName).toLowerCase().contains(s.toString().toLowerCase())) {
                filteredRequests.add(tenant);
            }
        }
        adapter.notifyDataSetChanged();
    }
}
