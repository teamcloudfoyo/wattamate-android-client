package com.codeairs.wattamatte.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.ImageButton;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.codeairs.wattamatte.R;
import com.codeairs.wattamatte.activity.EditRoomPreferencesActivity;
import com.codeairs.wattamatte.activity.MainActivity;
import com.codeairs.wattamatte.adapters.PlacesAutoCompleteAdapter;
import com.codeairs.wattamatte.constants.ApiPaths;
import com.codeairs.wattamatte.listeners.RoomsInteractionListener;
import com.codeairs.wattamatte.models.ApiResponse;
import com.codeairs.wattamatte.models.Place;
import com.codeairs.wattamatte.models.rooms.Property;
import com.codeairs.wattamatte.models.rooms.Room;
import com.codeairs.wattamatte.utils.AndroidUtils;
import com.codeairs.wattamatte.utils.CommonUtils;
import com.codeairs.wattamatte.utils.JsonUtils;
import com.codeairs.wattamatte.utils.SharedPrefManager;
import com.codeairs.wattamatte.utils.internet.NetworkUtils;
import com.codeairs.wattamatte.utils.internet.VolleySingleton;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

import static com.codeairs.wattamatte.activity.MainActivity.REQUEST_CODE_FILTER_ROOMS;

public class RoomsFragment extends Fragment implements View.OnClickListener,
        RoomsInteractionListener {

    private static String TAG = RoomsFragment.class.getSimpleName();

    private boolean isListSelected = true;

    public static List<Property> filterResults;

    private List<String> favRoomsIds;
    private List<Property> roomsUnfiltered, roomsFiltered;

    private CountDownLatch countDownLatch = new CountDownLatch(2);
    private boolean apiError = false;

    private ImageButton listButton;
    private ImageButton locationButton;
    private AutoCompleteTextView autocompleteView;

    private Context mContext;
    private View view;

    public RoomsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (view != null) {
            fetch();
            return view;
        }

        view = inflater.inflate(R.layout.fragment_rooms, container, false);
        initToggleButtons(view);
        initAutoComplete(view);
        loadFragment(new RoomsListFragment(), "fr1");
        fetch();
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        MainActivity.adjustToolbarHeight(view, getContext());
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CODE_FILTER_ROOMS && data != null) {
            if (data.getBooleanExtra("filterResult", false)) {
                roomsFiltered = filterResults;
                filterResults = null;
                displayData();
            }
            return;
        }
        List<Fragment> fragments = getChildFragmentManager().getFragments();
        if (fragments != null) {
            for (Fragment fragment : fragments) {
                fragment.onActivityResult(requestCode, resultCode, data);
            }
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            if (getActivity() != null) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    getActivity().getWindow().getDecorView().setSystemUiVisibility(
                            View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR |
                                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
                }
            }
        }
    }

    private void initToggleButtons(View view) {
        listButton = view.findViewById(R.id.listButton);
        locationButton = view.findViewById(R.id.locationButton);
        view.findViewById(R.id.change).setOnClickListener(this);
        listButton.setOnClickListener(this);
        locationButton.setOnClickListener(this);
        listButton.setColorFilter(ContextCompat.getColor(mContext, R.color.colorAccent));
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.listButton) {
            if (isListSelected)
                return;
            isListSelected = true;
            listButton.setColorFilter(ContextCompat.getColor(mContext, R.color.colorAccent));
            locationButton.setColorFilter(ContextCompat.getColor(mContext, R.color.darkSlateBlue));
            loadFragment(new RoomsListFragment(), "fr1");
            new Handler().postDelayed(this::displayData, 500);
        } else if (id == R.id.locationButton) {
            if (!isListSelected)
                return;
            isListSelected = false;
            locationButton.setColorFilter(ContextCompat.getColor(mContext, R.color.colorAccent));
            listButton.setColorFilter(ContextCompat.getColor(mContext, R.color.darkSlateBlue));
            loadFragment(new RoomLocationsFragment(), "fr2");
            new Handler().postDelayed(this::displayData, 500);
        } else if (id == R.id.change) {
            startActivityForResult(new Intent(mContext, EditRoomPreferencesActivity.class),
                    REQUEST_CODE_FILTER_ROOMS);
        }
    }

    private void fetch() {
        if (!TextUtils.isEmpty(getSearchKey())) {
            fetchSearchResults();
        } else {
            fetchRooms();
        }
        fetchFavoriteRooms();

        final Handler mainThreadHandler = new Handler(Looper.getMainLooper());
        new Thread(() -> {
            try {
                countDownLatch.await();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            mainThreadHandler.post(() -> {
                if (apiError) {
                    return;
                }
                displayData();
            });
        }).start();
    }

    private void fetchRooms() {
        Log.d(TAG, "Rooms.url : " + ApiPaths.PROPERTIES);
        Log.d(TAG, "Rooms.request : " + NetworkUtils.commonPostBody(mContext).toString());
        JsonObjectRequest stringRequest = NetworkUtils.authenticatedPostRequest(mContext,
                ApiPaths.PROPERTIES, NetworkUtils.commonPostBody(mContext),
                this::handleFetchRoomsResponse, this::handleFetchRoomsError);
        stringRequest.setShouldCache(false);
        VolleySingleton.getInstance(mContext).addToRequestQueue(stringRequest);
    }

    private void handleFetchRoomsResponse(JSONObject response) {
        Log.d(TAG, "Rooms.Response : " + response.toString());
        ApiResponse<List<Room>> res = JsonUtils.fromJsonList(response.toString(), Room.class);
        if (!res.success) {
            // TODO: handle accordingly
            return;
        }
        roomsUnfiltered = CommonUtils.getProperties(res.data2);
        roomsFiltered = CommonUtils.getProperties(res.data2);
        countDownLatch.countDown();
    }

    private void handleFetchRoomsError(VolleyError error) {
        Log.d(TAG, "Rooms.Error Response : " + error.getMessage());
        apiError = true;
        countDownLatch.countDown();
    }

    private void fetchFavoriteRooms() {
        Log.d(TAG, "FavProperty.Url : " + ApiPaths.PROPERTY_GET_ALL_FAVORITES + SharedPrefManager.getUserId(mContext));
        StringRequest request = new StringRequest(Request.Method.GET,
                ApiPaths.PROPERTY_GET_ALL_FAVORITES + SharedPrefManager.getUserId(mContext),
                this::handleFetchFavoriteRoomsResponse,
                error -> {
                    Log.d(TAG, "FavProperty.Error Resp : " + error.getMessage());
                    apiError = true;
                    countDownLatch.countDown();
                }) {

            @Override
            public Map<String, String> getHeaders() {
                return NetworkUtils.authenticatedHeader(mContext);
            }

        };
        request.setShouldCache(true);
        VolleySingleton.getInstance(getContext()).addToRequestQueue(request);
    }

    private void handleFetchFavoriteRoomsResponse(String response) {
        Log.d(TAG, "FavProperty.Response : " + response);
        try {
            JSONObject root = new JSONObject(response);
            if (root.getBoolean("success")) {
                favRoomsIds = JsonUtils.getFavoriteRoomIds(response);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        countDownLatch.countDown();
    }

    private void fetchSearchResults() {
        Log.d(TAG, "fetchSearchResults.url : " + ApiPaths.SEARCH_PROPERTY);
        JSONObject body = NetworkUtils.commonPostBody(mContext);
        try {
            body.put("name", getSearchKey());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest stringRequest = NetworkUtils.authenticatedPostRequest(mContext,
                ApiPaths.SEARCH_PROPERTY, body, this::handleSearchResponse, this::handleSearchError);
        stringRequest.setShouldCache(false);
        VolleySingleton.getInstance(getContext()).addToRequestQueue(stringRequest);
    }

    private void handleSearchResponse(JSONObject response) {
        Log.d(TAG, "fetchSearchResults.resp : " + response.toString());
        ApiResponse<List<Property>> res = JsonUtils.fromJsonList(response.toString(), Property.class);
        if (res.success) {
            roomsFiltered = res.data;
        }

        if (countDownLatch.getCount() > 0) {
            countDownLatch.countDown();
        } else {
            displayData();
        }
    }

    private void handleSearchError(VolleyError error) {

    }

    private void loadFragment(Fragment fragment, String tag) {
        final FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        transaction.add(R.id.roomsFrame, fragment, tag).commit();
    }

    private void initAutoComplete(View view) {
        autocompleteView = view.findViewById(R.id.search);
        autocompleteView.setAdapter(
                new PlacesAutoCompleteAdapter(getActivity(), R.layout.autocomplete_list_item,
                        () -> ((Activity) mContext).runOnUiThread(() -> {
                            roomsFiltered = roomsUnfiltered;
                            displayData();
                        })));
        autocompleteView.setOnItemClickListener((parent, view1, position, id) -> {
            autocompleteView.clearFocus();
            if (getActivity() != null)
                AndroidUtils.hideSoftKeyboard(getActivity());
            Place place = (Place) parent.getItemAtPosition(position);
            autocompleteView.setText(place.location);
            fetchSearchResults();
        });
    }

    private void displayData() {
        Log.d(TAG,"displayData() called with isListSelected as "+isListSelected);
        if (isListSelected) {
            RoomsListFragment listFragment = (RoomsListFragment)
                    getChildFragmentManager().findFragmentByTag("fr1");
            if (listFragment != null && listFragment.isVisible()) {
                listFragment.displayData();
            }
        } else {
            RoomLocationsFragment locationsFragment = (RoomLocationsFragment)
                    getChildFragmentManager().findFragmentByTag("fr2");
            if (locationsFragment != null && locationsFragment.isVisible()) {
                locationsFragment.displayData();
            }
        }
    }

    @Override
    public String getSearchKey() {
        return autocompleteView.getText().toString();
    }

    @Override
    public void clearSearchKey() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            autocompleteView.setText(null, false);
        } else {
            autocompleteView.setText(null);
        }
    }

    @Override
    public List<Property> getRoomsToDisplay() {
        return roomsFiltered;
    }

    @Override
    public List<String> getFavoriteRoomsIds() {
        return favRoomsIds;
    }

    @Override
    public void removeFavorite(String id) {
        favRoomsIds.add(id);
    }

    @Override
    public void addFavorite(String id) {
        favRoomsIds.add(id);
    }

}
