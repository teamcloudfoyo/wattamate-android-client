package com.codeairs.wattamatte.fragments.completeprofile;

import android.app.DatePickerDialog;
import android.content.Context;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.codeairs.wattamatte.R;
import com.codeairs.wattamatte.constants.FragmentTag;
import com.codeairs.wattamatte.customcontrols.Circle;
import com.codeairs.wattamatte.customcontrols.FadePageTransformer;
import com.codeairs.wattamatte.listeners.CompleteProfileInteractionListener;
import com.codeairs.wattamatte.listeners.CompleteProfileNextArrowListener;
import com.codeairs.wattamatte.listeners.CompleteProfileViewPagerOnNextInteractionListener;
import com.codeairs.wattamatte.utils.AndroidUtils;
import com.nex3z.togglebuttongroup.SingleSelectToggleGroup;
import com.rd.PageIndicatorView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class PersonalInfoFragment extends Fragment
        implements ViewPager.OnPageChangeListener, CompleteProfileViewPagerOnNextInteractionListener {

    private static final int PAGE_NAME = 0;
    private static final int PAGE_DOB = 1;
    private static final int PAGE_GENDER = 2;
    private static final int PAGE_PROFESSION = 3;
    private static final int PAGE_PROFESSION_INFO = 4;

    private CompleteProfileInteractionListener mListener;

    private PageIndicatorView pageIndicatorView;
    private ViewPager viewPager;

    public PersonalInfoFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.layout_complete_profile_viewpager, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViewPager(view);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof CompleteProfileInteractionListener) {
            mListener = (CompleteProfileInteractionListener) context;
        } else {
            throw new RuntimeException("The parent fragment must implement OnChildFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void initViewPager(View view) {
        pageIndicatorView = view.findViewById(R.id.pageIndicatorView);
        pageIndicatorView.setCount(5); // specify total count of indicators
        pageIndicatorView.setSelection(0);

        viewPager = view.findViewById(R.id.viewPager);
        viewPager.setOffscreenPageLimit(1);
        viewPager.setPageTransformer(false, new FadePageTransformer());

        PersonalInfoAdapter adapter = new PersonalInfoAdapter(getChildFragmentManager());
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(this);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }

    @Override
    public void onPageSelected(int position) {
        changeViewPagerIndicatorColor(position);
        pageIndicatorView.setSelection(position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {
    }

    private void changeViewPagerIndicatorColor(int position) {
        if (getContext() == null)
            return;

        switch (position) {
            case PAGE_NAME:
                pageIndicatorView.setSelectedColor(ContextCompat.getColor(getContext(), R.color.darkSlateBlue));
                break;
            case PAGE_DOB:
                pageIndicatorView.setSelectedColor(ContextCompat.getColor(getContext(), R.color.radicalRed));
                break;
            case PAGE_GENDER:
                pageIndicatorView.setSelectedColor(ContextCompat.getColor(getContext(), R.color.mediumTurquoise));
                break;
            case PAGE_PROFESSION:
                pageIndicatorView.setSelectedColor(ContextCompat.getColor(getContext(), R.color.corn));
                break;
            case PAGE_PROFESSION_INFO:
                pageIndicatorView.setSelectedColor(ContextCompat.getColor(getContext(), R.color.mediumTurquoise));
                break;
        }
    }

    @Override
    public void handleNextButtonClick(int page) {
        if (page != 4) {
            changeViewPagerIndicatorColor(page + 1);
            viewPager.setCurrentItem(page + 1);
            return;
        }
        mListener.handleOnNext(FragmentTag.PERSONAL_INFO);
    }

    @Override
    public void disableSwipeToNextPage() {

    }

    @Override
    public void enableSwipeToNextPage() {

    }

    private static class PersonalInfoAdapter extends FragmentPagerAdapter {

        PersonalInfoAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return TextInputFragment.newInstance(R.string.your_name,
                            R.color.darkSlateBlue, R.string.first_name, PAGE_NAME);
                case 1:
                    return TextInputFragment.newInstance(R.string.your_dob,
                            R.color.radicalRed, R.string.dob_hint, PAGE_DOB);
                case 2:
                    return new GenderFragment();
                case 3:
                    return new ProfessionFragment();
                default:
                    return TextInputFragment.newInstance(R.string.i_am_a,
                            R.color.mediumTurquoise, R.string.my_job, PAGE_PROFESSION_INFO);
            }
        }

        @Override
        public int getCount() {
            return 5;
        }
    }

    public static class TextInputFragment extends Fragment {

        private static String PAGE_NO = "pageno", TITLE_RES_ID = "titleresid",
                TITLE_COLOR_RES_ID = "titlecolorresid", HINT_RES_ID = "hintresid";

        private int pageNo, titleResId, titleColorResId, hintResId, circleColor;
        private Calendar dob;

        private CompleteProfileViewPagerOnNextInteractionListener nextButtonClickListener;
        private CompleteProfileInteractionListener mListener;
        private DatePickerDialog.OnDateSetListener dateSetListener;

        private View view, line;
        private EditText input;
        private Button nextButton;
        private ImageButton nextArrowButton;

        static TextInputFragment newInstance(int title, int titleColor, int hint, int pageNo) {
            TextInputFragment fragment = new TextInputFragment();
            Bundle bundle = new Bundle();
            bundle.putInt(TITLE_RES_ID, title);
            bundle.putInt(TITLE_COLOR_RES_ID, titleColor);
            bundle.putInt(PAGE_NO, pageNo);
            bundle.putInt(HINT_RES_ID, hint);
            fragment.setArguments(bundle);
            return fragment;
        }

        public TextInputFragment() {
            // Required empty public constructor
        }

        @Override
        public void onAttach(Context context) {
            super.onAttach(context);
            if (getParentFragment() instanceof CompleteProfileViewPagerOnNextInteractionListener) {
                nextButtonClickListener = (CompleteProfileViewPagerOnNextInteractionListener) getParentFragment();
            } else {
                throw new RuntimeException("The parent fragment must implement OnChildFragmentInteractionListener");
            }
            if (getContext() instanceof CompleteProfileInteractionListener) {
                mListener = (CompleteProfileInteractionListener) getContext();
            } else {
                throw new RuntimeException("Not implemented");
            }
        }

        @Override
        public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            if (getArguments() != null) {
                pageNo = getArguments().getInt(PAGE_NO);
                titleResId = getArguments().getInt(TITLE_RES_ID);
                titleColorResId = getArguments().getInt(TITLE_COLOR_RES_ID);
                hintResId = getArguments().getInt(HINT_RES_ID);
            }
            view = inflater.inflate(R.layout.layout_personal_info, container, false);
            return view;
        }

        @Override
        public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
            super.onViewCreated(view, savedInstanceState);

            initTitle(view, getContext(), titleColorResId, titleResId);
            initEditText();
            initNextButton(view);
            initNextArrowButton(view);
            setInputText();

            if (pageNo == PAGE_PROFESSION)
                view.findViewById(R.id.titleHint).setVisibility(View.VISIBLE);
            if (pageNo == PAGE_DOB)
                makeInputADatePicker();
        }

        @Override
        public void onDetach() {
            super.onDetach();
            nextButtonClickListener = null;
            mListener = null;
        }

        @Override
        public void setUserVisibleHint(boolean isVisibleToUser) {
            super.setUserVisibleHint(isVisibleToUser);
            if (isVisibleToUser) {
                if (pageNo == 4)
                    if (mListener.getProfessionId() == 0) {
                        initTitle(view, getContext(), R.color.stPatricksBlue, R.string.which_university);
                        initInput(getContext(), input, R.color.mediumTurquoise, R.string.inseec,
                                view.findViewById(R.id.underline));
                    } else if (mListener.getProfessionId() == 1) {
                        initTitle(view, getContext(), R.color.stPatricksBlue, R.string.what_job);
                        initInput(getContext(), input, R.color.mediumTurquoise, R.string.cashier,
                                view.findViewById(R.id.underline));
                    }
            }
        }

        //to bring back the line and input on going back to previous page
        private void showFieldTextField() {
            if (getContext() == null)
                return;
            input.setVisibility(View.VISIBLE);
            line.setVisibility(View.VISIBLE);
            line.setBackgroundColor(ContextCompat.getColor(getContext(), titleColorResId));
        }

        private void initEditText() {
            input = view.findViewById(R.id.editText);
            initInput(getContext(), input, titleColorResId, hintResId,
                    view.findViewById(R.id.underline));

            input.addTextChangedListener(new TextWatcher() {

                public void onTextChanged(CharSequence s, int start, int before, int count) {
                }

                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                public void afterTextChanged(Editable s) {
                    if (TextUtils.isEmpty(input.getText().toString())) {
                        AndroidUtils.disableViews(nextButton, nextArrowButton);
                    } else {
                        AndroidUtils.enableViews(nextButton, nextArrowButton);
                    }
                }
            });
        }

        private void setInputText() {
            switch (pageNo) {
                case PAGE_NAME:
                    input.setText(mListener.getName());
                    break;
                case PAGE_DOB:
                    input.setText(mListener.getDob());
                    break;
                case PAGE_PROFESSION_INFO:
                    input.setText(mListener.getProfession());
            }
        }

        private void initNextButton(View view) {
            nextButton = view.findViewById(R.id.nextButton);
            AndroidUtils.disableView(nextButton);
            nextButton.setOnClickListener(view1 -> {
                syncData();
                nextButtonClickListener.handleNextButtonClick(pageNo);
            });
        }

        private void initNextArrowButton(View view) {
            if (null == getContext())
                return;

            nextArrowButton = view.findViewById(R.id.btn);
            nextArrowButton.setColorFilter(ContextCompat.getColor(getContext(), titleColorResId), PorterDuff.Mode.SRC_IN);
            AndroidUtils.disableView(nextArrowButton);

            line = view.findViewById(R.id.underline);
            Circle circle = view.findViewById(R.id.circle);
            initCircleColor();

            nextArrowButton.setOnClickListener(new CompleteProfileNextArrowListener(
                    getActivity(), getContext(), circle, circleColor, input, line,
                    () -> {
                        syncData();
                        nextButtonClickListener.handleNextButtonClick(pageNo);
                        new Handler().postDelayed(this::showFieldTextField, 1500);
                    }));
        }

        private void syncData() {
            switch (pageNo) {
                case PAGE_NAME:
                    mListener.handleOnNameChange(input.getText().toString());
                    break;
                case PAGE_DOB:
                    mListener.handleOnDobChange(input.getText().toString());
                    break;
                case PAGE_PROFESSION_INFO:
                    mListener.handleOnProfessionChange(input.getText().toString());
            }
        }

        private void initCircleColor() {
            if (null == getContext())
                return;

            switch (pageNo) {
                case PAGE_NAME:
                    circleColor = ContextCompat.getColor(getContext(), R.color.darkSlateBlue);
                    break;
                case PAGE_DOB:
                    circleColor = ContextCompat.getColor(getContext(), R.color.radicalRed);
                    break;
                case PAGE_PROFESSION_INFO:
                    circleColor = ContextCompat.getColor(getContext(), R.color.mediumTurquoise);
            }
        }

        private void makeInputADatePicker() {
            if (getContext() == null)
                return;
            dob = Calendar.getInstance();
            dob.set(1991, 5, 29);

            input.setKeyListener(null);
            input.setCursorVisible(false);
            input.setPressed(false);
            input.setFocusable(false);
            initDobPickerListener();
            input.setOnClickListener(view -> new DatePickerDialog(getContext(),
                    R.style.DarkSlateDialog, dateSetListener, dob.get(Calendar.YEAR),
                    dob.get(Calendar.MONTH), dob.get(Calendar.DAY_OF_MONTH)).show()
            );
        }

        private void initDobPickerListener() {
            dateSetListener = (view, year, monthOfYear, dayOfMonth) -> {
                dob.set(Calendar.YEAR, year);
                dob.set(Calendar.MONTH, monthOfYear);
                dob.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateInput();
            };
        }

        private void updateInput() {
            String myFormat = "dd / MM / yyyy";
            SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.getDefault());
            input.setText(sdf.format(dob.getTime()));
        }

    }

    public static class ProfessionFragment extends Fragment {

        private CompleteProfileViewPagerOnNextInteractionListener nextButtonClickListener;
        private CompleteProfileInteractionListener mListener;

        private Button nextButton;
        private SingleSelectToggleGroup toggleGroup;

        public ProfessionFragment() {
            // Required empty public constructor
        }

        @Override
        public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            return inflater.inflate(R.layout.layout_personal_info_profession, container, false);
        }

        @Override
        public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
            super.onViewCreated(view, savedInstanceState);
            initTitle(view, getContext(), R.color.mediumTurquoise, R.string.you_are);
            initNextButton(view);
            initToggleButtons(view);
        }

        @Override
        public void onAttach(Context context) {
            super.onAttach(context);
            if (getParentFragment() instanceof CompleteProfileViewPagerOnNextInteractionListener) {
                nextButtonClickListener = (CompleteProfileViewPagerOnNextInteractionListener) getParentFragment();
            } else {
                throw new RuntimeException("The parent fragment must implement OnChildFragmentInteractionListener");
            }
            if (getContext() instanceof CompleteProfileInteractionListener) {
                mListener = (CompleteProfileInteractionListener) getContext();
            } else {
                throw new RuntimeException("Not implemented");
            }
        }

        @Override
        public void onDetach() {
            super.onDetach();
            nextButtonClickListener = null;
        }

        private void initNextButton(View view) {
            nextButton = view.findViewById(R.id.nextButton);
            AndroidUtils.disableView(nextButton);
            nextButton.setOnClickListener(view1 -> {
                mListener.handleOnProfessionIdChange(getSelectedProfessionId());
                nextButtonClickListener.handleNextButtonClick(3);
            });
        }

        private void initToggleButtons(View view) {
            toggleGroup = view.findViewById(R.id.toggleGroup);

            toggleGroup.setOnCheckedChangeListener((group, checkedId) ->
                    AndroidUtils.enableView(nextButton));

            int professionId = mListener.getProfessionId();
            if (professionId == 0) {
                toggleGroup.check(R.id.student);
            } else if (professionId == 1) {
                toggleGroup.check(R.id.employee);
            } else if (professionId == 2) {
                toggleGroup.check(R.id.inSearch);
            }
        }

        private int getSelectedProfessionId() {
            if (toggleGroup.getCheckedId() == R.id.student)
                return 0;
            if (toggleGroup.getCheckedId() == R.id.employee)
                return 1;
            if (toggleGroup.getCheckedId() == R.id.inSearch)
                return 2;
            return 3;
        }
    }

    public static class GenderFragment extends Fragment {

        private CompleteProfileViewPagerOnNextInteractionListener nextButtonClickListener;
        private CompleteProfileInteractionListener mListener;

        private Button nextButton;
        private SingleSelectToggleGroup toggleGroup;

        public GenderFragment() {
            // Required empty public constructor
        }

        @Override
        public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            return inflater.inflate(R.layout.layout_personal_info_gender, container, false);
        }

        @Override
        public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
            super.onViewCreated(view, savedInstanceState);
            initTitle(view, getContext(), R.color.mediumTurquoise, R.string.you_are);
            initNextButton(view);
            initToggleButtons(view);
        }

        @Override
        public void onAttach(Context context) {
            super.onAttach(context);
            if (getParentFragment() instanceof CompleteProfileViewPagerOnNextInteractionListener) {
                nextButtonClickListener = (CompleteProfileViewPagerOnNextInteractionListener) getParentFragment();
            } else {
                throw new RuntimeException("The parent fragment must implement OnChildFragmentInteractionListener");
            }
            if (getContext() instanceof CompleteProfileInteractionListener) {
                mListener = (CompleteProfileInteractionListener) getContext();
            } else {
                throw new RuntimeException("Not implemented");
            }
        }

        @Override
        public void onDetach() {
            super.onDetach();
            nextButtonClickListener = null;
        }

        private void initNextButton(View view) {
            nextButton = view.findViewById(R.id.nextButton);
            AndroidUtils.disableView(nextButton);
            nextButton.setOnClickListener(view1 -> {
                mListener.handleGenderChange(getSelectedGenderId());
                nextButtonClickListener.handleNextButtonClick(2);
            });
        }

        private void initToggleButtons(View view) {
            toggleGroup = view.findViewById(R.id.toggleGroup);

            toggleGroup.setOnCheckedChangeListener((group, checkedId) ->
                    AndroidUtils.enableView(nextButton));

            int gender = mListener.getGender();
            if (gender == 1) {
                toggleGroup.check(R.id.male);
            } else if (gender == 2) {
                toggleGroup.check(R.id.female);
            }
        }

        private int getSelectedGenderId() {
            if (toggleGroup.getCheckedId() == R.id.male)
                return 1;
            if (toggleGroup.getCheckedId() == R.id.female)
                return 2;
            return 0;
        }
    }

    private static void initTitle(View view, Context context, int color, int titleRes) {
        TextView title = view.findViewById(R.id.title);
        title.setText(titleRes);
        title.setTextColor(ContextCompat.getColor(context, color));
    }

    private static void initInput(Context context, EditText input, int textColor, int hintRes,
                                  View underline) {
        input.setHint(hintRes);
        input.setTextColor(ContextCompat.getColor(context, R.color.darkSlateBlue));
        input.setInputType(InputType.TYPE_TEXT_VARIATION_PERSON_NAME);

        underline.setBackgroundColor(ContextCompat.getColor(context, textColor));

    }
}


