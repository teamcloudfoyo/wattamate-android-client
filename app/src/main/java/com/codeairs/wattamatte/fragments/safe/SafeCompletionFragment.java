package com.codeairs.wattamatte.fragments.safe;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.akexorcist.roundcornerprogressbar.RoundCornerProgressBar;
import com.codeairs.wattamatte.R;
import com.codeairs.wattamatte.listeners.SafeListener;
import com.codeairs.wattamatte.models.SafeVault.MySafe;
import com.codeairs.wattamatte.utils.AndroidUtils;

public class SafeCompletionFragment extends Fragment {

    private View step1, step2, step3, step4, step5, step6;

    private SafeListener mListener;

    private MySafe mySafe;

    private RoundCornerProgressBar progress;

    public SafeCompletionFragment() {
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_safe_completion, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initToolbar(view);
        initSteps(view);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof SafeListener)
            mListener = (SafeListener) context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void initToolbar(View view) {
        Toolbar toolbar = view.findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_back_chevron_white_24dp);
        AppCompatActivity activity = (AppCompatActivity) getActivity();
        if (activity == null)
            return;
        activity.setSupportActionBar(toolbar);
        if (activity.getSupportActionBar() != null) {
            activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            activity.getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
        toolbar.setNavigationOnClickListener(v -> getActivity().onBackPressed());
    }

    private void initSteps(View view) {
        step1 = view.findViewById(R.id.step1);
        step2 = view.findViewById(R.id.step2);
        step3 = view.findViewById(R.id.step3);
        step4 = view.findViewById(R.id.step4);
        step5 = view.findViewById(R.id.step5);
        step6 = view.findViewById(R.id.step6);
        progress = view.findViewById(R.id.progress_1);
        initText();
        ImageView iv = step1.findViewById(R.id.img1);
        iv.setImageResource(R.drawable.stepper_tick);
        step1.findViewById(R.id.img2).setVisibility(View.VISIBLE);

        step2.setOnClickListener(v -> mListener.openPieceIdentity());
        step3.setOnClickListener(v -> mListener.openAddressProof());
        step4.setOnClickListener(v -> mListener.openGuarantor());
        step5.setOnClickListener(v -> mListener.openSupportCAF());
        step6.setOnClickListener(v -> mListener.openStudentCard());
        mySafe = mListener.getMySafe();
        updateSafeCompletionStatus(mySafe);
    }

    public void updateSafeCompletionStatus(MySafe mySafe) {
        if (mySafe == null) {
            return;
        }
        progress.setProgress(mySafe.getCompletionPercentage());
        if (mySafe.getMyIdentity() == null) {
            AndroidUtils.setImage(step2.findViewById(R.id.img1), R.drawable.circle_gray_50px);
            step2.findViewById(R.id.img2).setVisibility(View.INVISIBLE);
        } else {
            AndroidUtils.setImage(step2.findViewById(R.id.img1), R.drawable.stepper_tick);
            step2.findViewById(R.id.img2).setVisibility(View.VISIBLE);
        }
        if (mySafe.getMyAddressProof() == null) {
            AndroidUtils.setImage(step3.findViewById(R.id.img1), R.drawable.circle_gray_50px);
            step3.findViewById(R.id.img2).setVisibility(View.INVISIBLE);
        } else {
            AndroidUtils.setImage(step3.findViewById(R.id.img1), R.drawable.stepper_tick);
            step3.findViewById(R.id.img2).setVisibility(View.VISIBLE);
        }
        if (mySafe.getGuarantor() == null) {
            AndroidUtils.setImage(step4.findViewById(R.id.img1), R.drawable.circle_gray_50px);
            step4.findViewById(R.id.img2).setVisibility(View.INVISIBLE);
        } else if (mySafe.isGuarantorCompleted()) {
            AndroidUtils.setImage(step4.findViewById(R.id.img1), R.drawable.stepper_tick);
            step4.findViewById(R.id.img2).setVisibility(View.VISIBLE);
        }
        if (mySafe.getCafHelp() == null) {
            AndroidUtils.setImage(step5.findViewById(R.id.img1), R.drawable.circle_gray_50px);
            step5.findViewById(R.id.img2).setVisibility(View.INVISIBLE);
        } else {
            AndroidUtils.setImage(step5.findViewById(R.id.img1), R.drawable.stepper_tick);
            step5.findViewById(R.id.img2).setVisibility(View.VISIBLE);
        }
        if (mySafe.getStudentCard() == null) {
            AndroidUtils.setImage(step6.findViewById(R.id.img1), R.drawable.circle_gray_50px);
            step6.findViewById(R.id.img2).setVisibility(View.INVISIBLE);
        } else {
            AndroidUtils.setImage(step6.findViewById(R.id.img1), R.drawable.stepper_tick);
            step6.findViewById(R.id.img2).setVisibility(View.VISIBLE);
        }
    }

    private void initText() {
        AndroidUtils.setText(step1, R.id.text, R.string.personal_info);
        AndroidUtils.setText(step2, R.id.text, R.string.piece_identity);
        AndroidUtils.setText(step3, R.id.text, R.string.address_proff);
        AndroidUtils.setText(step4, R.id.text, R.string.guarantor);
        AndroidUtils.setText(step5, R.id.text, R.string.caf);
        AndroidUtils.setText(step6, R.id.text, R.string.st_card);
    }
}

