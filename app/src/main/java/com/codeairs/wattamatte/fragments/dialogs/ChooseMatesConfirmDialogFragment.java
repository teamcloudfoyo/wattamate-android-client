package com.codeairs.wattamatte.fragments.dialogs;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import com.codeairs.wattamatte.R;
import com.codeairs.wattamatte.models.SafeVault.SafeTenant;
import com.codeairs.wattamatte.models.Tenant.Tenant;
import com.codeairs.wattamatte.utils.AndroidUtils;
import com.codeairs.wattamatte.utils.SpacesItemDecoration;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.text.MessageFormat;
import java.util.List;

import static com.codeairs.wattamatte.utils.CommonUtils.loadPicture;

public class ChooseMatesConfirmDialogFragment extends DialogFragment {

    private static final String PARAM_USERS = "users";
    private static final String PARAM_TIME = "requestTime";

    private Context mContext;

    private List<SafeTenant> users;
    private long resTime;

    public ChooseMatesConfirmDialogFragment() {
    }

    static ChooseMatesConfirmDialogFragment newInstance(List<SafeTenant> users, long totalRequestTime) {
        ChooseMatesConfirmDialogFragment fragment = new ChooseMatesConfirmDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putString(PARAM_USERS, new Gson().toJson(users));
        bundle.putLong(PARAM_TIME, totalRequestTime);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            users = new Gson().fromJson(getArguments().getString(PARAM_USERS),
                    new TypeToken<List<SafeTenant>>() {
                    }.getType());
            resTime = getArguments().getLong(PARAM_TIME);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_choose_mates_confirm_list, container, false);
        initClose(view);
        initList(view);
        initResTime(view);
        view.findViewById(R.id.next).setOnClickListener(v -> dismiss());
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private void initClose(View view) {
        ImageButton btn = view.findViewById(R.id.close);
        btn.setOnClickListener(v -> dismiss());
    }

    private void initList(View view) {
        RecyclerView recyclerView = view.findViewById(R.id.list);
        recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        recyclerView.setAdapter(new MatesConfirmAdapter());

        recyclerView.addItemDecoration(new SpacesItemDecoration(24, 0));
    }

    private void initResTime(View view) {
        double seconds = resTime / 1000.0;
        String text = MessageFormat.format(getString(R.string.response_time), "9h");
        AndroidUtils.setText(view, R.id.resTime, text);
    }

    private class MatesConfirmAdapter extends RecyclerView.Adapter<MatesConfirmAdapter.ViewHolder> {

        MatesConfirmAdapter() {

        }

        @NonNull
        @Override
        public MatesConfirmAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.fragment_choose_mates_confirm, parent, false);
            return new MatesConfirmAdapter.ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull final MatesConfirmAdapter.ViewHolder holder, int position) {
            holder.mItem = users.get(position);
            String name = holder.mItem.getUser().getUFirstName();
            holder.name.setText(name);
            holder.details.setText(holder.mItem.getProfession());
            //loadPicture(holder.mItem.picture.getName(), holder.img);
            holder.mView.setOnClickListener(v -> {
            });
        }

        @Override
        public int getItemCount() {
            return users.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            public final View mView;
            public final ImageView img;
            public final TextView name;
            public final TextView details;
            public SafeTenant mItem;

            public ViewHolder(View view) {
                super(view);
                mView = view;
                img = view.findViewById(R.id.img);
                name = view.findViewById(R.id.name);
                details = view.findViewById(R.id.details);
            }

        }
    }
}
