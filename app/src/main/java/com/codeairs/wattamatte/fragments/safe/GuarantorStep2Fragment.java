package com.codeairs.wattamatte.fragments.safe;


import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.codeairs.wattamatte.R;
import com.codeairs.wattamatte.constants.ApiPaths;
import com.codeairs.wattamatte.listeners.SafeListener;
import com.codeairs.wattamatte.models.ApiResponse;
import com.codeairs.wattamatte.models.MultipartData;
import com.codeairs.wattamatte.models.SafeVault.Guarantor.AccomodationCertificate;
import com.codeairs.wattamatte.models.SafeVault.Guarantor.Guarantor;
import com.codeairs.wattamatte.models.SafeVault.Guarantor.GuarantorIdentitiyPiece;
import com.codeairs.wattamatte.models.SafeVault.Safe;
import com.codeairs.wattamatte.utils.AndroidUtils;
import com.codeairs.wattamatte.utils.CommonUtils;
import com.codeairs.wattamatte.utils.JsonUtils;
import com.codeairs.wattamatte.utils.SharedPrefManager;
import com.codeairs.wattamatte.utils.internet.MultipartRequest;
import com.codeairs.wattamatte.utils.internet.VolleySingleton;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;

import static android.app.Activity.RESULT_OK;

public class GuarantorStep2Fragment extends BaseSafeFragment {

    private TextView addProof;
    private ImageView proofImage;
    private Button validate;
    private File imageFile;
    private SweetAlertDialog pDialog;
    private SafeListener mListener;

    public GuarantorStep2Fragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_guarantor_step2, container, false);
        addProof = view.findViewById(R.id.text);
        proofImage = view.findViewById(R.id.img1);
        validate = view.findViewById(R.id.nextButton);
        validate.setOnClickListener(validateClick);
        addProof.setOnClickListener(captureImageClicked);
        proofImage.setOnClickListener(captureImageClicked);
        return view;
    }

    View.OnClickListener captureImageClicked = view -> CropImage.activity()
            .setGuidelines(CropImageView.Guidelines.ON)
            .setCropShape(CropImageView.CropShape.RECTANGLE)
            .setAspectRatio(3370, 2125)
            .start(getContext(), GuarantorStep2Fragment.this);

    View.OnClickListener validateClick = view -> handleValidateClick();

    private void handleValidateClick() {
        boolean isValid = true;
        if (imageFile == null) {
            Toast.makeText(getContext(), "Please capture Document photo", Toast.LENGTH_SHORT).show();
            isValid = false;
        }
        if (isValid)
            postIdentityPiece();
    }

    private void postIdentityPiece() {
        List<MultipartData> multipartData = new ArrayList<>();
        multipartData.add(new MultipartData("userId", SharedPrefManager.getUserId(getContext())));
        multipartData.add(new MultipartData("type", "fr_attest_hebergement"));
        pDialog = CommonUtils.getDialog(getContext(), getString(R.string.please_wait));
        pDialog.show();
        MultipartRequest imageUploadReq = new MultipartRequest(
                SharedPrefManager.getUserId(getContext()),
                ApiPaths.SAFE_IDENTITY_PIECE,
                (Response.Listener<String>) response -> {
                    handleResponse(response);
                }, this::handleError, multipartData, imageFile);
        VolleySingleton.getInstance(getContext()).addToRequestQueue(imageUploadReq);
    }

    private void goToUpdatedCompletonFragment() {
        Guarantor guarantor;
        if (mListener.getMySafe().getGuarantor() == null)
            guarantor = new Guarantor();
        else
            guarantor = mListener.getMySafe().getGuarantor();
        guarantor.setAccomodationCertificate(new AccomodationCertificate());
        mListener.getMySafe().setGuarantor(guarantor);
        mListener.updateSafeCompletionStatus();
        pDialog.dismiss();
        if (getActivity() != null)
            getActivity().getSupportFragmentManager().popBackStackImmediate();
    }

    private void handleResponse(String response) {
        pDialog.dismiss();
        ApiResponse<Safe> safe = JsonUtils.fromJson(response, Safe.class);
        if (safe.success) {
            Toast.makeText(getContext(), safe.data.getMsg(), Toast.LENGTH_SHORT).show();
            goToUpdatedCompletonFragment();
        } else
            Toast.makeText(getContext(), safe.error.message, Toast.LENGTH_SHORT).show();
    }

    private void handleError(VolleyError error) {
        Toast.makeText(getContext(), "Sunucuya bağlanılamadı!", Toast.LENGTH_LONG).show();
        goToUpdatedCompletonFragment();
        /*String body;
        String statusCode = String.valueOf(error.networkResponse.statusCode);
        if (error.networkResponse.data != null) {
            try {
                body = new String(error.networkResponse.data, "UTF-8");
                Log.d("error", body);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }*/
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            Toast.makeText(getContext(), "Image Captured", Toast.LENGTH_SHORT).show();
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                imageFile = new File(resultUri.getPath());
                proofImage.setImageURI(resultUri);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof SafeListener)
            mListener = (SafeListener) context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (getActivity() != null)
            AndroidUtils.changeStatusBarToWhite(getActivity().getWindow(), getContext());
    }

}
