package com.codeairs.wattamatte.fragments.completeprofile.completeroomdetails;

import android.content.Context;
import android.graphics.PorterDuff;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.*;
import com.codeairs.wattamatte.R;
import com.codeairs.wattamatte.adapters.PlacesAutoCompleteAdapter;
import com.codeairs.wattamatte.customcontrols.Circle;
import com.codeairs.wattamatte.listeners.CompleteProfileInteractionListener;
import com.codeairs.wattamatte.listeners.CompleteProfileNextArrowListener;
import com.codeairs.wattamatte.listeners.CompleteProfileViewPagerOnNextInteractionListener;
import com.codeairs.wattamatte.models.Place;
import com.codeairs.wattamatte.models.completeprofiledetails.Address;
import com.codeairs.wattamatte.utils.AndroidUtils;
import com.codeairs.wattamatte.utils.internet.PlaceDetailsAPI;

public class ChooseLocationFragment extends Fragment {

    private CompleteProfileViewPagerOnNextInteractionListener viewPagerListener;
    private CompleteProfileInteractionListener mListener;
    private Context mContext;

    private Button nextButton;
    private ImageButton nextArrowButton;
    private AutoCompleteTextView autocompleteView;
    private View line;

    public ChooseLocationFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_choose_location, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initNextButton(view);
        initAutoComplete(view);
        initNextArrowButton(view);
        initSelection();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        if (getParentFragment() instanceof CompleteProfileViewPagerOnNextInteractionListener) {
            viewPagerListener = (CompleteProfileViewPagerOnNextInteractionListener) getParentFragment();
        } else {
            throw new RuntimeException("The parent fragment must implement OnChildFragmentInteractionListener");
        }
        if (context instanceof CompleteProfileInteractionListener) {
            mListener = (CompleteProfileInteractionListener) context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        viewPagerListener = null;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (autocompleteView != null && line != null) { // to bring back the line and input on going back to previous page
            autocompleteView.setVisibility(View.VISIBLE);
            line.setVisibility(View.VISIBLE);
            line.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.corn));
        }
    }

    private void initSelection() {
        Place place = mListener.getLocation();
        if (place != null) {
            autocompleteView.setText(place.description);
            AndroidUtils.enableView(nextButton);
        }
    }

    private void initNextButton(View view) {
        nextButton = view.findViewById(R.id.nextButton);
        AndroidUtils.disableView(nextButton);
        nextButton.setOnClickListener(btn ->
                viewPagerListener.handleNextButtonClick(4));
    }

    private void initNextArrowButton(View view) {
        nextArrowButton = view.findViewById(R.id.btn);
        nextArrowButton.setColorFilter(ContextCompat.getColor(mContext, R.color.corn), PorterDuff.Mode.SRC_IN);
        AndroidUtils.disableView(nextArrowButton);

        line = view.findViewById(R.id.underline);
        Circle circle = view.findViewById(R.id.circle);
        int circleColor = ContextCompat.getColor(mContext, R.color.corn);

        nextArrowButton.setOnClickListener(new CompleteProfileNextArrowListener(
                getActivity(), getContext(), circle, circleColor, autocompleteView, line,
                () -> viewPagerListener.handleNextButtonClick(4)));
    }

    private void initAutoComplete(View view) {
        autocompleteView = view.findViewById(R.id.autocomplete);
        autocompleteView.setAdapter(new PlacesAutoCompleteAdapter(getActivity(), R.layout.autocomplete_list_item));
        autocompleteView.setOnItemClickListener((parent, view1, position, id) -> {
            Place place = (Place) parent.getItemAtPosition(position);
            autocompleteView.setText(place.description);
            AndroidUtils.enableView(nextArrowButton);
            mListener.setLocation(place);
            new PlaceDetailsTask().execute(place.placeId);
            AndroidUtils.enableView(nextButton);
        });
    }

    class PlaceDetailsTask extends AsyncTask<String, Void, Address> {

        protected Address doInBackground(String... urls) {
            try {
                PlaceDetailsAPI api =  new PlaceDetailsAPI();
                return api.getDetails(urls[0]);
            } catch (Exception e) {
                return null;
            }
        }

        protected void onPostExecute(Address address) {
            mListener.getLocation().address = address;
        }

    }


}
