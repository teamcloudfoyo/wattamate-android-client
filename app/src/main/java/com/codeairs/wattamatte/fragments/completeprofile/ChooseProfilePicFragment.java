package com.codeairs.wattamatte.fragments.completeprofile;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.codeairs.wattamatte.AppController;
import com.codeairs.wattamatte.R;
import com.codeairs.wattamatte.activity.CompleteProfileDetailsActivity;
import com.codeairs.wattamatte.constants.ApiPaths;
import com.codeairs.wattamatte.constants.Constants;
import com.codeairs.wattamatte.constants.FragmentTag;
import com.codeairs.wattamatte.constants.RequestCode;
import com.codeairs.wattamatte.listeners.CompleteProfileInteractionListener;
import com.codeairs.wattamatte.utils.CommonUtils;
import com.codeairs.wattamatte.utils.SharedPrefManager;
import com.codeairs.wattamatte.utils.internet.PhotoMultipartRequest;
import com.codeairs.wattamatte.utils.internet.VolleySingleton;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Callback;
import com.theartofdev.edmodo.cropper.CropImage;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;

import id.zelory.compressor.Compressor;

import static android.app.Activity.RESULT_OK;

public class ChooseProfilePicFragment extends Fragment implements View.OnClickListener {

    private static final String TAG = "ProfilePicFragment";

    private ImageButton changeProfilePicButton;
    private CircularImageView circleImage;
    private Button nextButton;
    private ImageButton plusButton;

    private CompleteProfileInteractionListener mListener;

    public ChooseProfilePicFragment() {
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_choose_profile_pic, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initChangeProfilePicButton(view);
        initNextButton(view);

        initPic();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        CropImage.ActivityResult result = CropImage.getActivityResult(data);
        if (resultCode == RESULT_OK) {
            Uri resultUri = result.getUri();
            handlePicSelectSuccess(resultUri, true);
            mListener.handleProfilePicChange(resultUri);
        } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
            Exception error = result.getError();
            Log.e(TAG, error.getMessage());
        }
    }

    @Override
    public void onClick(View view) {
        CompleteProfileDetailsActivity.REQUEST_CODE = RequestCode.PersonalInfoProfilePic.id();
        CommonUtils.startImagePicker(getActivity());
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof CompleteProfileInteractionListener) {
            mListener = (CompleteProfileInteractionListener) context;
        } else {
            throw new RuntimeException("The parent fragment must implement OnChildFragmentInteractionListener");
        }
    }

    private void initPic() {
        if (mListener.getDidUploadPic()) {
            Uri uri = mListener.getProfilePic();
            if (uri != null)
                handlePicSelectSuccess(uri, false);
        } else {
            if (SharedPrefManager.getLoginType(getContext()) == Constants.LOGIN_TYPE_FACEBOOK) {
                String url = mListener.getFBProfilePic();
                AppController.getPicasso().load(url).into(circleImage, imgCallback);
            }
        }
    }

    private Callback imgCallback = new Callback() {
        @Override
        public void onSuccess() {
            new Handler().postDelayed(() -> {

                String dirName = Environment.getExternalStorageDirectory().toString() + "/Wattamate/images";
                final File storageDir = new File(dirName);
                if (!storageDir.exists()) {
                    storageDir.mkdirs();
                }
                final String img_path = dirName + "/" + "fbprofilepic.jpg";

                // Save bitmap to local
                Bitmap bitmap = ((BitmapDrawable) circleImage.getDrawable()).getBitmap();
                File file = null;
                try {
                    file = savebitmap(bitmap);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                changeProfilePicButton.setVisibility(View.GONE);
                plusButton.setVisibility(View.GONE);

//                                circleImage.setImageURI(resultUri);
                circleImage.setVisibility(View.VISIBLE);
                enableNextButton();

                if (null != file) {
                    Uri uri = Uri.fromFile(file);
                    uploadPic(uri);
                }
            }, 100);
        }

        @Override
        public void onError(Exception e) {

        }
    };

    public File savebitmap(Bitmap pictureBitmap) throws IOException {
        String path = Environment.getExternalStorageDirectory().toString();
        OutputStream fOut = null;
        Integer counter = 0;
        File file = new File(path, "wattafbpic" + counter + ".jpg"); // the File to save , append increasing numeric counter to prevent files from getting overwritten.
        fOut = new FileOutputStream(file);

        pictureBitmap.compress(Bitmap.CompressFormat.JPEG, 85, fOut); // saving the Bitmap to a file compressed as a JPEG with 85% compression rate
        fOut.flush(); // Not really required
        fOut.close(); // do not forget to close the stream

        MediaStore.Images.Media.insertImage(getActivity().getContentResolver(),
                file.getAbsolutePath(), file.getName(), file.getName());
        return file;
    }

    private void initChangeProfilePicButton(View view) {
        circleImage = view.findViewById(R.id.circleImage);
        circleImage.setVisibility(View.GONE);
        circleImage.setOnClickListener(this);

        changeProfilePicButton = view.findViewById(R.id.imgButton);
        changeProfilePicButton.setOnClickListener(this);

        plusButton = view.findViewById(R.id.plusButton);
        plusButton.setOnClickListener(this);
    }

    private void initNextButton(View view) {
        nextButton = view.findViewById(R.id.nextButton);
//        nextButton.setEnabled(false);
//        nextButton.getBackground().setAlpha(100);
        nextButton.setOnClickListener(view1 ->
                mListener.handleOnNext(FragmentTag.CHOOSE_PROFILE_PIC));
    }

    private void enableNextButton() {
        nextButton.setEnabled(true);
        nextButton.getBackground().setAlpha(255);
    }

    private void handlePicSelectSuccess(Uri resultUri, boolean upload) {
        changeProfilePicButton.setVisibility(View.GONE);
        plusButton.setVisibility(View.GONE);

        circleImage.setImageURI(resultUri);
        circleImage.setVisibility(View.VISIBLE);
        enableNextButton();
        // TODO: upload it

        if (upload)
            uploadPic(resultUri);
    }

    private void uploadPic(Uri resultUri) {
        try {
            mListener.showLoading("");
            File compressedImageFile;
            File actualFile = new File(resultUri.getPath());
            try {
                compressedImageFile = new Compressor(getContext()).compressToFile(actualFile);
            } catch (IOException e) {
                e.printStackTrace();
                compressedImageFile = actualFile;
            }
            PhotoMultipartRequest imageUploadReq = new PhotoMultipartRequest(
                    SharedPrefManager.getUserId(getContext()),
                    ApiPaths.PATH_PREFIX + "/user/avatar/add",
                    (Response.Listener<String>) response -> {
                        if (!TextUtils.isEmpty(response))
                            Log.d("profilePic.Response", response);
                        mListener.hideLoading();
                        mListener.setDidUploadPic(true);
                    }, error -> {
                handleError(error);
                Toast.makeText(getContext(),
                        "Sunucuya bağlanılamadı!", Toast.LENGTH_LONG)
                        .show();
            }, compressedImageFile);
            VolleySingleton.getInstance(getContext()).addToRequestQueue(imageUploadReq);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void handleSuccess(String response) {
        mListener.hideLoading();
        Log.d("uploadPic.Response: ", response);
    }

    private void handleError(VolleyError error) {
        mListener.hideLoading();
        String body;
        //get status code here
        String statusCode = String.valueOf(error.networkResponse.statusCode);
        //get response body and parse with appropriate encoding
        if (error.networkResponse.data != null) {
            try {
                body = new String(error.networkResponse.data, "UTF-8");
                Log.d("error", body);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
    }

    private String getStringImage(Bitmap bmp) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }
}
