package com.codeairs.wattamatte.fragments.completeprofile.completeroomdetails;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.Button;
import android.widget.EditText;
import com.codeairs.wattamatte.R;
import com.codeairs.wattamatte.listeners.CompleteProfileInteractionListener;
import com.codeairs.wattamatte.listeners.CompleteProfileViewPagerOnNextInteractionListener;
import io.apptik.widget.MultiSlider;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChooseBudgetFragment extends Fragment {

    private static final int BUDGET_MIN = 0;
    private static final int BUDGET_MAX = 2000;

    private static final int BUDGET_DEFAULT_MIN = 0;
    private static final int BUDGET_DEFAULT_MAX = 2000;

    private CompleteProfileInteractionListener mainListener;
    private CompleteProfileViewPagerOnNextInteractionListener mListener;

    private EditText budgetFrom;
    private EditText budgetTo;
    private Button nextButton;
    private MultiSlider slider;

    public ChooseBudgetFragment() {
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_choose_budget, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initEditTexts(view);
        initNextButton(view);
        initSlider(view);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (getParentFragment() instanceof CompleteProfileViewPagerOnNextInteractionListener) {
            mListener = (CompleteProfileViewPagerOnNextInteractionListener) getParentFragment();
            mListener.disableSwipeToNextPage();
        }
        if (getContext() instanceof CompleteProfileInteractionListener) {
            mainListener = (CompleteProfileInteractionListener) getContext();
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && mListener != null)
            mListener.disableSwipeToNextPage();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        mainListener = null;
    }

    private void initEditTexts(View view) {
        budgetFrom = view.findViewById(R.id.budgetFrom);
        budgetFrom.setText(String.valueOf(mainListener.getBudgetMin()));

        budgetTo = view.findViewById(R.id.budgetTo);
        budgetTo.setText(String.valueOf(mainListener.getBudgetMax()));
    }

    private void initNextButton(View view) {
        nextButton = view.findViewById(R.id.nextButton);
        nextButton.getBackground().setAlpha(80);
        nextButton.setEnabled(false);
        nextButton.setOnClickListener(btn ->
                mListener.handleNextButtonClick(6));
    }

    private void initSlider(View view) {
        if (getContext() == null)
            return;

        slider = view.findViewById(R.id.slider);
        slider.setMin(BUDGET_MIN);
        slider.setMax(BUDGET_MAX);
        slider.setStep(50);

//        slider.getThumb(0).setThumb(ContextCompat.getDrawable(getContext(),
//                R.drawable.ic_slider_thumb));
//        slider.getThumb(1).setThumb(ContextCompat.getDrawable(getContext(),
//                R.drawable.ic_slider_thumb));

        slider.getThumb(0).setValue(mainListener.getBudgetMin());
        slider.getThumb(1).setValue(mainListener.getBudgetMax());

        slider.setOnThumbValueChangeListener((multiSlider, thumb, thumbIndex, value) -> {
            budgetFrom.removeTextChangedListener(budgetMinTextWatcher);
            budgetTo.removeTextChangedListener(budgetMaxTextWatcher);

            if (!nextButton.isEnabled()) {
                enableNext();
            }

            if (thumbIndex == 0) {
                budgetFrom.setText(String.valueOf(thumb.getValue()));
                mainListener.setBudgetMin(thumb.getValue());
            } else {
                budgetTo.setText(String.valueOf(thumb.getValue()));
                mainListener.setBudgetMax(thumb.getValue());
            }
            budgetFrom.addTextChangedListener(budgetMinTextWatcher);
            budgetTo.addTextChangedListener(budgetMaxTextWatcher);
            mainListener.setBudgetChanged();
        });
        budgetFrom.addTextChangedListener(budgetMinTextWatcher);
        budgetTo.addTextChangedListener(budgetMaxTextWatcher);

        if (mainListener.hasBudgetChanged())
            enableNext();
    }

    private void enableNext() {
        nextButton.getBackground().setAlpha(255);
        nextButton.setEnabled(true);
        mListener.enableSwipeToNextPage();
    }

    private TextWatcher budgetMinTextWatcher = new TextWatcher() {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            if (TextUtils.isEmpty(s)) {
                slider.getThumb(0).setValue(BUDGET_DEFAULT_MIN);
                return;
            }
            slider.getThumb(0).setValue(Integer.valueOf(s.toString()));
        }
    };

    private TextWatcher budgetMaxTextWatcher = new TextWatcher() {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            if (TextUtils.isEmpty(s)) {
                slider.getThumb(1).setValue(BUDGET_DEFAULT_MAX);
                return;
            }
            slider.getThumb(1).setValue(Integer.valueOf(s.toString()));
        }
    };
}
