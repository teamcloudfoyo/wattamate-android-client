package com.codeairs.wattamatte.fragments.completeprofile.completeroomdetails;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import com.codeairs.wattamatte.R;
import com.codeairs.wattamatte.customcontrols.togglebutton.InterestToggleButton;
import com.codeairs.wattamatte.listeners.CompleteProfileInteractionListener;
import com.codeairs.wattamatte.listeners.CompleteProfileViewPagerOnNextInteractionListener;
import com.codeairs.wattamatte.utils.AndroidUtils;
import com.codeairs.wattamatte.utils.UIHelper;
import com.nex3z.togglebuttongroup.SingleSelectToggleGroup;

public class HaveChildrenFragment extends Fragment {

    private static final int ID_HAVE_CHILDREN = 101;
    private static final int ID_NOT_HAVE_CHILDREN = 102;

    private CompleteProfileViewPagerOnNextInteractionListener viewPagerListener;
    private CompleteProfileInteractionListener mListener;

    private SingleSelectToggleGroup toggleGroup;

    private Button nextButton;

    public HaveChildrenFragment() {
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.layout_complete_profile_toggle, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        AndroidUtils.setText(view, R.id.title, R.string.have_children);
        initNextButton(view);
        initToggleButton(view);
        initSelection();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (getParentFragment() instanceof CompleteProfileViewPagerOnNextInteractionListener) {
            viewPagerListener = (CompleteProfileViewPagerOnNextInteractionListener) getParentFragment();
        } else {
            throw new RuntimeException("The parent fragment must implement OnChildFragmentInteractionListener");
        }
        if (getContext() instanceof CompleteProfileInteractionListener) {
            mListener = (CompleteProfileInteractionListener) getContext();
        } else {
            throw new RuntimeException("Not implemented");
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser)
            new Handler().postDelayed(() -> {
                if (mListener.getHaveChildren() == 2)
                    viewPagerListener.disableSwipeToNextPage();
                else
                    viewPagerListener.enableSwipeToNextPage();
            }, 300);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        viewPagerListener = null;
        mListener = null;
    }

    private void initNextButton(View view) {
        nextButton = view.findViewById(R.id.nextButton);
        AndroidUtils.disableView(nextButton);
        nextButton.setOnClickListener(btn -> {
                    syncData();
                    viewPagerListener.handleNextButtonClick(2);
                }
        );
    }

    private void syncData() {
        mListener.handleHaveChildrenChange(toggleGroup.getCheckedId() == ID_HAVE_CHILDREN ? 1 : 0);
    }

    private void initSelection() {
        switch (mListener.getHaveChildren()) {
            case 0:
                toggleGroup.check(ID_NOT_HAVE_CHILDREN);
                break;
            case 1:
                toggleGroup.check(ID_HAVE_CHILDREN);
        }
    }

    private void initToggleButton(View view) {
        if (getContext() == null)
            return;

        toggleGroup = view.findViewById(R.id.toggleGroup);

        SingleSelectToggleGroup.LayoutParams params = new SingleSelectToggleGroup.LayoutParams(
                AndroidUtils.dpToPx(getContext(), 112),
                AndroidUtils.dpToPx(getContext(), 142)
        );

        InterestToggleButton btn1 = UIHelper.getInterestsToggleButton(getContext(),
                ID_HAVE_CHILDREN, R.color.white, R.color.mediumTurquoise, R.string.yes, R.drawable.tick_green);

        InterestToggleButton btn2 = UIHelper.getInterestsToggleButton(getContext(),
                ID_NOT_HAVE_CHILDREN, R.color.white, R.color.mediumTurquoise, R.string.no, R.drawable.cross_red);

        toggleGroup.addView(btn1, 0, params);
        toggleGroup.addView(btn2, 1, params);

        toggleGroup.setOnCheckedChangeListener((group, checkedId) -> {
            AndroidUtils.enableView(nextButton);
            viewPagerListener.enableSwipeToNextPage();
        });
    }
}

