package com.codeairs.wattamatte.fragments.completeprofile;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import com.codeairs.wattamatte.R;
import com.codeairs.wattamatte.activity.CompleteProfileDetailsActivity;
import com.codeairs.wattamatte.adapters.StyleRecyclerViewAdapter;
import com.codeairs.wattamatte.constants.FragmentTag;
import com.codeairs.wattamatte.fragments.dummy.DummyContent;
import com.codeairs.wattamatte.listeners.ClipArtStateChangeListener;
import com.codeairs.wattamatte.listeners.CompleteProfileInteractionListener;
import com.codeairs.wattamatte.utils.AndroidUtils;
import com.codeairs.wattamatte.utils.CompleteProfileHelper;
import com.codeairs.wattamatte.utils.GridSpacingItemDecoration;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChooseRoommateInterestsFragment extends Fragment implements ClipArtStateChangeListener {

    private View header;
    private RecyclerView recyclerView;
    private Button nextButton;

    private CompleteProfileDetailsActivity activity;

    private ChooseInterestFragment.OnListFragmentInteractionListener mListener;

    private CompleteProfileInteractionListener nextListener;

    public ChooseRoommateInterestsFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = (CompleteProfileDetailsActivity) getActivity();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_complete_profile_list, container, false);
        recyclerView = view.findViewById(R.id.list);
        header = inflater.inflate(R.layout.layout_complete_profile_list_header
                , recyclerView, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initHeader();
        initRecyclerView();
        initNextButton(view);
    }

    private void initNextButton(View view) {
        nextButton = view.findViewById(R.id.nextButton);
        nextButton.setOnClickListener(view1 ->
                nextListener.handleOnNext(FragmentTag.CHOOSE_ROOM_MATE_INTERESTS));
        AndroidUtils.disableView(nextButton);
    }

    private void initRecyclerView() {
        recyclerView.post(() -> {
            recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 3));
            recyclerView.addItemDecoration(new GridSpacingItemDecoration(3,
                    AndroidUtils.dpToPx(getContext(), 0), false, 0));
            recyclerView.setAdapter(new StyleRecyclerViewAdapter(activity.mateFeatures, getContext(),
                    this, recyclerView.getHeight() / 4));
        });
    }

    private void initHeader() {
        AndroidUtils.setText(header, R.id.title, R.string.roommate_interest_title);
        AndroidUtils.setText(header, R.id.titleHint, R.string.roommate_interest_hint);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof CompleteProfileInteractionListener) {
            nextListener = (CompleteProfileInteractionListener) context;
        } else {
            throw new RuntimeException("The parent fragment must implement OnChildFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void handleStateChange() {
        CompleteProfileHelper.handleClipArtStateChange(activity.mateFeatures, nextButton);
    }

    public interface OnListFragmentInteractionListener {
        void onListFragmentInteraction(DummyContent.DummyItem item);
    }


}
