package com.codeairs.wattamatte.fragments;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.codeairs.wattamatte.AppController;
import com.codeairs.wattamatte.R;
import com.codeairs.wattamatte.activity.RoomDetailsActivity;
import com.codeairs.wattamatte.constants.ApiPaths;
import com.codeairs.wattamatte.listeners.RoomsInteractionListener;
import com.codeairs.wattamatte.models.rooms.Property;
import com.codeairs.wattamatte.utils.AndroidUtils;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Callback;

import java.util.HashMap;
import java.util.Hashtable;

import static android.content.Context.LOCATION_SERVICE;
import static com.codeairs.wattamatte.utils.AndroidUtils.createDrawableFromView;

public class RoomLocationsFragment extends Fragment
        implements OnMapReadyCallback, LocationListener, GoogleMap.OnInfoWindowClickListener {

    private static final String TAG = RoomLocationsFragment.class.getSimpleName();
    private RoomsInteractionListener roomsListener;

    private GoogleMap mMap;
    private LocationManager locationManager;
    private View roomsMarker, favRoomsMarker, view;

    private HashMap<Marker, Property> mMarkersHashMap = new HashMap<>();

    private float screenWidthDp;
    private Context mContext;

    public RoomLocationsFragment() {
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (null == roomsMarker)
            roomsMarker = inflater.inflate(R.layout.rooms_map_marker, container, false);
        if (null == favRoomsMarker)
            favRoomsMarker = inflater.inflate(R.layout.fav_rooms_map_marker, container, false);
        if (null != view) {
            return view;
        }
        view = inflater.inflate(R.layout.fragment_room_locations, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.g_map);
        if (mapFragment != null && getActivity() != null) {
            screenWidthDp = AndroidUtils.pxToDp(mContext, AndroidUtils.getScreenWidth(getActivity()));
            locationManager = (LocationManager) getActivity().getSystemService(LOCATION_SERVICE);
            mapFragment.getMapAsync(this);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        if (getParentFragment() instanceof RoomsInteractionListener)
            roomsListener = (RoomsInteractionListener) getParentFragment();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnInfoWindowClickListener(this);
        setupMap();
    }

    @Override
    public void onLocationChanged(Location location) {
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 10);
        mMap.animateCamera(cameraUpdate);
        locationManager.removeUpdates(this);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        Intent intent = new Intent(mContext, RoomDetailsActivity.class);
        intent.putExtra(RoomDetailsActivity.PARAM_ID, mMarkersHashMap.get(marker).id);
        startActivity(intent);
    }

    private void setupMap() {

        if (ActivityCompat.checkSelfPermission(mContext,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(mContext,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mMap.setMyLocationEnabled(true);

        LocationManager locationManager = (LocationManager) mContext.getSystemService(LOCATION_SERVICE);
        if (locationManager == null)
            return;
        Criteria criteria = new Criteria();
        String provider = locationManager.getBestProvider(criteria, true);
        Location myLocation = locationManager.getLastKnownLocation(provider);
        mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);

        //todo this is to avoid crashing for latitude and longitude
        if (myLocation == null)
            return;

        double latitude = myLocation.getLatitude();
        double longitude = myLocation.getLongitude();
        LatLng latLng = new LatLng(latitude, longitude);

        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));

        // Zoom in the Google Map
        mMap.animateCamera(CameraUpdateFactory.zoomTo(20));
        mMap.addMarker(new MarkerOptions().position(new LatLng(latitude, longitude)).title("You are here!"));
    }

    private void addRoomMarkers() {
        mMarkersHashMap = new HashMap<>();
        int i = 0;

        for (Property room : roomsListener.getRoomsToDisplay()) {
            if (room.address != null && room.address.latitude != null && room.address.longitude != null) {
                View view = roomsListener.getFavoriteRoomsIds()
                        .contains(room.id) ? favRoomsMarker : roomsMarker;
                AndroidUtils.setText(view, R.id.text, room.price + "€");
                LatLng latLng = new LatLng(Double.valueOf(room.address.latitude),
                        Double.valueOf(room.address.longitude));

                Marker currentMarker = mMap.addMarker(new MarkerOptions()
                        .position(latLng)
                        .title(room.name)
                        .snippet(room.description)
                        .icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(mContext, view))));
                mMarkersHashMap.put(currentMarker, room);
                markerSet.put(currentMarker.getId(), false);

                if (i == 0) {
                    mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                    mMap.animateCamera(CameraUpdateFactory.zoomTo(14.0f));
                    i++;
                }
            }
        }
        mMap.setInfoWindowAdapter(new MarkerInfoWindowAdapter());
    }

    private Hashtable<String, Boolean> markerSet = new Hashtable<>();

    public class MarkerInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {

        MarkerInfoWindowAdapter() {
        }

        @Override
        public View getInfoWindow(Marker marker) {
            return null;
        }

        @Override
        public View getInfoContents(Marker marker) {
            View v = getLayoutInflater().inflate(R.layout.layout_room_marker_info, null);

            Property room = mMarkersHashMap.get(marker);

            ImageView img = v.findViewById(R.id.img);

            TextView name = v.findViewById(R.id.name);
            name.setText(room.name);
            AndroidUtils.setViewWidth(name, AndroidUtils.dpToPx(mContext, screenWidthDp - 80 - 48));

            AndroidUtils.setText(v, R.id.location, room.address.city);
            AndroidUtils.setText(v, R.id.price, room.price + "€");

            if (room.pictures != null && room.pictures.size() > 0) {
                boolean isImageLoaded = markerSet.get(marker.getId());
                String pic = ApiPaths.IMG_PATH_PREFIX + room.pictures.get(0);
                Log.d(TAG, "Image Url : " + pic);
                Log.d(TAG, "Image Loading : " + isImageLoaded);
                if (isImageLoaded) {
                    AppController.getPicasso()
                            .load(pic)
                            .placeholder(R.drawable.splash_logo)
                            .resize(200, 200)
                            .into(img);
                } else {
                    markerSet.put(marker.getId(), true);
                    AppController.getPicasso()
                            .load(pic)
                            .placeholder(R.drawable.splash_logo)
                            .into(img, new MarkerCallback(marker));
                }
            }

            return v;
        }
    }

    static class MarkerCallback implements Callback {

        final Marker marker;

        MarkerCallback(Marker marker) {
            this.marker = marker;
        }

        @Override
        public void onError(Exception e) {
            Log.e(getClass().getSimpleName(), "Error loading thumbnail!");
        }

        @Override
        public void onSuccess() {
            if (marker != null && marker.isInfoWindowShown()) {
                marker.showInfoWindow();
            }
        }
    }

    //todo Now the map is null hence we added a delay of 500ms to make up loading time for maps. Review this approach.
    void displayData() {
        if (mMap == null) {
            new Handler().postDelayed(() -> {
                if (mMap != null) {
                    mMap.clear();
                    addRoomMarkers();
                }
            }, 500);
        } else {
            mMap.clear();
            addRoomMarkers();
        }
    }

}
