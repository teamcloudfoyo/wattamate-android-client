package com.codeairs.wattamatte.fragments.safe;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.codeairs.wattamatte.R;
import com.codeairs.wattamatte.constants.FragmentTag;
import com.codeairs.wattamatte.listeners.SafeListener;
import com.codeairs.wattamatte.models.SafeVault.MySafe;
import com.codeairs.wattamatte.utils.AndroidUtils;

public class GuarantorFragment extends BaseSafeFragment {

    private View step1, step2, step3;
    private MySafe mySafe;
    private SafeListener mListener;
    private Button validate;

    public GuarantorFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_guarantor, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initSteps(view);
    }

    private void initSteps(View view) {
        validate = view.findViewById(R.id.nextButton);
        step1 = view.findViewById(R.id.step1);
        step2 = view.findViewById(R.id.step2);
        step3 = view.findViewById(R.id.step3);

        AndroidUtils.setText(step1, R.id.text, R.string.piece_identity);
        AndroidUtils.setText(step2, R.id.text, R.string.accomodation_certificate);
        AndroidUtils.setText(step3, R.id.text, R.string.proof_of_home);

        step1.setOnClickListener(v -> {
            if (mListener.getGuarantorIdentityPiece() != null)
                openFragment(new GuarantorStep1Fragment(), FragmentTag.GUARANTOR_STEP1);
            else
                mListener.updateSafeInitializer();
        });
        step2.setOnClickListener(v -> openFragment(new GuarantorStep2Fragment(), FragmentTag.GUARANTOR_STEP2));
        step3.setOnClickListener(v -> openFragment(new GuarantorStep3Fragment(), FragmentTag.GUARANTOR_STEP3));
        mySafe = mListener.getMySafe();
        updateSafeCompletionStatus(mySafe);
        validate.setOnClickListener(validateClick);
    }

    View.OnClickListener validateClick = view -> {
        if (getActivity() != null)
            getActivity().getSupportFragmentManager().popBackStackImmediate();
    };

    private void openFragment(Fragment fragment, String tag) {
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction()
                .add(R.id.frame, fragment, tag)
                .addToBackStack(tag)
                .commit();
    }

    public void updateSafeCompletionStatus(MySafe mySafe) {
        if (mySafe.getGuarantor() == null) {
            Log.d("Aswin", "Guaranator Null");
            return;
        }
        if (mySafe.getGuarantor().getIdentitiyPiece() == null) {
            AndroidUtils.setImage(step1.findViewById(R.id.img1), R.drawable.circle_gray_50px);
            step1.findViewById(R.id.img2).setVisibility(View.INVISIBLE);
        } else {
            AndroidUtils.setImage(step1.findViewById(R.id.img1), R.drawable.stepper_tick);
            step1.findViewById(R.id.img2).setVisibility(View.VISIBLE);
        }
        if (mySafe.getGuarantor().getAccomodationCertificate() == null) {
            AndroidUtils.setImage(step2.findViewById(R.id.img1), R.drawable.circle_gray_50px);
            step2.findViewById(R.id.img2).setVisibility(View.INVISIBLE);
        } else {
            AndroidUtils.setImage(step2.findViewById(R.id.img1), R.drawable.stepper_tick);
            step2.findViewById(R.id.img2).setVisibility(View.VISIBLE);
        }
        if (mySafe.getGuarantor().getResidenceProof() == null) {
            AndroidUtils.setImage(step3.findViewById(R.id.img1), R.drawable.circle_gray_50px);
            step3.findViewById(R.id.img2).setVisibility(View.INVISIBLE);
        } else {
            AndroidUtils.setImage(step3.findViewById(R.id.img1), R.drawable.stepper_tick);
            step3.findViewById(R.id.img2).setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof SafeListener)
            mListener = (SafeListener) context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
}
