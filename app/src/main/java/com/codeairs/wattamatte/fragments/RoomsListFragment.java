package com.codeairs.wattamatte.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.codeairs.wattamatte.R;
import com.codeairs.wattamatte.adapters.RoomsListRecyclerViewAdapter;
import com.codeairs.wattamatte.listeners.RoomsInteractionListener;
import com.codeairs.wattamatte.models.rooms.Property;
import com.codeairs.wattamatte.utils.AndroidUtils;
import com.codeairs.wattamatte.utils.CommonUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.List;

import static com.codeairs.wattamatte.activity.MainActivity.REQUEST_CODE_FILTER_ROOMS;
import static com.codeairs.wattamatte.activity.MainActivity.REQUEST_CODE_ROOM_FAV_CHANGE;
import static com.codeairs.wattamatte.activity.MainActivity.REQUEST_CODE_ROOM_FAV_CHANGE_MY_PROFILE;

public class RoomsListFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener,
        RoomsListRecyclerViewAdapter.RoomListInteractionListener {

    public static boolean needsRefresh;

    private OnListFragmentInteractionListener mListener;
    private RoomsInteractionListener roomsListener;

    private SwipeRefreshLayout swipeLayout;

    private RecyclerView recyclerView;
    private View emptyView;

    private RoomsListRecyclerViewAdapter adapter;

    private Context mContext;

    private View view;

    public RoomsListFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (view != null) {
            recyclerView.setAdapter(null); // new adapter will be set from RoomsFragment
            return view;
        }

        view = inflater.inflate(R.layout.fragment_room_list, container, false);

        swipeLayout = view.findViewById(R.id.swipe_container);
        swipeLayout.setOnRefreshListener(this);
        recyclerView = view.findViewById(R.id.list);
        initEmptyView(view);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        }
        if (getParentFragment() instanceof RoomsInteractionListener)
            roomsListener = (RoomsInteractionListener) getParentFragment();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onRefresh() {
        new Handler().postDelayed(() -> {
//            if (TextUtils.isEmpty(roomsListener.getSearchKey()))
//                fetch();
//            else
//                adapter.getFilter().filter(roomsListener.getSearchKey());
            swipeLayout.setRefreshing(false);
        }, 3000);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CODE_FILTER_ROOMS && data != null) {
            String result = data.getStringExtra("filterResult");
            List<Property> rooms = new Gson().fromJson(result, new TypeToken<List<Property>>() {
            }.getType());
            roomsListener.clearSearchKey();

        } else if ((requestCode == REQUEST_CODE_ROOM_FAV_CHANGE || requestCode == REQUEST_CODE_ROOM_FAV_CHANGE_MY_PROFILE) && data != null) {
            String propertyId = data.getStringExtra("propertyId");
            boolean status = data.getBooleanExtra("favStatus", false);
            CommonUtils.addOrRemoveFavorite(roomsListener.getFavoriteRoomsIds(), propertyId, status);
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void hideEmptyView() {
        emptyView.setVisibility(View.GONE);
    }

    @Override
    public void showEmptyView() {
        emptyView.setVisibility(View.VISIBLE);
    }

    public interface OnListFragmentInteractionListener {
        void onListFragmentInteraction(Property item, boolean isFavorite);
    }

    private void initEmptyView(View view) {
        emptyView = view.findViewById(R.id.emptyView);
        ImageView iv = emptyView.findViewById(R.id.img);
        iv.setImageResource(R.drawable.empty_property);
        AndroidUtils.setText(view, R.id.msg, R.string.rooms_empty);
    }

    void displayData() {
        recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        adapter = new RoomsListRecyclerViewAdapter(roomsListener.getRoomsToDisplay(),
                roomsListener.getFavoriteRoomsIds(), mListener, this, mContext);
        recyclerView.setAdapter(adapter);
    }

}
